$(document).ready(function () {
    var attribute_combinations = info.other_info.attribute_combinations;
    var attribute_prices = info.other_info.prices;
    var attribute_discount = info.other_info.discount;
    var attribute_availability = [];
    var attribute_quantity = [];
    if (info.by_quantity_or_availability == 0) {
        //Manage using availability
        attribute_availability = info.other_info.is_product_available;
    } else {
        //Manage using quantity
        attribute_quantity = info.other_info.quantity;
    }
    $(document).on('click', '.choose-attribute li', function () {
        $(this).closest('ul').find('li').removeClass('active-new-design');
        $(this).addClass('active-new-design');
        get_set_price();
    });

    function get_set_price() {
        loading(true);
        var id_unique = [];
        $('.choose-attribute').each(function () {
            if ($(this).find('li.active-new-design').length > 0) {
                id_unique.push($(this).find('li.active-new-design').attr('data-id-unique'));
            }
        });
        var price = 0;
        var discount = 0;
        var total_price = 0;
        var selected_attribute_key = '';
        $.each(attribute_combinations, function (key, value) {
            var status = true;
            $.each(id_unique, function (key_new, value_new) {
                if (value.includes(value_new) == false) {
                    status = false;
                    return false;
                }
            });
            if (status == true) {
                selected_attribute_key = key;
                return false;
            }
        });

        total_price = attribute_prices[selected_attribute_key];
        discount = parseFloat(attribute_discount[selected_attribute_key]);
        $('[data-discount]').text(discount);
        var discounted_price = total_price;
        if (discount > 0) {
            discount = calculate_discount('0', total_price, discount);
            discounted_price = total_price - parseFloat(discount);
        }
        var price_text = '';
        if (discount > 0) {
            price_text = discounted_price + 'SAR <strike>' + total_price+'SAR</strike>';
        } else {
            price_text = discounted_price + 'SAR';
        }
        //$('[data-discounted-price]').text('SAR'+discounted_price+' - SAR'+total_price);
        //$('[data-total-price]').text('/' + total_price);


        if (info.by_quantity_or_availability == 0) {
            //Manage using availability
            attribute_availability = info.other_info.is_product_available;
            if (attribute_availability[selected_attribute_key] == '1') {
                //$('[data-availability]').text($('[data-availability]').attr('available-text'));
                $('[data-availability]').text('');
                $('.add-to-cart').removeClass('disabled');
                $('[data-discounted-price]').html(price_text);
            } else {
                $('[data-availability]').text($('[data-availability]').attr('not-available-text'));
                $('[data-discounted-price]').text('');
                $('.add-to-cart').addClass('disabled');
            }
        } else {
            //Manage using quantity
            attribute_quantity = info.other_info.quantity;
            console.log(attribute_quantity[selected_attribute_key] );
            if (attribute_quantity[selected_attribute_key] > 0) {
                //$('[data-availability]').text($('[data-availability]').attr('available-text'));
                $('[data-availability]').text('');
                $('.add-to-cart').removeClass('disabled');
                $('[data-discounted-price]').closest('h4').removeClass('hidden');
                $('[data-discounted-price]').html(price_text);
                $('.sku').removeClass('hidden');
            } else {
                $('[data-availability]').text($('[data-availability]').attr('not-available-text'));
                $('[data-discounted-price]').text('');
                $('.add-to-cart').addClass('disabled');
                $('[data-discounted-price]').closest('h4').addClass('hidden');
                $('.sku').addClass('hidden');
            }
        }
       
        $.ajax({
            url: BASE_URL + 'ajax/get_attribute_image',
            method: 'POST',
            data: { 'id_unique': id_unique.join(','), 'id_unique_product': info.id_unique },
            dataType: 'json',
            success: function (response) {
                setTimeout(function () {
                    loading(false);
                }, 150);
                if (response.status) {
                    /*$(document).find('.glass-case').remove();
                    $(document).find('.new-glass-case').html(response.data);
                    $(document).find('#glasscase').glassCase({
                        'thumbsPosition': 'bottom',
                        'widthDisplay': 560
                    });*/
                    $('#glasscase').find('img[src="'+response.data+'"]').trigger('click');
                } else {
                }
            }
        });
    }

    function calculate_discount($discount_type, $total_amount, $discount) {
        if ($discount_type == '1') {//Amount
            return $discount;
        } else if ($discount_type == '0') {//Percentage            
            return ((parseFloat($discount) * parseFloat($total_amount)) / 100);
        }
    }

    $(document).on('click', '.add-to-cart', function () {
        loading(true);
        var current = $(this);
        var id_unique_attribute_values = [];
        $('.choose-attribute').each(function () {
            if ($(this).find('li.active-new-design').length > 0) {
                id_unique_attribute_values.push($(this).find('li.active-new-design').attr('data-id-unique'));
            }
        });
        id_unique_attribute_values = id_unique_attribute_values.join(',');
        var customized_image = current.attr('data-custom-image');
        var data = { 'id_unique': current.attr('data-id'), 'id_unique_attribute_values': id_unique_attribute_values, 'customized_image':customized_image };
        add_to_cart(data);
    });


});