$(document).ready(function() {
    $(document).on('click', '.remove-from-cart', function() {
        loading(true);
        var current = $(this);
        $.ajax({
            url: BASE_URL + 'cart/remove',
            method: 'POST',
            data: { 'id': current.attr('data-id') },
            dataType: 'json',
            success: function(response) {
                setTimeout(function() {
                    //loading(false);
                }, 150);
                if (response.status) {
                    toastr.remove();
                    window.location = window.location.href;
                    toastr.success(response.message);
                } else {
                    toastr.remove();
                    toastr.error(response.message);
                }
            }
        });
    });

    $(document).on('focusout', '.quantity-input', function() {
        loading(true);
        var current = $(this);
        $.ajax({
            url: BASE_URL + 'cart/update-quantity',
            method: 'POST',
            data: { 'id': current.attr('data-id'), 'quantity': current.val() },
            dataType: 'json',
            success: function(response) {
                if (response.status) {
                    toastr.remove();

                    toastr.success(response.message);
                    window.location = window.location.href;
                } else {
                    toastr.remove();
                    setTimeout(function() {
                        loading(false);
                    }, 150);
                    toastr.error(response.message);
                }
            }
        });
    });

    $('[name="is_shipping_same_as_billing"]').change(function() {
        if ($(this).is(":checked")) {
            $('[name="shipping_name"]').val($('[name="billing_name"]').val());
            $('[name="shipping_address_line_1"]').val($('[name="billing_address_line_1"]').val());
            $('[name="shipping_address_line_2"]').val($('[name="billing_address_line_2"]').val());
            $('[name="shipping_state"]').val($('[name="billing_state"]').val());
            $('[name="shipping_city"]').val($('[name="billing_city"]').val());
            $('[name="shipping_mobile"]').val($('[name="billing_mobile"]').val());
            $('[name="shipping_pincode"]').val($('[name="billing_pincode"]').val());
        }
    });

    $(document).on('click', '.add-order-btn', function() {
        loading(true);
        var current = $(this);
        $.ajax({
            url: BASE_URL + 'cart/add-order',
            method: 'POST',
            data: $('[name="add-order"]').serialize(),
            dataType: 'json',
            success: function(response) {
                setTimeout(function() {
                    loading(false);
                }, 150);
                if (response.status) {
                    window.location = response.data.redirect;
                } else {
                    toastr.remove();
                    toastr.error(response.message);
                }
            }
        });
    });

    $(document).on('click','.sidebar-link-new [data-category-id] span',function(){
        $('[name="category"]').val($(this).closest('li').attr('data-category-id')).trigger('change');
    });

    $(document).on('change', '[name="category"],.attribute-filter,.sorting_filter', function() {
        filter_product();
    });

    function filter_product() {
        loading(true);
        var attribute_id = [];
        $('.attribute-filter').each(function() {
            if ($(this).val() != '') {
                attribute_id.push($(this).val());
            }
        });

        var sort = $(".sorting_filter").val();

        var data = { 'id_category': $('[name="category"]').val(), 'attribute_id': attribute_id, 'current_url': current_url, 'sort':sort };
        $.ajax({
            url: BASE_URL + 'ajax/filter_product',
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(response) {
                if (response.status) {
                    setTimeout(function() {
                        loading(false);
                    }, 150);
                    $('.filter-result').html(response.data);
                }
            }
        });
    }


    $(document).on('click','.add-to-cart-from-wishlist',function(){
        loading(true);
        var closest = $(this).closest('tr').find('.single-checkbox');
        var data = { 'id_unique': closest.val(), 'id_unique_attribute_values': closest.attr('data-attr-values') };
        $(this).closest('.single-wish-item').find('.wishlist').trigger('click');
        add_to_cart(data);        
    });

    $(document).on('click','.add-to-cart-all-from-wishlist',function(){
        loading(true);
        $('.single-checkbox').each(function(){
            var closest = $(this);
            var data = { 'id_unique': closest.val(), 'id_unique_attribute_values': closest.attr('data-attr-values') };
            $(this).closest('.single-wish-item').find('.wishlist').trigger('click');
            add_to_cart(data);            
        });        
    });

    $(document).on('click','.add-to-cart-selected-from-wishlist',function(){        
        if($('.single-checkbox:checked').length > 0) {   
            loading(true);
            $('.single-checkbox').each(function(){
                var closest = $(this);
                var data = { 'id_unique': closest.val(), 'id_unique_attribute_values': closest.attr('data-attr-values') };
                $(this).closest('.single-wish-item').find('.wishlist').trigger('click');
                add_to_cart(data);
            });            
        } else {
              
        }   
    });

    $(document).on('click','.apply-button',function(){                
         if($('[name="product_actions"]').val() == 'add_selected') {             
            $('.add-to-cart-selected-from-wishlist').trigger('click');
         } else if($('[name="product_actions"]').val() == 'remove') {
            $('.single-checkbox:checked').closest('tr').find('.from-wishlist').trigger('click');
         }
    });


    $(document).on('click','[name="apply_coupon"]',function(){
        loading(true);
        $.ajax({
            url: BASE_URL + 'coupon/apply',
            method: 'POST',
            data: {'coupon_code':$('[name="coupon_code"]').val()},
            dataType: 'json',
            success: function(response) {
                loading(false);
                if (response.status) {
                    $('[data-discount]').text(response.data.total_discount);
                    $('[data-cart-total]').text(response.data.discounted_total);
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
            }
        });
   });
});

function add_to_cart(data) {    
    $.ajax({
        url: BASE_URL + 'cart/add',
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function (response) {
            setTimeout(function () {
                loading(false);
            }, 150);
            if (response.status) {
                toastr.remove();
                toastr.success(response.message);
                $('span.cart-items').text(response.data.total_cart_product);
            } else {
                toastr.remove();
                toastr.error(response.message);
            }
        }
    });
}