$(document).ready(function () {
    /*Get state names based on country selection*/
    $('#id_country').change(function () {
        var current = $(this);
        if (current.val() != '') {
            var post_data = {'id_country': current.val()};
            $.ajax({
                url: BASE_URL + 'ajax/get_states',
                method: 'POST',
                data: post_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        $('#id_state').html(response.data);
                        $('#id_city').html('');
                    }
                }
            });
        }
        else{
            $('#id_state').html('');
            $('#id_city').html('');
        }
    });
    /*By default country india selected if any other country is not selected*/
    if ($('#id_country').val() == '') {
        $('#id_country option[value="101"]').attr('selected', 'selected');
        $('#id_country').trigger('change');
    }
    /*Get cities based on state selection*/
    $('#id_state').change(function () {
        var current = $(this);
        if (current.val() != '') {
            var post_data = {'id_state': current.val()};
            $.ajax({
                url: BASE_URL + 'ajax/get_cities',
                method: 'POST',
                data: post_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        $('#id_city').html(response.data);
                    }
                }
            });
        }
        else{
            $('#id_city').html('');
        }
    });
});