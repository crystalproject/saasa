$(document).ready(function () {
    $(document).on('click', '.delete-btn', function () {
        var current = $(this);
        $('.delete-btn').removeClass('selected');
        current.addClass('selected');
        $('#modal-delete').modal('show');
    });

    $(document).on('click', '.delete-confirm-yes', function () {
        var current = $('.delete-btn.selected');
        if (typeof current.attr('data-href') != 'undefined') {
            cover_spin(true);
            $.ajax({
                url: current.attr('data-href'),
                method: 'POST',
                data: {},
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        cover_spin(false);
                        current.closest('tr').remove();
                        toastr.success(response.message);
                    } else {
                        window.location = window.location.href;
                    }
                }
            });
        }
    });
});