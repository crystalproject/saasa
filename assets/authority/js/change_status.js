$(document).ready(function() {
	$(document).on('click','.change-status',function(){
		var current_element = jQuery(this);
		var id = jQuery(this).data('id');
		var table = jQuery(this).data('table');
		var current_status = jQuery(this).attr('data-current-status');
		var post_data = {
			'action': 'change_status',
			'id': id,
			'table': table,
			'current_status': current_status,
		}
		$.ajax({
			type: "POST",
			url: BASE_URL + 'authority/ajax/change_status',
			data: post_data,
			async: false,
			beforeSend: function() {
	            loading(true);
	        },
			success: function (response) {
				var response = JSON.parse(response);
				if (response.success) {
					current_element.toggleClass('bg-gradient-danger bg-gradient-success');
					if(current_element.hasClass('bg-gradient-success')){
						current_element.html('<i class="fa fa-check" aria-hidden="true"></i>');
						current_element.attr('data-current-status','1');
						} else {
						current_element.html('<i class="fa fa-times" aria-hidden="true"></i>');
						current_element.attr('data-current-status','0');
					}
					} else {
					window.location = window.location.href;
				}
			},
			complete: function() {
	            setTimeout(function() {
	                loading(false);
	            }, 100);
	        },
		});
	});

	/*New added*/
	$(document).on('click','.change-status-tmp',function(){
		var current_element = jQuery(this);
		var id = jQuery(this).data('id');
		var table = jQuery(this).data('table');
		var current_status = jQuery(this).attr('data-current-status');
		var post_data = {
			'action': 'change_status',
			'id': id,
			'table': table,
			'current_status': current_status,
		}
		$.ajax({
			type: "POST",
			url: BASE_URL + 'authority/ajax/change_category_status',
			data: post_data,
			async: false,
			beforeSend: function() {
	            loading(true);
	        },
			success: function (response) {
				var response = JSON.parse(response);
				if (response.success) {
					current_element.toggleClass('bg-gradient-danger bg-gradient-success');
					if(current_element.hasClass('bg-gradient-success')){
						current_element.html('<i class="fa fa-check" aria-hidden="true"></i>');
						current_element.attr('data-current-status','1');
					} else {
						current_element.html('<i class="fa fa-times" aria-hidden="true"></i>');
						current_element.attr('data-current-status','0');
					}
				}
				else {
					// window.location = window.location.href;
				}
			},
			complete: function() {
	            setTimeout(function() {
	                loading(false);
	            }, 100);
	        },
		});
	});
	$(document).on('click','.change-status-subtmp',function(){
		var current_element = jQuery(this);
		var id = jQuery(this).data('id');
		var table = jQuery(this).data('table');
		var current_status = jQuery(this).attr('data-current-status');
		var parent_id = jQuery(this).data('parent_id');
		var post_data = {
			'action': 'change_status',
			'id': id,
			'table': table,
			'current_status': current_status,
			'parent_id': parent_id,
		}
		$.ajax({
			type: "POST",
			url: BASE_URL + 'authority/ajax/change_sub_category_status',
			data: post_data,
			async: false,
			beforeSend: function() {
	            loading(true);
	        },
			success: function (response) {
				var response = JSON.parse(response);
				if (response.error) {
					toastr.error('Please active parent category.!');
                    return false;
                }
				if (response.success) {
					current_element.toggleClass('bg-gradient-danger bg-gradient-success');
					if(current_element.hasClass('bg-gradient-success')){
						current_element.html('<i class="fa fa-check" aria-hidden="true"></i>');
						current_element.attr('data-current-status','1');
					} else {
						current_element.html('<i class="fa fa-times" aria-hidden="true"></i>');
						current_element.attr('data-current-status','0');
					}
				}
				else {
					window.location = window.location.href;
				}
			},
			complete: function() {
	            setTimeout(function() {
	                loading(false);
	            }, 100);
	        },
		});
	});
});