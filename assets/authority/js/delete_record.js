$(document).ready(function () {
    $(document).on('click', '.delete_record', function (e) {
        var id = []
        id.push($(this).attr('id'));
        var table = $(this).attr('data-table');
        var image_path = $(this).attr('data-image-path');
        var post_data = {'id': id, 'table': table, 'image_path': image_path};
        var current = $(this).closest('tr');
        $.confirm({
            title: 'Confirm',
            type: 'red',
            content: 'Are you sure you want to delete record?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        url: BASE_URL + 'authority/ajax/delete',
                        type: 'POST',
                        dataType: 'json',
                        data: post_data,
                        beforeSend: function () {
                            loading(true);
                        },
                        success: function (response) {
                            if (response.success) {
                                current.remove();
                                toastr.success(response.message);
                            } else if (response.success == false) {
                                toastr.error(response.message);
                            } else if (response.error) {
                                toastr.error(response.message);
                            }
                        },
                        complete: function () {
                            setTimeout(function () {
                                loading(false);
                            }, 1000);
                        },
                    });
                },
                cancel: function () {
                },
            }
        });
    });

    // multiple delete //
    $(document).on('click', '.delete_all', function () {
        var id = [];
        $(".chk_all:checked").each(function () {
            id.push($(this).val());
        });
        var table = $(this).attr('data-table');
        var image_path = $(this).attr('data-image-path');
        var post_data = {'id': id, 'table': table, 'image_path': image_path};
        var boxes = $('.chk_all:checkbox');
        if (boxes.length > 0) {
            if ($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
                    title: 'Opps!',
                    type: 'red',
                    content: 'Please select at least one checkbox',
                });
                return false;
            } else {
                $.confirm({
                    title: 'Confirm',
                    type: 'red',
                    content: 'Are you sure you want to delete record?',
                    buttons: {
                        confirm: function () {
                            $.ajax({
                                url: BASE_URL + 'authority/ajax/delete',
                                type: 'POST',
                                dataType: 'json',
                                data: post_data,
                                beforeSend: function () {
                                    loading(true);
                                },
                                success: function (response) {
                                    if (response.success) {
                                        $(".chk_all:checked").each(function () {
                                            id.push($(this).closest('tr').remove());
                                        });
                                        toastr.success(response.message);
                                    } else if (response.success == false) {
                                        toastr.error(response.message);
                                    } else if (response.error) {
                                        toastr.error(response.message);
                                    }
                                },
                                complete: function () {
                                    setTimeout(function () {
                                        loading(false);
                                    }, 1000);
                                },
                            });
                        },
                        cancel: function () {
                        },
                    }
                });
                return false;
            }
        }
    });

    /*New added*/
    $(document).on('click', '.delete_record_cat', function (e) {
        var id = []
        id.push($(this).attr('id'));
        var table = $(this).attr('data-table');
        var image_path = $(this).attr('data-image-path');
        var post_data = {'id': id, 'table': table, 'image_path': image_path};
        var current = $(this).closest('tr');
        $.confirm({
            title: 'Confirm',
            type: 'red',
            content: 'Are you sure you want to delete record?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        url: BASE_URL + 'authority/ajax/delete_category',
                        type: 'POST',
                        dataType: 'json',
                        data: post_data,
                        beforeSend: function () {
                            loading(true);
                        },
                        success: function (response) {
                            if (response.success) {
                                current.remove();
                                toastr.success(response.message);
                            } else if (response.error) {
                                toastr.error(response.message);
                            }
                        },
                        complete: function () {
                            setTimeout(function () {
                                loading(false);
                            }, 1000);
                        },
                    });
                },
                cancel: function () {
                },
            }
        });
    });

    // multiple delete //
    $(document).on('click', '.delete_all_cat', function () {
        var id = [];
        $(".chk_all:checked").each(function () {
            id.push($(this).val());
        });
        var table = $(this).attr('data-table');
        var image_path = $(this).attr('data-image-path');
        var post_data = {'id': id, 'table': table, 'image_path': image_path};
        var boxes = $('.chk_all:checkbox');
        if (boxes.length > 0) {
            if ($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
                    title: 'Opps!',
                    type: 'red',
                    content: 'Please select at least one checkbox',
                });
                return false;
            } else {
                $.confirm({
                    title: 'Confirm',
                    type: 'red',
                    content: 'Are you sure you want to delete record?',
                    buttons: {
                        confirm: function () {
                            $.ajax({
                                url: BASE_URL + 'authority/ajax/delete_category',
                                type: 'POST',
                                dataType: 'json',
                                data: post_data,
                                beforeSend: function () {
                                    loading(true);
                                },
                                success: function (response) {
                                    if (response.success) {
                                        $(".chk_all:checked").each(function () {
                                            id.push($(this).closest('tr').remove());
                                        });
                                        toastr.success(response.message);
                                    } else if (response.error) {
                                        toastr.error(response.message);
                                    }
                                },
                                complete: function () {
                                    setTimeout(function () {
                                        loading(false);
                                    }, 1000);
                                },
                            });
                        },
                        cancel: function () {
                        },
                    }
                });
                return false;
            }
        }
    });
});