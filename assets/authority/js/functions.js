$(document).ready(function () {
    cover_spin(false);
    $(document).on('click', '.show-spin', function () {
        cover_spin(true);
    });

    $(document).on('click', '#select_all', function () {        
        if (this.checked) {
            $('.chk_all').each(function () {
                this.checked = true;
            });
        } else {
            $('.chk_all').each(function () {
                this.checked = false;
            });
        }
    });

    $(document).on('click', '.chk_all', function () {
        if ($('.chk_all:checked').length == $('.chk_all').length) {
            $('#select_all').prop('checked', true);
        } else {
            $('#select_all').prop('checked', false);
        }
    });

    $(document).on('click', '#select_all_new', function () {        
        if (this.checked) {
            $('.chk_all_new').each(function () {
                this.checked = true;
            });
        } else {
            $('.chk_all_new').each(function () {
                this.checked = false;
            });
        }
    });

    $(document).on('click', '.chk_all_new', function () {
        if ($('.chk_all_new:checked').length == $('.chk_all_new').length) {
            $('#select_all_new').prop('checked', true);
        } else {
            $('#select_all_new').prop('checked', false);
        }
    });
});

function cover_spin(status) {
    if (status == true) {
        $('#cover-spin').show();
    } else {
        $('#cover-spin').hide();
    }
}

function read_url(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.img-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
