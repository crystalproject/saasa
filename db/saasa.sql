-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2020 at 03:48 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saasa`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `email_address_website` varchar(255) NOT NULL,
  `mobile_number_1` varchar(15) NOT NULL,
  `mobile_number_2` varchar(15) NOT NULL,
  `address` mediumtext NOT NULL,
  `gender` enum('Male','Female','Transgender') NOT NULL,
  `role` enum('Admin','Subadmin') NOT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `is_enable` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `full_name`, `username`, `email_address`, `email_address_website`, `mobile_number_1`, `mobile_number_2`, `address`, `gender`, `role`, `profile_photo`, `salt`, `password`, `is_enable`, `created_at`, `updated_at`) VALUES
(1, 'Saasa', 'Customizer', 'saasa@gmail.com', 'saasa@gmail.com', '99719 76487', '85273 77295', 'dolor sit amet, \r\nconsectetur elasdkj ,\r\nadipisicing elit.', 'Male', 'Admin', '0622c3ec23069d34febc76547ca17f9f.png', 'e94049558d21fabbd8124d874355b1ad', 'eb0bc03b321ab5343591e63489b5e475218b531a', '1', '2019-08-29 11:03:49', '2020-09-19 07:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) NOT NULL,
  `id_unique` varchar(15) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `attribute_name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1-Active, 0-Inactive',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `id_unique`, `id_language`, `attribute_name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '5edb5dd865cbc', 1, 'Weight', 1, '2020-06-06 14:41:52', '2020-06-06 14:50:25'),
(2, '5edb5dd865cbc', 2, 'वजन', 1, '2020-06-06 14:41:52', '2020-06-06 14:50:25'),
(3, '5ede1f746aadd', 1, 'Gram', 1, '2020-06-08 16:52:28', '2020-06-08 16:52:28'),
(4, '5ede1f746aadd', 2, 'ग्राम', 1, '2020-06-08 16:52:28', '2020-06-08 16:52:28');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` bigint(20) NOT NULL,
  `id_unique_attributes` varchar(25) NOT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `attribute_value` varchar(100) NOT NULL,
  `image` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0-InActive, 1-Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `id_unique_attributes`, `id_unique`, `id_language`, `attribute_value`, `image`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '5edb5dd865cbc', '5edb5f4dcbc9f', 1, '1 kg', '', 1, '2020-06-06 14:48:05', '2020-06-06 14:48:05'),
(2, '5edb5dd865cbc', '5edb5f4dcbc9f', 2, '1 किलोग्राम', '', 1, '2020-06-06 14:48:05', '2020-06-06 14:48:05'),
(3, '5ede1f746aadd', '5ede1fa39ad91', 1, '500', '', 1, '2020-06-08 16:53:15', '2020-06-08 16:53:15'),
(4, '5ede1f746aadd', '5ede1fa39ad91', 2, '500', '', 1, '2020-06-08 16:53:15', '2020-06-08 16:53:15'),
(5, '5ede1f746aadd', '5ede1fab57ebe', 1, '200', '', 1, '2020-06-08 16:53:23', '2020-06-08 16:53:23'),
(6, '5ede1f746aadd', '5ede1fab57ebe', 2, '200', '', 1, '2020-06-08 16:53:23', '2020-06-08 16:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `bag`
--

CREATE TABLE `bag` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_turkish` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `image` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `name_hindi` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `name_hindi`, `image`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Pedigree', 'पेडिग्री', NULL, '2020-06-06 19:25:53', '2020-09-19 13:13:51', 1),
(2, 'Himalaya', 'हिमालय', NULL, '2020-06-09 13:04:34', '2020-09-19 13:08:27', 1),
(3, 'Gnawlers', 'ग्नव्लेर्स', NULL, '2020-06-09 13:05:12', '2020-09-19 13:11:14', 1),
(4, 'Goodies', 'गुडिस', NULL, '2020-06-09 13:05:50', '2020-09-19 13:12:26', 1),
(5, 'First Bark', 'फर्स्ट बार्क', NULL, '2020-06-09 13:06:32', '2020-09-19 13:19:17', 1),
(6, 'Royal Canin', 'रोयाल केनिन', NULL, '2020-06-09 13:07:38', '2020-09-19 13:14:37', 1),
(7, 'Petswill', 'पेट्स्विल', NULL, '2020-06-09 13:08:04', '2020-09-19 13:15:09', 1),
(8, 'Purnima', 'पूर्णिमा', NULL, '2020-06-09 13:08:27', '2020-09-19 13:15:42', 1),
(9, 'Chappi', 'चप्पि', NULL, '2020-06-09 13:09:07', '2020-09-19 13:17:15', 1),
(10, 'Drools', 'ड्रूल्स', NULL, '2020-06-09 13:10:52', '2020-09-19 13:17:37', 1),
(11, 'Meow Fun', 'मेओव फन', NULL, '2020-06-09 13:13:44', '2020-09-19 13:18:14', 1),
(12, 'Delicio', 'डेलििओ', NULL, '2020-06-09 13:14:16', '2020-09-19 13:18:45', 1),
(13, 'First Meow', 'फर्स्ट मेओव', NULL, '2020-06-09 13:15:02', '2020-09-19 13:19:43', 1),
(14, 'Jinny', 'जिन्नि', NULL, '2020-06-09 13:15:24', '2020-09-19 13:20:05', 1),
(15, 'Whiskas', 'व्हिस्कास', NULL, '2020-06-09 13:15:56', '2020-09-19 13:37:21', 1),
(16, 'Dental light', 'डेन्टल लइट', NULL, '2020-06-09 13:16:28', '2020-09-19 16:13:58', 1),
(17, 'Pet Story', 'पेट स्टोरी', NULL, '2020-06-09 13:17:46', '2020-09-19 16:14:24', 1),
(18, 'Oroo', 'ऑरु', NULL, '2020-06-09 13:21:16', '2020-09-19 16:14:41', 1),
(19, 'ooyelik', 'ऊयेलिक', NULL, '2020-06-09 13:24:23', '2020-09-19 16:15:04', 1),
(20, 'Royal Pet', 'रोयाल पेट', NULL, '2020-06-09 13:24:53', '2020-09-19 16:15:26', 1),
(21, 'Fresh Friends', 'फ्रेश फ्ररेन्ड्स', NULL, '2020-06-09 13:25:30', '2020-09-19 16:16:00', 1),
(22, 'Choostix', 'चूस्टिक्स', NULL, '2020-06-09 13:25:57', '2020-09-19 16:16:30', 1),
(23, 'Bratt', 'ब्रेट्ट', 'e984a35fb2dab3bf093e9635c14fb574.png', '2020-06-09 13:30:41', '2020-09-19 14:14:43', 1),
(24, 'Patina', 'पेटिना', 'd05c883cd4d0ce7d3baecaa1d9200657.png', '2020-06-09 13:31:00', '2020-09-19 14:14:03', 1),
(25, 'Virbac', 'विर्बाक', 'b8d11d2053fc7b3a5cda7885a57dfcbb.png', '2020-06-09 13:31:22', '2020-09-19 16:17:38', 1),
(26, 'Pet Head', 'पेट हेड', '3e8ad2646688237c17cf3e327892335a.png', '2020-06-09 13:31:57', '2020-09-19 16:18:12', 1),
(27, 'Beowuff', 'बेवुफ', 'bc5645425a5fb35c4bd3a0ae8c2dfadb.png', '2020-06-09 13:32:24', '2020-09-19 14:43:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `id_unique_product_attributes` varchar(50) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `customized_image` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `id_user`, `id_unique_product`, `id_unique_product_attributes`, `quantity`, `customized_image`, `created_at`, `updated_at`) VALUES
(1, 879552, '5f644210c6ca0', NULL, 1, '', '2020-09-18 16:22:17', '2020-09-18 16:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `name_hindi` varchar(50) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `slug_hindi` varchar(255) NOT NULL,
  `image` varchar(50) NOT NULL,
  `image_hindi` varchar(50) NOT NULL,
  `banner` varchar(50) DEFAULT NULL,
  `banner_hindi` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `menu_id`, `name`, `name_hindi`, `slug`, `slug_hindi`, `image`, `image_hindi`, `banner`, `banner_hindi`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 0, '2', 'Food', 'खाना', 'food', 'food', '572623ffbbc99f7b229b5a4ec2c8bbfe.png', '5a3dbb35d465567e7ecd9242da424ccf.png', 'fa434f993b4b62d86c446ada17bc4184.jpg', '', '2020-06-06 19:25:01', '2020-09-19 13:48:27', 1),
(2, 1, '', 'Dry Bones', 'ड्राई बोनस', 'dry-bones', 'डरई-बनस', '', '', NULL, NULL, '2020-06-08 14:25:40', '2020-06-09 13:40:36', 1),
(3, 1, '', 'Dry Chews', 'ड्राई चूस', 'dry-chews', 'डरई-चस', '', '', NULL, NULL, '2020-06-09 13:41:12', '2020-06-09 13:41:12', 1),
(4, 1, '', 'Dog\'s Biscuits', 'डॉग बिस्किट्स', 'dogs-biscuits', 'डग-बसकटस', '', '', NULL, NULL, '2020-06-09 13:42:11', '2020-06-09 13:42:11', 1),
(5, 0, '6', 'Training  & Control', 'ट्रेनिंग & कण्ट्रोल', 'training-control', 'टरनग-कणटरल', 'a58c08ac45130342bda07333e3bdbf1c.png', 'a3f58ee94c4baa70536d2d290e1ea97b.png', 'cd784a910472cb7382b5869d5a8dc939.jpg', '1493de774c6f3ac8d4dc97a0043887ce.jpg', '2020-06-09 13:45:43', '2020-06-09 13:45:43', 1),
(6, 5, '', 'Adjustable leash', 'अडजस्टेबले लीश', 'adjustable-leash', 'अडजसटबल-लश', '', '', NULL, NULL, '2020-06-09 13:47:55', '2020-06-09 13:47:55', 1),
(7, 5, '', 'Collar', 'कालर', 'collar', 'कलर', '', '', NULL, NULL, '2020-06-09 13:48:26', '2020-06-09 13:48:26', 1),
(8, 5, '', 'Leash', 'लीश', 'leash', 'लश', '', '', NULL, NULL, '2020-06-09 13:48:46', '2020-06-09 13:48:46', 1),
(9, 5, '', 'Harness', 'हार्नेस', 'harness', 'हरनस', '', '', NULL, NULL, '2020-06-09 13:49:08', '2020-06-09 13:49:08', 1),
(10, 5, '', 'Waist leash', 'वैस्ट लीश', 'waist-leash', 'वसट-लश', '', '', NULL, NULL, '2020-06-09 13:49:43', '2020-06-09 13:49:43', 1),
(11, 0, '6', 'Beds & mets', 'बिस्तर और चटाई', 'beds-mets', 'beds-mets', '8f0ead08398c415e273755d8005f98ca.jpg', 'f4a0b174d866d08e5d22a30a7d255b0e.jpg', '9108132302c3e712df898ed55dad69dd.jpg', '', '2020-06-09 14:05:09', '2020-09-19 13:48:15', 1),
(12, 0, '6', 'Accessories', 'सामान', 'accessories', 'accessories', '19612b9a49613b3fef467214ebe001f8.jpg', '', 'bbeb37b8ecbad4340c058e556548a316.jpg', '', '2020-06-09 14:07:44', '2020-09-19 13:48:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_banner`
--

CREATE TABLE `category_banner` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_arabic` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `image` mediumtext NOT NULL,
  `banner_for` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1-Category, 1- Sub Category, 3- Product',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `category_description`
--

CREATE TABLE `category_description` (
  `id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_description`
--

INSERT INTO `category_description` (`id`, `id_category`, `id_language`, `description`) VALUES
(1, 2, 1, ''),
(2, 2, 2, ''),
(3, 3, 1, ''),
(4, 3, 2, ''),
(5, 4, 1, ''),
(6, 4, 2, ''),
(7, 6, 1, ''),
(8, 6, 2, ''),
(9, 7, 1, ''),
(10, 7, 2, ''),
(11, 8, 1, ''),
(12, 8, 2, ''),
(13, 9, 1, ''),
(14, 9, 2, ''),
(15, 10, 1, ''),
(16, 10, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) NOT NULL,
  `coupon_code` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `discount_type` varchar(10) NOT NULL,
  `discount` float NOT NULL COMMENT 'ruppes or percentage',
  `minimum_amount_cart_status` varchar(5) NOT NULL,
  `minimum_amount_cart` float NOT NULL,
  `maximum_coupon_used` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_hindi` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `description_hindi` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `title`, `title_hindi`, `description`, `description_hindi`, `created_at`, `updated_at`, `is_active`) VALUES
(6, 'How do I pick one or more items from a list of more than 10,000 at random?', ' मैं यादृच्छिक पर 10,000 से अधिक की सूची से एक या एक से अधिक आइटम कैसे ले सकता हूं?', 'In this case, you will need a subscription to the Premium Generator. Then use the following procedure:\r\n\r\nMake sure all your items are numbered sequentially, for example by pasting them into rows in a spreadsheet.\r\nGo to the Premium Generator and enter the number of items you want to pick into the first field (the number of integers to generate).\r\nEnter the number of the first and the last item in your list (e.g., the row numbers in your spreadsheet) into the next two fields (the range for the integers).\r\nSelect the ‘Unique’ option to make sure each item can only be picked once.\r\nSubmit the form and use the resulting numbers as indices into your list. If you\'re using a spreadsheet, they will be the numbers of the rows that contain the items picked.', 'इस मामले में, आपको प्रीमियम जेनरेटर की सदस्यता की आवश्यकता होगी। फिर निम्नलिखित प्रक्रिया का उपयोग करें:\r\n\r\nसुनिश्चित करें कि आपके सभी आइटम क्रमिक रूप से गिने जाते हैं, उदाहरण के लिए उन्हें एक स्प्रैडशीट में पंक्तियों में चिपकाकर।\r\nप्रीमियम जेनरेटर पर जाएं और उन आइटमों की संख्या दर्ज करें, जिन्हें आप पहले फ़ील्ड (उत्पन्न करने के लिए पूर्णांक की संख्या) में लेना चाहते हैं।\r\nअपनी सूची में पहले और अंतिम आइटम की संख्या दर्ज करें (जैसे, आपकी स्प्रेडशीट में पंक्ति संख्या) अगले दो फ़ील्ड (पूर्णांक के लिए सीमा) में।\r\nयह सुनिश्चित करने के लिए कि प्रत्येक आइटम को केवल एक बार चुना जा सकता है, \'विशिष्ट\' विकल्प चुनें।\r\nफॉर्म जमा करें और परिणामी संख्याओं को अपनी सूची में सूचकांक के रूप में उपयोग करें। यदि आप एक स्प्रेडशीट का उपयोग कर रहे हैं, तो वे उन पंक्तियों की संख्याएँ होंगी जिनमें आइटम उठाए गए हैं।', '2020-06-05 12:07:13', '2020-06-05 12:08:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gift_wraping`
--

CREATE TABLE `gift_wraping` (
  `id` int(11) NOT NULL,
  `id_unique` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `happy_clients`
--

CREATE TABLE `happy_clients` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(50) NOT NULL,
  `name_hindi` varchar(100) NOT NULL,
  `designation_hindi` varchar(100) NOT NULL,
  `description_hindi` mediumtext NOT NULL,
  `image_hindi` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_hindi` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `description_hindi` mediumtext NOT NULL,
  `image` mediumtext NOT NULL,
  `image_hindi` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `link_hindi` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`id`, `title`, `title_hindi`, `description`, `description_hindi`, `image`, `image_hindi`, `link`, `link_hindi`, `created_at`, `updated_at`, `is_active`) VALUES
(7, 'We Provide The Best Care For Your Pets', 'हम आपके पालतू जानवरों के लिए सबसे अच्छी देखभाल प्रदान करते हैं', 'The best pets clinic at melbourne, more than 20.000+ customers happy.', 'मेलबोर्न में सबसे अच्छा पालतू जानवर क्लिनिक, 20.000 से अधिक + ग्राहक खुश।', 'e52fba238474018130a72b2ea3a25685.jpg', '1b895120db77fa9cd77be77ea2f5c6f8.jpg', '', '', '2020-08-24 15:34:34', '2020-09-19 17:18:39', 1),
(8, 'We Provide The Best Care For Your Pets', 'हम आपके पालतू जानवरों के लिए सबसे अच्छी देखभाल प्रदान करते हैं', 'The best pets clinic at melbourne, more than 20.000+ customers happy.', 'मेलबोर्न में सबसे अच्छा पालतू जानवर क्लिनिक, 20.000 से अधिक + ग्राहक खुश।', 'c796e1aadaaf5e62eda2760bb09e24d6.jpg', '61c3129a3131f4e2e6876555a2457030.jpg', '', '', '2020-08-24 15:35:51', '2020-09-19 17:18:43', 1),
(9, 'We Provide The Best Care For Your Pets', 'हम आपके पालतू जानवरों के लिए सबसे अच्छी देखभाल प्रदान करते हैं', 'The best pets clinic at melbourne, more than 20.000+ customers happy.', 'मेलबोर्न में सबसे अच्छा पालतू जानवर क्लिनिक, 20.000 से अधिक + ग्राहक खुश।', 'df11b7f909cc611e7f5da23361a6e3d9.jpg', '38149343be1aa61b26d17c2432a1f0aa.jpg', '', '', '2020-08-24 15:36:17', '2020-09-19 14:48:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `default_language` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `slug`, `default_language`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'English', 'english', 1, '2020-02-04 12:51:36', '2020-08-29 16:02:22', 1),
(2, 'Hindi', 'hindi', 0, '2020-02-04 17:35:20', '2020-08-29 16:02:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `name_hindi` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `name_hindi`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Home', 'घर', '2020-06-05 12:17:02', '2020-08-29 18:24:01', 1),
(2, 'Dogs', 'कुत्ते', '2020-06-05 12:17:02', '2020-06-05 12:17:02', 1),
(3, 'Cats', 'बिल्ली ', '2020-06-05 12:17:29', '2020-06-05 12:17:29', 1),
(4, 'Birds', 'पक्षी', '2020-06-05 12:22:47', '2020-06-05 12:22:47', 1),
(5, 'Hygeine', 'स्वच्छता', '2020-06-05 12:23:11', '2020-06-05 12:23:11', 1),
(6, 'Wholesale', 'थोक', '2020-06-05 12:23:29', '2020-06-05 12:23:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_turkish` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_turkish` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification_status`
--

CREATE TABLE `notification_status` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT 0 COMMENT '1 - read, 0 - not-read',
  `remove_status` int(11) NOT NULL DEFAULT 0 COMMENT '1 - remove, 0 - not-remove'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification_token`
--

CREATE TABLE `notification_token` (
  `id` int(11) NOT NULL,
  `token` mediumtext NOT NULL,
  `device_id` mediumtext NOT NULL,
  `device_type` varchar(7) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `otp_verify`
--

CREATE TABLE `otp_verify` (
  `id` int(11) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `otp` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payment_mode`
--

CREATE TABLE `payment_mode` (
  `id` int(11) NOT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1-Active, 0-Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_mode`
--

INSERT INTO `payment_mode` (`id`, `id_unique`, `id_language`, `payment_mode`, `description`, `is_active`) VALUES
(1, '1', 1, 'Cheque Payment', 'Please send a cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.', 1),
(2, '1', 2, 'Cheque Payment', 'कृपया स्टोर नाम, स्टोर स्ट्रीट, स्टोर टाउन, स्टोर स्टेट / काउंटी, स्टोर पोस्टकोड में एक चेक भेजें।', 1),
(3, '2', 1, 'Cash On Delivery', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our privacy policy.', 1),
(4, '2', 2, 'Cash On Delivery', 'आपके व्यक्तिगत डेटा का उपयोग आपके ऑर्डर को संसाधित करने, इस वेबसाइट पर आपके अनुभव का समर्थन करने और हमारी गोपनीयता नीति में वर्णित अन्य उद्देश्यों के लिए किया जाएगा।', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `pet_id` int(11) NOT NULL,
  `pet_name` varchar(255) DEFAULT NULL,
  `pet_type` varchar(255) DEFAULT NULL,
  `pet_gender` varchar(255) DEFAULT NULL,
  `pet_dob` date DEFAULT NULL,
  `pet_breed` varchar(255) DEFAULT NULL,
  `pet_weight` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`pet_id`, `pet_name`, `pet_type`, `pet_gender`, `pet_dob`, `pet_breed`, `pet_weight`, `user_id`, `created_at`) VALUES
(1, 'abc', '0', '0', '2020-09-09', 'abc', '15 kg', 1, '2020-06-09 15:41:07'),
(2, 'Tufy', '0', '0', '2001-11-25', 'Pomerian', '25 kg', 2, '2020-08-29 15:41:07'),
(3, 'Catty', 'Cat', '1', '2020-09-08', 'Leopardy ', '10 kg', 1, '2020-08-29 15:41:07'),
(4, 'Tommy', '0', '0', '2020-09-11', 'Pomerian', '25 Kg', 0, '2020-09-19 15:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `title` varchar(500) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `long_description` text NOT NULL,
  `additional_description` text NOT NULL,
  `has_attributes` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0-No,1-Yes',
  `id_attributes` varchar(500) NOT NULL,
  `by_quantity_or_availability` int(1) NOT NULL DEFAULT 0 COMMENT '1-Quantity, 0-Availability',
  `is_popular` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0-No, 1-Yes',
  `product_video` varchar(50) DEFAULT NULL,
  `youtube_link` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1-Active,0-Inactive',
  `main_image` varchar(50) DEFAULT NULL,
  `sku` varchar(50) NOT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'For display new tag in front side',
  `is_gift_wraping` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0-Not contain gift,1-Product is gift wraping',
  `is_customizable` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0- Not customizable, 1-Customizable',
  `is_best_seller` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - Yes',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `id_unique`, `id_category`, `id_brand`, `id_language`, `title`, `slug`, `description`, `long_description`, `additional_description`, `has_attributes`, `id_attributes`, `by_quantity_or_availability`, `is_popular`, `product_video`, `youtube_link`, `is_active`, `main_image`, `sku`, `is_new`, `is_gift_wraping`, `is_customizable`, `is_best_seller`, `created_at`, `updated_at`) VALUES
(1, '5f450249d36b9', 12, 23, 1, 'Product 1', 'product-1', '<p>Product 1Product 1Product 1Product 1<br></p></br>', '', '', 0, '', 1, 0, NULL, NULL, 1, '4fcc7090b31fa6a7cedabc5df478197e.jpg', '', 0, 0, 0, 1, '2020-08-25 17:51:29', '2020-08-28 18:42:52'),
(2, '5f450249d36b9', 12, 23, 2, 'Product 1', 'product-1', '<p>Product 1Product 1Product 1<br></p></br>', '', '', 0, '', 1, 0, NULL, NULL, 1, '4fcc7090b31fa6a7cedabc5df478197e.jpg', '', 0, 0, 0, 1, '2020-08-25 17:51:29', '2020-08-28 18:42:52'),
(3, '5f644210c6ca0', 11, 12, 1, 'Pet Bag', 'pet-bag', '<p>Pet Bag<br></p></br>', '', '', 0, '', 1, 0, NULL, NULL, 1, 'dfdb37794289794c7ee28042899a9a9a.jpg', '', 0, 0, 0, 1, '2020-09-18 10:43:52', '2020-09-18 10:44:29'),
(4, '5f644210c6ca0', 11, 12, 2, 'Pet Bag', 'pet-bag', '<p>Pet Bag<br></p></br>', '', '', 0, '', 1, 0, NULL, NULL, 1, 'dfdb37794289794c7ee28042899a9a9a.jpg', '', 0, 0, 0, 1, '2020-09-18 10:43:52', '2020-09-18 10:44:29'),
(5, '5f64429653f21', 11, 27, 1, 'Product Promo', 'product-promo', '<p>Product Promo<br></p></br>', '', '', 1, '5edb5dd865cbc', 1, 0, NULL, NULL, 1, '8254fa52bfa0c96375a7e9531569bded.png', '', 0, 0, 0, 1, '2020-09-18 10:46:06', '2020-09-18 10:59:17'),
(6, '5f64429653f21', 11, 27, 2, 'Product Promo', 'product-promo', '<p>Product Promo<br></p></br>', '', '', 1, '5edb5dd865cbc', 1, 0, NULL, NULL, 1, '8254fa52bfa0c96375a7e9531569bded.png', '', 0, 0, 0, 1, '2020-09-18 10:46:06', '2020-09-18 10:59:17'),
(7, '5f6442d5f1cbd', 5, 22, 1, 'Train a pet', 'train-a-pet', '<p>Train a pet<br></p></br>', '', '', 0, '5edb5dd865cbc', 1, 0, NULL, NULL, 1, 'a2f5ec9eda0b0fc6e9e22b648d307e19.jpg', '', 0, 0, 0, 0, '2020-09-18 10:47:09', '2020-09-18 10:47:09'),
(8, '5f6442d5f1cbd', 5, 22, 2, 'Train a pet', 'train-a-pet', '<p>Train a pet<br></p></br>', '', '', 0, '5edb5dd865cbc', 1, 0, NULL, NULL, 1, 'a2f5ec9eda0b0fc6e9e22b648d307e19.jpg', '', 0, 0, 0, 0, '2020-09-18 10:47:09', '2020-09-18 10:47:09'),
(9, '5f644d8dbb318', 9, 14, 1, 'Dog harness', 'dog-harness', '<p>Dog harness&nbsp;&nbsp;&nbsp;&nbsp;<br></p></br>', '', '', 0, '5edb5dd865cbc', 1, 0, NULL, NULL, 1, '6e76c773aec18018714a800c18caafe0.jpg', '', 0, 0, 0, 1, '2020-09-18 11:32:53', '2020-09-18 11:32:53'),
(10, '5f644d8dbb318', 9, 14, 2, 'Dog harness', 'dog-harness', '<p>Dog harness<br></p></br>', '', '', 0, '5edb5dd865cbc', 1, 0, NULL, NULL, 1, '6e76c773aec18018714a800c18caafe0.jpg', '', 0, 0, 0, 1, '2020-09-18 11:32:53', '2020-09-18 11:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) DEFAULT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_unique_attributes` varchar(50) NOT NULL,
  `id_unique_attribute_values` varchar(50) NOT NULL,
  `is_ordered` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `id_unique_product`, `id_unique`, `id_unique_attributes`, `id_unique_attribute_values`, `is_ordered`) VALUES
(4, '5f64429653f21', '5f64429655e62', '5edb5dd865cbc', '5edb5f4dcbc9f', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes_image`
--

CREATE TABLE `product_attributes_image` (
  `id` int(11) NOT NULL,
  `id_unique_product_attributes` varchar(25) NOT NULL,
  `image_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `image_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `id_unique_product`, `image_name`) VALUES
(1, '5f450249d36b9', 'd5b2102cec9071764583cde20312181e.jpg'),
(2, '5f644210c6ca0', 'b837249add1afbd3ca2b6ecd1ff2ee44.png'),
(3, '5f644210c6ca0', 'ab26f092a4c80958e7fab9e1e4a4805b.jpg'),
(4, '5f644210c6ca0', 'a0ec16ba2f542cd62930115889f4bc7c.jpg'),
(5, '5f64429653f21', 'e6a1e2ac08c93d3617811137481c2c27.jpg'),
(6, '5f64429653f21', '11d76fe2d0de4837a5bd9b029d922f5d.jpg'),
(7, '5f6442d5f1cbd', '2a955a22e834df4b952004541cda6c6c.jpg'),
(8, '5f6442d5f1cbd', '2e24b4e009f8f5659914e34105214c29.jpg'),
(9, '5f6442d5f1cbd', '7485254cb15e5a90913bbf49110e691f.jpg'),
(10, '5f644d8dbb318', '2371a2bb2f5191f6fc793066a0e72e42.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `order_date_time` datetime NOT NULL,
  `address` mediumtext DEFAULT NULL,
  `payment_mode` tinyint(1) NOT NULL COMMENT '0-Cash on Delivery, 1-Online',
  `payment_info` varchar(500) DEFAULT NULL,
  `payment_status` tinyint(1) NOT NULL,
  `order_status` tinyint(1) NOT NULL,
  `id_coupon` bigint(20) DEFAULT NULL,
  `coupon_code` varchar(50) DEFAULT NULL,
  `discount_type` tinyint(1) DEFAULT NULL COMMENT '0-Percentage, 1-Amount',
  `discount_value` float DEFAULT NULL,
  `billing_info` text NOT NULL,
  `shipping_info` text NOT NULL,
  `order_notes` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `id_user`, `order_id`, `order_date_time`, `address`, `payment_mode`, `payment_info`, `payment_status`, `order_status`, `id_coupon`, `coupon_code`, `discount_type`, `discount_value`, `billing_info`, `shipping_info`, `order_notes`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '2020-06-02 18:15:05', 'new delhi', 1, NULL, 0, 0, NULL, NULL, NULL, NULL, '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_order_history`
--

CREATE TABLE `product_order_history` (
  `id` bigint(20) NOT NULL,
  `id_product_order` varchar(50) NOT NULL,
  `order_status` tinyint(2) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_order_meta`
--

CREATE TABLE `product_order_meta` (
  `id` bigint(20) NOT NULL,
  `id_product_order` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `id_unique_product_attributes` varchar(50) DEFAULT NULL,
  `price` float NOT NULL,
  `discount` float DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `customized_image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_prices`
--

CREATE TABLE `product_prices` (
  `id` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `id_unique_product_attributes` varchar(50) DEFAULT NULL,
  `id_currency` bigint(20) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `discount` float NOT NULL,
  `is_product_available` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1- Available (In Stock),0-Not Available(Out Of Stock)',
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_prices`
--

INSERT INTO `product_prices` (`id`, `id_unique_product`, `id_unique_product_attributes`, `id_currency`, `price`, `discount`, `is_product_available`, `quantity`) VALUES
(2, '5f450249d36b9', NULL, NULL, 250, 5, 1, 10),
(4, '5f644210c6ca0', NULL, NULL, 1500, 5, 1, 50),
(6, '5f6442d5f1cbd', NULL, NULL, 2500, 3, 1, 300),
(9, '5f64429653f21', '5f64429655e62', NULL, 2500, 5, 1, 100),
(10, '5f644d8dbb318', NULL, NULL, 250, 2, 1, 500);

-- --------------------------------------------------------

--
-- Table structure for table `product_review`
--

CREATE TABLE `product_review` (
  `id` int(11) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `rating` tinyint(1) NOT NULL DEFAULT 0,
  `is_approved` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1-Approved,0-Not Approved\r\n',
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` varchar(15) NOT NULL,
  `city` varchar(255) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `password` text NOT NULL,
  `pet_ids` varchar(255) NOT NULL,
  `news_leter` tinyint(4) NOT NULL COMMENT '0 - no , 1 - yes',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `full_name`, `email`, `mobile_number`, `city`, `date_of_birth`, `password`, `pet_ids`, `news_leter`, `created_at`, `modified_at`, `is_active`) VALUES
(1, 'John Doe', 'john-doe@example.com', '1234567890', 'Mumbai1', '2020-09-11', '1214d3ac65817bb993d3f2b173d9fd0805b4c80d4e2fd970e3f8b0ca4d5273f819a658d05dc7fa89785c57e55e1500d9a963ea2742c868dac46141607079f0b9lIwBczq+WYJh+doKQHLA91eqnYLhyPhr6VArW+E2gkI=', '', 0, '2020-06-09 15:36:17', '2020-09-17 15:16:15', '1'),
(2, 'Alax Stuart', 'alex@example.com', '1234567891', 'Goa', NULL, '1214d3ac65817bb993d3f2b173d9fd0805b4c80d4e2fd970e3f8b0ca4d5273f819a658d05dc7fa89785c57e55e1500d9a963ea2742c868dac46141607079f0b9lIwBczq+WYJh+doKQHLA91eqnYLhyPhr6VArW+E2gkI=', '', 0, '2020-06-09 15:36:17', '2020-08-29 16:49:27', '1'),
(3, 'xyz', 'xyz@gmail.com', '98565455', 'Rajkot', NULL, '37f3ebf8535bef8e3e06344153388b2856a0e8e48f67200dab3c7fd3eda352e6607d4ae197343df0e52e3d73e2bba087a34eeee6ba8b8f5e36857977b9833019SP84GK9N46ae0j77MnL1zZIvUXkm2iNpVrL65/wGdGY=', '', 1, '2020-09-19 15:24:22', '2020-09-19 15:24:22', '1');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `setting_title` varchar(255) NOT NULL,
  `setting_key` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `is_required` enum('1','0') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `setting_title`, `setting_key`, `setting_value`, `is_required`, `created_at`, `updated_at`) VALUES
(1, 'Site Title', 'SITE_TITLE', 'Saasa Pet Care', '1', '2018-03-10 17:31:13', '2020-08-24 12:25:22'),
(2, 'From Email', 'FROM_EMAIL', 'info@myemojiart.com', '1', '2018-03-10 17:31:13', '2020-05-20 08:42:31'),
(3, 'From Email Title', 'FROM_EMAIL_TITLE', 'Saasa Pet Care', '1', '2018-03-10 17:31:32', '2020-08-24 12:25:22'),
(4, 'Admin Email', 'ADMIN_EMAIL', 'info@myemojiart.com', '1', '2018-03-10 17:32:01', '2020-05-20 08:42:35'),
(5, 'SMTP HOST', 'SMTP_HOST', 'mail.myemojiart.com', '1', '2019-08-31 00:00:00', '2020-05-20 08:56:13'),
(6, 'SMTP PORT', 'SMTP_PORT', '587', '1', '2019-08-31 00:00:00', '2020-05-20 08:56:18'),
(7, 'SMTP USER', 'SMTP_USERNAME', 'info@myemojiart.com', '1', '2019-08-31 00:00:00', '2020-05-20 08:56:24'),
(8, 'SMTP PASSWORD', 'SMTP_PASSWORD', 'bZLxmAn3', '1', '2019-08-31 00:00:00', '2020-05-20 08:56:53'),
(9, 'Million Customer', 'MILLION_CUSTOMER', '10', '1', '2020-05-30 08:49:33', '2020-06-05 06:16:14'),
(10, 'Team Member', 'TEAM_MEMBER', '15', '1', '2020-05-30 08:49:33', '2020-05-30 03:19:33'),
(11, 'Support Available', 'SUPPORT_AVAILABLE', '20', '1', '2020-05-30 08:50:22', '2020-05-30 03:20:22'),
(12, 'CUP_OF_COFFEE', 'CUP_OF_COFFEE', '25', '1', '2020-05-30 08:50:22', '2020-05-30 03:20:22'),
(13, 'POSITIVE_REVIEWS', 'POSITIVE_REVIEWS', '30', '1', '2020-05-30 08:50:57', '2020-05-30 03:20:57');

-- --------------------------------------------------------

--
-- Table structure for table `static_page`
--

CREATE TABLE `static_page` (
  `id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(50) NOT NULL,
  `title_hindi` varchar(100) NOT NULL,
  `description_hindi` mediumtext NOT NULL,
  `image_hindi` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page`
--

INSERT INTO `static_page` (`id`, `title`, `description`, `image`, `title_hindi`, `description_hindi`, `image_hindi`, `created_at`, `updated_at`, `is_active`) VALUES
(2, 'Privacy policy', '<div class=\"contact-us-content terms-and-condition-content\">\r\n\r\n							<p>Lorem Ipsum is simply dummy text of the printing and \r\ntypesetting industry. Lorem Ipsum has been the industry\'s standard dummy\r\n text ever since the 1500s, when an unknown printer took a galley of \r\ntype and scrambled it to make a type specimen book. It has survived not \r\nonly five centuries, but also the leap into electronic typesetting, \r\nremaining essentially unchanged. It was popularised in the 1960s with \r\nthe release of Letraset sheets containing Lorem Ipsum passages, and more\r\n recently with desktop publishing software like Aldus PageMaker \r\nincluding versions of Lorem Ipsum.</p><br></div>', '0d8caaf66dd168023b207d42dbbce04b.jpg', ' गोपनीयता नीति', '<p><span style=\"font-weight: bolder;\">लोरम इप्सम केवल मुद्रण और टंकण उद्योग का डमी पाठ है। लोरम इप्सम 1500 के दशक के बाद से उद्योग का मानक डमी पाठ रहा है, जब एक अज्ञात प्रिंटर ने एक प्रकार की गली ली और एक प्रकार की पुस्तक बनाने के लिए इसे छान डाला। यह न केवल पाँच शताब्दियों तक जीवित रहा है, बल्कि इलेक्ट्रॉनिक टाइपसेटिंग में भी छलांग है, अनिवार्य रूप से अपरिवर्तित शेष है। यह 1960 में लोरम इप्सम मार्ग से युक्त लेट्रसेट शीट के विमोचन के साथ लोकप्रिय हुआ था, और हाल ही में एल्दस पेजमेकर जैसे डेस्कटॉप प्रकाशन सॉफ्टवेयर के साथ जिसमें लोरम इप्सम के संस्करण भी शामिल हैं।</span></p><div><br></div>', '', '2020-01-02 18:24:18', '2020-06-05 11:32:33', 1),
(3, 'Terms and conditions', '<div class=\"contact-us-content terms-and-condition-content\">\r\n\r\n							<p>Lorem Ipsum is simply dummy text of the printing and \r\ntypesetting industry. Lorem Ipsum has been the industry\'s standard dummy\r\n text ever since the 1500s, when an unknown printer took a galley of \r\ntype and scrambled it to make a type specimen book. It has survived not \r\nonly five centuries, but also the leap into electronic typesetting, \r\nremaining essentially unchanged. It was popularised in the 1960s with \r\nthe release of Letraset sheets containing Lorem Ipsum passages, and more\r\n recently with desktop publishing software like Aldus PageMaker \r\nincluding versions of Lorem Ipsum.</p></div>', '779d2e4374d2f1ee122ec44a12f8524e.png', ' नियम और शर्तें', '<div><p><span style=\"font-weight: bolder;\">लोरम इप्सम केवल मुद्रण और टंकण उद्योग का डमी पाठ है। लोरम इप्सम 1500 के दशक के बाद से उद्योग का मानक डमी पाठ रहा है, जब एक अज्ञात प्रिंटर ने एक प्रकार की गली ली और एक प्रकार की पुस्तक बनाने के लिए इसे छान डाला। यह न केवल पाँच शताब्दियों तक जीवित रहा है, बल्कि इलेक्ट्रॉनिक टाइपसेटिंग में भी छलांग है, अनिवार्य रूप से अपरिवर्तित शेष है। यह 1960 में लोरम इप्सम मार्ग से युक्त लेट्रसेट शीट के विमोचन के साथ लोकप्रिय हुआ था, और हाल ही में एल्दस पेजमेकर जैसे डेस्कटॉप प्रकाशन सॉफ्टवेयर के साथ जिसमें लोरम इप्सम के संस्करण भी शामिल हैं।</span></p><div><br></div></div>', '', '2020-01-02 18:25:14', '2020-06-05 11:32:11', 1),
(4, 'About us', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting \r\nindustry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book. It has survived not only five\r\n centuries, but also the leap into electronic typesetting, remaining \r\nessentially unchanged.</p>\r\n					<p>Lorem Ipsum is simply dummy text of the printing and typesetting\r\n industry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book.</p>\r\n					<p>“ Many desktop publishing packages and web page editors now use \r\nLorem Ipsum as their default model search for evolved over sometimes by \r\naccident, sometimes on purpose ”</p>', 'd1ea3c77ddc94db0096912a92cde0dbc.png', ' हमारे बारे में', '<p><span style=\"font-weight: bolder;\">लोरम इप्सम केवल मुद्रण और टंकण उद्योग का डमी पाठ है। लोरम इप्सम 1500 के दशक के बाद से उद्योग का मानक डमी पाठ रहा है, जब एक अज्ञात प्रिंटर ने एक प्रकार की गली ली और एक प्रकार की पुस्तक बनाने के लिए इसे छान डाला। यह न केवल पाँच शताब्दियों तक जीवित रहा है, बल्कि इलेक्ट्रॉनिक टाइपसेटिंग में भी छलांग है, अनिवार्य रूप से अपरिवर्तित शेष है। यह 1960 में लोरम इप्सम मार्ग से युक्त लेट्रसेट शीट के विमोचन के साथ लोकप्रिय हुआ था, और हाल ही में एल्दस पेजमेकर जैसे डेस्कटॉप प्रकाशन सॉफ्टवेयर के साथ जिसमें लोरम इप्सम के संस्करण भी शामिल हैं।</span></p><div><br></div>', 'fb903e7c6d48284973fdc7b2ed7a4f4e.jpg', '2020-01-27 16:21:26', '2020-06-05 11:32:05', 1),
(5, 'Return policy', '<p><strong style=\"margin: 0px; padding: 0px; font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem Ipsum</strong><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><br></p>', '58e9d428598d8205dc337487669ef23a.jpg', ' वापसी नीति', '<p><b>लोरम इप्सम केवल मुद्रण और टंकण उद्योग का डमी पाठ है। लोरम इप्सम 1500 के दशक के बाद से उद्योग का मानक डमी पाठ रहा है, जब एक अज्ञात प्रिंटर ने एक प्रकार की गली ली और एक प्रकार की पुस्तक बनाने के लिए इसे छान डाला। यह न केवल पाँच शताब्दियों तक जीवित रहा है, बल्कि इलेक्ट्रॉनिक टाइपसेटिंग में भी छलांग है, अनिवार्य रूप से अपरिवर्तित शेष है। यह 1960 में लोरम इप्सम मार्ग से युक्त लेट्रसेट शीट के विमोचन के साथ लोकप्रिय हुआ था, और हाल ही में एल्दस पेजमेकर जैसे डेस्कटॉप प्रकाशन सॉफ्टवेयर के साथ जिसमें लोरम इप्सम के संस्करण भी शामिल हैं।</b></p><div><br></div>', '0655e8cb9cb6c50e502fbac2d7e72369.jpg', '2020-04-28 06:00:46', '2020-06-05 11:31:53', 1),
(6, 'Pricing policy', '<p><strong style=\"margin: 0px; padding: 0px; font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem Ipsum</strong><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><br></p>', '', ' वापसी नीति', '<p><b>लोरम इप्सम केवल मुद्रण और टंकण उद्योग का डमी पाठ है। लोरम इप्सम 1500 के दशक के बाद से उद्योग का मानक डमी पाठ रहा है, जब एक अज्ञात प्रिंटर ने एक प्रकार की गली ली और एक प्रकार की पुस्तक बनाने के लिए इसे छान डाला। यह न केवल पाँच शताब्दियों तक जीवित रहा है, बल्कि इलेक्ट्रॉनिक टाइपसेटिंग में भी छलांग है, अनिवार्य रूप से अपरिवर्तित शेष है। यह 1960 में लोरम इप्सम मार्ग से युक्त लेट्रसेट शीट के विमोचन के साथ लोकप्रिय हुआ था, और हाल ही में एल्दस पेजमेकर जैसे डेस्कटॉप प्रकाशन सॉफ्टवेयर के साथ जिसमें लोरम इप्सम के संस्करण भी शामिल हैं।</b></p><div><br></div>', '0655e8cb9cb6c50e502fbac2d7e72369.jpg', '2020-04-28 06:00:46', '2020-08-28 19:36:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(15) DEFAULT NULL,
  `address` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `name`, `address`, `created_at`, `modified_at`) VALUES
(3, 1, 'adas', 'asdasdas\r\nadas\r\nadas\r\nadasda,\r\nadasd\r\n', '2020-09-17 18:00:57', '2020-09-17 18:00:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_cards`
--

CREATE TABLE `user_cards` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `card_holder_name` varchar(500) NOT NULL,
  `card_number` varchar(500) NOT NULL,
  `expiry_month` varchar(500) NOT NULL,
  `expiry_year` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `why_choose_us`
--

CREATE TABLE `why_choose_us` (
  `id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `title_arabic` varchar(100) NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `why_choose_us`
--

INSERT INTO `why_choose_us` (`id`, `title`, `description`, `title_arabic`, `description_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Free Shipping', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', 'الشحن مجانا', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '2020-04-29 09:07:15', '2020-04-29 10:22:27', 1),
(2, '100% money back guarantee.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '100% money back guarantee.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '2020-04-29 09:07:34', '2020-04-29 09:07:34', 1),
(3, 'Online Support 24/7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', 'Online Support 24/7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '2020-04-29 09:07:57', '2020-05-26 12:39:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `id_product` varchar(50) NOT NULL,
  `login_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `id_product`, `login_id`, `created_at`) VALUES
(51, '5f64429653f21', 879552, '2020-09-18 13:49:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bag`
--
ALTER TABLE `bag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_banner`
--
ALTER TABLE `category_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_description`
--
ALTER TABLE `category_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gift_wraping`
--
ALTER TABLE `gift_wraping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `happy_clients`
--
ALTER TABLE `happy_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_status`
--
ALTER TABLE `notification_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_id` (`notification_id`);

--
-- Indexes for table `notification_token`
--
ALTER TABLE `notification_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp_verify`
--
ALTER TABLE `otp_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_mode`
--
ALTER TABLE `payment_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`pet_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes_image`
--
ALTER TABLE `product_attributes_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order_history`
--
ALTER TABLE `product_order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order_meta`
--
ALTER TABLE `product_order_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_prices`
--
ALTER TABLE `product_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_review`
--
ALTER TABLE `product_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_page`
--
ALTER TABLE `static_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_cards`
--
ALTER TABLE `user_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `why_choose_us`
--
ALTER TABLE `why_choose_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bag`
--
ALTER TABLE `bag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `category_banner`
--
ALTER TABLE `category_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_description`
--
ALTER TABLE `category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `gift_wraping`
--
ALTER TABLE `gift_wraping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `happy_clients`
--
ALTER TABLE `happy_clients`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_status`
--
ALTER TABLE `notification_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_token`
--
ALTER TABLE `notification_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `otp_verify`
--
ALTER TABLE `otp_verify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_mode`
--
ALTER TABLE `payment_mode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `pet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_attributes_image`
--
ALTER TABLE `product_attributes_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_order_history`
--
ALTER TABLE `product_order_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_order_meta`
--
ALTER TABLE `product_order_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_prices`
--
ALTER TABLE `product_prices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_review`
--
ALTER TABLE `product_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `static_page`
--
ALTER TABLE `static_page`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_cards`
--
ALTER TABLE `user_cards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `why_choose_us`
--
ALTER TABLE `why_choose_us`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
