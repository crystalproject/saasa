<?php

defined('BASEPATH') or exit('No direct script access allowed');

function get_details($table_name, $where = array(),$asc_dsc="desc")
{
    $CI = &get_instance();
    $info = $CI->production_model->get_all_with_where($table_name, 'id', $asc_dsc, $where);
    if (isset($info) && $info != null) {
        return $info;
    } else {
        return array();
    }
}

function get_records($table_name, $conditions = array())
{
    $CI = &get_instance();
    $info = $CI->common_model->select_data($table_name, $conditions);
    if ($info['row_count'] > 0) {
        return $info['data'];
    } else {
        return array();
    }
}


/**
 * This email is used for sending an email with setting
 */
function send_mail($settings)
{
    $config = array(
        'protocol' => 'smtp',
        'smtp_host' => SMTP_HOST,
        'smtp_port' => SMTP_PORT,
        'smtp_user' => SMTP_USERNAME,
        'smtp_pass' => SMTP_PASSWORD,
        'mailtype'  => 'html',
        'charset' => 'utf-8',
        'wordwrap' => TRUE

    );
    $CI = &get_instance();
    $CI->load->library('email', $config);

    // prepare email
    $CI->email
        ->from(FROM_EMAIL, FROM_EMAIL_TITLE)
        ->to($settings['to'])
        ->subject($settings['subject'])
        ->message($settings['html'])
        ->set_mailtype('html');

    // send email
    return $CI->email->send();
    exit;
    //For Php mailer library    
    //require(APPPATH . '/libraries/class.phpmailer.php');    
    $mail = new PHPMailer;
    $mail->CharSet = 'utf-8';
    $mail->IsSMTP();
    $mail->Host = SMTP_HOST; // Specify main and backup server
    $mail->Port = SMTP_PORT; // Set the SMTP port
    $mail->SMTPAuth = true;  // Enable SMTP authentication
    $mail->Username = SMTP_USERNAME; // SMTP username
    $mail->Password = SMTP_PASSWORD; // SMTP password
    $mail->IsMail();
    $mail->From = FROM_EMAIL;
    $mail->FromName = FROM_EMAIL_TITLE;
    $mail->AddAddress($settings['to']);
    $mail->IsHTML(true);
    $mail->Subject = $settings['subject'];;
    $mail->Body = $settings['html'];
    if (!$mail->Send()) {
        // echo 'Message could not be sent.';
        log_message('error', 'Mailer Error: ' . $mail->ErrorInfo);
        return false;
        //   exit;
    } else {
        // echo 'Message sent.';
        return true;
    }
}
