<?php

defined('BASEPATH') or exit('No direct script access allowed');

function save_order($form_data = array())
{
    $response_array = array('status' => false, 'message' => '', 'data' => null);
    $CI = &get_instance();
    $form_data['coupon_code'] = isset($CI->session->coupon_code) ? $CI->session->coupon_code : '';
    $billing_info = array();
    $shipping_info  = array();
    $fields = array(
        'first_name', 'last_name', 'company_name', 'country', 'street', 'apartment_suite_etc', 'city', 'pincode', 'phone', 'email_address'
    );
    foreach ($fields as  $key => $value) {
        $billing_info[$value] = isset($form_data[$value]) ? $form_data[$value] : '';
    }
    $billing_info = json_encode($billing_info);
    $shipping_info = '';

    $product_order = array(
        'id_user' => $form_data['id_user'],
        'order_date_time' => CURRENT_TIME,
        'payment_mode' => isset($form_data['payment_mode']) ? $form_data['payment_mode'] : 0,
        'payment_info' => null,
        'payment_status' => 0,
        'order_status' => 0,
        'id_coupon' => null,
        'coupon_code' => null,
        'discount_type' => null,
        'discount_value' => null,
        'billing_info' => $billing_info,
        'shipping_info' => $shipping_info,
        'order_notes' => isset($form_data['order_notes']) ? $form_data['order_notes'] : '',
        'created_at' => CURRENT_TIME,
        'updated_at' => CURRENT_TIME,
    );
    //For Product Process  
    $error = false;
    $cart_products = get_cart_products($form_data);
    if ($cart_products['status'] == true) {

        if (isset($form_data['coupon_code']) && $form_data['coupon_code'] != null) {
            //For coupon code process
            $coupon_code = apply_coupon_code($form_data);
            if ($coupon_code['status'] == true) {
                $product_order['id_coupon'] = $coupon_code['data']['id_coupon'];
                $product_order['coupon_code'] = $coupon_code['data']['coupon_code'];
                $product_order['discount_type'] = $coupon_code['data']['discount_type'] == 'amount' ? 1 : 0;
                $product_order['discount_value'] = $coupon_code['data']['coupon_discount'];
            }
            unset($coupon_code);
        }
        $product_order['order_id'] = uniqid();
        $id_product_order = $CI->common_model->insert_data('product_order', $product_order);

        if ($id_product_order > 0) {
            //Add to history table
            $records = array(
                'id_product_order' => $id_product_order,
                'order_status' => 0,
                'created_at' => CURRENT_TIME,
            );
            $CI->common_model->insert_data('product_order_history', $records);

            $records = array();
            foreach ($cart_products['data'] as $value) {
                $records[] = array(
                    'id_product_order' => $id_product_order,
                    'id_unique_product' => $value['id_unique'],
                    'id_unique_product_attributes' => $value['id_unique_product_attributes'],
                    'quantity' => $value['quantity'],
                    'price' => $value['other_info']['prices'][0],
                    'discount' => $value['other_info']['discount'][0],
                    'customized_image' => isset($value['customized_image_plain']) ? $value['customized_image_plain'] : '',
                );

                if (isset($value['by_quantity_or_availability']) && $value['by_quantity_or_availability'] == '1') {
                    $temp = $value['id_unique_product_attributes'] != null ? ' AND id_unique_product_attributes = "' . $value['id_unique_product_attributes'] . '" ' : '';
                    $sql = 'UPDATE product_prices SET quantity = quantity - "' . $value['quantity'] . '" WHERE
                             id_unique_product = "' . $value['id_unique'] . '" ' . $temp . '  ';
                    $CI->common_model->simple_query($sql);
                }
            }
            if (!empty($records)) {
                $temp = array_column($records, 'id_unique_product_attributes');
                $temp = array_unique($temp);
                if (!empty($temp)) {
                    //Update is_ordered status to 1 so if admin update that product then that combination will not be delete
                    $CI->common_model->update_data('product_attributes', array('is_ordered' => 1), array('IN' => array('id_unique' => $temp)));
                }
                $CI->common_model->insert_data('product_order_meta', $records, true);
                unset($records);
            }

            empty_cart(array('id_user' => $form_data['id_user']));
            $CI->session->coupon_code = '';
            $response_array['status'] = true;
            $response_array['message'] = $CI->lang->line('order_success');
            $response_array['data'] = array('order_id' => $product_order['order_id']);
            unset($form_data);
        } else {
            $response_array['message'] = $CI->lang->line('order_failure');
        }
    } else {
        $response_array['message'] = $CI->lang->line('something_wrong');
    }
    return $response_array;
}

function get_order_detail($id, $slug, $language_default)
{
    $CI = &get_instance();
    $sql = 'SELECT billing_info,shipping_info,id,order_id,order_date_time,payment_mode,order_status,id_coupon,coupon_code,discount_type,discount_value,address FROM product_order po WHERE po.id = "' . $id . '"';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        $data['order_info'] = $info['data'][0];
        if ($data['order_info']['order_status'] == 0) {
            $data['order_info']['is_cancel_enable'] = 1;
        } else {
            $data['order_info']['is_cancel_enable'] = 0;
        }
        $payment_mode = 'SELECT payment_mode FROM payment_mode WHERE id_language = "' . $language_default['id'] . '" AND id_unique = "' . $data['order_info']['payment_mode'] . '" ';
        $payment_mode = $CI->common_model->get_data_with_sql($payment_mode);
        if ($payment_mode['row_count'] > 0) {
            $data['order_info']['payment_mode_info'] = $payment_mode['data'][0]['payment_mode'];
        } else {
            $data['order_info']['payment_mode_info'] = "";
        }

        /* $data['address_info'] = !empty($data['order_info']['address']) ? json_decode($data['order_info']['address'],true) : null;
        if(empty($data['address_info'])) {
            $data['address_info'] = null;
        }
        if(isset($data['address_info']['country_id'])) {
            unset($data['address_info']['country_id']);
            unset($data['address_info']['state_id']);
            unset($data['address_info']['city_id']);
        } */

        $data['address_info'] = !empty($data['order_info']['address']) ? $data['order_info']['address'] : '';

        if (isset($data['order_info']['address'])) {
            unset($data['order_info']['address']);
        }
        $data['order_price'] = array(
            'product_total' => 0,
            'discount' => 0,
            'product_discounted_total' => 0,
            'bag_total' => 0,
            'final_total' => 0,
        );
        $id_product_order = $data['order_info']['id'];
        $data['product_info'] = null;
        $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = p.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = p.id_category) as category_name';
        //Quantity,Price and discount comes from product order meta table
        $sql = 'SELECT title,pom.price,pom.customized_image,pom.discount,pom.quantity,pom.id_unique_product_attributes' . $sub_sub_query . ' FROM product p,product_order_meta pom WHERE pom.id_unique_product = p.id_unique AND 
                    pom.id_product_order = "' . $id_product_order . '" AND p.id_language = "' . $language_default['id'] . '"   ';
        $info = $CI->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            foreach ($info['data'] as &$value) {
                if ($value['image_name'] != null) {
                    $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                }
                $value['attributes'] = null;
                $total_amount = $value['price'] * $value['quantity'];
                $discount_type = 0;
                $discounted_price = ($total_amount - calculate_discount($discount_type, $total_amount, $value['discount']));
                $data['order_price']['product_total'] += $discounted_price;
                $value['discounted_price'] = $discounted_price;
                $value['total_product_price'] = $discounted_price * $value['quantity'];
                $data['order_price']['product_discounted_total'] = $data['order_price']['product_total'];
                if ($value['id_unique_product_attributes'] != null) {
                    $sub_query = ',IF(image="","",CONCAT("' . base_url('uploads/attribute/') . '",image)) AS image';
                    $sql = 'SELECT attribute_name,attribute_value' . $sub_query . ' FROM 
                                attributes a,attribute_values av,product_attributes pa WHERE 
                                pa.id_unique_attributes = a.id_unique AND pa.id_unique_attribute_values = av.id_unique AND 
                                a.id_language = "' . $language_default['id'] . '" AND av.id_language = "' . $language_default['id'] . '" AND pa.id_unique = "' . $value['id_unique_product_attributes'] . '" ';
                    $attributes = $CI->common_model->get_data_with_sql($sql);
                    if ($attributes['row_count'] > 0) {
                        //For value user attribute_value
                        $value['attributes'] = $attributes['data'];
                    }
                }
            }
            unset($value);
            $data['product_info'] = $info['data'];
        }


        $data['history_info'] = null;
        $sql = 'SELECT poh.order_status,poh.created_at FROM product_order_history poh WHERE poh.id_product_order = "' . $id_product_order . '" ORDER BY poh.id ASC ';
        $info = $CI->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $count = 0;
            $available = array();
            $check_active = true;
            if ($data['order_info']['order_status'] == '3') {
                $check_active = false;
            }

            foreach ($info['data'] as $key => $value) {
                $available[] = $value['order_status'];
                $temp = get_order_status($value['order_status']);
                $info['data'][$count] = $value;
                $info['data'][$count]['order_status'] = (int) $info['data'][$count]['order_status'];
                $info['data'][$count]['is_active'] = $check_active == true ? '1' : '0';
                if ($value['order_status'] == 3) {
                    $info['data'][$count]['is_active'] = 1;
                }
                $info['data'][$count]['status_name'] = $temp['name'];
                $info['data'][$count]['description'] = $temp['description'];
                $info['data'][$count]['status_img'] = base_url('assets/images/' . $value['order_status'] . '.png');
                $count++;
            }
            foreach (get_order_status() as $key => $value) {
                if (!in_array($key, $available)) {
                    $info['data'][$count] = array('order_status' => $key, 'created_at' => '');;
                    $info['data'][$count]['is_active'] = '0';
                    $info['data'][$count]['status_name'] = $value['name'];
                    $info['data'][$count]['description'] = $value['description'];
                    $info['data'][$count]['status_img'] = base_url('assets/images/' . $key . '.png');
                    $count++;
                }
            }
            $data['history_info'] = $info['data'];
            usort($data['history_info'], function ($item1, $item2) {
                //return $item1['order_status'] <=> $item2['order_status'];
                if ($item1['order_status'] == $item2['order_status']) {
                    return 0;
                }
                return ($item1['order_status'] < $item2['order_status']) ? -1 : 1;
            });
        }

        if ($data['order_info']['discount_value'] > 0) {
            $data['order_price']['discount'] = 0;
            $discount_type = $data['order_info']['discount_type'];
            $discount = $data['order_info']['discount_value'];
            $total_amount = $data['order_price']['product_total'];
            $data['order_price']['discount'] = (float) calculate_discount($discount_type, $total_amount, $discount);
            $data['order_price']['product_discounted_total'] = $data['order_price']['product_total'] - $data['order_price']['discount'];
        }
        $data['order_price']['final_total'] = $data['order_price']['product_discounted_total'] + $data['order_price']['bag_total'];
    }
    return $data;
}


function get_address($id_address)
{
    $CI = &get_instance();

    $get_address = $CI->common_model->select_data('user_address', array('where' => array('id' => $id_address)));
    $response = array();
    if ($get_address['row_count'] > 0) {
        $get_address = $get_address['data'][0];
        $value = $get_address;

        $temp = $CI->common_model->select_data('country', array('where' => array('id' => $get_address['id_country'])));
        if ($temp['row_count'] > 0) {
            $country_name = $temp['data'][0]['country_name'];
        } else {
            $country_name = '';
        }

        $temp = $CI->common_model->select_data('state', array('where' => array('id' => $get_address['id_state'])));
        if ($temp['row_count'] > 0) {
            $state_name = $temp['data'][0]['state_name'];
        } else {
            $state_name = '';
        }

        $temp = $CI->common_model->select_data('city', array('where' => array('id' => $get_address['id_city'])));
        if ($temp['row_count'] > 0) {
            $city_name = $temp['data'][0]['city_name'];
        } else {
            $city_name = '';
        }

        $response['name'] = $value['name'];
        $response['address'] = $value['address'];
        $response['country_id'] = $value['id_country'];
        $response['country_info'] = isset($country_name) && $country_name != null ? $country_name : '';
        $response['state_id'] = $value['id_state'];
        $response['state_info'] = isset($state_name) && $state_name != null ? $state_name : '';
        $response['city_id'] = $value['id_city'];
        $response['city_info'] = isset($city_name) && $city_name != null ? $city_name : '';
    }
    return json_encode($response);
}

function get_total_order($id_user)
{
    $CI = &get_instance();
    $sql = 'SELECT COUNT(id) AS total_records FROM product_order WHERE id_user = "' . $id_user . '"';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        return $info['data'][0]['total_records'] ? $info['data'][0]['total_records'] : 0;
    }
    return 0;
}
