<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * This function is used for getting cart total of particular user
 * @param type $id_user
 * @return type
 */
function get_cart_total($id_user)
{
    $CI = &get_instance();
    $sql = 'SELECT SUM((((pp.price - (round(( (pp.price * pp.discount) / 100 ),2)))) * c.quantity)) as total FROM 
            product_prices pp,cart c WHERE 
            c.id_user = "' . $id_user . '" AND c.id_unique_product = pp.id_unique_product AND IF(c.id_unique_product_attributes IS NOT NULL, c.id_unique_product_attributes =pp.id_unique_product_attributes, 1=1) = 1 ';
    $info = $CI->common_model->get_data_with_sql($sql);
    return $info['row_count'] > 0 ? format_number($info['data'][0]['total']) : format_number(0);
}

/**
 * This function is used for deleting product from cart
 * @param type $settings
 */
function delete_cart_product($settings = array())
{
    $return = true;
    $CI = &get_instance();
    if (isset($settings['id']) && $settings['id'] > 0) {
        $CI->common_model->delete_data('cart', array('where' => array('id' => $settings['id'])));
    }
    return $return;
}

function get_cart_products($form_data)
{
    $slug = $form_data['language_slug'];
    $CI = &get_instance();
    $response_array = array('status' => false, 'message' => '', 'data' => null);
    if ($form_data['id_user'] == '') {
        $form_data['id_user'] = 0;
    }
    $sql = "SELECT id_unique_product,id_unique_product_attributes,quantity,id,customized_image AS customized_image_plain,IF(customized_image = '','',CONCAT('" . base_url('uploads/product/') . "',customized_image)) AS customized_image FROM cart WHERE id_user = " . $form_data['id_user'] . " ";
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        $response_array['status'] = true;
        $response_array['data'] = $info['data'];
        $products = array();
        $temp = array();
        foreach ($response_array['data'] as $cart_value) {
            if (check_product_available_quantity($cart_value['id_unique_product'], $cart_value['id_unique_product_attributes'], $cart_value['quantity'])) {
                //If product available then we will display that product
                $temp[] = $cart_value;
            } else {
                delete_cart_product(array('id' => $cart_value['id']));
            }
        }
        $response_array['data'] = $temp;
        foreach ($response_array['data'] as $cart_value) {
            $sub_query = ' AND id_unique = "' . $cart_value['id_unique_product'] . '" ';
            $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
            $sql = 'SELECT id_unique,slug,main_image,title,has_attributes,by_quantity_or_availability' . $sub_sub_query . ' FROM product WHERE id_language = "' . $form_data['id_language'] . '" ' . $sub_query . ' ORDER BY id_unique ASC ';
            $info = $CI->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                $response_array['status'] = true;
                foreach ($info['data'] as &$value) {
                    $value['id_unique_product_attributes'] = $cart_value['id_unique_product_attributes'];
                    $value['customized_image'] = $cart_value['customized_image'];
                    $value['customized_image_plain'] = $cart_value['customized_image_plain'];
                    $value['quantity'] = $cart_value['quantity'];
                    $value['id'] = $cart_value['id'];
                    if ($value['main_image'] != null) {
                        $value['main_image'] = base_url('uploads/product/') . $value['main_image'];
                    }
                    if ($value['image_name'] != null) {
                        $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                    }
                    $value['other_info'] = get_product_detail($value, $form_data['id_language'], 'detail', $cart_value);
                    //$response_array['other_info']['total_product']++;
                    //$response_array['other_info']['total_quantity']+= $value['quantity'];
                    //unset($value['has_attributes']);
                    unset($value['category_name']);
                    //$value['total_price'] = $value['quantity'] * $value['other_info']['prices'][0];
                }
                $products[] = $info['data'][0];
            }
        }
        $response_array['data'] = $products;
    } else {
        $response_array['message'] = get_line('product_not_available_in_cart');
    }
    return $response_array;
}

/**
 * For clearing cart products
 * @param type $form_data
 * @return boolean
 */
function empty_cart($form_data = array())
{
    $CI = &get_instance();
    $conditions = array('where' => array('id_user' => $form_data['id_user']));
    $CI->common_model->delete_data('cart', $conditions);
    return true;
}

/**
 * This function is used to add product to the cart
 */
function add_to_cart($form_data)
{
    $CI = &get_instance();
    $response_array = array('status' => false, 'message' => '', 'data' => null);
    $id_unique = $form_data['id_unique']; //Product Unique ID

    $conditions = array('SELECT' => 'id_category,has_attributes,id_attributes,by_quantity_or_availability', 'WHERE' => array('id_unique' => $id_unique, 'id_language' => $form_data['id_language'], 'is_active' => '1'));
    $product_info = $CI->common_model->select_data('product', $conditions);
    if ($product_info['row_count'] > 0) {
        $product_info = $product_info['data'][0];
        if ($product_info['has_attributes'] == '1') {
            //Product with attribute
            if (!empty($form_data['id_unique_attribute_values'])) {
                $id_unique_attribute_values = explode(',', $form_data['id_unique_attribute_values']);
                $product_attributes = explode(',', $product_info['id_attributes']);

                if (count($id_unique_attribute_values) == count($product_attributes)) {
                    //Check for if combination available or not
                    //echo $sql = 'SELECT DISTINCT(id_unique) FROM product_attributes WHERE id_unique_product = "' . $id_unique . '" AND id_unique_attribute_values IN ("' . implode('","', $id_unique_attribute_values) . '") ';
                    $sql = 'SELECT  COUNT(id_unique) AS combination,id_unique FROM product_attributes WHERE id_unique_product = "' . $id_unique . '" AND id_unique_attribute_values IN ("' . implode('","', $id_unique_attribute_values) . '") GROUP BY id_unique ORDER BY combination DESC LIMIT 1';
                    $info = $CI->common_model->get_data_with_sql($sql);
                    if ($info['row_count'] > 0) {
                        $info = $info['data'][0];
                        if (count($id_unique_attribute_values) == $info['combination']) {
                            $id_unique_product_attributes = $info['id_unique'];

                            $records = array(
                                'id_user' => $form_data['id_user'],
                                'id_unique_product' => $id_unique,
                                'id_unique_product_attributes' => $id_unique_product_attributes,
                            );
                            $duplicate = $CI->common_model->select_data('cart', array('SELECT' => 'id,quantity', 'WHERE' => $records));
                            if ($duplicate['row_count'] > 0) {
                                //Update quantity if user calls this API again
                                $quantity = $duplicate['data'][0]['quantity'] + 1;
                                if (check_product_available_quantity($id_unique, $id_unique_product_attributes, $quantity)) {
                                    $customized_image = isset($form_data['customized_image']) ? $form_data['customized_image'] : '';
                                    if ($customized_image != '') {
                                        $sql = 'UPDATE cart SET quantity = quantity + 1,customized_image = "' . $customized_image . '" WHERE id = "' . $duplicate['data'][0]['id'] . '" ';
                                    } else {
                                        $sql = 'UPDATE cart SET quantity = quantity + 1 WHERE id = "' . $duplicate['data'][0]['id'] . '" ';
                                    }
                                    $CI->common_model->simple_query($sql);
                                    $response_array['status'] = true;
                                    $response_array['message'] = $CI->lang->line('product_added_cart');
                                } else {
                                    $response_array['message'] = $CI->lang->line('product_not_available');
                                }
                            } else {
                                if (check_product_available_quantity($id_unique, $id_unique_product_attributes, 1)) {
                                    $customized_image = isset($form_data['customized_image']) ? $form_data['customized_image'] : '';
                                    $records = array_merge($records, array('quantity' => 1, 'customized_image' => $customized_image, 'created_at' => CURRENT_TIME, 'updated_at' => CURRENT_TIME));
                                    $CI->common_model->insert_data('cart', $records);
                                    $response_array['status'] = true;
                                    $response_array['message'] = $CI->lang->line('product_added_cart');
                                } else {
                                    $response_array['message'] = $CI->lang->line('product_not_available');
                                }
                            }
                        } else {
                            $response_array['message'] = get_line('combination_not_available');
                        }
                    } else {
                        $response_array['message'] = get_line('combination_not_available');
                    }
                } else {
                    $response_array['message'] = get_line('attribute_not_selected_properly');
                }
            } else {
                $response_array['message'] = get_line('attribute_not_selected');
            }
        } else {
            //Product without attribute
            $records = array(
                'id_user' => $form_data['id_user'],
                'id_unique_product' => $id_unique,
                'id_unique_product_attributes' => null,
            );
            $duplicate = $CI->common_model->select_data('cart', array('SELECT' => 'id,quantity', 'WHERE' => $records));
            if ($duplicate['row_count'] > 0) {
                //Update quantity if user calls this API again
                $quantity = $duplicate['data'][0]['quantity'] + 1;
                if (check_product_available_quantity($id_unique, null, $quantity)) {
                    $customized_image = isset($form_data['customized_image']) ? $form_data['customized_image'] : '';
                    if ($customized_image != '') {
                        $sql = 'UPDATE cart SET quantity = quantity + 1, customized_image = "' . $customized_image . '" WHERE id = "' . $duplicate['data'][0]['id'] . '" ';
                    } else {
                        $sql = 'UPDATE cart SET quantity = quantity + 1 WHERE id = "' . $duplicate['data'][0]['id'] . '" ';
                    }
                    $CI->common_model->simple_query($sql);
                    $response_array['status'] = true;
                    $response_array['message'] = $CI->lang->line('product_added_cart');
                } else {
                    $response_array['message'] = $CI->lang->line('product_not_available');
                }
            } else {
                if (check_product_available_quantity($id_unique, null, 1)) {
                    $customized_image = isset($form_data['customized_image']) ? $form_data['customized_image'] : '';
                    $records = array_merge($records, array('quantity' => 1, 'customized_image' => $customized_image, 'created_at' => CURRENT_TIME, 'updated_at' => CURRENT_TIME));
                    $CI->common_model->insert_data('cart', $records);
                    $response_array['status'] = true;
                    $response_array['message'] = $CI->lang->line('product_added_cart');
                } else {
                    $response_array['message'] = $CI->lang->line('product_not_available');
                }
            }
        }
    } else {
        $response_array['message'] = $CI->lang->line('product_not_available');
    }
    return $response_array;
}
