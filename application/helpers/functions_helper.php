<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists("encrypt_cookie")) {

    function encrypt_cookie($value)
    {
        if (!$value) {
            return false;
        }
        $key = 'htd7ZxUONSQ4NMAd';
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($value, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }
}
if (!function_exists("decrypt_cookie")) {

    /**
     * 
     * @param type $value
     * @return boolean
     */
    function decrypt_cookie($value)
    {
        if (!$value) {
            return false;
        }
        $key = 'htd7ZxUONSQ4NMAd';
        $encryption_key = base64_decode($key);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($value), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }
}
if (!function_exists('is_json_string')) {

    function is_json_string($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
if (!function_exists('check_api_key')) {

    function check_api_key($headers)
    {
        if (isset($headers['apikey']) && $headers['apikey'] == API_KEY) {
            return true;
        } else {
            return false;
        }
    }
}

/* Editor content get */

function closetags($html)
{
    preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i = 0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</' . $openedtags[$i] . '>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}

/* Used for api */

function load_language($language_type)
{
    $CI = &get_instance();
    if ($language_type == 1) {
        $CI->lang->load('english_lang', 'english');
    } else if ($language_type == 2) {
        $CI->lang->load('hindi_lang', 'hindi');
    }
}

function get_language($type = 'front')
{
    $CI = &get_instance();
    if ($type == 'front') {
        $get_language = get_details('language', array('id' => get_language_id(), 'is_active' => 1));
        if (isset($get_language) && $get_language != null && $get_language[0]['id'] != 1) {
            return strtolower('_' . $get_language[0]['slug']);
        } else {
            return '';
        }
    } else {
        $get_language = get_details('language', array('default_language' => 1, 'is_active' => 1));
        if (isset($get_language) && $get_language != null && $get_language[0]['id'] != 1) {
            return strtolower('_' . $get_language[0]['slug']);
        } else {
            return '';
        }
    }
}

function get_language_id()
{
    $CI = &get_instance();
    return $CI->session->userdata('language') != null ? $CI->session->userdata('language') : DEFAULT_LANGUAGE;
}

function get_language_front()
{
    $CI = &get_instance();
    $get_language = get_details('language', array('id' => get_language_id(), 'is_active' => 1));
    if (isset($get_language) && $get_language != null && $get_language[0]['id'] != 1) {
        return strtolower('_' . $get_language[0]['slug']);
    } else {
        return '';
    }
}

function get_language_info($id_language)
{
    $CI = &get_instance();
    $get_language = get_details('language', array('is_active' => 1, 'id' => $id_language));
    if (!empty($get_language)) {
        return $get_language[0];
    } else {
        return array();
    }
}

function write_log($file_name, $method_name, $form_data)
{
    $select = "\n" . $file_name . " -> " . $method_name . "\n";
    $file = fopen("request-data.txt", "a");
    fwrite($file, $select);
    fwrite($file, 'Form Data:  ' . $form_data . "\n\n");
    fclose($file);
}

function check_headers_and_data()
{
    $CI = &get_instance();
    $response_array = array('error' => false, 'message' => '', 'form_data' => null);
    $headers = $CI->input->request_headers();
    if (check_api_key($headers)) {
        $form_data = $CI->input->post('data');
        if (!empty($form_data) && is_json_string($form_data)) {
            $response_array['form_data'] = (array) json_decode($form_data);
            if (isset($response_array['form_data']['id_language'])) {
                load_language($response_array['form_data']['id_language']);
            }
        } else {
            $response_array['message'] = 'json data is not correct';
            $response_array['error'] = true;
        }
    } else {
        $response_array['message'] = 'api key is not correct';
        $response_array['error'] = true;
    }
    return $response_array;
}

function format_number($number, $type = 'float')
{
    if ($number == null) {
        return 0;
    }
    if ($type == 'float') {
        return round($number, 2, PHP_ROUND_HALF_UP);
    } else if ($type == 'int') {
        return (int) $number;
    }
}

/**
 * For deleting bags
 * @param type $ids
 * @return type
 */
function delete_bag($ids)
{
    $CI = &get_instance();
    $response_array = array('success' => false, 'message' => '');
    $conditions = array('SELECT' => 'id', 'IN' => array('id_bag' => $ids));
    $restriction = $CI->common_model->select_data('product_order_bags', $conditions);
    if ($restriction['row_count'] > 0) {
        $response_array['message'] = $CI->lang->line('associated_with_other_records_delete');
    } else {
        foreach ($ids as $key => $value) {
            $get_image = $CI->production_model->get_all_with_where('bag', '', '', array('id' => $value));
            if ($get_image != null && !empty($get_image[0]['image'])) {
                @unlink('uploads/bag/' . $get_image[0]['image']);
            }
        }
        $record = $CI->production_model->get_delete_where_in('bag', 'id', $ids);
        $response_array['success'] = true;
        $response_array['message'] = $CI->lang->line('deleted_successfully');
    }
    return $response_array;
}

function get_payment_status($id)
{
    $array = array('0' => array('name' => 'Pending'), '1' => array('name' => 'Success'), '2' => array('name' => 'Failed'));
    if (array_key_exists($id, $array)) {
        return $array[$id];
    } else {
        return $id;
    }
}

function get_order_status($id = '')
{
    $CI = &get_instance();
    $array = array(
        '0' => array('name' => $CI->lang->line('pending'), 'description' => $CI->lang->line('description_pending')),
        '1' => array('name' => $CI->lang->line('approval'), 'description' => $CI->lang->line('description_approval')),
        '2' => array('name' => $CI->lang->line('preparing'), 'description' => $CI->lang->line('description_preparing')),
        '3' => array('name' => $CI->lang->line('cancelled'), 'description' => $CI->lang->line('description_cancelled')),
        '4' => array('name' => $CI->lang->line('ready_to_deliver'), 'description' => $CI->lang->line('description_ready_to_deliver')),
        '5' => array('name' => $CI->lang->line('on_the_way'), 'description' => $CI->lang->line('description_on_the_way')),
        '6' => array('name' => $CI->lang->line('delivered'), 'description' => $CI->lang->line('description_delivered')),
    );
    if ($id != '' && array_key_exists($id, $array)) {
        return $array[$id];
    } else {
        return $array;
    }
}

function get_payment_mode($id, $id_language)
{
    $CI = &get_instance();
    $payment_modes = $CI->config->item('payment_modes');
    if (is_array($payment_modes) && !empty($payment_modes)) {
    } else {
        $sql = 'SELECT id_unique,id_language,payment_mode FROM payment_mode';
        $info = $CI->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $info = $info['data'];
        } else {
            $info = array();
        }
        $CI->config->set_item('payment_modes', $info);
        $payment_modes = $info;
    }
    $return = '';
    foreach ($payment_modes as $value) {
        if ($value['id_unique'] == $id && $value['id_language'] == $id_language) {
            $return = $value['payment_mode'];
            break;
        }
    }
    return $return;
    /*$array = array('0' => array('name' => 'Cash On Delivery'), '1' => array('name' => 'Online'));
    if (array_key_exists($id, $array)) {
        return $array[$id];
    } else {
        return $id;
    }*/
}

function get_discount_type($id)
{
    $array = array('0' => array('name' => 'Percentage'), '1' => array('name' => 'Amount'));
    if (array_key_exists($id, $array)) {
        return $array[$id];
    } else {
        return $id;
    }
}

function calculate_discount($discount_type, $total_amount, $discount)
{
    if ($discount_type == '1') { //Amount
        return $discount;
    } else if ($discount_type == '0') { //Percentage
        return format_number((($discount * $total_amount) / 100));
    }
}

function get_line($slug)
{
    $CI = &get_instance();
    if ($CI->lang->line($slug) != null) {
        return $CI->lang->line($slug);
    } else {
        $current = "\n" . '$lang["' . $slug . '"] = "' . ucwords(str_replace('_', ' ', $slug)) . '"; ';
        file_put_contents(APPPATH . 'language/english/english_lang.php', $current, FILE_APPEND | LOCK_EX);
        file_put_contents(APPPATH . 'language/hindi/hindi_lang.php', $current, FILE_APPEND | LOCK_EX);
        return $slug;
    }
}

function get_line_front($slug)
{
    // return get_line($slug);
    $CI = &get_instance();
    if ($CI->lang->line($slug) != null) {
        return $CI->lang->line($slug);
    } else {
        $current = "\n" . '$lang["' . $slug . '"] = "' . ucwords(str_replace('_', ' ', $slug)) . '"; ';
        file_put_contents(APPPATH . 'language/english/english_lang.php', $current, FILE_APPEND | LOCK_EX);
        file_put_contents(APPPATH . 'language/hindi/hindi_lang.php', $current, FILE_APPEND | LOCK_EX);
        return $slug;
    }
}

function write_language_csv()
{
    $CI = &get_instance();
    $file = fopen("keywords.csv", "w");
    foreach ($CI->lang->language as $key => $line) {
        $line = array($key, $line);
        fputcsv($file, $line);
    }
    fclose($file);
}


function send_push_notification($settings = array())
{
    $token = $settings['token'];
    unset($settings['token']);
    $device_type = $settings['device_type'];
    unset($settings['device_type']);

    if ($settings['language_code'] == 1) {
        $title = $settings['title'];
        $msg = $settings['message'];
    } else {
        $title = $settings['title_turkish'];
        $msg = $settings['message_turkish'];
    }
    unset($settings['language_code']);
    if ($device_type == 'android') {
        $fields = array(
            'registration_ids' => array($token),
            'data' => array('message' => $settings)
        );
    } else {
        $fields = array(
            'registration_ids' => array($token),
            'priority' => 10,
            'data' => $settings,
            'notification' => array('title' => $title, 'body' => $msg, 'Time' => "06:00", 'sound' => 'Default'),
        );
        if (isset($settings['order_id'])) {
            $fields['order_id'] = $settings['order_id'];
        }
    }
    //header includes Content type and api key
    $headers = array(
        'Content-Type:application/json',
        'Authorization:key=' . FCMKey
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
    }
    curl_close($ch);
    return true;
}

function get_banner_for($type = '')
{
    $array = array('1' => get_line('category'), '2' => get_line('sub_category'), '3' => get_line('product'));
    if (array_key_exists($type, $array)) {
        return $array[$type];
    } else {
        return $array;
    }
}

function revert_quantity($id_product_order)
{
    $CI = &get_instance();
    $sql = "SELECT p.by_quantity_or_availability,pom.id_unique_product,pom.id_unique_product_attributes,pom.quantity FROM product p,product_order_meta pom WHERE 
    pom.id_unique_product = p.id_unique AND p.by_quantity_or_availability = '1'
    AND pom.id_product_order = '" . $id_product_order . "' GROUP BY p.id_unique";
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        $info = $info['data'][0];
        if ($info['id_unique_product_attributes'] == null) {
            $sql = 'UPDATE product_prices SET quantity = quantity + ' . $info['quantity'] . ' WHERE id_unique_product = "' . $info['id_unique_product'] . '" AND id_unique_product_attributes IS NULL ';
        } else {
            $sql = 'UPDATE product_prices SET quantity = quantity + ' . $info['quantity'] . ' WHERE id_unique_product = "' . $info['id_unique_product'] . '" AND id_unique_product_attributes = "' . $info['id_unique_product_attributes'] . '" ';
        }
        $CI->common_model->simple_query($sql);
    }
    return true;
}

function get_url_from_category($category_info)
{
    $conditions = array(
        'where' => array('id' => $category_info['parent_id'])
    );
    $CI = &get_instance();
    $info = $CI->common_model->select_data('category', $conditions);
    if ($info['row_count'] > 0) {
        $GLOBALS['current_category'][] = $info['data'][0]['slug' . get_language()];
        $GLOBALS['current_category_ids'][] = $info['data'][0]['id'];
        get_url_from_category($info['data'][0]);
    } else {
        return true;
    }
}

function get_child_category_ids($id)
{
    $conditions = array(
        'where' => array('parent_id' => $id)
    );
    $CI = &get_instance();
    $info = $CI->common_model->select_data('category', $conditions);
    if ($info['row_count'] > 0) {
        $GLOBALS['child_category_ids'][] = $info['data'][0]['id'];
        get_child_category_ids($info['data'][0]['id']);
    } else {
        return true;
    }
}

/**
 * For getting last category id based on current category id
 */
function get_last_id($id)
{
    $conditions = array(
        'where' => array('parent_id' => $id)
    );
    $CI = &get_instance();
    $info = $CI->common_model->select_data('category', $conditions);
    if ($info['row_count'] > 0) {
        $GLOBALS['last_id'] = $info['data'][0]['id'];
        get_last_id($info['data'][0]['id']);
    } else {
        return true;
    }
}

/**
 * For getting first category id based on current category id
 */
function get_first_id($id)
{
    $conditions = array(
        'where' => array('id' => $id, 'parent_id !=' => 0)
    );
    $CI = &get_instance();
    $info = $CI->common_model->select_data('category', $conditions);
    if ($info['row_count'] > 0) {
        $GLOBALS['first_id'] = $info['data'][0]['parent_id'];
        get_first_id($info['data'][0]['parent_id']);
    } else {
        return true;
    }
}


function get_total_cart_product()
{
    $CI = &get_instance();
    $sql = 'SELECT COUNT(id) AS total_product FROM cart WHERE id_user = "' . get_front_login_id() . '" ';
    $total_product = 0;
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        $total_product = isset($info['data'][0]['total_product']) ? (int) $info['data'][0]['total_product'] : 0;
    }
    return $total_product;
}


function get_payment_mode_static($payment_mode)
{
    $array = array(1 => array('name' => 'cheque_payment'), 2 => array('name' => 'cash_on_delivery'));
    if (array_key_exists($payment_mode, $array)) {
        return $array[$payment_mode];
    } else {
        return '';
    }
}

/**
 * This function is used for getting current user login id
 */
function get_front_login_id()
{
    $CI = &get_instance();
    if (isset($CI->session->userdata['login_id']) && !empty($CI->session->userdata['login_id'])) {
        $login_id = $CI->session->userdata['login_id'];
    } else {
        $login_id = COOKIE_USER;
    }
    return $login_id;
}

function order_status_options(){
    $days = array(
        'all' => 'All',
        'confirmed' => 'Confirmed',
        'shipped' => 'Shipped',
        'delivered' => 'Delivered',
        'pending' => 'Pending',
        'failed' => 'Failed',
        'cancelled' => 'Cancelled',
    );
    return $days;
}
function get_pet_type($id = '')
{
    $array = array(
        '0' => get_line('dog'),
        '1' => get_line('cat'),
        '2' => get_line('rabbit'),
        '3' => get_line('turtle'),
        '4' => get_line('hamster')
    );
    if ($id != '' && array_key_exists($id, $array)) {
        return $array[$id];
    } else {
        return $array;
    }
}
function get_gender($key = '')
{
    $array = array(
        '0' => get_line('male'),
        '1' => get_line('female')
    );
    if ($key != '' && array_key_exists($key, $array)) {
        return $array[$key];
    } else {
        return $array;
    }
}