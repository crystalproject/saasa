<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * This function creates directory if not available
 * @param type $path
 */
if (!function_exists("create_directory")) {

    function create_directory($path)
    {
        $path = explode('/', $path);
        $string = '';
        foreach ($path as $key => $value) {
            $string .= $value;
            if (!is_dir($string)) {
                mkdir($string);
                @chmod($string, 0777);
            }
            $string .= '/';
        }
    }
}

/**
 * This function is used to upload any type of file
 * @param type $file_name
 * @param type $path
 * @return type
 */
function upload_file($file_name, $path)
{
    $CI = &get_instance();
    create_directory($path);
    $return = array('status' => false, 'file_name' => '', 'error' => '');
    if (isset($_FILES[$file_name]) && $_FILES[$file_name]['name'] != "") {
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = true;
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        if (!$CI->upload->do_upload($file_name)) {
            $return['error'] = $CI->upload->display_errors();
        } else {
            $upload_data = $CI->upload->data();
            $file_name = $upload_data['file_name'];
            @chmod($config['upload_path'] . $file_name, 0777);
            $return['status'] = true;
            $return['file_name'] = $file_name;
        }
    }
    return $return;
}

function upload_file_multiple($file_name, $path)
{
    $CI = &get_instance();
    create_directory($path);
    $return = array('status' => false, 'file_name' => array(), 'error' => '');
    if (isset($_FILES[$file_name]) && !empty($_FILES[$file_name]['name'])) {
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = true;
        $file_count = count($_FILES[$file_name]['name']);
        for ($i = 0; $i < $file_count; $i++) {
            $_FILES['file']['name'] = $_FILES[$file_name]['name'][$i];
            $_FILES['file']['type'] = $_FILES[$file_name]['type'][$i];
            $_FILES['file']['tmp_name'] = $_FILES[$file_name]['tmp_name'][$i];
            $_FILES['file']['error'] = $_FILES[$file_name]['error'][$i];
            $_FILES['file']['size'] = $_FILES[$file_name]['size'][$i];
            $CI->load->library('upload', $config);
            $CI->upload->initialize($config);
            if (!$CI->upload->do_upload('file')) {
                $return['error'] = $CI->upload->display_errors();
            } else {
                $upload_data = $CI->upload->data();
                $file_name_upload = $upload_data['file_name'];
                @chmod($config['upload_path'] . $file_name_upload, 0777);
                $return['status'] = true;
                $return['file_name'][] = $file_name_upload;
            }
        }
    }
    return $return;
}

/**
 * This function validates file array
 * @param type $extensions
 */
function validate_extensions($extensions = array(), $file_name = '')
{
    $error = true;
    if (isset($_FILES[$file_name]['name']) && is_array($_FILES[$file_name]['name']) && !empty($_FILES[$file_name]['name'])) {
        foreach ($_FILES[$file_name]['name'] as $key => $value) {
            $file_extension = explode('.', $value);
            if (!in_array(strtolower($file_extension[count($file_extension) - 1]), $extensions)) {
                $error = false;
                break;
            }
        }
    } else if (isset($_FILES[$file_name]['name']) && $_FILES[$file_name]['name'] != '') {
        $file_extension = explode('.', $_FILES[$file_name]['name']);
        if (!in_array(strtolower($file_extension[count($file_extension) - 1]), $extensions)) {
            $error = false;
        }
    }
    return $error;
}

if (!function_exists("delete_file")) {

    function delete_file($path)
    {
        if (file_exists($path)) {
            @unlink($path);
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('validate_file_size')) {

    function validate_file_size($max_file_size, $file_name = '')
    {
        $error = true;
        if (isset($_FILES[$file_name]['name']) && is_array($_FILES[$file_name]['name']) && !empty($_FILES[$file_name]['name'])) {
            foreach ($_FILES[$file_name]['name'] as $key => $value) {
                if ($_FILES[$file_name]['size'][$key] >= $max_file_size) {
                    $error = true;
                    break;
                }
            }
        } else if (isset($_FILES[$file_name]['name']) && $_FILES[$file_name]['name'] != '') {
            if ($_FILES[$file_name]['size'] >= $max_file_size) {
                $error = true;
            }
        }
        return $error;
    }
}

/**
 * This function validates file array
 * @param type $extensions
 */
function is_valid_extension($extensions = array(), $file_name = '')
{
    $return = true;
    if (isset($_FILES[$file_name]['name']) && is_array($_FILES[$file_name]['name']) && !empty($_FILES[$file_name]['name'])) {
        foreach ($_FILES[$file_name]['name'] as $key => $value) {
            $file_extension = explode('.', $value);
            if (!in_array(strtolower($file_extension[count($file_extension) - 1]), $extensions)) {
                $return = false;
                break;
            }
        }
    } else if (isset($_FILES[$file_name]['name']) && $_FILES[$file_name]['name'] != '') {
        $file_extension = explode('.', $_FILES[$file_name]['name']);
        if (!in_array(strtolower($file_extension[count($file_extension) - 1]), $extensions)) {
            $return = false;
        }
    }
    return $return;
}

/**
 * This function is used for deleting file from table records
 */
function delete_file_from_table($table, $upload_path,  $column_name, $conditions)
{
    $CI = &get_instance();
    $conditions['SELECT'] = $column_name;
    $info = $CI->common_model->select_data($table, $conditions);
    if ($info['row_count'] > 0) {
        foreach ($info['data'] as $value) {
            if (file_exists($upload_path . $value[$column_name])) {
                @unlink($upload_path . $value[$column_name]);
            }
        }
    }
    return true;
}
