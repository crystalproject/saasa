<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * @param type $id_user
 * @param type $id_coupon
 */
function get_coupon_used_count_by_user($id_user, $id_coupon) {
    $CI = &get_instance();
    $conditions = array(
        'WHERE' => array('id_user'=>$id_user,'id_coupon'=>$id_coupon),
        'SELECT' => 'id',
    );
    $info = $CI->common_model->select_data('product_order',$conditions);
    return $info['row_count'];
}

/**
 * 
 */
function apply_coupon_code($form_data) {    
    $CI = &get_instance();
    $CI->session->coupon_code = '';
    $response_array = array('status' => false, 'message' => '', 'data' => null);
    $id_user = $form_data['id_user'];
    $conditions = array(
        'SELECT' => 'id,coupon_code,description,discount_type,discount,minimum_amount_cart_status,minimum_amount_cart,maximum_coupon_used',
        'WHERE' => array('is_active' => '1', 'coupon_code' => $form_data['coupon_code'])
    );
    $info = $CI->common_model->select_data('coupons', $conditions);
    if ($info['row_count'] > 0) {
        //Check discount amount status
        $error = false;
        $info = $info['data'][0];        
        $cart_total = get_cart_total($id_user);        
        if ($info['minimum_amount_cart_status'] == true && $cart_total < $info['minimum_amount_cart']) {
            //Check total of cart                        
            $error = true;
            
            $response_array['message'] = $CI->lang->line('invalid_coupon_code');
        } else if (!$error && get_coupon_used_count_by_user($id_user, $info['id']) > $info['maximum_coupon_used']) {
            //Check total coupon redemption
            $error = true;
            $response_array['message'] = $CI->lang->line('coupon_code_limit_exceeded');
        } else {
            $return = array(
                'id_coupon' => $info['id'],
                'discount_type' => $info['discount_type'],
                'coupon_code' => $info['coupon_code'],
                'coupon_discount' => format_number($info['discount']),
                'cart_total' => format_number($cart_total),
                'discounted_total' => format_number(0),
                'total_discount' => format_number(0),
            );
            if ($info['discount_type'] == 'percentage') {
                $return['total_discount'] = format_number($return['cart_total'] * $return['coupon_discount'] / 100);
                $return['discounted_total'] = format_number($return['cart_total'] - $return['total_discount']);
            } else if ($info['discount_type'] == 'amount') {
                if ($return['cart_total'] > $return['coupon_discount']) {
                    $return['total_discount'] = format_number($return['coupon_discount']);
                    $return['discounted_total'] = format_number($return['cart_total'] - $return['total_discount']);                    
                } else {
                    $error = true;
                    $response_array['message'] = $CI->lang->line('invalid_coupon_code');
                }
            }
            if (!$error) {                
                $CI->session->coupon_code = $form_data['coupon_code'];
                $response_array['status'] = true;
                $response_array['message'] = $CI->lang->line('coupon_code_applied_successfully');
                $response_array['data'] = $return;
            }
        }
    } else {
        $response_array['message'] = $CI->lang->line('incorrect_coupon_code');
    }
    return $response_array;
}
