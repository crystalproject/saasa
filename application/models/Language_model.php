<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Language_model extends CI_model
{
    public function __construct()
    {
        if ($this->uri->segment(1) == 'authority') {
            $get_language = get_details('language', array('is_active' => 1, 'default_language' => 1));
            if (isset($get_language) && $get_language != null && $get_language[0]['id'] == 1) {
                $this->lang->load('english_lang', 'english'); // fisrt para. of filename , foldername.
            } else if (isset($get_language) && $get_language != null && $get_language[0]['id'] == 2) {
                $this->lang->load('hindi_lang', 'hindi'); // fisrt para. of filename , foldername.
            } else {
                $this->lang->load('english_lang', 'english');
            }
        } else {
            $get_language = get_details('language', array('id' => get_language_id(), 'is_active' => 1));
            if (isset($get_language) && $get_language != null && $get_language[0]['id'] == 1) {
                $this->lang->load('english_lang', 'english'); // fisrt para. of filename , foldername.
            } else if (isset($get_language) && $get_language != null && $get_language[0]['id'] == 2) {
                $this->lang->load('hindi_lang', 'hindi'); // fisrt para. of filename , foldername.
            }
        }
    }
}
