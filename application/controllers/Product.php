<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Product extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('product_helper');
    }
    public function index($id = '')
    {
        $data = array();
        $data['current_url'] = current_url();
        $data['current_url'] = str_replace('/product/', '/product/details/', $data['current_url']);        
        $total_segment = $this->uri->total_segments();
        $id = $this->uri->segment($total_segment);

        $GLOBALS['current_category'] = $id;

        $data['child_category_ids'] = array($id);
        $GLOBALS['child_category_ids'] = $data['child_category_ids'];
        get_child_category_ids($id);
        $data['child_category_ids'] = $GLOBALS['child_category_ids'];

        $GLOBALS['last_id'] =   $id;
        get_last_id($id);
        $id = $GLOBALS['last_id'];
        $GLOBALS['first_id'] = $id;
        get_first_id($id);
        $data['first_id'] = $GLOBALS['first_id'];

        // Getting first category info
        $data['first_category_info'] = array();
        $conditions = array(
            'where' => array('id' => $data['first_id'])
        );
        $info =  $this->common_model->select_data('category', $conditions);
        if ($info['row_count'] > 0) {
            $data['first_category_info'] = $info['data'][0];
        } else {
            redirect(base_url());
        }

        if ($id == '') {
            redirect(base_url());
        }
        $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;

        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $this->_default_limit = 12;
        $form_data = array();

        $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
        $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

        $form_data['view_by'] = isset($form_data['view_by']) ? $form_data['view_by'] : 'all';
        $slug = get_language_front();
        $sub_query = '';
        if ($form_data['view_by'] == 'discounted') {
            $form_data['is_popular'] = 0;
            $form_data['id_category'] = 0;
            $sub_query .= ' AND id_unique IN (SELECT DISTINCT(id_unique_product) AS id_unique_product FROM product_prices WHERE discount > 0) ';
        }
        if (isset($form_data['search_value']) && $form_data['search_value'] != null) {
            $sub_query .= ' AND (title LIKE "%' . $form_data['search_value'] . '%" OR description LIKE "%' . $form_data['search_value'] . '%") ';
        }
        if (isset($form_data['is_popular']) && $form_data['is_popular'] == '1') {
            $sub_query .= ' AND is_popular = "1" ';
        }
        /*if (isset($form_data['id_category']) && $form_data['id_category'] > 0) {*/
        //For displaying product of all child categories
        // $child_category_ids = $GLOBALS['child_category_ids'];
        // $sub_query .= ' AND id_category IN (' . implode(',', $child_category_ids) . ') ';

        $child_category_ids = array($GLOBALS['current_category']);
        $sub_query .= ' AND id_category IN (' . implode(',', $child_category_ids) . ') ';
        /*}*/
        $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
        $sql = 'SELECT id_unique,is_new,title,main_image,slug,has_attributes,by_quantity_or_availability' . $sub_sub_query . ' FROM product WHERE id_language = "' . get_language_id() . '" ' . $sub_query . ' ORDER BY id_unique ASC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'] . ' ';
        $info = $this->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $response_array['status'] = true;
            foreach ($info['data'] as &$value) {
                if ($value['image_name'] != null) {
                    $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                }
                if ($value['main_image'] != null) {
                    $value['main_image'] = base_url('uploads/product/') . $value['main_image'];
                }
                $value['other_info'] = get_product_detail($value, get_language_id());
                //unset($value['has_attributes']);
                unset($value['by_quantity_or_availability']);
                unset($value['category_name']);
            }
            $response_array['data'] = $info['data'];
            unset($value);
        } else {
            // $response_array['status'] = true;
            // $response_array['data'] = null;
            // $response_array['message'] = get_line('product_not_available');
        }
        $data['details'] = null;
        $data['details'] = $response_array['data'];

        $data['pagination'] = '';
        $data['no'] = '';

        //getting attriutes
        $data['attribute_info'] = array();
        $conditions = array('where' => array('is_active' => '1', 'id_language' => get_language_id()), 'IN' => array('id_unique' => array('5ea6744289b00', '5eb2a7134e9c5')), 'limit' => 2);
        $info = $this->common_model->select_data('attributes', $conditions);
        if ($info['row_count'] > 0) {
            foreach ($info['data'] as $key => $value) {
                $conditions = array('where' => array('id_unique_attributes' => $value['id_unique'], 'is_active' => '1', 'id_language' => get_language_id()));
                $temp = $this->common_model->select_data('attribute_values', $conditions);
                if ($temp['row_count'] > 0) {
                    $info['data'][$key]['attribute_info'] = $temp['data'];
                } else {
                    unset($data[$key]);
                }
            }
            $data['attribute_info'] = $info['data'];
        }
        $conditions = array('where' => array('is_active' => '1'));
        $brands_details = $this->common_model->select_data('brands', $conditions);
        if (isset($brands_details) && !empty($brands_details)) {
            $data['brands_details'] = $brands_details['data'];
        }
        $this->load->view('product/index', $data);
    }

    /*public function details($id = '')
    {
        $this->load->view('product/details-fpd');
    }*/
    public function details($id = '')
    {
        
        
        $total_segment = $this->uri->total_segments();
        $id = $this->uri->segment($total_segment);
        if ($id == '') {
            redirect(base_url());
        }
        $data['banner'] = get_details('category_banner', array('id' => 1));
        $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
        $this->_language_default = get_language_info($set_language);
        $slug = '_' . $this->_language_default['slug'];
        if ($this->_language_default['id'] == '1') {
            $slug = '';
        }
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $form_data = array();
        $form_data['id_unique'] = $id;
        $id_category = '';
        $sub_query = '';
        $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
        $sql = 'SELECT id_unique,id_category,title,is_customizable,description,long_description,sku,additional_description,has_attributes,main_image,by_quantity_or_availability,product_video,youtube_link' . $sub_sub_query . ' FROM product WHERE id_unique = "' . $form_data['id_unique'] . '" AND id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . '  ';
        $info = $this->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $response_array['status'] = true;
            foreach ($info['data'] as &$value) {
                $id_category = $value['id_category'];
                if ($value['image_name'] != null) {
                    $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                }
                if ($value['main_image'] != null) {
                    $value['main_image'] = base_url('uploads/product/') . $value['main_image'];
                }
                if ($value['product_video'] != null) {
                    $value['product_video'] = base_url('uploads/product/') . $value['product_video'];
                }
                $value['average_review'] =  get_average_review($value['id_unique']);                
                $value['total_review'] = get_total_review($value['id_unique']);
                $value['other_info'] = get_product_detail($value, $this->_language_default['id'], 'detail');
            }
            $response_array['data'] = $info['data'];
        } else {
            $response_array['status'] = false;
            $response_array['data'] = null;
            $response_array['message'] = get_line('product_not_available');
        }
        if ($response_array['status'] == true) {
            $data['info'] = $response_array['data'][0];
        } else {
            redirect(base_url());
        }

        //Getting related product
        $data['child_category_ids'] = array($id_category);
        $GLOBALS['child_category_ids'] = $data['child_category_ids'];
        get_child_category_ids($id_category);
        $data['child_category_ids'] = $GLOBALS['child_category_ids'];
        $data['related_product'] = null;

        $sub_query = ' AND id_unique != "' . $form_data['id_unique'] . '" AND id_category IN (' . implode(',', $data['child_category_ids']) . ') ';
        $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
        $sql = 'SELECT id_unique,slug,id_category,title,description,long_description,sku,additional_description,has_attributes,main_image,by_quantity_or_availability,product_video,youtube_link' . $sub_sub_query . ' FROM product WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' LIMIT 9 ';
        $info = $this->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $response_array['status'] = true;
            foreach ($info['data'] as &$value) {
                if ($value['image_name'] != null) {
                    $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                }
                if ($value['main_image'] != null) {
                    $value['main_image'] = base_url('uploads/product/') . $value['main_image'];
                }
                if ($value['product_video'] != null) {
                    $value['product_video'] = base_url('uploads/product/') . $value['product_video'];
                }
                $value['other_info'] = get_product_detail($value, $this->_language_default['id']);
            }
            $data['related_product'] = $info['data'];
        }
        // echo "<pre>";print_r($data);exit;
        $this->load->view('product/details', $data);
    }

    public function details_m($id = '')
    {
        if ($id == '') {
            redirect(base_url());
        }
        $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
        $condition['product.is_active'] = 1;
        $condition['product.id_unique'] = $id;
        $condition['product.id_language'] = $set_language;

        $join[0]['table_name'] = 'category';
        $join[0]['column_name'] = 'category.id = product.id_category';
        $join[0]['type'] = 'left';

        $join[1]['table_name'] = 'product_images';
        $join[1]['column_name'] = 'product_images.id_unique_product = product.id_unique';
        $join[1]['type'] = 'left';

        $join[2]['table_name'] = 'product_prices';
        $join[2]['column_name'] = 'product_prices.id_unique_product = product.id_unique';
        $join[2]['type'] = 'left';

        $details = $this->production_model->jointable_descending(array('product.*', 'category.name as category_name', 'product_images.image_name', 'product_prices.price', 'product_prices.discount', 'product_prices.is_product_available'), 'product', '', $join, 'id', 'desc', $condition);

        $product_images['product_images'] = $this->production_model->get_all_with_where('product_images', 'id', 'asc', array('id_unique_product' => $details[0]['id_unique']));

        /*Related product*/
        $condition1['product.is_active'] = 1;
        $condition1['product.is_popular'] = 1;

        $join1[0]['table_name'] = 'category';
        $join1[0]['column_name'] = 'category.id = product.id_category';
        $join1[0]['type'] = 'left';

        $join1[1]['table_name'] = 'product_images';
        $join1[1]['column_name'] = 'product_images.id_unique_product = product.id_unique';
        $join1[1]['type'] = 'left';

        $join1[2]['table_name'] = 'product_prices';
        $join1[2]['column_name'] = 'product_prices.id_unique_product = product.id_unique';
        $join1[2]['type'] = 'left';

        $related_product['related_product'] = $this->production_model->jointable_descending(array('product.*', 'category.name as category_name', 'product_images.image_name', 'product_prices.price', 'product_prices.is_product_available'), 'product', '', $join1, 'product.id', 'desc', $condition1, '', 'product_images.id_unique_product');

        $data = array_merge($details[0], $product_images, $related_product);

        $this->load->view('product/details', $data);
    }
}
