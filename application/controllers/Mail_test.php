<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Mail_test extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        ini_set('display_errors', true);
        error_reporting(E_ALL);

        $settings = array(
            'to' => 'crystalinfoway12@gmail.com',
            'subject' => 'Testing Email',
            'html' => '<html><head></head><body>Hello</body></html>'
        );
        $mail = new PHPMailer;
        $mail->CharSet = 'utf-8';
        $mail->IsSMTP();
        $mail->Host = SMTP_HOST; // Specify main and backup server
        $mail->Port = SMTP_PORT; // Set the SMTP port
        $mail->SMTPAuth = true;  // Enable SMTP authentication
        $mail->Username = SMTP_USERNAME; // SMTP username
        $mail->Password = SMTP_PASSWORD; // SMTP password
        $mail->IsMail();
        $mail->From = FROM_EMAIL;
        $mail->FromName = FROM_EMAIL_TITLE;
        $mail->AddAddress($settings['to']);
        $mail->IsHTML(true);
        $mail->Subject = $settings['subject'];;
        $mail->Body = $settings['html'];
        if (!$mail->Send()) {
            // echo 'Message could not be sent.';
            log_message('error', 'Mailer Error: ' . $mail->ErrorInfo);
            return false;
            //   exit;
        } else {
            // echo 'Message sent.';
            return true;
        }
    }
}
