<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cart extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cart_helper');
        $this->load->helper('product_helper');
        $this->load->helper('order_helper');
        $this->load->helper('upload_helper');
        $this->load->helper('coupon_helper');
    }

    public function index()
    {
        $data = array();
        $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
        $form_data = array();
        $login_id = get_front_login_id();
        $form_data['id_user'] = $login_id;
        $form_data['language_slug'] = get_language_front();
        $form_data['id_language'] = $set_language;
        $data = get_cart_products($form_data);
        $data['user_info'] = $form_data;
        // echo "<pre>";print_r($data);exit;
        $this->load->view('cart/view', $data);
    }

    public function add()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id_unique', '', 'trim|required', array('required' => get_line('something_wrong')));
            //$this->form_validation->set_rules('id_unique_attribute_values', '', 'trim|required', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $form_data = $this->input->post();
                $login_id = get_front_login_id();
                $form_data['id_user'] = $login_id;
                if ($form_data['id_user'] > 0) {
                    $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
                    $form_data['id_language'] = $set_language;
                    $response_array = add_to_cart($form_data);
                    $response_array['data']['total_cart_product'] = get_total_cart_product();
                } else {
                    $response_array['message'] = get_line('please_login');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function remove()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $form_data = $this->input->post();
                $login_id = get_front_login_id();
                $form_data['id_user'] = $login_id;
                if ($form_data['id_user'] > 0) {
                    $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
                    $conditions = array('where' => array('id_user' => $form_data['id_user'], 'id' => $this->input->post('id')));
                    $this->common_model->delete_data('cart', $conditions);
                    $response_array['status'] = true;
                    $response_array['message'] = get_line('cart_remove_success');
                } else {
                    $response_array['message'] = get_line('please_login');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function update_quantity()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $this->form_validation->set_rules('id', '', 'trim|required|numeric|greater_than[0]', array('required' => 'cart id not available in the request'));
        $this->form_validation->set_rules('quantity', '', 'trim|required|numeric|greater_than[0]', array('required' => 'quantity available in the request'));
        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $value) {
                $response_array['message'] = $value;
                break;
            }
        } else {
            $form_data = $this->input->post();
            $login_id = get_front_login_id();
            $form_data['id_user'] = $login_id;
            if ($form_data['id_user'] > 0) {
                $sql = 'SELECT id_unique_product,id_unique_product_attributes FROM cart WHERE id = "' . $form_data['id'] . '" ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $info = $info['data'][0];
                    if (check_product_available_quantity($info['id_unique_product'], $info['id_unique_product_attributes'], $form_data['quantity'])) {
                        $conditions = array('where' => array('id' => $form_data['id'], 'id_user' => $form_data['id_user']));
                        $this->common_model->update_data('cart', array('quantity' => $form_data['quantity']), $conditions);
                        $response_array['status'] = true;
                        $response_array['message'] = $this->lang->line('product_quantity_updated');
                    } else {
                        $response_array['message'] = $this->lang->line('product_not_available');
                    }
                } else {
                    $response_array['message'] = $this->lang->line('product_not_available');
                }
            } else {
                $response_array['message'] = get_line('please_login');
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function checkout()
    {
        $data = array();
        $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
        $login_id = get_front_login_id();
        $form_data['id_user'] = $login_id;
        $form_data['language_slug'] = get_language_front();
        $form_data['id_language'] = $set_language;
        $data = get_cart_products($form_data);
        if ($data['status'] == false) {
            redirect(base_url());
        }
        $data['user_info'] = $form_data;

        $data['loggeed_in_user_info'] = array();
        if ($this->session->userdata('login_id') != null) {
            $info = $this->common_model->select_data('register', array('where' => array('id' => $this->session->userdata('login_id'))));
            if ($info['row_count'] > 0) {
                $data['loggeed_in_user_info'] = $info['data'][0];
            }
        }
        $this->load->view('cart/checkout', $data);
    }

    public function add_order()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $this->form_validation->set_rules('first_name', '', 'trim|required', array('required' => get_line('first_name_required')));
        $this->form_validation->set_rules('last_name', '', 'trim|required', array('required' => get_line('last_name_required')));
        $this->form_validation->set_rules('country', '', 'trim|required', array('required' => get_line('country_required')));
        $this->form_validation->set_rules('street', '', 'trim|required', array('required' => get_line('street_required')));
        $this->form_validation->set_rules('apartment_suite_etc', '', 'trim|required', array('required' => get_line('apartment_suite_etc_required')));
        $this->form_validation->set_rules('city', '', 'trim|required', array('required' => get_line('city_required')));
        $this->form_validation->set_rules('pincode', '', 'trim|numeric', array('required' => get_line('pincode_required')));
        $this->form_validation->set_rules('phone', '', 'trim|required|numeric|min_length[9]', array('required' => get_line('phone_required')));
        $this->form_validation->set_rules('email_address', '', 'trim|required|valid_email', array('required' => get_line('email_address_required')));
        $this->form_validation->set_rules('payment_mode', '', 'trim|required|in_list[1,2]', array('required' => get_line('something_wrong'), 'in_list' => get_line('something_wrong')));
        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $value) {
                $response_array['message'] = $value;
                break;
            }
        } else {
            $form_data = $this->input->post();
            $is_guest_user = 0;
            $otp = '';
            //Need to add registration code
            $login_id = get_front_login_id();
            if ($login_id == COOKIE_USER) {
                //User not registered at
                //new registration required
                $conditions = array(
                    'where' => array('email' => $this->input->post('email_address'))
                );
                $info = $this->common_model->select_data('register', $conditions);
                if ($info['row_count'] > 0) {
                    $info = $info['data'][0];
                    $session = array(
                        'login_id' => $info['id'],
                        'username' => $info['first_name'],
                        'useremail' => $info['email'],
                        'useremobile' => isset($info['mobile_number']) ? $info['mobile_number'] : '',
                    );
                    //$this->session->set_userdata($session);
                } else {
                    $otp = '';
                    for ($i = 0; $i < 6; $i++) {
                        $otp .= mt_rand(0, 9);
                    }
                    $password = $this->encryption->encrypt($otp);
                    $records = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email_address'),
                        'password' => $password,
                        'created_at' => CURRENT_TIME,
                        'modified_at' => CURRENT_TIME,
                    );
                    $login_id = $this->common_model->insert_data('register', $records);
                    if ($login_id > 0) {
                        $is_guest_user = 1;
                        $session = array(
                            'login_id' => $login_id,
                            'username' => $records['first_name'],
                            'useremail' => $records['email'],
                            'useremobile' => isset($records['mobile_number']) ? $records['mobile_number'] : '',
                        );
                        //$this->session->set_userdata($session);

                        $conditions = array(
                            'where' => array('id_user' => $session['login_id'])
                        );
                        $this->common_model->update_data('cart', array('id_user' => $session['login_id']), $conditions);
                    } else {
                        $response_array['message'] = get_line('something_wrong');
                        echo json_encode($response_array);
                        exit;
                    }
                }

                $conditions = array(
                    'where' => array('id_user' => COOKIE_USER)
                );
                $this->common_model->update_data('cart', array('id_user' => $session['login_id']), $conditions);
                $login_id = $session['login_id'];
            }
            $form_data['id_user'] = $login_id;
            if ($form_data['id_user'] > 0) {
                $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
                load_language($set_language);
                $slug = '';
                if ($set_language == 2) {
                    $slug = '_arabic';
                }
                $form_data['language_slug'] = $slug;
                $form_data['id_language'] = $set_language;
                $response_array = save_order($form_data);
                if ($response_array['status'] == true) {
                    
                    $conditions = array(
                        'where' => array('email' => $this->input->post('email_address'))
                    );
                    $info = $this->common_model->select_data('register', $conditions);
                    if ($info['row_count'] > 0) {
                        $info = $info['data'][0];
                        $session = array(
                            'login_id' => $info['id'],
                            'username' => $info['first_name'],
                            'useremail' => $info['email'],
                            'useremobile' => isset($info['mobile_number']) ? $info['mobile_number'] : '',
                        );                        
                    } 

                    //For sending an email
                    $temp = array(
                        'is_guest_user' => $is_guest_user,
                        'password' => $otp,
                        'first_name' => $session['username'],
                        'order_id' => $response_array['data']['order_id']
                    );

                    $html = $this->load->view('email-template/new-order', $temp, true);                    
                    send_mail(array('to' =>$session['useremail'], 'subject' => SITE_TITLE . ': Order Placed', 'html' => $html));

                    $this->session->set_flashdata('order_success', $response_array['message']);
                    $response_array['data']['redirect'] = base_url('my-order');
                }
            } else {
                $response_array['message'] = get_line('please_login');
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function save_customized_product()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        /*$base64_str = substr($_POST['base64_image'], strpos($_POST['base64_image'], ",") + 1);
        //decode base64 string
        $decoded = base64_decode($base64_str);
        $png_url = uniqid() . "-custom.png";
        //create png from decoded base 64 string and save the image in the parent folder
        $result = file_put_contents('uploads/product/' . $png_url, $decoded);
        if ($result) {
            $response_array['status'] = true;
            $response_array['message'] = get_line('image_saved_successfully');
            $response_array['data'] = $png_url;
        } else {
            $response_array['message'] = get_line('something_wrong');
        }*/

        if (!isset($_FILES['image']['name']) || empty($_FILES['image']['name'])) {
            $response_array['message'] = 'Please select correct file to upload a';
        } else {
            $_FILES['image']['name'] = $_FILES['image']['name'] . '.' . explode('/', $_FILES['image']['type'])[1];
            if (!is_valid_extension(array('jpg', 'png'), 'image')) {
                $response_array['message'] = 'Please select correct file to upload';
            } else {
                $file = upload_file('image', 'uploads/product');
                if ($file['status'] == true) {
                    $response_array['status'] = true;
                    $response_array['message'] = get_line('image_saved_successfully');
                    $response_array['data'] = $file['file_name'];
                } else {
                    $response_array['message'] = get_line('something_wrong');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }
}