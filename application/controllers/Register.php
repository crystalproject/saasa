<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    function index()
    {
        $data = $this->input->post();
        if ($this->input->method() == 'post') {
            
            $this->form_validation->set_rules('full_name', 'full_name', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[register.email]', array('required' => get_line_front('this_field_is_required'), "valid_email" => get_line_front('please_enter_valid_email_address'), 'is_unique' => get_line_front('email_already_exist')));
            $this->form_validation->set_rules('password', 'password', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('mobile_number', 'mobile_number', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('pet_name[]', 'pet_name', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('pet_type[]', 'pet_type', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('pet_gender[]', 'pet_gender', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('pet_dob[]', 'pet_dob', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('pet_breed[]', 'pet_breed', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('pet_weight[]', 'pet_weight', 'required', array('required' => get_line_front('this_field_is_required')));
            if ($this->form_validation->run() == FALSE) {
                $data['data'] = array_merge($data, $this->form_validation->error_array());
            } else {
                $user_data['password'] = $this->encryption->encrypt($data['password']);    
                $user_data['full_name']=$data['full_name'];
                $user_data['email']=$data['email'];
                $user_data['city']=$data['city'];
                $user_data['mobile_number']=$data['mobile_number'];
                $user_data['news_leter']=(isset($data['news_leter']) && $data['news_leter'] == 1)?1:0;
                // if (array_key_exists('otp', $data)) {
                //     unset($data['otp']);
                // }
                // echo "<pre>";print_r($data);exit;
                $record = $this->production_model->insert_record('register', $user_data);
                if(isset($record) && $record != ""){
                    $count_records = count($this->input->post('pet_name'));
                    $pet_names = $this->input->post('pet_name');
                    $pet_types = $this->input->post('pet_type');
                    $pet_genders = $this->input->post('pet_gender');
                    $pet_dobs = $this->input->post('pet_dob');
                    $pet_breeds = $this->input->post('pet_breed');
                    $pet_weights = $this->input->post('pet_weight');
                    for ($i=0; $i < $count_records ; $i++) { 
                        $pet_data['pet_name'] = $record;
                        $pet_data['pet_name'] = $pet_names[$i];
                        $pet_data['pet_type'] = $pet_types[$i];
                        $pet_data['pet_gender'] = $pet_genders[$i];
                        $pet_data['pet_dob'] = date('Y-m-d', strtotime($pet_dobs[$i]));
                        $pet_data['pet_breed'] = $pet_breeds[$i];
                        $pet_data['pet_weight'] = $pet_weights[$i];
                        $pet_record = $this->production_model->insert_record('pets', $pet_data);
                    }
                }
                // $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',array($data['email']),'','mail_form/thankyou_page/contact_us','',''); // user send email thank-you page

                // $admin_email = get_details('administrator'); 
                // $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/contact_us',$data,''); // admin send mail
                $this->session->set_flashdata('success', get_line_front('thanks_for_registration'));
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        $this->load->view('register/index', $data);
    }
    public function profile(){
        $data = array();
        $user_id = $this->session->userdata('login_id');
        $data = get_records('register',array('WHERE'=>['id'=>$user_id]));
        if(isset($data) && !empty($data)){
            $data= $data[0];
        }
        // echo "<pre>".$this->db->last_query();print_r();exit;
        $data['pet_data'] = get_records('pets',['WHERE'=>['user_id'=>$user_id]]);
        // echo "<pre>";print_r($data);exit;
        $this->load->view('register/profile',$data);
    }
    public function update_profile(){
        if ($this->input->method() == 'post') {   
            $data =  $this->input->post();
            // echo "<pre>";print_r($data);exit;
            $user_data['full_name']=$data['full_name'];
            $user_data['date_of_birth']=$data['date_of_birth'];
            $user_data['city']=$data['city'];
            $user_data['mobile_number']=$data['mobile_number'];
            $user_data['date_of_birth'] = date('Y-m-d', strtotime($user_data['date_of_birth']));
            
            $record = $this->production_model->update_record('register', $user_data, array('id' => $this->session->userdata('login_id')));
            if($this->input->post('pet_id')){
                $pet_ids = $this->input->post('pet_id');
                $count_records = count($pet_ids);
                $pet_names = $this->input->post('pet_name');
                $pet_types = $this->input->post('pet_type');
                $pet_genders = $this->input->post('pet_gender');
                $pet_dobs = $this->input->post('pet_dob');
                $pet_breeds = $this->input->post('pet_breed');
                $pet_weights = $this->input->post('pet_weight');
                for ($i=0; $i < $count_records ; $i++) { 
                    // $pet_data['pet_id'] = $pet_ids[$i];
                    $pet_data['pet_name'] = $pet_names[$i];
                    $pet_data['pet_type'] = $pet_types[$i];
                    $pet_data['pet_gender'] = $pet_genders[$i];
                    $pet_data['pet_dob'] = date('Y-m-d', strtotime($pet_dobs[$i]));
                    $pet_data['pet_breed'] = $pet_breeds[$i];
                    $pet_data['pet_weight'] = $pet_weights[$i];
                    $record = $this->production_model->update_record('pets', $pet_data, array('pet_id' => $pet_ids[$i]));
                    // echo "<pre>".$this->db->last_query();exit;
                    // echo "<pre>";print_r($pet_data);exit;
                }
                $this->session->set_flashdata('success', get_line_front('updated'));
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }
}
