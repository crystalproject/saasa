<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Card extends REST_Controller {

    public $_language_default = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper('product_helper');
        $this->load->helper('coupon_helper');
        $this->load->helper('cart_helper');
    }

    public function add_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('card_holder_name', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('card_number', '', 'trim|required|callback_check_unique', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('expiry_month', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('expiry_year', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $records = array(
                    'id_user' => $form_data['id_user'],
                    'card_holder_name' => $form_data['card_holder_name'],
                    'card_number' => $form_data['card_number'],
                    'expiry_month' => $form_data['expiry_month'],
                    'expiry_year' => $form_data['expiry_year'],
                    'created_at' => CURRENT_TIME,
                );
                $this->common_model->insert_data('user_cards', $records);
                $response_array['status'] = true;
                $response_array['message'] = get_line('card_saved_successfully');
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    public function get_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => array('bag_info' => null));
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
                $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

                $sql = 'SELECT card_holder_name,card_number,expiry_month,expiry_year FROM user_cards WHERE id_user = "' . $form_data['id_user'] . '" ORDER BY id DESC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'] . ' ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    $response_array['data'] = array('card_info' => $info['data']);
                } else {
                    $response_array['status'] = true;
                    $response_array['data'] = array('card_info' => null);
                    $response_array['message'] = get_line('card_not_available');
                }
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    public function delete_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('card_number', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $conditions = array('where' => array(
                        'card_number' => $form_data['card_number'],
                        'id_user' => $form_data['id_user'],
                ));
                $this->common_model->delete_data('user_cards', $conditions);
                $response_array['status'] = true;
                $response_array['message'] = get_line('card_deleted');
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    public function check_unique(){
        $conditions = array('where'=>array('id_user'=>$this->input->post('id_user'),'card_number'=>$this->input->post('card_number')));
        $info = $this->common_model->select_data('user_cards',$conditions);
        if($info['row_count'] > 0) {
            $this->form_validation->set_message('check_unique',get_line('card_available'));
            return false;
        } else {
            return true;
        }
    }
}
