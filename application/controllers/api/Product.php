<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Product extends REST_Controller {

    private $_default_limit = 2;
    public $_language_default = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper('product_helper');
    }

    public function get_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $this->_language_default = get_language_info($form_data['id_language']);
                $slug = '_' . $this->_language_default['slug'];
                if ($this->_language_default['id'] == '1') {
                    $slug = '';
                }
                $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
                $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

                $form_data['view_by'] = isset($form_data['view_by']) ? $form_data['view_by'] : 'all';

                $sub_query = '';
                if ($form_data['view_by'] == 'discounted') {
                    $form_data['is_popular'] = 0;
                    $form_data['id_category'] = 0;
                    $sub_query .= ' AND id_unique IN (SELECT DISTINCT(id_unique_product) AS id_unique_product FROM product_prices WHERE discount > 0) ';
                }
                if (isset($form_data['search_value']) && $form_data['search_value'] != null) {
                    $sub_query .= ' AND (title LIKE "%' . $form_data['search_value'] . '%" OR description LIKE "%' . $form_data['search_value'] . '%") ';
                }
                if (isset($form_data['is_popular']) && $form_data['is_popular'] == '1') {
                    $sub_query .= ' AND is_popular = "1" ';
                }
                if (isset($form_data['id_category']) && $form_data['id_category'] > 0) {
                    $sub_query .= ' AND id_category = "'.$form_data['id_category'].'" ';
                }
                $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
                $sql = 'SELECT id_unique,title,has_attributes,by_quantity_or_availability' . $sub_sub_query . ' FROM product WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' ORDER BY id_unique ASC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'] . ' ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    foreach ($info['data'] as &$value) {
                        if ($value['image_name'] != null) {
                            $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                        }
                        $value['other_info'] = get_product_detail($value, $this->_language_default['id']);
                        //unset($value['has_attributes']);
                        unset($value['by_quantity_or_availability']);
                        unset($value['category_name']);
                    }
                    $response_array['data'] = $info['data'];
                } else {
                    $response_array['status'] = true;
                    $response_array['data'] = null;
                    $response_array['message'] = get_line('product_not_available');
                }
                if(isset($value)) {
                    unset($value);
                }
                $get_banner = get_details('category_banner', array('is_active' => 1,'banner_for'=>3));
                if (isset($get_banner) && $get_banner != null) {
                    $banner_response = array();
                    foreach ($get_banner as $key => $value) {
                        $banner_response[$key]['id'] = $value['id'];
                        $banner_response[$key]['title'] = $value['title'];
                        $banner_response[$key]['title_turkish'] = $value['title_turkish'];
                        $banner_response[$key]['description'] = $value['description'];
                        $banner_response[$key]['description_turkish'] = $value['description_turkish'];
                        $banner_response[$key]['image'] = isset($value['image']) && $value['image'] != null ? base_url(CATEGORY_BANNER . $value['image']) : '';
                    }
                    $response_array['status'] = true;
                    $response_array['banner'] = $banner_response;
                } else {
                    $response_array['banner'] = null;
                }
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    public function detail_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_unique', '', 'trim|required', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $this->_language_default = get_language_info($form_data['id_language']);
                $slug = '_' . $this->_language_default['slug'];
                if ($this->_language_default['id'] == '1') {
                    $slug = '';
                }
                $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
                $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;
                $sub_query = '';
                if (isset($form_data['search_value']) && $form_data['search_value'] != null) {
                    $sub_query = ' AND (title LIKE "%' . $form_data['search_value'] . '%" OR description LIKE "%' . $form_data['search_value'] . '%") ';
                }
                $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
                $sql = 'SELECT id_unique,title,description,has_attributes,by_quantity_or_availability' . $sub_sub_query . ' FROM product WHERE id_unique = "' . $form_data['id_unique'] . '" AND id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . '  ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    foreach ($info['data'] as &$value) {
                        if ($value['image_name'] != null) {
                            $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                        }
                        $value['other_info'] = get_product_detail($value, $this->_language_default['id'], 'detail');
                    }
                    $response_array['data'] = $info['data'];
                } else {
                    $response_array['status'] = false;
                    $response_array['data'] = null;
                    $response_array['message'] = get_line('product_not_available');
                }
            }
        }
        $this->response($response_array);
    }

}
