<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Payment_mode extends REST_Controller {

    public $_language_default = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper('product_helper');
        $this->load->helper('coupon_helper');
        $this->load->helper('cart_helper');
    }

    public function get_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => array('bag_info' => null));
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));            
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $conditions = array('SELECT'=>'id_unique,payment_mode','WHERE'=>array('id_language'=>$form_data['id_language'],'is_active'=>'1'));
                $info = $this->common_model->select_data('payment_mode', $conditions);
                $data['info'] = array();
                if ($info['row_count'] > 0) {                    
                    $response_array['status'] = true;
                    $response_array['data'] = array('payment_mode_info' => $info['data']);
                } else {
                    $response_array['status'] = true;
                    $response_array['data'] = array('payment_mode_info' => null);                    
                }
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

}
