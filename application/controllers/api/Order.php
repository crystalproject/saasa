<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Order extends REST_Controller {

    private $_default_limit = 2;
    public $_language_default = array();
    public $_column_slug = '';

    public function __construct() {
        parent::__construct();
        $this->load->helper('product_helper');
        $this->load->helper('cart_helper');
        $this->load->helper('coupon_helper');
        $this->load->helper('order_helper');
    }

    /**
     * Add product to the cart
     */
    public function add_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $_POST = $form_data;
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|in_list[1,6]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('payment_mode', '', 'trim|required|check_exist[payment_mode.id_unique]|callback_check_card_detail', array('required' => $this->lang->line('something_wrong'),'check_exist'=>get_line('check_exist')));
            $this->form_validation->set_rules('id_address', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $this->_language_default = get_language_info($form_data['id_language']);
                $slug = '_' . $this->_language_default['slug'];
                if ($this->_language_default['id'] == '1') {
                    $slug = '';
                }
                $form_data['language_slug'] = $slug;
                $response_array = save_order($form_data);
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    public function get_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|in_list[1,6]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $this->_language_default = get_language_info($form_data['id_language']);
                $slug = '_' . $this->_language_default['slug'];
                if ($this->_language_default['id'] == '1') {
                    $slug = '';
                }
                $this->_column_slug = $slug;

                $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
                $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

                $form_data['view_by'] = isset($form_data['view_by']) ? $form_data['view_by'] : 'all';

                $sub_query = ' AND u.id = "' . $form_data['id_user'] . '" ';
                
                if($form_data['view_by'] == 'cancelled') {
                    $sub_query = ' AND po.order_status = "3" ';
                }
                $sql = 'SELECT po.order_date_time,po.id,po.order_id,po.payment_mode,po.order_status,po.order_status FROM product_order po,register u WHERE po.id_user = u.id ' . $sub_query . ' ORDER BY po.id DESC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'];
                $info = $this->common_model->get_data_with_sql($sql);
                $data['info'] = array();
                if ($info['row_count'] > 0) {
                    $data['info'] = array();
                    foreach ($info['data'] as $value) {
                        $data['info'][] = get_order_detail($value['id'], $this->_column_slug, $this->_language_default);;
                    }
                }
                $response_array['status'] = true;
                $response_array['data'] = $data;
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    public function check_card_detail(){
        $post = $this->input->post();
        $return = true;
        if($post['payment_mode'] == '1') {
            if(isset($post['card_detail']) && !empty($post['card_detail'])) {
                $card_holder_name = isset($post['card_detail']->card_holder_name) ? $post['card_detail']->card_holder_name : '';
                $card_number = isset($post['card_detail']->card_number) ? $post['card_detail']->card_number : '';
                $expiry_month = isset($post['card_detail']->expiry_month) ? $post['card_detail']->expiry_month : '';
                $expiry_year = isset($post['card_detail']->expiry_year) ? $post['card_detail']->expiry_year : '';

                if(empty($card_holder_name) || empty($card_number) || empty($expiry_month) || empty($expiry_year)) {
                    $return = false;
                    $this->form_validation->set_message('check_card_detail',get_line('check_card_detail'));
                } else {
                    if(isset($post['save_card']) && $post['save_card'] == '1') {
                        $conditions = array(
                            'SELECT' => 'id',
                            'WHERE' => array('id_user'=>$post['id_user'],'card_number'=>$card_number)
                        );
                        $info = $this->common_model->select_data('user_cards',$conditions); 
                        if($info['row_count'] <= 0) {
                            $records = array(
                                'card_holder_name' => $card_holder_name,
                                'card_number' => $card_number,
                                'expiry_month' => $expiry_month,
                                'expiry_year' => $expiry_year,
                                'id_user' => $post['id_user'],
                                'created_at' => CURRENT_TIME
                            );
                            $this->common_model->insert_data('user_cards',$records);
                        }
                    }
                }
            }
        }
        return $return;
    }

    public function detail_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|in_list[1,6]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('order_id', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $this->_language_default = get_language_info($form_data['id_language']);
                $slug = '_' . $this->_language_default['slug'];
                if ($this->_language_default['id'] == '1') {
                    $slug = '';
                }
                $this->_column_slug = $slug;

                $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
                $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

                $form_data['view_by'] = isset($form_data['view_by']) ? $form_data['view_by'] : 'all';

                $sub_query = ' AND u.id = "' . $form_data['id_user'] . '" ';
                
                if($form_data['view_by'] == 'cancelled') {
                    $sub_query .= ' AND po.order_status = "3" ';
                }
                $sub_query .= ' AND po.order_id = "'.$form_data['order_id'].'" ';
                $sql = 'SELECT po.order_date_time,po.id,po.order_id,po.payment_mode,po.order_status,po.order_status FROM product_order po,register u WHERE po.id_user = u.id ' . $sub_query . ' ORDER BY po.id DESC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'];
                $info = $this->common_model->get_data_with_sql($sql);
                $data['info'] = array();
                if ($info['row_count'] > 0) {
                    $data['info'] = array();
                    foreach ($info['data'] as $value) {
                        $data['info'][] = get_order_detail($value['id'], $this->_column_slug, $this->_language_default);;
                    }
                    $response_array['status'] = true;
                    $response_array['data'] = $data;
                } else {                    
                    $response_array['message'] = get_line('something_wrong');
                }
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    public function cancel_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|in_list[1,6]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('order_id', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $sql = "SELECT id FROM product_order WHERE order_id  = '".$form_data['order_id']."'";
                $info = $this->common_model->get_data_with_sql($sql);
                if($info['row_count'] > 0) {
                    $info = $info['data'][0];
                    revert_quantity($info['id']);
                }

                $sql = 'UPDATE product_order SET order_status = "3" WHERE id_user = "'.$form_data['id_user'].'" AND order_id = "'.$form_data['order_id'].'" AND order_status = "0" ';
                if($this->common_model->simple_query($sql)) {
                    if(isset($info['id'])) {
                        $records = array(
                            'id_product_order' => $info['id'],
                            'order_status' => 3,
                            'created_at' => CURRENT_TIME,
                        );
                        $this->common_model->insert_data('product_order_history', $records);
                    }
                    $response_array['status'] = true;
                    $response_array['message'] = get_line('order_cancelled_successfully');
                } else {
                    $response_array['message'] = get_line('something_wrong');
                }               
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }
}
