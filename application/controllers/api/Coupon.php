<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Coupon extends REST_Controller {

    public $_language_default = array();
    public $_upload_path = 'uploads/bag/';

    public function __construct() {
        parent::__construct();
        $this->load->helper('product_helper');
        $this->load->helper('coupon_helper');
        $this->load->helper('cart_helper');
    }

    public function apply_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|greater_than[0]', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('coupon_code', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $response_array = apply_coupon_code($form_data);
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

}
