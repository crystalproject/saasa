<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Bag extends REST_Controller {

    private $_default_limit = 100;
    public $_language_default = array();
    public $_upload_path = 'uploads/bag/';

    public function __construct() {
        parent::__construct();
        $this->load->helper('product_helper');
    }

    public function get_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => array('bag_info' => null));
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
                $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

                $language_info = get_language_info($form_data['id_language']);
                $slug = !empty($language_info) && $language_info['default_language'] != 1 ? '_' . $language_info['slug'] : '';

                $url = base_url() . $this->_upload_path;
                $sql = 'SELECT id,title' . $slug . ' AS title,price,CONCAT("' . $url . '",image) AS image FROM bag ORDER BY id DESC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'] . ' ';                
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    $response_array['data'] = array('bag_info' => $info['data']);
                } else {
                    $response_array['status'] = true;
                    $response_array['data'] = array('bag_info' => null);
                    $response_array['message'] = get_line('bag_not_available');
                }
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

}
