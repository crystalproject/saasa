<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Sub_category extends CI_Controller {
        public function __construct() {
            parent::__construct();
        }
        public function index($id = '') {
            $data['banner'] = array();
            if ($id == '') {
                redirect(base_url());
            }
            $condition = array('parent_id'=>$id,'is_active'=>'1');
        	$data['sub_category'] = get_details('category',$condition);

            if (isset($data['sub_category']) && $data['sub_category'] !=null) {
                $data['banner'] = get_details('category',array('id'=>$data['sub_category'][0]['parent_id']));
            }
            else{
                redirect(base_url('product/'.$id));
            }
            $this->load->view('sub_category/index',$data);
        }
    }
?>