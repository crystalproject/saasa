<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $total_segment = $this->uri->total_segments();
        $id = $this->uri->segment($total_segment);
        $data['banner'] = array();
        if ($id == '') {
            redirect(base_url());
        }
        $condition = array('parent_id' => $id, 'is_active' => '1');
        $data['sub_category'] = get_details('category', $condition);

        if (isset($data['sub_category']) && $data['sub_category'] != null) {
            $data['banner'] = get_details('category', array('id' => $data['sub_category'][0]['parent_id']));
            foreach ($data['sub_category'] as $key => $value) {
                $GLOBALS['current_category'] = array($value['slug' . get_language()]);
                get_url_from_category($value);
                $categories = array_reverse($GLOBALS['current_category']);
                if ($total_segment == 3) {
                    $data['sub_category'][$key]['base_url'] = base_url('product/') . implode('/', $categories) . '/' . $value['id'];
                } else {
                    $data['sub_category'][$key]['base_url'] = base_url('category/') . implode('/', $categories) . '/' . $value['id'];
                }                
            }
        } else {
            $current_url = current_url();
            $current_url = str_replace('/category/', '/product/', $current_url);
            redirect($current_url);
        }
        $this->load->view('category/view', $data);
    }
}
