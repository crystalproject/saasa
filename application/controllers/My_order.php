<?php

defined('BASEPATH') or exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class My_order extends CI_Controller
{

    public $_default_limit = 10;

    public function __construct()
    {
        parent::__construct();
        if (isset($this->session->userdata['login_id']) && $this->session->userdata['login_id'] != null) {
        } else {
            redirect(base_url());
        }
        $this->load->helper('cart_helper');
        $this->load->helper('product_helper');
        $this->load->helper('order_helper');
    }

    public function index()
    {
        $data = array();
        $data['success_message'] = $this->session->flashdata('order_success');
        $data['total_order'] = get_total_order($this->session->userdata['login_id']);

        $form_data = array();
        $form_data['id_user'] = $this->session->userdata['login_id'];
        $form_data['id_language'] = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
        $this->_language_default = get_language_info($form_data['id_language']);

        $this->_column_slug = get_language();

        $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
        $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

        $form_data['view_by'] = isset($form_data['view_by']) ? $form_data['view_by'] : 'all';

        $sub_query = ' AND u.id = "' . $form_data['id_user'] . '" ';

        if ($form_data['view_by'] == 'cancelled') {
            $sub_query = ' AND po.order_status = "3" ';
        }
        $sql = 'SELECT po.order_date_time,po.id,po.order_id,po.payment_mode,po.order_status,po.order_status FROM product_order po,register u WHERE po.id_user = u.id ' . $sub_query . ' ORDER BY po.id DESC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'];
        $info = $this->common_model->get_data_with_sql($sql);
        $data['info'] = array();
        if ($info['row_count'] > 0) {
            $data['info'] = array();
            foreach ($info['data'] as $value) {
                $data['info'][] = get_order_detail($value['id'], $this->_column_slug, $this->_language_default);;
            }
        }
        $data['status'] = true;
        $data['data'] = $data;
        $this->load->view('my-order/view', $data);
    }

    public function detail($order_id = '')
    {
        $data = array();
        $form_data = array();
        $form_data['id_user'] = $this->session->userdata['login_id'];
        $form_data['id_language'] = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
        $this->_language_default = get_language_info($form_data['id_language']);

        $this->_column_slug = get_language();

        $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
        $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

        $form_data['view_by'] = isset($form_data['view_by']) ? $form_data['view_by'] : 'all';

        $sub_query = ' AND u.id = "' . $form_data['id_user'] . '" ';

        if ($form_data['view_by'] == 'cancelled') {
            $sub_query .= ' AND po.order_status = "3" ';
        }
        $sub_query .= ' AND po.order_id = "' . $order_id . '"';
        $sql = 'SELECT po.billing_info,po.shipping_info,po.order_date_time,po.id,po.order_id,po.payment_mode,po.order_status,po.order_status FROM product_order po,register u WHERE po.id_user = u.id ' . $sub_query . ' ORDER BY po.id DESC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'];
        $info = $this->common_model->get_data_with_sql($sql);
        $data['info'] = array();
        if ($info['row_count'] > 0) {
            $data['info'] = array();
            foreach ($info['data'] as $value) {
                $data['info'][] = get_order_detail($value['id'], $this->_column_slug, $this->_language_default);;
            }
        } else {
            redirect(base_url('my-order'));
        }
        $data['status'] = true;
        $data['data'] = $data;
        $data['data']= $data['data']['info'][0];
        //print_r($data['data']);exit;
        $this->load->view('my-order/detail', $data);
    }
}
