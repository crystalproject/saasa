<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Coupon extends CI_Controller
{

    public $_language_default = array();
    public $_upload_path = 'uploads/bag/';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('product_helper');
        $this->load->helper('coupon_helper');
        $this->load->helper('cart_helper');
    }

    public function apply()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => '');
        $form_data = $this->input->post();
        $form_data['id_language'] = get_language_id();
        $this->form_validation->set_rules('coupon_code', '', 'trim|required', array('required' => $this->lang->line('coupon_code').' '.$this->lang->line('required')));
        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $value) {
                $response_array['message'] = $value;
                break;
            }
        } else {
            $form_data['id_user'] = get_front_login_id();
            $response_array = apply_coupon_code($form_data);
        }
        echo json_encode($response_array);
        exit;        
    }
}
