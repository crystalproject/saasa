<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Home extends CI_Controller {
        public function __construct() {
            parent::__construct();
        }
        public function index() {
        	$condition = array('is_active'=>'1');
            $data['slider_details'] = get_details('home_slider',$condition);
            $data['brands_details'] = get_details('brands',$condition);

            $condition = array('parent_id'=>0,'is_active'=>'1');
        	$data['category_details'] = get_details('category',$condition);
            $data['current_url'] = current_url();
            $data['current_url'] = str_replace('/product/', '/product/details/', $data['current_url']);
            $sql = 'SELECT id_unique,is_new,title,main_image,slug,has_attributes,by_quantity_or_availability FROM product WHERE id_language = "' . get_language_id() . '" and is_best_seller = 1 ORDER BY id_unique ASC';
            
            $best_seller = $this->common_model->get_data_with_sql($sql);
            if (isset($best_seller) && !empty($best_seller)) {
                if ($best_seller['row_count'] > 0) {
                    $response_array['status'] = true;
                    foreach ($best_seller['data'] as &$value) {
                        if ($value['image_name'] != null) {
                            $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                        }
                        if ($value['main_image'] != null) {
                            $value['main_image'] = base_url('uploads/product/') . $value['main_image'];
                        }
                        $value['other_best_seller'] = get_product_detail($value, get_language_id());
                        //unset($value['has_attributes']);
                        unset($value['by_quantity_or_availability']);
                        unset($value['category_name']);
                    }
                    
                    $data['best_seller'] = $best_seller['data'];
                    unset($value);
                }
            }
            // echo "<pre>".$this->db->last_query();print_r($data['best_seller']);exit;
            $this->load->view('index',$data);
        }

        function change_language() {    
            $response_array = array('status' => false, 'message' => '');
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => 'This field is required'));
            $data = array();
            if ($this->form_validation->run() === FALSE) {
                $data = array_merge($data, $_POST);
            } else {              
                if ($this->input->post('id_language') == 1) {
                    $this->session->language = 1;
                }
                else if ($this->input->post('id_language') == 2) {
                    $this->session->language = 2;
                }
                $response_array['status'] = true;
            }
            echo json_encode($response_array);
            exit;
        }
        
    }
?>