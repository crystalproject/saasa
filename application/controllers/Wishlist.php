<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wishlist extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('wishlist');
	}

	function index()
	{
		$data = array();
		//Getting products that are available in wishlist
		/*$info = $this->common_model->select_data('wishlist', array('SELECT' => 'id_product', 'where' => array('login_id' => get_front_login_id())));
		$id_products = null;
		if ($info['row_count'] > 0) {
			$id_products = array_unique(array_column($info['data'], 'id_product'));
		}*/

		$response_array = array('status' => false, 'message' => '', 'data' => null);
		$this->_default_limit = 12;
		$form_data = array();

		$form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
		$form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

		$slug = get_language_front();
		$sub_query = '';
		/*if (empty($id_products)) {
			$id_products = '"' . implode('","', $id_products) . '"';
			$sub_query .= ' AND id_unique IN (' . $id_products . ') ';
		}*/

		$sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
		$sql = 'SELECT id_unique,title,main_image,slug,has_attributes,by_quantity_or_availability' . $sub_sub_query . ',w.created_at FROM product, wishlist w WHERE product.id_unique = w.id_product AND w.login_id = "' . get_front_login_id() . '" AND id_language = "' . get_language_id() . '" ' . $sub_query . ' ORDER BY id_unique ASC LIMIT ' . $form_data['start'] . ',' . $form_data['limit'] . ' ';
		$info = $this->common_model->get_data_with_sql($sql);
		if ($info['row_count'] > 0) {
			$response_array['status'] = true;
			foreach ($info['data'] as &$value) {
				if ($value['image_name'] != null) {
					$value['image_name'] = base_url('uploads/product/') . $value['image_name'];
				}
				if ($value['main_image'] != null) {
					$value['main_image'] = base_url('uploads/product/') . $value['main_image'];
				}
				$value['other_info'] = get_product_detail($value, get_language_id());
				if ($value['by_quantity_or_availability'] == 0) {
					if ($value['other_info']['is_product_available'][0] == 1) {
						$value['stock'] = get_line('in_stock');
					} else {
						$value['stock'] = get_line('out_of_stock');
					}
				} else {
					if ($value['other_info']['quantity'][0] > 0) {
						$value['stock'] = $value['other_info']['quantity'][0] . ' ' . get_line('in_stock');
					} else {
						$value['stock'] = get_line('out_of_stock');
					}
				}
				//unset($value['has_attributes']);
				unset($value['by_quantity_or_availability']);
				unset($value['category_name']);
			}
			$response_array['data'] = $info['data'];
			unset($value);
		} else {
			// $response_array['status'] = true;
			// $response_array['data'] = null;
			// $response_array['message'] = get_line('product_not_available');
		}
		$data['details'] = null;
		$data['details'] = $response_array['data'];

		$this->load->view('wishlist/index', $data);
	}

	function add_wishlist()
	{
		$response_array['data'] = null;
		$id_product = $this->input->post('id_product');
		$data = array(
			'id_product' => $id_product,
			'login_id' => get_front_login_id()
		);
		if (get_front_login_id() != null) {
			$get_pro_id = $this->production_model->get_all_with_where('wishlist', '', '', array('id_product' => $id_product, 'login_id' => get_front_login_id()));
			if (count($get_pro_id) > 0) {
				$record = $this->production_model->delete_record('wishlist', array('login_id' => get_front_login_id(), 'id_product' => $id_product));
				$response_array['delete'] = true;
				$response_array['data'] = array('total_product' => get_total_wishlist());
			} else {
				$add_record = $this->production_model->insert_record('wishlist', $data);
				if ($add_record != '') {
					$response_array['success'] = true;
					$response_array['data'] = array('total_product' => get_total_wishlist());
				}
			}
		} else {
			$response_array['error'] = false;
		}
		echo json_encode($response_array);
		exit;
	}

	function remove_wishlist($id)
	{
		$this->production_model->delete_record('wishlist', array('id' => $id));
		$this->session->set_flashdata('success', get_line_front('wishlist_removed_successfully'));
		redirect($_SERVER['HTTP_REFERER']);
	}
}
