<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This class is use for all the common ajax request.
 */
class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function change_status() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $error = false;
        $response_array = array('success' => false);
        $required_fields = array(
            'id' => $this->lang->line('something_wrong'),
            'table' => $this->lang->line('something_wrong'),
            'current_status' => $this->lang->line('something_wrong'),
        );
        foreach ($required_fields as $key => $value) {
            if (strlen(trim($this->input->post($key))) <= 0) {
                $response_array['msg'] = $value;
                $error = true;
                break;
            }
        }
        if (!$error) {
            if ($this->input->post('current_status') == '0') {
                $status = '1';
            }
            if ($this->input->post('current_status') == '1') {
                $status = '0';
            }
            $records = array(
                'is_active' => $status,
            );
            $conditions = array(
                "where" => array("id" => $this->input->post('id')),
            );
            $this->common_model->update_data($this->input->post('table'), $records, $conditions);
            $response_array['success'] = true;
        }
        echo json_encode($response_array);
        exit;
    }

    public function change_category_status() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $error = false;
        $response_array = array('success' => false);
        $required_fields = array(
            'id' => $this->lang->line('something_wrong'),
            'table' => $this->lang->line('something_wrong'),
            'current_status' => $this->lang->line('something_wrong'),
        );
        foreach ($required_fields as $key => $value) {
            if (strlen(trim($this->input->post($key))) <= 0) {
                $response_array['msg'] = $value;
                $error = true;
                break;
            }
        }
        if (!$error) {
            if ($this->input->post('current_status') == '0') {
                $is_active = '1';
            }
            if ($this->input->post('current_status') == '1') {
                $is_active = '0';
            }
            $records = array(
                'is_active' => $is_active,
            );
            $conditions = array(
                "where" => array("id" => $this->input->post('id')),
            );
            $this->common_model->update_data($this->input->post('table'), $records, $conditions);
            $this->change_child_category_status($is_active, $this->input->post('id'));
            $response_array['success'] = true;
        }
        echo json_encode($response_array);
        exit;
    }

    function change_child_category_status($is_active = '', $id = '') {
        $records = array('is_active' => $is_active);
        $tmp = $this->production_model->update_record('category', $records, array("parent_id" => $id));

        $get_sub_cat = $this->production_model->get_all_with_where_in('category', '', '', 'parent_id', $id, array());
        if (isset($get_sub_cat) && $get_sub_cat != null) {
            foreach ($get_sub_cat as $key => $value) {
                $this->change_child_category_status($is_active, $value['id']);
            }
        }
    }

    public function change_sub_category_status() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $error = false;
        $response_array = array('success' => false);
        $required_fields = array(
            'id' => $this->lang->line('something_wrong'),
            'table' => $this->lang->line('something_wrong'),
            'current_status' => $this->lang->line('something_wrong'),
        );
        foreach ($required_fields as $key => $value) {
            if (strlen(trim($this->input->post($key))) <= 0) {
                $response_array['msg'] = $value;
                $error = true;
                break;
            }
        }
        if (!$error) {
            if ($this->input->post('current_status') == '0') {
                $is_active = '1';
            }
            if ($this->input->post('current_status') == '1') {
                $is_active = '0';
            }
            $records = array(
                'is_active' => $is_active,
            );
            $conditions = array(
                "where" => array("id" => $this->input->post('id')),
            );
            /* Check parent category is active or not */
            if ($this->input->post('parent_id') != null) {
                $get_details = $this->production_model->get_all_with_where('category', '', '', array("id" => $this->input->post('parent_id')));
                if (isset($get_details) && $get_details != null) {
                    if ($get_details[0]['is_active'] == '0') {
                        $response_array['error'] = true;
                    } else {
                        $this->common_model->update_data($this->input->post('table'), $records, $conditions);
                        $response_array['success'] = true;
                    }
                }
            }
            $response_array['success'] = true;
        }
        echo json_encode($response_array);
        exit;
    }

    public function delete_category() {
        $data = $this->input->post();
        if (isset($data['id']) && $data['id'] != null) {
            foreach ($data['id'] as $key => $value) {
                $get_image = $this->production_model->get_all_with_where($data['table'], '', '', array('id' => $value));
                if ($get_image != null && !empty($get_image[0]['image'])) {
                    @unlink($data['image_path'] . $get_image[0]['image']);
                    @unlink($data['image_path'] . 'thumbnail/' . $get_image[0]['image']);
                }
                if ($data['table'] == 'category') {
                    @unlink($data['image_path'] . $get_image[0]['image_arabic']);
                    @unlink(CAT_SUB_CAT_BANNER . $get_image[0]['banner']);
                    @unlink(CAT_SUB_CAT_BANNER . 'thumbnail/' . $get_image[0]['banner']);
                    
                    @unlink(CAT_SUB_CAT_BANNER . $get_image[0]['banner_arabic']);
                    @unlink(CAT_SUB_CAT_BANNER . 'thumbnail/' . $get_image[0]['banner_arabic']);
                }
            }
            $record = $this->production_model->get_delete_where_in($data['table'], 'id', $data['id']);
            $this->delete_child_category($data['id']);
        }
        $response_array['success'] = true;
        $response_array['message'] = $this->lang->line('deleted_successfully');

        echo json_encode($response_array);
        exit;
    }

    function delete_child_category($id = '') {
        $tmp = $this->production_model->get_delete_where_in('category', 'id', $id);

        $get_sub_cat = $this->production_model->get_all_with_where_in('category', '', '', 'parent_id', $id, array());
        if (isset($get_sub_cat) && $get_sub_cat != null) {
            foreach ($get_sub_cat as $key => $value) {
                $this->delete_child_category($value['id']);
            }
        }
    }

    public function delete() {
        $data = $this->input->post();
        $response_array = array('success' => false, 'message' => '');
        if (isset($data['id']) && $data['id'] != null) {
            if ($data['table'] == 'bag') {
                $response_array = delete_bag($data['id']);
            } else {

                if($data['table'] == 'pets'){

                    $record = $this->production_model->get_delete_where_in($data['table'], 'pet_id', $data['id']);

                }else{

                    foreach ($data['id'] as $key => $value) {
                        $get_image = $this->production_model->get_all_with_where($data['table'], '', '', array('id' => $value));
                        if ($get_image != null && !empty($get_image[0]['image'])) {
                            @unlink($data['image_path'] . $get_image[0]['image']);
                            @unlink($data['image_path'] . 'thumbnail/' . $get_image[0]['image']);
                        }
                    } 

                    $record = $this->production_model->get_delete_where_in($data['table'], 'id', $data['id']);     

                }              
                

                $response_array['success'] = true;
                $response_array['message'] = $this->lang->line('deleted_successfully');
            }
        }
        echo json_encode($response_array);
        exit;
    }

}

?>