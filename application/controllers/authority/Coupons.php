<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupons extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->coupons_info = array();
            redirect(base_url('authority/coupons'));
        }
        $data = array();
        $tmp_data = get_details('coupons');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/coupons/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('coupons', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];
        $this->load->view('authority/coupons/view', $data);
    }

    public function add_edit($id = '') {
        $message = '';
        $data = $this->input->post();
        $details = array();
        if (isset($id) && $id != null) {
            $condition = array('id' => $id);
            $details = get_details('coupons', $condition);
        }
        if ($this->input->method() == 'post') {
            if ($id == '') { // Add
                if ($this->input->method() == 'post') {
                    $this->form_validation->set_rules('coupon_code', 'coupon_code', 'required|is_unique[coupons.coupon_code]|max_length[50]', array('required' => $this->lang->line('please_enter_value'), "is_unique" => $this->lang->line('this_record_is_available')));
                    $this->form_validation->set_rules('discount_type', 'discount_type', 'required', array('required' => $this->lang->line('please_select_value')));
                    if ($data['discount_type'] == 'percentage') {
                        $this->form_validation->set_rules('discount', 'discount', 'required|numeric|max_length[5]|less_than_equal_to[100]', array('required' => $this->lang->line('please_enter_value')));
                    } elseif ($data['discount_type'] == 'amount') {
                        $this->form_validation->set_rules('discount', 'discount', 'required|numeric|max_length[5]', array('required' => $this->lang->line('please_enter_value')));
                    }
                    $this->form_validation->set_rules('minimum_amount_cart_status', 'minimum amount cart status', 'required', array('required' => $this->lang->line('please_select_value')));
                    if ($data['minimum_amount_cart_status'] == 'true') {
                        $this->form_validation->set_rules('minimum_amount_cart', 'minimum amount cart', 'required|numeric|max_length[5]|greater_than['.$data['discount'].']', array('required' => $this->lang->line('please_enter_value')));
                    }
                    $this->form_validation->set_rules('maximum_coupon_used', 'maximum_coupon_used', 'required|numeric|max_length[5]', array('required' => $this->lang->line('please_enter_value')));
                    if ($this->form_validation->run() == FALSE) {
                        $data = array_merge($data, $this->form_validation->error_array());
                    } else {
                        $data['discount'] = format_number($data['discount']);
                        $data['minimum_amount_cart'] = format_number($data['minimum_amount_cart']);
                        $data['maximum_coupon_used'] = format_number($data['maximum_coupon_used'], 'int');
                        $record = $this->production_model->insert_record('coupons', $data);
                        $message = 'add';
                    }
                }
            } else { // Edit                
                $this->form_validation->set_rules('coupon_code', 'coupon_code', 'required|is_unique_with_except_record[coupons.coupon_code.id.' . $data['id'] . ']', array('required' => $this->lang->line('please_enter_value'), "is_unique" => $this->lang->line('this_record_is_available')));
                $this->form_validation->set_rules('discount_type', 'discount_type', 'required', array('required' => $this->lang->line('please_select_value')));
                if ($data['discount_type'] == 'percentage') {
                    $this->form_validation->set_rules('discount', 'discount', 'required|numeric|max_length[5]|less_than_equal_to[100]', array('required' => $this->lang->line('please_enter_value')));
                } elseif ($data['discount_type'] == 'amount') {
                    $this->form_validation->set_rules('discount', 'discount', 'required|numeric|max_length[5]', array('required' => $this->lang->line('please_enter_value')));
                }
                $this->form_validation->set_rules('minimum_amount_cart_status', 'minimum_amount_cart_status', 'required', array('required' => $this->lang->line('please_select_value')));
                if ($data['minimum_amount_cart_status'] == 'true') {
                    $this->form_validation->set_rules('minimum_amount_cart', 'minimum amount cart', 'required|numeric|max_length[5]|greater_than['.$data['discount'].']', array('required' => $this->lang->line('please_enter_value')));
                }
                $this->form_validation->set_rules('maximum_coupon_used', 'maximum_coupon_used', 'required|numeric|max_length[5]', array('required' => $this->lang->line('please_enter_value')));
                if ($this->form_validation->run() == FALSE) {
                    $data = array_merge($details[0], $this->form_validation->error_array());
                } else {
                    $data['discount'] = format_number($data['discount']);
                    $data['minimum_amount_cart'] = format_number($data['minimum_amount_cart']);
                    $data['maximum_coupon_used'] = format_number($data['maximum_coupon_used'], 'int');                    
                    $record = $this->production_model->update_record('coupons', $data, array('id' => $id));
                    $message = 'edit';
                }
            }
            if (isset($message) && $message != '') {
                if ($message == 'add') {
                    $this->session->set_flashdata('success', $this->lang->line('added_successfully'));
                } elseif ($message == 'edit') {
                    $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
                }
                redirect(base_url('authority/coupons'));
            }
        }
        $data = isset($details) && $details != null ? $details[0] : $this->input->post();
        $this->load->view('authority/coupons/add-edit', $data);
    }

    public function filter() {
        $this->session->coupons_info = $_POST;
        $name = isset($this->session->coupons_info['name']) ? $this->session->coupons_info['name'] : '';
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('coupon_code', $name);
            $this->db->or_like('discount_type', $name);
            $this->db->or_like('discount', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post();
        $tmp_data = get_details('coupons');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/coupons/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('coupon_code', $name);
            $this->db->or_like('discount_type', $name);
            $this->db->or_like('discount', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('coupons', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];
        ob_start();
        if (isset($filteredData) && !empty($filteredData)) {
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                <tr>
                    <td style="width: 10px;">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id; ?>" value="<?= $id ?>">
                            <label for="customCheckbox<?= $id; ?>" class="custom-control-label"></label>
                        </div>
                    </td>
                    <td><?= $value['coupon_code']; ?></td>
                    <td><?= $value['discount_type']; ?></td>
                    <td>
                        <?php
                        if ($value['discount_type'] == 'percentage') {
                            echo $value['discount'] . '%';
                        }
                        if ($value['discount_type'] == 'amount') {
                            echo '' . $value['discount'];
                        }
                        ?>   
                    </td>
                    <td>
                        <a href="<?= base_url('authority/coupons/add-edit/' . $id); ?>" class="btn bg-gradient-primary btn-xs"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0)" data-table="coupons" data-image-path="" class="btn bg-gradient-danger btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a>
                        <?php
                        if ($value['is_active'] == '1') {
                            echo '<span class="btn bg-gradient-success btn-xs change-status-tmp" data-table="coupons" data-id="' . $id . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                        } else {
                            echo '<span class="btn bg-gradient-danger btn-xs change-status-tmp" data-table="coupons" data-id="' . $id . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td style="width: 10px;">
                    <button type="button" class="btn btn-sm btn-danger delete_all" data-table="coupons" data-image-path="">Delete</button>
                </td>
                <td colspan="3">
                    <?php
                    if (isset($pagination) && $pagination != null) {
                        echo $pagination;
                    }
                    ?>
                </td>
            </tr>
            <?php
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();
            $response_array['pagination'] = $data['pagination'];
        } else {
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">' . $this->lang->line('records_not_found') . '</td>
                                            </tr>';
            $response_array['pagination'] = '';
        }
        echo json_encode($response_array);
        exit;
    }

}
