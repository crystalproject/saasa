<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->users_info = array();
            redirect(base_url('authority/users'));
        }
        $data = array();
        $tmp_data = get_details('register');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/users/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('register', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];
        $this->load->view('authority/users/view', $data);
    }

    public function filter() {
        $this->session->users_info = $_POST;
        $name = isset($this->session->users_info['name']) ? $this->session->users_info['name'] : '';
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('full_name', $name);            
            $this->db->or_like('email', $name);
            $this->db->or_like('mobile_number', $name);
            $this->db->or_like('city', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post();
        $tmp_data = get_details('register');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/users/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('full_name', $name);            
            $this->db->or_like('email', $name);
            $this->db->or_like('mobile_number', $name);
            $this->db->or_like('city', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('register', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];
        ob_start();
        if (isset($filteredData) && !empty($filteredData)) {
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                <tr>
                    <td style="width: 10px;">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id; ?>" value="<?= $id ?>">
                            <label for="customCheckbox<?= $id; ?>" class="custom-control-label"></label>
                        </div>
                    </td>
                    <td><?= $value['full_name']; ?></td>                                                    
                    <td><?= $value['email']; ?></td>
                    <td><?= $value['city']; ?></td> 
                    <td><?= $value['mobile_number']; ?></td>
                    <td>
                        <a href="javascript:void(0)" data-table="register" data-image-path="" class="btn bg-gradient-danger btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a>
                        <a href="javascript:void(0)" class="btn bg-primary btn-xs btn_view_pets" data-id="<?= $id; ?>"><i class="fa fa-paw"></i></a>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td style="width: 10px;">
                    <button type="button" class="btn btn-sm btn-danger delete_all" data-table="register" data-image-path="">Delete</button>
                </td>
                <td colspan="8">
                    <?php
                    if (isset($pagination) && $pagination != null) {
                        echo $pagination;
                    }
                    ?>
                </td>
            </tr>
            <?php
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();
            $response_array['pagination'] = $data['pagination'];
        } else {
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="8" align="center">' . $this->lang->line('records_not_found') . '</td>
                                            </tr>';
            $response_array['pagination'] = '';
        }
        echo json_encode($response_array);
        exit;
    }

    public function pet_details()
    {
        $response_array = array('status' => false, 'message' => '');
        $user_id = $this->input->post('id');
        $tmp_data = get_records('pets',['WHERE'=>['user_id'=>$user_id]]);
        if(count($tmp_data) > 0)
        {
            $tmp_data['data'] = $tmp_data;
            $response_array['status'] = true;
            $response_array['data'] = $this->load->view('authority/users/pet_details', $tmp_data, true);
        }else{
            $response_array['message'] = 'No Record Available';
        }
        echo json_encode($response_array);
        exit;
    }

}
