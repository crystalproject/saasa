<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Gift_wraping extends CI_Controller
{

    public $_table = 'gift_wraping';
    public $_slug = 'gift-wraping';
    public $_language_info = array();
    public $_language_default = array();
    public $_valid_extensions = array('jpg', 'png');
    public $_upload_path = 'uploads/gift-wraping/';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_check_model');

        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                break;
            }
        }
        $this->load->helper('upload_helper');
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = $this->lang->line('attribute');
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/view', $data);
    }

    public function get_records($page_number = '')
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $data = array();
            $sub_query = '';
            if ($this->input->post('search_value') != null) {
                $sub_query = ' AND title LIKE "%' . trim($this->input->post('search_value')) . '%" ';
            }
            $sql = 'SELECT count(id) AS total_records FROM ' . $this->_table . ' WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' ';
            $info = $this->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                $total_records = $info['data'][0]['total_records'];
            } else {
                $total_records = 0;
            }
            $settings = array(
                'total_record' => $total_records,
                'url' => base_url(AUTHORITY . '/' . $this->_slug . '/get_records'),
                'per_page' => RECORDS_PER_PAGE,
                'page_number' => $page_number
            );
            $pagination = $this->common_model->only_pagination($settings);
            $data['pagination'] = $pagination['pagination'];
            $sql = 'SELECT * FROM ' . $this->_table . ' WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' LIMIT ' . $pagination['start'] . ',' . $pagination['limit'] . ' ';
            $info = $this->common_model->get_data_with_sql($sql);
            $data['info'] = array();
            if ($info['row_count'] > 0) {
                $data['info'] = $info['data'];
            }
            $response_array['status'] = true;
            $response_array['data'] = $this->load->view(AUTHORITY . '/' . $this->_slug . '/listing', $data, true);
        }
        echo json_encode($response_array);
        exit;
    }

    public function add_edit($id_unique = '')
    {
        $data = array();
        $data['page_title'] = $this->lang->line('add') . ' ' . get_line('gift_wraping');
        $data['old_info'] = array();
        if ($id_unique != '') {
            $data['old_info'] = $this->get_detail($id_unique);
            if (!empty($data['old_info'])) {
                $temp = array();
                foreach ($data['old_info'] as $key => $value) {
                    $temp[$value['id_language']] = $value;
                }
                $data['old_info'] = $temp;
            } else {
                redirect(base_url(AUTHORITY . '/' . $this->_slug));
            }
            $data['page_title'] = $this->lang->line('edit') . ' ' . get_line('gift_wraping');
        }
        $data['input_fields'] = array();
        if ($this->input->is_ajax_request()) {
            $response_array = array('status' => false, 'message' => '');
            foreach ($this->_language_info as $value) {
                $this->form_validation->set_rules('title_' . $value['id'], get_line('title'), 'trim|required');
                $this->form_validation->set_rules('description_' . $value['id'], get_line('description'), 'trim|required');
                if ($id_unique == '') {
                    $image_field = 'image_' . $value['id'];
                    if (isset($_FILES[$image_field]) && !empty($_FILES[$image_field]['name'])) {
                        if (!is_valid_extension(array('jpg', 'png'), $image_field)) {
                            $this->form_validation->set_rules('image', '', 'required', array('required' =>  get_line('file_size_extension') . ' ' . implode(', ', $this->_valid_extensions)));
                            break;
                        } else if (!validate_file_size(MAX_IMAGE_UPLOAD_SIZE, $image_field)) {
                            $this->form_validation->set_rules('image', '', 'required', array('required' =>  get_line('file_size_less_than') . ' ' . (MAX_IMAGE_UPLOAD_SIZE / 1000000) . 'MB'));
                            break;
                        }
                    } else {
                        $this->form_validation->set_rules('image', '', 'required', array('required' =>  get_line('select_image')));
                        break;
                    }
                }
            }
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $error_value) {
                    $response_array['message'] = $error_value;
                    break;
                }
            } else {
                $records = array();
                if ($id_unique == '') {
                    $id_unique = uniqid();
                    foreach ($this->_language_info as $value) {
                        $file_info = upload_file('image_' . $value['id'], $this->_upload_path);
                        $records[] = array(
                            'id_unique' => $id_unique,
                            'id_language' => $value['id'],
                            'title' => $this->input->post('title_' . $value['id']),
                            'description' => $this->input->post('description_' . $value['id']),
                            'image_name' => $file_info['status'] == true ? $file_info['file_name'] : '',
                            'created_at' => CURRENT_TIME,
                            'updated_at' => CURRENT_TIME,
                        );
                    }
                    $last_insert_id  = $this->common_model->insert_data($this->_table, $records, true);
                    $this->common_model->update_data($this->_table, array('id_unique' => $last_insert_id), array('where' => array('id_unique' => $id_unique)));
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('added_successfully');
                } else {
                    foreach ($this->_language_info as $value) {
                        $file_info = upload_file('image_' . $value['id'], $this->_upload_path);
                        $records = array(
                            'title' => $this->input->post('title_' . $value['id']),
                            'description' => $this->input->post('description_' . $value['id']),
                            'updated_at' => CURRENT_TIME,
                        );
                        if ($file_info['status'] == true) {
                            delete_file_from_table($this->_table, $this->_upload_path, 'image_name', array('where' => array('id_unique' => $id_unique, 'id_language' => $value['id'])));
                            $records['image_name'] = $file_info['file_name'];
                        }
                        $this->common_model->update_data($this->_table, $records, array('where' => array('id_unique' => $id_unique, 'id_language' => $value['id'])));
                    }
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('updated_successfully');
                }
            }
            echo json_encode($response_array);
            exit;
        }
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/add-edit', $data);
    }

    public function get_detail($id_unique)
    {
        $info = $this->common_model->select_data($this->_table, array('WHERE' => array('id_unique' => $id_unique)));
        if ($info['row_count'] > 0) {
            return $info['data'];
        } else {
            redirect(base_url(AUTHORITY . '/' . $this->_slug . '/view'));
        }
    }

    public function delete()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id')) && is_array($this->input->post('id'))) {
                $info['row_count'] = 0;
                if ($info['row_count'] <= 0) {
                    foreach ($this->input->post('id') as $id) {
                        $info = $this->common_model->select_data('gift_wraping', array('SELECT' => 'image_name', 'where' => array('id_unique' => $id)));
                        if ($info['row_count'] > 0) {
                            foreach ($info['data'] as $value) {
                                if (file_exists($this->_upload_path . $value['image_name'])) {
                                    @unlink($this->_upload_path . $value['image_name']);
                                }
                            }
                            $this->common_model->delete_data('gift_wraping', array('where' => array('id_unique' => $id)));
                        }
                    }
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('delete_success');
                } else {
                    $response_array['status'] = false;
                    $response_array['message'] = $this->lang->line('associated_with_other_records_delete');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }
}
