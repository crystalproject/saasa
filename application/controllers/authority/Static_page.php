<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Static_page extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->page_info = array();
            redirect(base_url('authority/static-page'));
        }
        $data = array();
        $tmp_data = get_details('static_page');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/static-page/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;

        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('static_page', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        $this->load->view('authority/static_page/view', $data);
    }

    function add_edit($id = '') {
        $message = '';
        $data = $this->input->post();
        $details = array();
        if (isset($id) && $id != null) {
            $condition = array('id' => $id);
            $details = get_details('static_page', $condition);
        }
        if ($this->input->method() == 'post') {
            if ($id == '') { // Add
                if ($this->input->method() == 'post') {
                    $this->form_validation->set_rules('title', 'title', 'required', array('required' => $this->lang->line('this_is_required_field')));
                    $this->form_validation->set_rules('description', 'description', 'required|trim', array('required' => $this->lang->line('this_is_required_field')));
                    $this->form_validation->set_rules('image', 'image', 'callback_file_selected');
                    $this->form_validation->set_rules('image_hindi', 'image', 'callback_file_selected_tmp');

                    $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('id !=' => 1, 'is_active' => 1));
                    if (isset($language) && $language != null) {
                        foreach ($language as $key => $value) {
                            $newname = strtolower('title_' . $value['slug']);
                            $newdescription = strtolower('description_' . $value['slug']);

                            $this->form_validation->set_rules($newname, $newname, 'required', array('required' => $this->lang->line('this_field_is_required') . ' ' . (strtolower($value['name'])) . ''));
                            $this->form_validation->set_rules($newdescription, $newdescription, 'required|trim', array('required' => $this->lang->line('this_field_is_required') . ' ' . (strtolower($value['name'])) . ''));
                        }
                    }
                    if ($this->form_validation->run() == FALSE) {
                        $data = array_merge($data, $this->form_validation->error_array());
                    } else {
                        $data['image'] = $this->production_model->image_upload(STATIC_PAGE, 'image');
                        $data['image_hindi'] = $this->production_model->image_upload(STATIC_PAGE, 'image_hindi');
                        $record = $this->production_model->insert_record('static_page', $data);
                        $message = 'add';
                    }
                }
            } else { // Edit       
                $this->form_validation->set_rules('title', 'title', 'required', array('required' => $this->lang->line('this_is_required_field')));
                $this->form_validation->set_rules('description', 'description', 'required|trim', array('required' => $this->lang->line('this_is_required_field')));

                $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('id !=' => 1, 'is_active' => 1));
                if (isset($language) && $language != null) {
                    foreach ($language as $key => $value) {
                        $newname = strtolower('title_' . $value['slug']);
                        $newdescription = strtolower('description_' . $value['slug']);

                        $this->form_validation->set_rules($newname, $newname, 'required', array('required' => $this->lang->line('this_field_is_required') . ' ' . (strtolower($value['name'])) . ''));
                        $this->form_validation->set_rules($newdescription, $newdescription, 'required|trim', array('required' => $this->lang->line('this_field_is_required') . ' ' . (strtolower($value['name'])) . ''));
                    }
                }
                if ($this->form_validation->run() == FALSE) {
                    $data = array_merge($details[0], $this->form_validation->error_array());
                } else {
                    if ($_FILES['image']['name'] != '') {
                        $data['image'] = $this->production_model->image_upload(STATIC_PAGE, 'image', 'static_page', $id);
                    }
                    if ($_FILES['image_hindi']['name'] != '') {
                        $data['image_hindi'] = $this->production_model->image_upload(STATIC_PAGE, 'image_hindi', 'static_page', $id);
                    }
                    $record = $this->production_model->update_record('static_page', $data, array('id' => $id));
                    $message = 'edit';
                }
            }
            if (isset($message) && $message != '') {
                if ($message == 'add') {
                    $this->session->set_flashdata('success', $this->lang->line('added_successfully'));
                } elseif ($message == 'edit') {
                    $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
                }
                redirect(base_url('authority/static-page'));
            }
        }
        $data = isset($details) && $details != null ? $details[0] : $this->input->post();
        $this->load->view('authority/static_page/add-edit', $data);
    }

    function file_selected_test() {
        $this->form_validation->set_message('file_selected_test', $this->lang->line('this_is_required_field'));
        if (empty($_FILES['image']['name'])) {
            return false;
        } else {
            return true;
        }
    }
    function file_selected_tmp() {
        $this->form_validation->set_message('file_selected_tmp', $this->lang->line('this_is_required_field').' '.strtolower($this->lang->line('hindi')));
        if (empty($_FILES['image_hindi']['name'])) {
            return false;
        } else {
            return true;
        }
    }
    function filter() {
        $this->session->page_info = $_POST;
        $name = isset($this->session->page_info['name']) ? $this->session->page_info['name'] : '';
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('title', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post();
        $tmp_data = get_details('static_page');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/static-page/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('title', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('static_page', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        ob_start();
        if (isset($filteredData) && !empty($filteredData)) {
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                <tr>
                    <td><?= $value['title'. get_language()]; ?></td>
                    <!-- <td>
                        <img src="<?= base_url(STATIC_PAGE . 'thumbnail/') . $value['image'. get_language()] ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                    </td> -->
                    <td>
                        <a href="<?= base_url('authority/static-page/edit/' . $id); ?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>

                        <!-- <a href="javascript:void(0)" data-image-path="<?= STATIC_PAGE; ?>" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a> -->

                        <?php
                        if ($value['is_active'] == '1') {
                            echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status" data-table="static_page" data-id="' . $id . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                        } else {
                            echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status" data-table="static_page" data-id="' . $id . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();
            $response_array['pagination'] = $data['pagination'];
        } else {
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">' . $this->lang->line('records_not_found') . '</td>
                                            </tr>';
            $response_array['pagination'] = '';
        }
        echo json_encode($response_array);
        exit;
    }
}