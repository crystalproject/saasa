<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Attributes extends CI_Controller
{

    public $_table = 'attributes';
    public $_slug = 'attributes';
    public $_language_info = array();
    public $_language_default = array();
    public $_valid_extensions = array('jpg','png');
    public $_upload_path = 'uploads/attribute/';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_check_model');

        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                break;
            }
        }
        $this->load->helper('upload_helper');
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = $this->lang->line('attribute');
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/view', $data);
    }

    public function get_records($page_number = '')
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $data = array();
            $sub_query = '';
            if ($this->input->post('search_value') != null) {
                $sub_query = ' AND attribute_name LIKE "%' . trim($this->input->post('search_value')) . '%" ';
            }
            $sql = 'SELECT count(id) AS total_records FROM attributes WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' ';
            $info = $this->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                $total_records = $info['data'][0]['total_records'];
            } else {
                $total_records = 0;
            }
            $settings = array(
                'total_record' => $total_records,
                'url' => base_url(AUTHORITY . '/' . $this->_slug . '/get_records'),
                'per_page' => RECORDS_PER_PAGE,
                'page_number' => $page_number
            );
            $pagination = $this->common_model->only_pagination($settings);
            $data['pagination'] = $pagination['pagination'];
            $sub_sub_query = ' ,(SELECT COUNT(id) FROM attribute_values WHERE id_unique_attributes = attributes.id_unique AND id_language = "' . $this->_language_default['id'] . '") AS total_values ';
            $sql = 'SELECT *' . $sub_sub_query . ' FROM attributes WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' LIMIT ' . $pagination['start'] . ',' . $pagination['limit'] . ' ';
            $info = $this->common_model->get_data_with_sql($sql);
            $data['info'] = array();
            if ($info['row_count'] > 0) {
                $data['info'] = $info['data'];
            }
            $response_array['status'] = true;
            $response_array['data'] = $this->load->view(AUTHORITY . '/' . $this->_slug . '/listing', $data, true);
        }
        echo json_encode($response_array);
        exit;
    }

    public function add_edit($id_unique = '')
    {
        $data = array();
        $data['page_title'] = $this->lang->line('add_attribute');
        $data['old_info'] = array();
        if ($id_unique != '') {
            $data['old_info'] = $this->get_detail($id_unique);
            $formatted = array();
            foreach ($data['old_info'] as $o_value) {
                $formatted[$o_value['id_language']] = $o_value['attribute_name'];
            }
            $data['old_info'] = $formatted;
            $data['page_title'] = $this->lang->line('edit_attribute');
        }


        $data['input_fields'] = array();
        foreach ($this->_language_info as $value) {
            $data['input_fields'][] = array('name' => 'attribute_name_' . $value['id'], 'title' => 'Attribute Name (' . $value['name'] . ')', 'id_language' => $value['id'], 'value' => (isset($data['old_info'][$value['id']]) ? $data['old_info'][$value['id']] : ''));
        }
        if ($this->input->is_ajax_request()) {
            $response_array = array('status' => false, 'message' => '');
            foreach ($data['input_fields'] as $if_value) {
                $this->form_validation->set_rules($if_value['name'], '', 'trim|required', array('required' => 'Please enter ' . $if_value['title']));
            }
            $this->form_validation->set_rules('temp', '', 'callback_check_unique', array('check_unique' => $this->lang->line('this_record_is_available')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $error_value) {
                    $response_array['message'] = $error_value;
                    break;
                }
            } else {
                $records = array();
                if ($id_unique == '') {
                    $id_unique = uniqid();
                    foreach ($data['input_fields'] as $if_value) {
                        $records[] = array(
                            'id_unique' => $id_unique,
                            'id_language' => $if_value['id_language'],
                            'attribute_name' => $this->input->post('attribute_name_' . $if_value['id_language']),
                            'created_at' => CURRENT_TIME,
                            'updated_at' => CURRENT_TIME,
                        );
                    }
                    $this->common_model->insert_data('attributes', $records, true);
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('attribute_success');
                } else {
                    foreach ($data['input_fields'] as $if_value) {
                        $conditions = array('where' => array('id_unique' => $id_unique, 'id_language' => $if_value['id_language']));
                        $duplicate = $this->common_model->select_data('attributes', $conditions);
                        if ($duplicate['row_count'] > 0) {
                            $records = array(
                                'attribute_name' => $this->input->post('attribute_name_' . $if_value['id_language']),
                                'updated_at' => CURRENT_TIME,
                            );
                            $this->common_model->update_data('attributes', $records, $conditions);
                        } else {
                            $records[] = array(
                                'id_unique' => $id_unique,
                                'id_language' => $if_value['id_language'],
                                'attribute_name' => $this->input->post('attribute_name_' . $if_value['id_language']),
                                'created_at' => CURRENT_TIME,
                                'updated_at' => CURRENT_TIME,
                            );
                            $this->common_model->insert_data('attributes', $records);
                        }
                    }
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('attribute_success_update');
                }
            }
            echo json_encode($response_array);
            exit;
        }
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/add-edit', $data);
    }

    public function get_detail($id_unique)
    {
        $info = $this->common_model->select_data('attributes', array('WHERE' => array('id_unique' => $id_unique)));
        if ($info['row_count'] > 0) {
            return $info['data'];
        } else {
            redirect(base_url(AUTHORITY . '/' . $this->_slug . '/view'));
        }
    }

    public function check_unique()
    {
        $id_unique = $this->uri->segment(4);
        $attribute_name = $this->input->post('attribute_name_' . $this->_language_default['id']);
        $conditions = array(
            'SELECT' => 'id',
            'WHERE' => array('id_language' => $this->_language_default['id'], 'attribute_name' => $attribute_name),
        );
        if ($id_unique != null) {
            $conditions['WHERE']['id_unique != '] = $id_unique;
        }
        $info = $this->common_model->select_data('attributes', $conditions);
        if ($info['row_count'] > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function change_status()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            $error = false;
            $response_array = array('success' => false);
            $required_fields = array(
                'id' => $this->lang->line('something_wrong'),
                'table' => $this->lang->line('something_wrong'),
                'current_status' => $this->lang->line('something_wrong'),
            );
            foreach ($required_fields as $key => $value) {
                if (strlen(trim($this->input->post($key))) <= 0) {
                    $response_array['msg'] = $value;
                    $error = true;
                    break;
                }
            }
            if (!$error) {
                if ($this->input->post('current_status') == '0') {
                    $status = '1';
                }
                if ($this->input->post('current_status') == '1') {
                    $status = '0';
                }
                if ($status == '0') {
                    $sql = 'SELECT id FROM product_attributes WHERE id_unique_attributes IN ("' . $this->input->post('id') . '") LIMIT 1';
                    $info = $this->common_model->get_data_with_sql($sql);
                    if ($info['row_count'] <= 0) {
                        $records = array(
                            'is_active' => $status,
                        );
                        $conditions = array(
                            "where" => array("id_unique" => $this->input->post('id')),
                        );
                        $this->common_model->update_data('attributes', $records, $conditions);
                        $response_array['success'] = true;
                    } else {
                        $response_array['message'] = $this->lang->line('associated_with_other_records');
                    }
                } else {
                    $records = array(
                        'is_active' => $status,
                    );
                    $conditions = array(
                        "where" => array("id_unique" => $this->input->post('id')),
                    );
                    $this->common_model->update_data('attributes', $records, $conditions);
                    $response_array['success'] = true;
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function delete()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id')) && is_array($this->input->post('id'))) {
                $sql = 'SELECT id FROM attribute_values WHERE id_unique_attributes IN ("' . implode('","', $this->input->post('id')) . '") LIMIT 1';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] <= 0) {
                    foreach ($this->input->post('id') as $id) {
                        $this->common_model->delete_data('attributes', array('where' => array('id_unique' => $id)));
                        $this->common_model->delete_data('attribute_values', array('where' => array('id_unique_attributes' => $id)));
                    }
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('delete_success');
                } else {
                    $response_array['status'] = false;
                    $response_array['message'] = $this->lang->line('associated_with_other_records_delete');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function add_edit_values()
    {
        $data = array();
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id_unique_attributes', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            foreach ($this->_language_info as $value) {
                $this->form_validation->set_rules('attribute_value_' . $value['id'], '', 'trim|required', array('required' => 'Please enter value IN ' . $value['name'] . ' Language'));
            }
            $this->form_validation->set_rules('attribute_value', '', 'callback_check_unique_value', array('required' => $this->lang->line('please_enter_value'), 'check_unique_value' => $this->lang->line('this_record_is_available')));
            $this->form_validation->set_rules('temp', '', 'callback_check_attribute_image');
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $error_value) {
                    $response_array['message'] = $error_value;
                    break;
                }
            } else {
                $records = array();
                $image = '';
                if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
                    $file_info = upload_file('image',$this->_upload_path);
                    if($file_info['status'] == 'success') {
                        $image = $file_info['file_name'];
                    }
                }
                if ($this->input->post('id_unique') == null) {
                    $id_unique = uniqid();
                    $records = array();
                    foreach ($this->_language_info as $value) {
                        $records[] = array(
                            'id_unique_attributes' => $this->input->post('id_unique_attributes'),
                            'id_unique' => $id_unique,
                            'id_language' => $value['id'],
                            'attribute_value' => $this->input->post('attribute_value_' . $value['id']),
                            'image' => $image,
                            'created_at' => CURRENT_TIME,
                            'updated_at' => CURRENT_TIME,
                        );
                    }
                    $this->common_model->insert_data('attribute_values', $records, true);

                    /*count attribute value*/
                    $sql = "SELECT id_unique from attribute_values where id_unique_attributes = '".$this->input->post('id_unique_attributes')."' group by id_unique";
                    $info = $this->common_model->get_data_with_sql($sql);
                    $count_value = $info['row_count'];

                    $response_array['status'] = true;
                    $response_array['count_value'] = $count_value;
                    $response_array['message'] = $this->lang->line('added_successfully');
                } else {
                    foreach ($this->_language_info as $value) {
                        $conditions = array('where' => array('id_unique_attributes' => $this->input->post('id_unique_attributes'), 'id_unique' => $this->input->post('id_unique'), 'id_language' => $value['id']));
                        $duplicate = $this->common_model->select_data('attribute_values', $conditions);
                        if ($duplicate['row_count'] > 0) {
                            if(isset($duplicate['data'][0]['image']) && !empty($duplicate['data'][0]['image'])) {
                                @unlink($this->_upload_path.$duplicate['data'][0]['image']);
                            }
                            $records = array(
                                'attribute_value' => $this->input->post('attribute_value_' . $value['id']),
                                'updated_at' => CURRENT_TIME,
                            );
                            if($image != '') {
                                $records['image'] = $image;
                            }
                            $this->common_model->update_data('attribute_values', $records, $conditions);
                        } else {
                            //If new language added and details not available then need to insert new records
                            $records = array(
                                'id_unique_attributes' => $this->input->post('id_unique_attributes'),
                                'id_unique' => $this->input->post('id_unique'),
                                'id_language' => $value['id'],
                                'attribute_value' => $this->input->post('attribute_value_' . $value['id']),
                                'created_at' => CURRENT_TIME,
                                'updated_at' => CURRENT_TIME,
                            );
                            if($image != '') {
                                $records['image'] = $image;
                            }
                            $this->common_model->insert_data('attribute_values', $records);
                        }
                    }
                    $response_array['status'] = true;
                    //$response_array['message'] = 'Attribute values updated successfully';
                    $response_array['message'] = $this->lang->line('updated_successfully');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function check_unique_value()
    {
        $return = true;
        foreach ($this->_language_info as $value) {
            if ($this->input->post('id_unique') != null) {
                $info = $this->common_model->select_data('attribute_values', array('WHERE' => array('id_unique_attributes' => $this->input->post('id_unique_attributes'), 'attribute_value' => $this->input->post('attribute_value_' . $value['id']), 'id_language' => $value['id'], 'id_unique !=' => $this->input->post('id_unique'))));
            } else {
                $info = $this->common_model->select_data('attribute_values', array('WHERE' => array('id_unique_attributes' => $this->input->post('id_unique_attributes'), 'attribute_value' => $this->input->post('attribute_value_' . $value['id']), 'id_language' => $value['id'])));
            }
            if ($info['row_count'] > 0) {
                return false;
            }
        }
        return $return;
    }

    public function get_attribute_values()
    {
        $data = array();
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id_unique_attributes', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $error_value) {
                    $response_array['message'] = $error_value;
                    break;
                }
            } else {
                $records = array();
                $info = $this->common_model->select_data('attribute_values', array('WHERE' => array('id_unique_attributes' => $this->input->post('id_unique_attributes'), 'id_language' => $this->_language_default['id'])));
                $data = array();
                $data['info'] = array();
                if ($info['row_count'] > 0) {
                    $data['info'] = $info['data'];
                }
                $response_array['status'] = true;
                $response_array['data'] = $this->load->view(AUTHORITY . '/' . $this->_slug . '/view-values', $data, true);
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function delete_value()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id')) && is_array($this->input->post('id'))) {
                $sql = 'SELECT id FROM product_attributes WHERE id_unique_attribute_values IN ("' . implode('","', $this->input->post('id')) . '") LIMIT 1';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] <= 0) {
                    foreach ($this->input->post('id') as $id) {
                        $this->common_model->delete_data('attribute_values', array('where' => array('id_unique' => $id)));
                    }
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('deleted_successfully');
                } else {
                    $response_array['status'] = false;
                    $response_array['message'] = $this->lang->line('associated_with_other_records_delete');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function get_value_detail()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id_unique_attributes', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            $this->form_validation->set_rules('id_unique', '', 'trim|required', array('required' => $this->lang->line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $error_value) {
                    $response_array['message'] = $error_value;
                    break;
                }
            } else {
                $conditions = array('WHERE' => array('id_unique_attributes' => $this->input->post('id_unique_attributes'), 'id_unique' => $this->input->post('id_unique')));
                $info = $this->common_model->select_data('attribute_values', $conditions);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    $response_array['data'] = $info['data'];
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function check_attribute_image()
    {
        if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
            if(!is_valid_extension(array('jpg','png'),'image')) {
                $this->form_validation->set_message('check_attribute_image',get_line('file_size_extension') . ' ' . implode(', ', $this->_valid_extensions));
                return false;
            } elseif (!validate_file_size(MAX_IMAGE_UPLOAD_SIZE, 'image')) {                
                $this->form_validation->set_message('check_attribute_image', get_line('file_size_less_than') . ' ' . (MAX_IMAGE_UPLOAD_SIZE / 1000000) . 'MB');
                return false;
            }
        }
        return true;
    }
}
