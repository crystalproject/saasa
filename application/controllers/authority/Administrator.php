<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function view($page_number = "") {
        $settings = array(
            "url" => site_url() . "authority/administrator/view/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "id,full_name,email_address,mobile_number_1");
        $data = $this->common_model->get_pagination('administrator', $conditions, $settings);
        if (isset($this->session->user_msg)) {
            $data = array_merge($data, array("success" => $this->session->user_msg));
            $this->session->unset_userdata('user_msg');
        }
        unset($settings, $conditions);
        $this->load->view('authority/administrator/view', $data);
    }

    public function edit($id = "") {
        $this->load->helper("form");
        $data = array("form_title" => 'Edit profile');
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data('administrator', $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/administrator/view");
            }
        } else {
            redirect("authority/administrator/view");
        }
        /* Form Validation */
        $this->form_validation->set_rules('full_name', 'full name', 'required', array('required' => 'Please enter user name'));
        $this->form_validation->set_rules('email_address', 'email address', 'required|valid_email|is_unique_with_except_record[administrator.email_address.id.' . $data['id'] . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));
        $this->form_validation->set_rules('email_address_website', 'email address', 'required|valid_email|is_unique_with_except_record[administrator.email_address_website.id.' . $data['id'] . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));
        $this->form_validation->set_rules('mobile_number_1', 'mobile number', 'required|min_length[10]|max_length[15]', array('required' => 'Please enter mobile number'));
        // $this->form_validation->set_rules('mobile_number_2', 'mobile number', 'required|min_length[10]|max_length[15]');

        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $error = false;
            $records = $_POST;
            if ($_FILES['profile_photo']['name'] !='') {
                $records['profile_photo'] = $this->production_model->image_upload(PROFILE_PICTURE,'profile_photo','administrator',$this->session->user_info['id'],'gif|jpg|png|jpeg');
            }
            if (!$error) {
                $conditions = array(
                    "where" => array("id" => $data['id']),
                );
                $this->common_model->update_data('administrator', $records, $conditions);
                if ($data['id'] == $this->session->user_info['id']) {
                    $this->common_functions->updatesession($data['id']);
                }
                $data = array_merge($data, $_POST);
                $_POST = array();
                $data = array_merge($data, $_POST);
                $this->session->set_flashdata('success', 'Record updated successful');
                redirect($_SERVER['HTTP_REFERER']); 
            }
        }
        $this->load->view('authority/administrator/add-edit', $data);
    }

    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "id,profile_photo", "where" => array("id" => intval($id)));
            $user = $this->common_model->select_data('administrator', $conditions);
            if ($user['row_count'] > 0) {
                $photo = $user['data'][0]['profile_photo'];
                if (!is_null($photo)) {
                    $path = PROFILE_PICTURE . $photo;
                    if (is_file($path)) {
                        @unlink($path);
                    }
                }
            }
            $conditions = array(
                "where" => array("id" => $id),
            );
            $this->common_model->delete_data('administrator', $conditions);
            $this->session->user_msg = "Record deleted successfully";
            redirect("authority/administrator/view");
        } else {
            redirect("authority/administrator/view");
        }
    }

    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data('administrator', $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/administrator/view");
            }
        } else {
            redirect("authority/administrator/view");
        }
        $this->load->view('authority/administrator/details', $data);
    }

}
