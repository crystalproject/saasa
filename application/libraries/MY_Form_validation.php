<?php

class MY_Form_validation extends CI_Form_validation {

    /**
     * Is Unique
     *
     * Check if the input value doesn't already exist
     * in the specified database field.
     *
     * @param	string	$str
     * @param	string	$field
     * @return	bool
     */
    public function is_unique_with_except_record($str, $field) {
        sscanf($field, '%[^.].%[^.].%[^.].%[^.]', $table, $field, $column, $value);
        return isset($this->CI->db) ? ($this->CI->db->limit(1)->where($field, $str)->where_not_in($column, $value)->get($table)->num_rows() === 0) : FALSE;
    }
    public function check_unique_by_user_with_except_record($str, $field) {
        sscanf($field, '%[^.].%[^.].%[^.].%[^.].%[^.].%[^.]', $table, $field, $user_column, $user_value, $except_column, $except_value);
        return isset($this->CI->db) ? ($this->CI->db->limit(1)->where($field, $str)->where_not_in($except_column, $except_value)->where($user_column, $user_value)->get($table)->num_rows() === 0) : FALSE;
    }

    public function check_exist($str, $value) {
        list($table, $column) = explode('.', $value, 2);        
        $query = $this->CI->db->query("SELECT COUNT($column) AS count FROM $table WHERE $column = $str");
        $row = $query->row();        
        return ($row->count > 0) ? true : false;
    }
}
