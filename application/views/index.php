<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
$set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE; /*
?>
<!-- Slider Section Strat Here -->
<div id="minimal-bootstrap-carousel" class="carousel slide carousel-fade slider-content-style slider-home-one">
    <div class="carousel-inner">
        <?php
        if (isset($slider_details) && $slider_details != null) {
            foreach ($slider_details as $key => $value) {
        ?>
                <div class="carousel-item <?= $key == 0 ? 'active' : ''; ?> slide-<?= $key + 1; ?>" style="background-image: url(<?= base_url(HOME_SLIDER . $value['image' . get_language_front()]); ?>);">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="box valign-middle">
                                <div class="content text-center">
                                    <h4 data-animation="animated fadeInUp"><?= $value['title'] != '' ? $value['title' . get_language_front()] : ''; ?></h4>
                                    <p data-animation="animated fadeInUp"><b><?= $value['description'] != '' ? $value['description' . get_language_front()] : ''; ?></b></p>
                                    <a data-animation="animated fadeInDown" href="<?= $value['link' . get_language_front()] != '' ? $value['link' . get_language_front()] : 'javascript:void(0)'; ?>" class="thm-btn"><?= get_line_front('start_now'); ?> <i class="fa fa-chevron-<?= $set_language == 1 ? 'right' : 'left'; ?>" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php }
        }
        ?>
    </div>
    <!-- Controls -->
    <a class="carousel-control-prev" href="#minimal-bootstrap-carousel" role="button" data-slide="prev">
        <img src="<?= base_url(); ?>assets/images/back.png" style="height: 25px; width: 25px;">
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#minimal-bootstrap-carousel" role="button" data-slide="next">
        <img src="<?= base_url(); ?>assets/images/next.png" style="height: 25px; width: 25px;">
        <span class="sr-only">Next</span>
    </a>
</div> */?>
<section id="oc-fullslider" class="banner owl-carousel">
    <?php
        if (isset($slider_details) && $slider_details != null) {
            foreach ($slider_details as $key => $value) {   ?>
                <div class="owl-slide">
                    <div class="item">
                        <img src="<?= base_url(HOME_SLIDER . $value['image' . get_language_front()]); ?>" alt="Slider">
                        <div class="slider-pos">
                            <div class="container">
                                <div class="wrap-caption">
                                    <h1 class="caption-heading text-primary"><?= $value['title'] != '' ? $value['title' . get_language_front()] : ''; ?></h1>
                                    <p class=""><?= $value['description'] != '' ? $value['description' . get_language_front()] : ''; ?></p>
                                    <a href="<?= $value['link' . get_language_front()] != '' ? $value['link' . get_language_front()] : 'javascript:void(0)'; ?>" class="btn btn-primary"><?=get_line_front('more_about_us')?></a>
                                </div>  
                            </div>
                        </div>
                    </div>  
                </div>
                <?php
            }
        }
    ?>
</section>
<div class="clearfix"></div>

<section class="section services">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="row no-gutters overlap">
                        <div class="col-sm-12 col-md-6">
                            <div class="rs-feature-box-1" data-background="<?= base_url() ?>assets/images/promo_tab1.jpg">
                                <div class="body">
                                    <h3>20% OFF ONLINE <br> EXCLUSIVES</h3> 
                                    <p>Shop & save on your favorite pet products available only online!</p>
                                    <a href="#" class="btn btn-secondary">Shop Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="rs-feature-box-1 bgdark" data-background="<?= base_url() ?>assets/images/promo_tab2.jpg">
                                <div class="body">
                                    <h3>SAVE UP TO 30% ALL ACCECORIES</h3> 
                                    <p>Shop & save on your favorite pet products available only online!</p>
                                    <a href="#" class="btn btn-secondary">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h2 class="section-heading text-center text-primary no-after mb-4">
                        <?=get_line_front('best_seller')?>
                    </h2>
                    <p class="subheading text-center mb-4"><?=get_line_front('health_and_well_being')?></p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-sm-12">
                    <div class="best-carousel owl-carousel">
                        <?php
                            if (isset($best_seller) && !empty($best_seller)) {
                                // echo "<pre>";print_r($best_seller);exit;
                                foreach ($best_seller as $key => $value) { 
                                    ?>
                                    <div class="item">
                                        <div class="rs-shop-box mb-5">
                                            <!-- <a href="<?=base_url('/product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>" class="link-wish">
                                                <i class="fa fa-heart"></i>
                                            </a> -->
                                            <?php
                                                $get_pro_id = get_details('wishlist', array('id_product' =>$value['id_unique'], 'login_id' => get_front_login_id()));
                                            if (isset($get_pro_id) && $get_pro_id != null) { ?>
                                                <a href="javascript:void(0)" class="link-wish wishlist fill-heart" id="<?= $value['id_unique'] ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                            <?php } else { ?>
                                                <a href="javascript:void(0)" class="link-wish wishlist " id="<?= $value['id_unique'] ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                            <?php } ?>
                                            <div class="media">
                                                <a href="<?=base_url('/product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>">
                                                    <img src="<?= $value['main_image']; ?>" alt="<?= $value['title']; ?>" class="img-fluid">
                                                </a>
                                            </div>
                                            <div class="body-text">
                                                <h4 class="title"><a href="#"><?= $value['title']; ?></a></h4>
                                                <div class="meta">
                                                    <div class="price">$<?= $value['other_best_seller']['final_price'][0]; ?></div>
                                                    <div class="rating">
                                                        <!-- <a href="#" data-custom-image="" data-id="<?php echo $info['id_unique']; ?>" class="btn btn-cart add-to-cart">Add to cart</a>-->
                                                        <a href="javascript:void(0)" data-custom-image="" data-id="<?php echo $info['id_unique']; ?>" class="btn btn-cart add-to-cart"><?= get_line_front('add_to_cart'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        ?>
                        <!-- <div class="item">
                            <div class="rs-shop-box mb-5">
                                <a href="javascript:void(0);" class="link-wish">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <div class="media">
                                    <a href="#"><img src="<?= base_url() ?>assets/images/pro-1.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="body-text">
                                    <h4 class="title"><a href="#">Original 6kg</a></h4>
                                    <div class="meta">
                                        <div class="price">$29</div>
                                        <div class="rating">
                                            <a href="#" class="btn btn-cart">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="rs-shop-box mb-5">
                                <a href="javascript:void(0);" class="link-wish">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <div class="media">
                                    <a href="#"><img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="body-text">
                                    <h4 class="title"><a href="#">Grass fed lamb 6kg</a></h4>
                                    <div class="meta">
                                        <div class="price">$29</div>
                                        <div class="rating">
                                            <a href="#" class="btn btn-cart">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="rs-shop-box mb-5">
                                <a href="javascript:void(0);" class="link-wish">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <div class="media">
                                    <a href="#"><img src="<?= base_url() ?>assets/images/pro-1.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="body-text">
                                    <h4 class="title"><a href="#">Original 6kg</a></h4>
                                    <div class="meta">
                                        <div class="price">$29</div>
                                        <div class="rating">
                                            <a href="#" class="btn btn-cart">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="rs-shop-box mb-5">
                                <a href="javascript:void(0);" class="link-wish">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <div class="media">
                                    <a href="#"><img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="body-text">
                                    <h4 class="title"><a href="#">Grass fed lamb 6Kg</a></h4>
                                    <div class="meta">
                                        <div class="price">$29</div>
                                        <div class="rating">
                                            <a href="#" class="btn btn-cart">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="rs-shop-box mb-5">
                                <a href="javascript:void(0);" class="link-wish">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <div class="media">
                                    <a href="#"><img src="<?= base_url() ?>assets/images/pro-1.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="body-text">
                                    <h4 class="title"><a href="#">Original 6kg</a></h4>
                                    <div class="meta">
                                        <div class="price">$29</div>
                                        <div class="rating">
                                            <a href="#" class="btn btn-cart">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="rs-shop-box mb-5">
                                <a href="javascript:void(0);" class="link-wish">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <div class="media">
                                    <a href="#"><img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="body-text">
                                    <h4 class="title"><a href="#">Grass fed lamb 6kg</a></h4>
                                    <div class="meta">
                                        <div class="price">$29</div>
                                        <div class="rating">
                                            <a href="#" class="btn btn-cart">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="rs-shop-box mb-5">
                                <a href="javascript:void(0);" class="link-wish">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <div class="media">
                                    <a href="#"><img src="<?= base_url() ?>assets/images/pro-1.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="body-text">
                                    <h4 class="title"><a href="#">Original 6kg</a></h4>
                                    <div class="meta">
                                        <div class="price">$29</div>
                                        <div class="rating">
                                            <a href="#" class="btn btn-cart">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section bgi-cover-center" data-background="<?= base_url() ?>assets/images/pattern_bg.jpg">
    <div class="content-wrap py-0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-6 col-md-6">
                    <div class="img-cta">
                        <img src="<?= base_url() ?>assets/images/product_promo.png" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="text-white">
                        <h1 class="section-heading no-after text-white">
                            SPECIAL OFFER THIS WEEK!
                        </h1>
                        <p class="uk21 text-white mb-4">Get Discount 10% Off Pet Food.</p>
                        <p class="mb-5">Sed orci dolor, pulvinar nec luctus a, malesuada ac nisl. Aliquam eleifend et dui et suscipit. Nam semper accumsan ante, ac dapibus urna dapibus et.</p>

                    </div>
                    <a href="#" class="btn btn-light">Shop Now</a>
                    <div class="spacer-50"></div>

                </div>

            </div>
        </div>
    </div>
</section>
<div class="spacer-90"></div>
<section class="category">
    <div class="content-wrap">
        <div class="container-fluid">
            <div class="row">
                
                <div class="col-12 col-lg-4">
                    <div class="rs-image-box cat-list">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-3.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title"><?=get_line_front('food')?></h3>
                            <ul>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain free</a>
                                </li>
                            </ul>
                            <a href="#" class="btn btn-petshop">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="rs-image-box cat-list">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-4.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title"><?=get_line_front('food')?></h3>
                            <ul>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain free</a>
                                </li>
                            </ul>
                            <a href="#" class="btn btn-petshop">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="rs-image-box cat-list">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-3.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title">Food</h3>
                            <ul>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain free</a>
                                </li>
                            </ul>
                            <a href="#" class="btn btn-petshop">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="rs-image-box cat-list">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-4.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title">Food</h3>
                            <ul>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain free</a>
                                </li>
                            </ul>
                            <a href="#" class="btn btn-petshop">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="rs-image-box cat-list">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-3.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title">Food</h3>
                            <ul>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain free</a>
                                </li>
                            </ul>
                            <a href="#" class="btn btn-petshop">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="rs-image-box cat-list">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-4.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title">Food</h3>
                            <ul>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Drt grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet chunks</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain food</a>
                                </li>
                                <li>
                                    <a href="#" class="food-link">Wet grain free</a>
                                </li>
                            </ul>
                            <a href="#" class="btn btn-petshop">
                                View All
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
<section class="testimonials">
    <div class="row">
        <div class="col-lg-6">
            <div class="testi-carousel owl-carousel">
                <div class="item">
                    <div class="testi-des">
                        <h2>Comments about</h2>
                        <p>
                            Loremdi ipsumcens in qui, in oblique accusam accusamus sit, ad eam consequat intellegebat Docendi adolescens in qui, in at lorem ipsum am accusamus sit, adeamen lorem.
                        </p>
                        <h4 class="testimonial-author">
                            <span class="active-hover">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="active-hover-left">
                                    <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                </svg>
                                <span class="active-hover-middle"></span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="active-hover-right">
                                    <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                </svg>
                            </span>
                            <span class="testimonials-author-name">Leyla Higgins</span>
                        </h4>
                    </div>
                </div>
                <div class="item">
                    <div class="testi-des">
                        <h2>Comments about</h2>
                        <p>
                            Loremdi ipsumcens in qui, in oblique accusam accusamus sit, ad eam consequat intellegebat Docendi adolescens in qui, in at lorem ipsum am accusamus sit, adeamen lorem.
                        </p>
                        <h4 class="testimonial-author">
                            <span class="active-hover">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="active-hover-left">
                                    <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                </svg>
                                <span class="active-hover-middle"></span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="active-hover-right">
                                    <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                </svg>
                            </span>
                            <span class="testimonials-author-name">Leyla Higgins</span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="testi-bgimg"></div>
        </div>
    </div>
</section>
<section class="category">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                
                <div class="col-12 col-sm-6">
                    <div class="rs-image-box">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-1.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title"><a href="#">Training & Control</a></h3>
                            Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            <a href="#" class="btn btn-petshop">
                                Shop Now <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6">
                    <div class="rs-image-box">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-2.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title"><a href="#">toys</a></h3>
                            Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            <a href="#" class="btn btn-petshop">
                                Shop Now <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6">
                    <div class="rs-image-box">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-2.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title"><a href="#">grooming</a></h3>
                            Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            <a href="#" class="btn btn-petshop">
                                Shop Now <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6">
                    <div class="rs-image-box">
                        <div class="media">
                            <a href="#"><img src="<?= base_url() ?>assets/images/cat-1.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="body-text">
                            <h3 class="title"><a href="#">Conditioner</a></h3>
                            Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            <a href="#" class="btn btn-petshop">
                                Shop Now <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
<section class="section care-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="care-img">
                    <div class="care-banner-image">
                        <div class="care-foreground-image">
                            <img src="<?= base_url() ?>assets/images/care-1.png" width="800" height="383">
                        </div>
                    </div>
                    <div class="mkdf-banner-text-holder">
                        <div class="mkdf-banner-text-outer">
                            <div class="mkdf-banner-text-inner">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        Cat care
                                    </h3>
                                </a>
                                <p class="mkdf-banner-text">
                                    From nutrition to health care. Everything in one place.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="care-img">
                    <div class="care-banner-image">
                        <div class="care-foreground-image">
                            <img src="<?= base_url() ?>assets/images/care-2.png" width="800" height="383">
                        </div>
                    </div>
                    <div class="mkdf-banner-text-holder">
                        <div class="mkdf-banner-text-outer">
                            <div class="mkdf-banner-text-inner">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        Vate tips
                                    </h3>
                                </a>
                                <p class="mkdf-banner-text">
                                    Anything related to your pets furry, flying, or crawling. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="care-img">
                    <div class="care-banner-image">
                        <div class="care-foreground-image">
                            <img src="<?= base_url() ?>assets/images/care-3.png" width="800" height="383">
                        </div>
                    </div>
                    <div class="mkdf-banner-text-holder">
                        <div class="mkdf-banner-text-outer">
                            <div class="mkdf-banner-text-inner">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        Dog care
                                    </h3>
                                </a>
                                <p class="mkdf-banner-text">
                                    Training, nurture, or anything else. We are here for you.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section our-brand">
    <div class="content-wrap content-wrap-40">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h2 class="section-heading text-center text-primary no-after mb-4">
                        <?=get_line_front('our_brands')?>
                    </h2>
                    <p class="subheading text-center mb-4"><?=get_line_front('our_brands_slogan')?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="best-carousel owl-carousel">
                        <?php
                            if (isset($brands_details) && $brands_details != null) {
                                foreach ($brands_details as $key => $value) { 
                                    if($value['image'] !=""){?>
                                        <div class="item">
                                            <div class="\">
                                                <a href="javascript:void(0)">
                                                    <img src="<?= base_url(BRAND_IMAGE.$value['image']) ?>" alt="" class="img-fluid">
                                                </a>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                        ?>
                        <!-- <div class="item">
                            <div class="client-img">
                                <a href="#">
                                    <img src="<?= base_url() ?>assets/images/client-2.png" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <a href="#">
                                    <img src="<?= base_url() ?>assets/images/client-3.png" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <a href="#">
                                    <img src="<?= base_url() ?>assets/images/client-4.png" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <a href="#">
                                    <img src="<?= base_url() ?>assets/images/client-1.png" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <a href="#">
                                    <img src="<?= base_url() ?>assets/images/client-2.png" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <a href="#">
                                    <img src="<?= base_url() ?>assets/images/client-3.png" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <a href="#">
                                    <img src="<?= base_url() ?>assets/images/client-4.png" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div> -->
                    </div>
                </div>                  
            </div>
        </div>
    </div>
</section>
<!-- Slider Section End Here -->
<!-- Product Section Strat  Here -->
<!-- <section class="home-product-section">
    <div class="container">
        <div class="row">
            <?php
            if (isset($category_details) && $category_details != null) {
                foreach ($category_details as $key => $value) {
                    $id = $value['id'];
            ?>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                        <div class="product-box text-center">
                            <a class="new-link" href="<?= base_url('category/' . $value['slug' . get_language_front()] . '/' . $id); ?>">
                                <img src="<?= base_url(CATEGORY_IMAGE . $value['image' . get_language_front()]); ?>">
                                <div class="product-box-title">
                                    <h3><?= $value['name' . get_language_front()]; ?></h3>
                                </div>
                            </a>
                            <a href="<?= base_url('category/' . $value['slug' . get_language_front()] . '/' . $id); ?>" class="product-box-button"><?php echo get_line('discover'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
            <?php }
            }
            ?>
        </div>
    </div>
</section> -->
<!-- Product Secction End Here -->

<?php $this->load->view('include/copyright'); ?>
<?php $this->load->view('include/footer'); ?>