<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<section class="myaccount">
	<div class="container">
		<div class="cart payment-tab">
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-3">
							<div class="profile-tab">
								<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
									<a class="nav-link active" id="myProfile-tab" data-toggle="pill" href="#myProfile" role="tab" aria-controls="myProfile" aria-selected="true">
										<i class="fa fa-user"></i>
										<?php echo get_line('my_profile'); ?>
									</a>
									<a class="nav-link" id="myOrder-tab" data-toggle="pill" href="#myOrder" role="tab" aria-controls="myOrder" aria-selected="false">
										<i class="fa fa-file-text"></i>
										<?php echo get_line('my_orders'); ?>
									</a>
									<a class="nav-link" id="myWishlist-tab" data-toggle="pill" href="#myWishlist" role="tab" aria-controls="myWishlist" aria-selected="false">
										<i class="fa fa-heart"></i>									
										<?php echo get_line('my_wishlist'); ?>
									</a>
									<a class="nav-link" id="myCart-tab" data-toggle="pill" href="#myCart" role="tab" aria-controls="myCart" aria-selected="false">
										<i class="fa fa-shopping-cart"></i>
										<?php echo get_line('cart'); ?>
									</a>
									<a class="nav-link" id="myAddress-tab" data-toggle="pill" href="#myAddress" role="tab" aria-controls="myAddress" aria-selected="false">
										<i class="fa fa-address-book"></i>
										<?php echo get_line('my_saved_addresses'); ?>
									</a>
									<a class="nav-link" id="myPassword-tab" data-toggle="pill" href="#myPassword" role="tab" aria-controls="myPassword" aria-selected="false">
										<i class="fa fa-key"></i>
										<?php echo get_line('change_password'); ?>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="tab-content login-form" id="v-pills-tabContent">
								<div class="tab-pane fade show active" id="myProfile" role="tabpanel" aria-labelledby="myProfile-tab">
									<h3 class="profile-head">
										<i class="fa fa-user"></i>
										<?php echo get_line('my_profile'); ?>
									</h3>
									<form method="post" action="<?=base_url('account/update_profile')?>">
										<div class="personal-detail">
											<h4>
												<?php echo get_line('personal_detail'); ?>
											</h4>
												<div class="row">
													<div class="col-lg-3">
														<div class="personal-label">
															<label>
																<?php echo get_line('email'); ?> :
															</label>
														</div>
													</div>
													<div class="col-lg-8">
														<div class="form-group">
															<input type="email" name="email" class="form-control" disabled="disabled" value="<?=(isset($email) && $email !='')?$email:'';?>">
														</div>
													</div>
													<div class="col-lg-3">
														<div class="personal-label">
															<label>
																<?php echo get_line('full_name'); ?> :
															</label>
														</div>
													</div>
													<div class="col-lg-8">
														<div class="form-group">
															<input type="text" class="form-control" placeholder="<?php echo get_line('full_name'); ?>" name="full_name" value="<?=(isset($full_name) && $full_name !='')?$full_name:'';?>">
														</div>
													</div>
													<!-- Alternate Email ID -->
													<!-- <div class="col-lg-3">
														<div class="personal-label">
															<label>
																Alternate Email ID :
															</label>
														</div>
													</div>
													<div class="col-lg-8">
														<div class="form-group">
															<input type="email" class="form-control" value="jhonwick1@demo.com">
														</div>
													</div> -->
													<!-- Gender -->
													<!-- <div class="col-lg-3">
														<div class="personal-label">
															<label>
																<?php echo get_line('gender'); ?> : :
															</label>
														</div>
													</div>
													<div class="col-lg-8">
														<div class="row">
															<div class="col-lg-3 col-5">
																<div class="form-group">
																	<label class="cust-check cust-radio-1 cust-radio">
																		<?php echo get_line('male'); ?>
																		<input type="radio" name="gender">
																		<span class="checkmark"></span>
																	</label>
																</div>
															</div>
															<div class="col-lg-3 col-5">
																<div class="form-group">
																	<label class="cust-check cust-radio-1 cust-radio">
																		<?php echo get_line('female'); ?>
																		<input type="radio" name="gender">
																		<span class="checkmark"></span>
																	</label>
																</div>
															</div>
														</div>
													</div> -->
													<div class="col-lg-3">
														<div class="personal-label">
															<label>
																<?php echo get_line('date_of_birth'); ?> :
															</label>
														</div>
													</div>
													<div class="col-lg-8">
														<div class="form-group">
															<input type="text" class="form-control date_of_birth" name="date_of_birth" placeholder="MM-DD-YYYY" value="<?=(isset($date_of_birth) && $date_of_birth !='')?$date_of_birth:'';?>">
														</div>
													</div>
													<div class="col-lg-3">
														<div class="personal-label">
															<label>
																<?php echo get_line('city'); ?> :
															</label>
														</div>
													</div>
													<div class="col-lg-8">
														<div class="form-group">
															<input type="text" class="form-control" placeholder="City" name="city" value="<?=(isset($city) && $city !='')?$city:'';?>">
														</div>
													</div>
													<div class="col-lg-3">
														<div class="personal-label">
															<label>
																<?php echo get_line('mobile_number'); ?> :
															</label>
														</div>
													</div>
													<div class="col-lg-8">
														<div class="form-group">
															<input type="text" class="form-control" placeholder="Mobile Number" name="mobile_number" value="<?=(isset($mobile_number) && $mobile_number !='')?$mobile_number:'';?>">
														</div>
													</div>
												</div>
										</div>
										<h3 class="profile-head">
											<i class="fa fa-paw"></i>
											<?php echo get_line('pet_details'); ?> :
										</h3>
										<?php
											if (isset($pet_data) && !empty($pet_data)) {
												$i = 0;
									            foreach ($pet_data as $key => $value) {
									        ?>
												<div class="personal-detail <?=($i !=0)?'pet-detail':''?>">
													<h4>
														<?php echo get_line('pet_details'); ?> <?=$i+1?>
													</h4>
														<div class="row">
															<input type="hidden" name="pet_id[]" value="<?=$value['pet_id']?>">
															<div class="col-lg-3">
																<div class="personal-label">
																	<label>
																		<?php echo get_line('pet_name'); ?> :
																	</label>
																</div>
															</div>
															<div class="col-lg-8">
																<div class="form-group">
																	<input type="text" class="form-control" name="pet_name[]" placeholder="Pet Name" value="<?=$value['pet_name']?>">
																</div>
															</div>
															<div class="col-lg-3">
																<div class="personal-label">
																	<label>
																		<?php echo get_line('pet_type'); ?> :
																	</label>
																</div>
															</div>
															<div class="col-lg-8">
																<div class="form-group">
																	<div class="select-wrap">
																		<select class="form-control" name="pet_type[]">
																			<option <?=($value['pet_type'] == 'Dog')?'selected':''?>>Dog</option>
																			<option <?=($value['pet_type'] == 'Cat')?'selected':''?>>Cat</option>
																			<option <?=($value['pet_type'] == 'Rabbit')?'selected':''?>>Rabbit</option>
																			<option <?=($value['pet_type'] == 'Turtle')?'selected':''?>>Turtle</option>
																			<option <?=($value['pet_type'] == 'Hamster')?'selected':''?>>Hamster</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-lg-3">
																<div class="personal-label">
																	<label>
																		<?php echo get_line('pet_gender'); ?>  :
																	</label>
																</div>
															</div>
															<div class="col-lg-8">
																<div class="form-group">
																	<div class="select-wrap">
																		<select class="form-control" name="pet_gender[]">
																			<option <?=($value['pet_gender'] == 'Male')?'selected':''?>><?php echo get_line('male'); ?></option>
																			<option <?=($value['pet_gender'] == 'Female')?'selected':''?>><?php echo get_line('female'); ?></option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-lg-3">
																<div class="personal-label">
																	<label>
																		<?php echo get_line('pet_dob'); ?> :
																	</label>
																</div>
															</div>
															<div class="col-lg-8">
																<div class="form-group">
																	<input type="text" class="form-control date_of_birth" name="pet_dob[]" placeholder="MM-DD-YYYY" value="<?=$value['pet_dob']?>">
																</div>
															</div>
															<div class="col-lg-3">
																<div class="personal-label">
																	<label>
																		<?php echo get_line('pet_breed'); ?>  :
																	</label>
																</div>
															</div>
															<div class="col-lg-8">
																<div class="form-group">
																	<input type="text" class="form-control" name="pet_breed[]" placeholder="Pet Breed" value="<?=$value['pet_breed']?>">
																</div>
															</div>
															<div class="col-lg-3">
																<div class="personal-label">
																	<label>
																		<?php echo get_line('pet_weight'); ?> :
																	</label>
																</div>
															</div>
															<div class="col-lg-8">
																<div class="form-group">
																	<input type="text" class="form-control" name="pet_weight[]" placeholder="Pet Weight" value="<?=$value['pet_weight']?>">
																</div>
															</div>
														</div>
												</div>
											<?php $i++;}
											}
										?>
										<button type="submit" class="btn btn-continue">Submit</a>
									</form>
								</div>
								<div class="tab-pane fade" id="myOrder" role="tabpanel" aria-labelledby="myOrder-tab">
									<h3 class="profile-head">
										<i class="fa fa-file-text"></i>
										My Orders
									</h3>
									<div class="search-order">
										<div class="personal-detail">
											<div class="row">
												<div class="offset-lg-8 col-lg-4">
													<div class="sort-by text-right">
														<div class="select-wrap">
													 		<?php
						                                        $options = array();
						                                        $order_status_option = get_order_status();
						                                        if (isset($order_status_option) && $order_status_option !=null) {
						                                            foreach ($order_status_option as $key => $value) {
						                                                $options[$key] = $value;
						                                            }
						                                        }
						                                        $selected = isset($main_type) && $main_type !=null ? $main_type : '';
						                                        echo form_dropdown('order_status',$options,$selected,'class="form-control" id="order_status"');
						                                    ?>
														</div>
													</div>
												</div>
												<div class="col-lg-4">
													<div class="form-group">
														<label>Search by ID</label>
														<div class="search-field">
															<input type="text" class="form-control" placeholder="Order Id">
															<button class="btn btn-search">
																<i class="fa fa-search"></i>
															</button>
														</div>
													</div>
												</div>
												<div class="col-lg-8">
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>From</label>
																<div class="search-field">
																	<input type="date" class="form-control" placeholder="Order Id">
																	<button class="btn btn-search">
																		<i class="fa fa-calendar"></i>
																	</button>
																</div>
															</div>
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>To</label>
																<div class="search-field">
																	<input type="date" class="form-control" placeholder="Order Id">
																	<button class="btn btn-search">
																		<i class="fa fa-calendar"></i>
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="checkout personal-detail">
										<div class="row">
											<div class="col-lg-8">
												<div class="cart-table">
													<div class="table-responsive">
														<table class="table">
															<thead>
																<tr>
																	<th>
																		Item
																	</th>
																	<th>
																		Qty
																	</th>
																	<th>
																		Unit Price
																	</th>
																	<th>
																		Total price
																	</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>
																		<a href="#">
																			Lorem Ipsum Simple dummy text
																		</a>
																	</td>
																	<td>
																		1
																	</td>
																	<td>
																		₹ 1790
																	</td>
																	<td>
																		₹ 1790
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="checkout-des">
													<p class="checkout-head">
														Invoice
													</p>
													<ul class="sub-total">
														<li>
															Cart sub total:
														</li>
														<li>
															₹ 1790
														</li>
													</ul>
													<ul class="sub-total">
														<li>
															Shipping &amp; Handling Price:
														</li>
														<li>
															Free
														</li>
													</ul>
													<ul>
														<li>
															Total payable
														</li>
														<li>
															₹ 1790
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="myWishlist" role="tabpanel" aria-labelledby="myWishlist-tab">
									<h3 class="profile-head">
										<i class="fa fa-heart"></i>
										My Wishlist
									</h3>
									<div class="personal-detail">
										<div class="row">
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-heart"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox" checked="checked">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock available">
															<i class="fa fa-check-circle"></i>
															Stock available
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	But Now
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-heart"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock out-of-stock">
															<i class="fa fa-times-circle"></i>
															Out of Stock
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	But Now
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-heart"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox" checked="checked">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock available">
															<i class="fa fa-check-circle"></i>
															Stock available
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	But Now
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-heart"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock out-of-stock">
															<i class="fa fa-times-circle"></i>
															Out of Stock
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	But Now
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="myCart" role="tabpanel" aria-labelledby="myCart-tab">
									<h3 class="profile-head">
										<i class="fa fa-shopping-cart"></i>
										My Saved Cart
									</h3>
									<div class="personal-detail">
										<div class="row">
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-trash"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox" checked="checked">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock available">
															<i class="fa fa-check-circle"></i>
															Stock available
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	Add to cart
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-trash"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock out-of-stock">
															<i class="fa fa-times-circle"></i>
															Out of Stock
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	Add to cart
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-trash"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox" checked="checked">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock available">
															<i class="fa fa-check-circle"></i>
															Stock available
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	Add to cart
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4 col-md-4">
												<div class="rs-shop-box mb-5">
													<a href="javascript:void(0);" class="link-wish">
														<i class="fa fa-trash"></i>
													</a>
													<label class="cust-check">
														<input type="checkbox">
														<span class="checkmark"></span>
													</label>
													<div class="media">
														<a href="#">
															<img src="<?= base_url() ?>assets/images/pro-2.jpg" alt="Images" class="img-fluid">
														</a>
													</div>
													<div class="body-text">
														<p class="stock out-of-stock">
															<i class="fa fa-times-circle"></i>
															Out of Stock
														</p>
														<h4 class="title">
															<a href="#">
																Grass fed lamb 6Kg
															</a>
														</h4>
														<div class="meta">
															<div class="price">
																$29
															</div>
															<div class="rating">
																<a href="#" class="btn btn-cart">
																	Add to cart
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="myAddress" role="tabpanel" aria-labelledby="myAddress-tab">
									<h3 class="profile-head">
										<i class="fa fa-address-book"></i>
										My Saved Address
									</h3>
									<div class="personal-detail">
										<div class="row">
											<?php
												if (isset($address) && $address !=null) {
													foreach ($address as $key => $value) {
														$id = $value['id'];
														?>
														<div class="col-lg-4 col-md-6">
															<div class="checkout-filldata">
																<i class="fa fa-check check-icon"></i>
																<h4>
																	<?= $value['name'];?>
																</h4>
																<p>
																	<?= $value['address'];?>
																</p>
																<!-- <p class="mobile-no">
																	+1 1234 567890
																</p> -->
																<a href="#" class="btn btn-continue">
																	Continue
																</a>
																<a href="javascript:void(0);" class="btn btn-edit" data-toggle="modal" data-target=".bd-example-modal-lg">
																	<i class="fa fa-pencil"></i>
																</a>

																	<a href="<?= base_url('account/remove-address/'.$id);?>" class="form-submit-btn btn btn-edit btn-del pull-right" style="width: 5%;" onclick="return confirm('<?= get_line_front('delete_confirm');?>')"><i class="fa fa-trash-o"></i></a>
																
																<!-- <a href="javascript:void(0);" class="btn btn-edit btn-del pull-right">
																	<i class="fa fa-trash" aria-hidden="true"></i>
																</a> -->
															</div>
														</div>
														<?php
													}
												}
											?>
											<!-- <div class="col-lg-4 col-md-6">
												<div class="checkout-filldata">
													<i class="fa fa-check check-icon"></i>
													<h4>
														Jhon Wick
													</h4>
													<p>
														36 Lorem Ipsum, Lorem Dummy 358420
													</p>
													<p class="mobile-no">
														+1 1234 567890
													</p>
													<a href="#" class="btn btn-continue">
														Continue
													</a>
													<a href="javascript:void(0);" class="btn btn-edit" data-toggle="modal" data-target=".bd-example-modal-lg">
														<i class="fa fa-pencil"></i>
													</a>
													<a href="javascript:void(0);" class="btn btn-edit btn-del pull-right">
														<i class="fa fa-trash" aria-hidden="true"></i>
													</a>
												</div>
											</div> -->
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="myPassword" role="tabpanel" aria-labelledby="myPassword-tab">
									<h3 class="profile-head">
										<i class="fa fa-key"></i>
										Change Password
									</h3>
									<div class="personal-detail">
										<form>
											<div class="row">
												<div class="col-lg-3">
													<div class="personal-label">
														<label>
															Current Password :
														</label>
													</div>
												</div>
												<div class="col-lg-8">
													<div class="form-group">
														<input type="password" class="form-control">
													</div>
												</div>
												<div class="col-lg-3">
													<div class="personal-label">
														<label>
															New Password :
														</label>
													</div>
												</div>
												<div class="col-lg-8">
													<div class="form-group">
														<input type="password" class="form-control">
													</div>
												</div>
												<div class="col-lg-3">
													<div class="personal-label">
														<label>
															Confirm Password :
														</label>
													</div>
												</div>
												<div class="col-lg-8">
													<div class="form-group">
														<input type="password" class="form-control">
													</div>
												</div>
											</div>
											<div class="change-list">
												<a href="#" class="btn btn-continue">Submit</a>
												<a href="#" class="btn btn-continue">Cancel</a>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
$footer_js = array(base_url('assets/authority/js/toastr.min.js'), 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script>
	$(document).ready(function() {
		$('.send-otp').click(function() {
			var current = $(this);
			loading(true);
			var data = $('#form').serialize();
			$.ajax({
				url: BASE_URL + 'ajax/send_otp',
				method: 'POST',
				data: data,
				dataType: 'json',
				success: function(response) {
					setTimeout(function() {
						loading(false);
					}, 150);
					toastr.remove();
					if (response.status) {
						toastr.success(response.message);
					} else {
						toastr.error(response.message);
					}
				}
			});
		});

		var today = new Date();
		$(".date_of_birth").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0",
			dateFormat: 'dd-mm-yy',
			endDate: "today",
			maxDate: today,
			showButtonPanel: true,
		});

		/*FORM VALIDATION*/
		$("#form").validate({
			rules: {
				email: {
					required: true,
					email: true
				},
				password: {
					required: true
				},
				first_name: {
					required: true
				},
				last_name: {
					required: true
				},
				gender: {
					required: true
				},
				date_of_birth: {
					required: true
				},
			},
			messages: {
				email: {
					required: "<?= get_line_front('this_field_is_required') ?>",
					email: "<?= get_line_front('please_enter_valid_email_address') ?>"
				},
				password: {
					required: '<?= get_line_front('this_field_is_required') ?>'
				},
				first_name: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				last_name: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				gender: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				date_of_birth: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
			}
		});
	});
</script>
<?php $this->load->view('include/footer'); ?>