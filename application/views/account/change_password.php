<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
    $this->load->view('include/messages');
?>
<section class="new-account-page-section">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-9 col-offset-1">
				<form id="form" method="post" class="registration-form">
					<div class="border-bottom row">
						<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
							<div class="form-title">
								<h4><?= get_line_front('change_password');?></h4>
							</div>
						</div>
						<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
							<div class="form-content">
								<div class="form-group row">
								    <label for="email" class="col-sm-3 col-form-label"><?= get_line_front('current_password');?></label>
								    <div class="col-sm-9">
								      <input type="password" name="current_password" class="form-control-plaintext" id="name" value="<?php echo (isset($current_password) ? $current_password : ""); ?>" style="padding-left: 10px !important;">
								      <?= form_error('current_password', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
							</div>
							<div class="form-content">
								<div class="form-group row">
								    <label for="email" class="col-sm-3 col-form-label"><?= get_line_front('new_password');?></label>
								    <div class="col-sm-9">
								      <input type="password" name="new_password" class="form-control-plaintext" id="new_password" value="" style="padding-left: 10px !important;">
								      <?= form_error('new_password', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
							</div>
							<div class="form-content">
								<div class="form-group row">
								    <label for="email" class="col-sm-3 col-form-label"><?= get_line_front('confirm_password');?></label>
								    <div class="col-sm-9">
								      <input type="password" name="confirm_new_password" class="form-control-plaintext" id="confirm_new_password" value="<?php echo (isset($confirm_new_password) ? $confirm_new_password : ""); ?>" style="padding-left: 10px !important;">
								      <?= form_error('confirm_new_password', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
							</div>
						</div>
					</div>
					<button class="form-submit-btn"><?= get_line_front('submit');?> <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
				</form>
			</div>
		</div>
	</div>
</section>
<?php 
    $footer_js = array(base_url('assets/authority/js/toastr.min.js'));
    $this->load->view('include/copyright',array('footer_js'=> $footer_js));
?>
<script>
    $(document).ready(function(){
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {        
                current_password: {required: true, minlength: 6},
	            new_password: {required: true, minlength: 6},
	            confirm_new_password: {required: true, minlength: 6, equalTo: "#new_password"},    
            },
            messages: {     
            	current_password: {required: "<?= get_line_front('please_enter_current_password')?>", minlength: '<?= get_line_front('please_enter_more_than_character')?>'},
	            new_password: {required: "<?= get_line_front('please_enter_new_password')?>", minlength: '<?= get_line_front('please_enter_more_than_character')?>'},
	            confirm_new_password: {required: "<?= get_line_front('Please_enter_confirm_password')?>", minlength: '<?= get_line_front('please_enter_more_than_character')?>', equalTo: "<?= get_line_front('password_and_confirm_password_should_be_same')?>"},  
	        }  
        });
    });
</script>
<?php $this->load->view('include/footer');?>