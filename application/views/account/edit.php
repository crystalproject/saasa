<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
    $this->load->view('include/messages');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<section class="new-account-page-section">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-9 col-offset-1">
				<form id="form" method="post" class="registration-form">
					<div class="border-bottom row">
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
							<div class="form-title">
								<h4><?= get_line_front('personal_info');?></h4>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
							<div class="form-content">
								<div class="form-group row">
								    <label for="email" class="col-sm-3 col-form-label"><?= get_line_front('email_address');?></label>
								    <div class="col-sm-9">
								      <input type="email" name="email" class="form-control-plaintext" id="email" value="<?= isset($email) && $email !=null ? $email : '';?>">
								      <?= form_error('email', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
							<div class="form-title">
								<h4><?= get_line_front('my_account');?></h4>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
							<div class="form-content">
								<div class="form-group row">
								    <label for="fname" class="col-sm-3 col-form-label"><?= get_line_front('first_name');?></label>
								    <div class="col-sm-9">
								      <input type="text" name="first_name" class="form-control-plaintext" id="fname" placeholder="<?= get_line_front('first_name');?>" value="<?= isset($first_name) && $first_name !=null ? $first_name : '';?>">
								    	<?= form_error('first_name', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
								<div class="form-group row">
								    <label for="lname" class="col-sm-3 col-form-label"><?= get_line_front('last_name');?></label>
								    <div class="col-sm-9">
								      <input type="text" name="last_name" class="form-control-plaintext" id="lname" placeholder="<?= get_line_front('last_name');?>" value="<?= isset($last_name) && $last_name !=null ? $last_name : '';?>">
								    	<?= form_error('last_name', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
								<div class="form-group row">
								    <label for="gender" class="col-sm-3 col-form-label"><?= get_line_front('gender');?></label>
								    <div class="col-sm-9">
								        <select class="form-control-plaintext" name="gender" id="gender">
								        	<option value=""><?= get_line_front('gender');?></option>
									      	<option value="male" <?= isset($gender) && $gender !=null && $gender == 'male' ? 'selected' : '';?>><?= get_line_front('male');?></option>
									      	<option value="female" <?= isset($gender) && $gender !=null && $gender == 'female' ? 'selected' : '';?>><?= get_line_front('female');?></option>
									    </select>
								    	<?= form_error('gender', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
								<div class="form-group row">
							        <label class="control-label col-sm-3 " for="date"><?= get_line_front('d.o.b');?></label>
							        <div class="col-sm-9">
							            <input name="date_of_birth" class="date_of_birth form-control-plaintext" type="text" placeholder="<?= get_line_front('d.o.b');?>" value="<?= isset($date_of_birth) && $date_of_birth !=null ? date('d-m-Y',strtotime($date_of_birth)) : '';?>">
							        	<?= form_error('date_of_birth', "<label class='error'>", "</label>");?>
							        </div>
							  	</div>
							</div>
						</div>
					</div>
					<button class="form-submit-btn"><?= get_line_front('submit');?> <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
				</form>
			</div>
		</div>
	</div>
</section>
<?php 
    $footer_js = array(base_url('assets/authority/js/toastr.min.js'),'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
    $this->load->view('include/copyright',array('footer_js'=> $footer_js));
?>
<script>
    $(document).ready(function(){
    	var today = new Date();
        $(".date_of_birth").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
            dateFormat: 'dd-mm-yy',
            endDate: "today",
            maxDate: today,
            showButtonPanel: true,
        });

        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {        
                email:{required : true,email:true},  
                first_name:{required : true},      
                last_name:{required : true},     
                gender:{required : true},     
                date_of_birth:{required : true},     
            },
            messages: {          
                email: {required: "<?= get_line_front('this_field_is_required')?>", email: "<?= get_line_front('please_enter_valid_email_address')?>"},    
                first_name: {required: "<?= get_line_front('this_field_is_required')?>"},     
                last_name: {required :"<?= get_line_front('this_field_is_required')?>"},       
                gender: {required :"<?= get_line_front('this_field_is_required')?>"},       
                date_of_birth: {required :"<?= get_line_front('this_field_is_required')?>"},       
            }
        });
    });
</script>
<?php $this->load->view('include/footer');?>