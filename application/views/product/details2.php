<?php

defined('BASEPATH') or exit('No direct script access allowed');

$this->load->view('include/header');

$this->load->view('include/messages');

$is_product_available = true;

?>

<section class="product-detail-page">

	<div class="container">

		<div class="row">

			<div class="col-md-12 col-lg-12 col-sm-12">

				<div class="product-detail-page-content">

					<div class="row">

						<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 new-glass-case">

							<div class="">

								<ul id="glasscase" class="gc-start">

									<?php									

									if (isset($info['main_image']) && !empty($info['main_image'])) { 

										?>

										<li><img src="<?= $info['main_image']; ?>"></li>

										<?php

									}

									if (isset($info['other_info']['image_info']) && !empty($info['other_info']['image_info'])) {

										foreach ($info['other_info']['image_info'] as $key => $value) {

									?>

											<li><img src="<?= $value['image_name']; ?>"></li>

									<?php }

									}

									?>

									<?php

									/* (isset($info['other_info']['attribute_images']) && !empty($info['other_info']['attribute_images'])) {

										foreach ($info['other_info']['attribute_images'] as $key => $value) {

									?>

											<li><img src="<?= $value['image_name']; ?>"></li>

									<?php }

									} */

									?>

								</ul>

							</div>

						</div>

						<div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">

							<?php

							$total_price = isset($info['other_info']['prices'][0]) ? $info['other_info']['prices'][0] : null;

							$discount = isset($info['other_info']['discount'][0]) ? $info['other_info']['discount'][0] : null;

							$total_discount = calculate_discount(0, $total_price, $discount);

							$discounted_price = $total_price - $total_discount;

							?>

							<div class="product-detail-page-title">

								<h3><b><?= isset($info['title']) ? $info['title'] : ''; ?></b></h3>

								<?php

								$class = "hidden";

								if (isset($discount) && $discount > 0) {

									$class = "";

								}

								$class = "hidden";

								echo '<h5 class="' . $class . '">' . get_line_front('discount') . ': <span data-discount="true">' . $discount . '</span>%' . '</h5>';

								?>

								<?php /*<h4><b data-discounted-price="true"><?php echo $discounted_price; ?></b><strike data-total-price="true"><?php echo !empty($total_price) ? '/' . $total_price : ''; ?></strike> SAR</h4>*/ ?>

								<h4><b data-discounted-price="true"><?php echo $discounted_price.'SAR'; ?> <strike><?php echo $total_price.'SAR'; ?></strike></b></h4>

							</div>

							<div class="unavailable">

								<h3 data-availability="true" available-text="<?php echo get_line_front('available'); ?>" not-available-text="<?php echo get_line_front('not_available'); ?>">

									<?php

									if (isset($info['by_quantity_or_availability']) && $info['by_quantity_or_availability'] == '0') {

										//Availability

										if (isset($info['other_info']['is_product_available'][0]) && $info['other_info']['is_product_available'][0] == '1') {

											//echo get_line_front('available');

										} else {

											echo get_line_front('not_available');

											$is_product_available = false;

										}

									} else {

										//By Quantity

										if (isset($info['other_info']['quantity'][0]) && $info['other_info']['quantity'][0] > 0) {

											//echo get_line_front('available');

										} else {

											echo get_line_front('not_available');

											$is_product_available = false;

										}

									}

									?>

								</h3>

							</div>

							<div class="row">

								<div class="col-md-6 col-lg-6 col-sm-12">

									<div class="review-star">

										<ul>

											<li><i class="fa fa-star"></i></li>

											<li><i class="fa fa-star"></i></li>

											<li><i class="fa fa-star"></i></li>

											<li><i class="fa fa-star"></i></li>

											<li><i class="fa fa-star"></i></li>

										</ul>

										<h4 class="review">(0)</h4>

									</div>

								</div>

								<div class="col-md-6 col-lg-6 col-sm-12">

									<div class="add-to-favourite">

										<h5>

											<?php

											$get_pro_id = get_details('wishlist', array('id_product' => $info['id_unique'], 'login_id' => $this->session->userdata('login_id')));

											if (isset($get_pro_id) && $get_pro_id != null) {

											?>

												<a href="javascript:void(0)" class="wishlist" id="<?= $info['id_unique'] ?>"><i class="fa fa-heart"></i> <span><?= get_line_front('remove_favorite'); ?></span></a>

											<?php } else {

											?>

												<a href="javascript:void(0)" class="wishlist" id="<?= $info['id_unique'] ?>"><i class="fa fa-heart-o"></i> <span><?= get_line_front('add_to_favorite'); ?></span></a>

											<?php

											}

											?>

										</h5>

									</div>

								</div>

							</div>

							<div class="product-detail-page-content">

								<b>SKU: <?php echo isset($info['sku']) ? $info['sku'] : ''; ?></b><br/>

								<?php echo isset($info['description']) ? $info['description'] : ''; ?>

							</div>

							<?php

							if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {

								$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();

								if (isset($info['other_info']['attribute_info']) && !empty($info['other_info']['attribute_info'])) {

									foreach ($info['other_info']['attribute_info'] as $value) {

							?>

										<div class="choose-size row">

											<div class="choose-size-title">

												<h4><?php // echo get_line_front('choose'); 

													?><b> <?php echo $value['attribute_name']; ?></b></h4>

											</div>

											<ul class="col-md-12 choose-attribute" style="width: 100%">

												<?php

												foreach ($value['attribute_values'] as $key => $value_row) {

													$selected = '';

													if (in_array($value_row['id_unique'], $default_selected)) {

														$selected = 'active';

													}

												?>

													<li class="<?php echo $selected; ?>" data-id-unique="<?php echo $value_row['id_unique']; ?>">

														<div class="size <?php echo $value_row['image'] != '' ? 'attr-img' : ''; ?>">

															<?php

															if ($value_row['image'] != '') {

																echo '<h5><img class="attribute-image" src="' . $value_row['image'] . '" /></h5>';

															} else {

																echo '<h5>' . $value_row['attribute_value'] . '</h5>';

															}

															?>

														</div>

													</li>

												<?php } ?>

											</ul>

										</div>

							<?php }

								}

							}

							?>

							<!-- <div class="choose-design">

											<div class="form-group">

											    <label for="exampleFormControlSelect1"><b>Example select</b></label>

											    <select class="form-control" id="exampleFormControlSelect1">

											      <option><b>Design for school spongebob</b></option>

											      <option><b>Design for school spongebob</b></option>

											      <option><b>Design for school spongebob</b></option>

											      <option><b>Design for school spongebob</b></option>

											    </select>

										  	</div>

									  	</div> -->

							<div class="product-detail-button">

								<?php /*<a href="#" class="custom-design-btn"><img src="images/brush.svg"> Custom Design</a>*/ ?>

								<?php

								$class = "";

								if ($is_product_available == false) {

									$class = "disabled";

								}

								?>

								<a href="javascript:void(0)" data-id="<?php echo $info['id_unique']; ?>" class="buy-now-btn add-to-cart <?php echo $class; ?>"><?= get_line_front('add_to_cart'); ?></a>

							</div>

						</div>

					</div>

					<div class="row" style="margin-top: 17%;">

						<div class="col-md-12 ">

							<nav>

								<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">

									<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><?php echo get_line('description'); ?></a>

									<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><?php echo get_line('additional_description'); ?></a>

									<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Reviews (0)</a>

									<a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">View 360</a>

									<a class="nav-item nav-link" id="nav-video-tab" data-toggle="tab" href="#nav-video" role="tab" aria-controls="nav-video" aria-selected="false"><?php echo get_line('video'); ?></a>

								</div>

							</nav>

							<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

								<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

									<?php echo isset($info['long_description']) ? $info['long_description'] : ''; ?>

								</div>

								<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

									<?php echo isset($info['additional_description']) ? $info['additional_description'] : ''; ?>

								</div>

								<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

									Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.

								</div>

								<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">

									Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.

								</div>

								<div class="tab-pane fade" id="nav-video" role="tabpanel" aria-labelledby="nav-about-tab">

									<div class="row">

										<?php

										if (isset($info['product_video']) && !empty($info['product_video'])) {

										?>

											<div class="col-md-12">

												<video style="width:100%;height:250px;" controls="">

													<source src="<?php echo $info['product_video']; ?>">

												</video>

											</div>

										<?php

										}

										?>

									</div>

								</div>

							</div>



						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</section>

<!-- Product Detail Section Strat Here -->



<div class="clearfix"></div>

<!-- Product Detail Secction End Here -->

<?php

if (isset($related_product) && !empty($related_product)) {

?>

	<!-- Related Product Section Strat Here -->

	<section class="related-product">

		<div class="container">

			<div class="related-product">

				<div class="related-product-title">

					<h3>Related Product</h3>

				</div>

			</div>

			<div id="blogCarousel" class="carousel slide" data-ride="carousel">



				<ol class="carousel-indicators">

					<li data-target="#blogCarousel" data-slide-to="0" class="active"></li>

					<li data-target="#blogCarousel" data-slide-to="1"></li>

				</ol>



				<!-- Carousel items -->

				<div class="carousel-inner">



					<div class="carousel-item active">

						<div class="row">

							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

								<div class="mens-clothes-box">

									<ul>

										<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

										<li class="new">New</li>

									</ul>

									<div class="mens-clothes-img text-center">

										<img src="images/men-clothes-1.png">

									</div>

									<div class="mens-clothes-name text-center">

										<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

										<h4>232 SAR</h4>

									</div>

								</div>

							</div>

							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

								<div class="mens-clothes-box">

									<ul>

										<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

									</ul>

									<div class="mens-clothes-img text-center">

										<img src="images/men-clothes-1.png">

									</div>

									<div class="mens-clothes-name text-center">

										<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

										<h4>232 SAR</h4>

									</div>

								</div>

							</div>

							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

								<div class="mens-clothes-box">

									<ul>

										<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

									</ul>

									<div class="mens-clothes-img text-center">

										<img src="images/men-clothes-2.png">

									</div>

									<div class="mens-clothes-name text-center">

										<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

										<h4>232 SAR</h4>

									</div>

								</div>

							</div>

						</div>

						<!--.row-->

					</div>

					<!--.item-->



					<div class="carousel-item">

						<div class="row">

							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

								<div class="mens-clothes-box">

									<ul>

										<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

										<li class="new">New</li>

									</ul>

									<div class="mens-clothes-img text-center">

										<img src="images/men-clothes-1.png">

									</div>

									<div class="mens-clothes-name text-center">

										<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

										<h4>232 SAR</h4>

									</div>

								</div>

							</div>

							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

								<div class="mens-clothes-box">

									<ul>

										<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

									</ul>

									<div class="mens-clothes-img text-center">

										<img src="images/men-clothes-1.png">

									</div>

									<div class="mens-clothes-name text-center">

										<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

										<h4>232 SAR</h4>

									</div>

								</div>

							</div>

							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

								<div class="mens-clothes-box">

									<ul>

										<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

									</ul>

									<div class="mens-clothes-img text-center">

										<img src="images/men-clothes-2.png">

									</div>

									<div class="mens-clothes-name text-center">

										<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

										<h4>232 SAR</h4>

									</div>

								</div>

							</div>

						</div>

					</div>

					<!--.item-->



				</div>

				<!--.carousel-inner-->

			</div>

			<!-- <div class="row">

						<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

							<div class="mens-clothes-box">

								<ul>

									<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

									<li class="new">New</li>

								</ul>

								<div class="mens-clothes-img text-center">

									<img src="images/men-clothes-1.png">

								</div>

								<div class="mens-clothes-name text-center">

									<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

									<h4>232 SAR</h4>

								</div>

							</div>

						</div>

						<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

							<div class="mens-clothes-box">

								<ul>

									<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

								</ul>

								<div class="mens-clothes-img text-center">

									<img src="images/men-clothes-1.png">

								</div>

								<div class="mens-clothes-name text-center">

									<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

									<h4>232 SAR</h4>

								</div>

							</div>

						</div>

						<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

							<div class="mens-clothes-box">

								<ul>

									<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

								</ul>

								<div class="mens-clothes-img text-center">

									<img src="images/men-clothes-2.png">

								</div>

								<div class="mens-clothes-name text-center">

									<h4 style="font-size: 18px;"><b>Small pink bags brand</b></h4>

									<h4>232 SAR</h4>

								</div>

							</div>

						</div>

					</div> -->

		</div>

	</section>

<?php

}

?>

<script type="text/javascript">

	<?php 

	unset($info['description'],$info['additional_description'],$info['long_description']);

	?>

	var info = '<?php echo isset($info) ? json_encode($info) : ''; ?>';

	if (info != '') {

		info = JSON.parse(info);

		console.log(info);

	}	

</script>

<?php

$footer_js = array(base_url('assets/js/image-zoom.js'), base_url('assets/js/product.js'));

$this->load->view('include/copyright', array('footer_js' => $footer_js));

?>

<script type="text/javascript">	

	$(document).ready(function() {

		//If your <ul> has the id "glasscase"

		$('#glasscase').glassCase({

			'thumbsPosition': 'bottom',

			'widthDisplay': 560

		});

		try {

			$('#blogCarousel').carousel({

				interval: 5000

			});

		} catch (err) {}

	});

</script>

<?php $this->load->view('include/footer'); ?>