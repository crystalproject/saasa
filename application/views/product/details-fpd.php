<!DOCTYPE HTML>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Fancy Product Designer</title>

    <!-- Style sheets -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/custom-design/'); ?>css/main.css">

    <!-- The CSS for the plugin itself - required -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/custom-design/'); ?>css/FancyProductDesigner-all.min.css" />

    <!-- Include required jQuery files -->
    <script src="<?php echo base_url('assets/custom-design/'); ?>js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/custom-design/'); ?>js/jquery-ui.min.js" type="text/javascript"></script>

    <!-- HTML5 canvas library - required -->
    <script src="<?php echo base_url('assets/custom-design/'); ?>js/fabric.min.js" type="text/javascript"></script>
    <!-- The plugin itself - required -->
    <script src="<?php echo base_url('assets/custom-design/'); ?>js/FancyProductDesigner-all.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            var BASE_URL = '<?php echo base_url(); ?>';
            var $yourDesigner = $('#clothing-designer'),
                pluginOpts = {
                    productsJSON: [
                        [{
                            "title": "Sweater",
                            "thumbnail": BASE_URL + 'assets/custom-design/images/sweater/preview.png',
                            "elements": [{
                                    "type": "image",
                                    "source": BASE_URL + 'assets/custom-design/images/sweater/basic.png',
                                    "title": "Base",
                                    "parameters": {
                                        "left": 332,
                                        "top": 311,
                                        "colors": "#D5D5D5,#990000,#cccccc",
                                        "price": 20
                                    }
                                },
                                {
                                    "type": "image",
                                    "source": BASE_URL + 'assets/custom-design/images/sweater/highlights.png',
                                    "title": "Hightlights",
                                    "parameters": {
                                        "left": 332,
                                        "top": 311
                                    }
                                },
                                {
                                    "type": "image",
                                    "source": BASE_URL + 'assets/custom-design/images/sweater/shadow.png',
                                    "title": "Shadow",
                                    "parameters": {
                                        "left": 332,
                                        "top": 309
                                    }
                                }
                            ]
                        }],

                    ],
                    designsJSON: [{
                            "source": BASE_URL + 'assets/custom-design/images/designs/swirl.png',
                            "title": "Swirl",
                            "parameters": {
                                "zChangeable": true,
                                "left": 215,
                                "top": 200,
                                "colors": "#000000",
                                "removable": true,
                                "draggable": true,
                                "rotatable": true,
                                "resizable": true,
                                "price": 10,
                                "boundingBox": "Base",
                                "autoCenter": true
                            }
                        },
                        {
                            "source": BASE_URL + 'assets/custom-design/images/designs/swirl2.png',
                            "title": "Swirl 2",
                            "parameters": {
                                "left": 215,
                                "top": 200,
                                "colors": "#000000",
                                "removable": true,
                                "draggable": true,
                                "rotatable": true,
                                "resizable": true,
                                "price": 5,
                                "boundingBox": "Base",
                                "autoCenter": true
                            }
                        },
                        {
                            "source": BASE_URL + 'assets/custom-design/imagesimages/designs/swirl3.png',
                            "title": "Swirl 3",
                            "parameters": {
                                "left": 215,
                                "top": 200,
                                "colors": "#000000",
                                "removable": true,
                                "draggable": true,
                                "rotatable": true,
                                "resizable": true,
                                "autoCenter": true
                            }
                        },
                        {
                            "source": BASE_URL + 'assets/custom-design/images/designs/heart_blur.png',
                            "title": "Heart Blur",
                            "parameters": {
                                "left": 215,
                                "top": 200,
                                "colors": "#bf0200",
                                "removable": true,
                                "draggable": true,
                                "rotatable": true,
                                "resizable": true,
                                "price": 5,
                                "boundingBox": "Base",
                                "autoCenter": true
                            }
                        },
                        {
                            "source": BASE_URL + 'assets/custom-design/images/designs/converse.png',
                            "title": "Converse",
                            "parameters": {
                                "left": 215,
                                "top": 200,
                                "colors": "#000000",
                                "removable": true,
                                "draggable": true,
                                "rotatable": true,
                                "resizable": true,
                                "autoCenter": true
                            }
                        },
                        {
                            "source": BASE_URL + 'assets/custom-design/images/designs/crown.png',
                            "title": "Crown",
                            "parameters": {
                                "left": 215,
                                "top": 200,
                                "colors": "#000000",
                                "removable": true,
                                "draggable": true,
                                "rotatable": true,
                                "resizable": true,
                                "autoCenter": true
                            }
                        },
                        {
                            "source": BASE_URL + 'assets/custom-design/images/designs/men_women.png',
                            "title": "Men hits Women",
                            "parameters": {
                                "left": 215,
                                "top": 200,
                                "colors": "#000000",
                                "removable": true,
                                "draggable": true,
                                "rotatable": true,
                                "resizable": true,
                                "boundingBox": "Base",
                                "autoCenter": true
                            }
                        }
                    ],
                    langJSON: {
                        "toolbar": {
                            "fill": "Fill",
                            "font_family_search": "Search Fonts",
                            "transform": "Transform",
                            "position": "Position",
                            "move_up": "Move Up",
                            "move_down": "Move Down",
                            "reset": "Reset",
                            "font_size": "Font Size",
                            "line_height": "Line Height",
                            "bold": "Bold",
                            "italic": "Italic",
                            "underline": "Underline",
                            "text_align": "Text Alignment",
                            "stroke": "Stroke",
                            "curved_text": "Curved Text",
                            "edit_text": "Edit Text",
                            "color": "Color",
                            "patterns": "Patterns",
                            "transparency": "Transparency",
                            "align_left": "Align Left",
                            "align_top": "Align Top",
                            "align_right": "Align Right",
                            "align_bottom": "Align Bottom",
                            "center_h": "Center Horizontal",
                            "center_v": "Center Vertical",
                            "flip_h": "Flip Horizontal",
                            "flip_v": "Flip Vertical",
                            "curved_text_switch": "Switch",
                            "curved_text_reverse": "Reverse",
                            "radius": "Radius",
                            "spacing": "Spacing",
                            "letter_spacing": "Letter Spacing",
                            "advanced_editing": "Advanced Editing",
                            "text_transform": "Text Transform",
                            "stroke_width": "Width",
                            "back": "Back",
                            "shadow": "Shadow",
                            "shadow_blur": "Blur",
                            "shadow_offset_x": "Offset X",
                            "shadow_offset_y": "Offset Y"
                        },
                        "actions": {
                            "reset_product": "Reset",
                            "zoom": "Zoom",
                            "magnify_glass": "Magnify Glass",
                            "download": "Download",
                            "download_current_view": "Only export current showing view",
                            "info": "Info",
                            "info_content": "Content for the Info Action Button. Change content via language JSON.",
                            "print": "Print",
                            "save": "Save",
                            "load": "Load Designs",
                            "manage_layers": "Manage Layers",
                            "qr_code": "Add QR-Code",
                            "qr_code_input": "Enter a URL, some text...",
                            "qr_code_add_btn": "Add QR-Code",
                            "undo": "Undo",
                            "redo": "Redo",
                            "snap": "Center Snap",
                            "preview_lightbox": "Preview",
                            "save_placeholder": "Optional: Enter a title",
                            "ruler": "Toggle Ruler",
                            "previous_view": "Previous View",
                            "next_view": "Next View",
                            "start_guided_tour": "Start Guided Tour",
                            "load_designs_empty": "No Designs saved yet!"
                        },
                        "modules": {
                            "products": "Swap Product",
                            "products_confirm_replacement": "Are you sure to replace the product?",
                            "products_confirm_button": "Yes, please!",
                            "images": "Add Image",
                            "upload_zone": "Click or drop images here",
                            "facebook_select_album": "Select An Album",
                            "text": "Add Text",
                            "text_input_placeholder": "Enter some text",
                            "enter_textbox_width": "Optional: Enter a fixed width",
                            "text_add_btn": "Add Text",
                            "designs": "Choose From Designs",
                            "designs_search_in": "Search in",
                            "manage_layers": "Manage Layers",
                            "images_agreement": "I have the rights to use the images.",
                            "images_confirm_button": "Confirm",
                            "images_pdf_upload_info": "Creating images from PDF...",
                            "pixabay_search": "Search in Pixabay library",
                            "depositphotos_search": "Search in depositphotos.com",
                            "depositphotos_search_category": "Search In ",
                            "text_layers_clear": "Clear",
                            "layouts": "Layouts",
                            "layouts_confirm_replacement": "Are you sure to replace all elements in the current view?",
                            "layouts_confirm_button": "Yes, got it!"
                        },
                        "image_editor": {
                            "mask": "Mask",
                            "filters": "Filters",
                            "color_manipulation": "Color Manipulation",
                            "custom_cropping": "Custom Cropping",
                            "filter_none": "None",
                            "brightness": "Brightness",
                            "contrast": "Contrast",
                            "remove_white": "Remove White",
                            "remove_white_distance": "Distance",
                            "restore": "Restore Original",
                            "save": "Save"
                        },
                        "misc": {
                            "initializing": "Initializing Product Designer",
                            "out_of_bounding_box": "Move element in its containment!",
                            "product_saved": "Product Saved!",
                            "loading_image": "Loading Image...",
                            "uploaded_image_size_alert": "Sorry! But the uploaded image size does not conform our indication of size.<br />Minimum Width: %minW pixels<br />Minimum Height: %minH pixels<br />Maximum Width: %maxW pixels<br />Maximum Height: %maxH pixels",
                            "modal_done": "Done",
                            "minimum_dpi_info": "The JPEG image does not have the required DPI of %dpi.",
                            "maximum_size_info": "The image %filename exceeds the maximum file size of %mbMB.",
                            "customization_required_info": "You need to customize the default design!",
                            "not_supported_device_info": "Sorry! But the product designer can not be displayed on your device. Please use a device with a larger screen!",
                            "image_added": "Image Added!",
                            "reset_confirm": "Are you sure to reset everything?",
                            "popup_blocker_alert": "Please disable your pop-up blocker and try again.",
                            "view_optional_unlock": "Unlock view",
                            "guided_tour_back": "Back",
                            "guided_tour_next": "Next"
                        },
                        "plus": {
                            "names_numbers": "Names & Numbers",
                            "names_numbers_add_new": "Add New",
                            "drawing": "Free Drawing",
                            "drawing_brush_type": "Brush Type",
                            "drawing_pencil": "Pencil",
                            "drawing_circle": "Circle",
                            "drawing_spray": "Spray",
                            "drawing_color": "Color",
                            "drawing_line_width": "Line Width",
                            "drawing_draw_here": "Draw Here",
                            "drawing_clear": "Clear",
                            "drawing_add": "Add",
                            "bulk_add_variations_title": "Bulk Order",
                            "bulk_add_variations_add": "Add",
                            "dynamic_views": "Manage Views",
                            "dynamic_views_add_view_info": "Add new view",
                            "dynamic_views_add_blank": "Blank",
                            "dynamic_views_add_from_layouts": "From Layouts"
                        }
                    },
                    stageWidth: 1200,
                    editorMode: false,
                    smartGuides: true,
                    templatesDirectory: BASE_URL + 'assets/custom-design/html/',
                    fonts: [{
                            name: 'Helvetica'
                        },
                        {
                            name: 'Times New Roman'
                        },
                        {
                            name: 'Pacifico',
                            url: BASE_URL + 'assets/custom-design/fonts/Pacifico.ttf',
                        },
                        {
                            name: 'Arial'
                        },
                        {
                            name: 'Lobster',
                            url: 'google'
                        }
                    ],
                    customTextParameters: {
                        colors: false,
                        removable: true,
                        resizable: true,
                        draggable: true,
                        rotatable: true,
                        autoCenter: true,
                        boundingBox: "Base"
                    },
                    customImageParameters: {
                        draggable: true,
                        removable: true,
                        resizable: true,
                        rotatable: true,
                        colors: '#000',
                        autoCenter: true,
                        boundingBox: "Base"
                    },
                    actions: {
                        'top': ['download', 'print', 'snap', 'preview-lightbox'],
                        'right': ['magnify-glass', 'zoom', 'reset-product', 'qr-code', 'ruler'],
                        'bottom': ['undo', 'redo'],
                        'left': ['manage-layers', 'info', 'save', 'load']
                    }
                },

                yourDesigner = new FancyProductDesigner($yourDesigner, pluginOpts);

            //print button
            $('#print-button').click(function() {
                yourDesigner.print();
                return false;
            });

            //create an image
            $('#image-button').click(function() {
                var image = yourDesigner.createImage();
                return false;
            });

            //checkout button with getProduct()
            $('#checkout-button').click(function() {
                var product = yourDesigner.getProduct();
                console.log(product);
                return false;
            });

            //event handler when the price is changing
            $yourDesigner.on('priceChange', function(evt, price, currentPrice) {
                $('#thsirt-price').text(currentPrice);
            });

            //save image on webserver
            $('#save-image-php').click(function() {

                yourDesigner.getProductDataURL(function(dataURL) {
                    $.post("php/save_image.php", {
                        base64_image: dataURL
                    });
                });

            });

            //send image via mail
            $('#send-image-mail-php').click(function() {

                yourDesigner.getProductDataURL(function(dataURL) {
                    $.post("php/send_image_via_mail.php", {
                        base64_image: dataURL
                    });
                });

            });

        });
    </script>
</head>

<body>
    <div id="main-container">
        <h3 id="clothing">Clothing Designer</h3>
        <div id="clothing-designer" class="fpd-container fpd-shadow-2 fpd-topbar fpd-tabs fpd-tabs-side fpd-top-actions-centered fpd-bottom-actions-centered fpd-views-inside-left"> </div>
        <br />

        <div class="fpd-clearfix" style="margin-top: 30px;">
            <div class="api-buttons fpd-container fpd-left">
                <a href="#" id="print-button" class="fpd-btn">Print</a>
                <a href="#" id="image-button" class="fpd-btn">Create Image</a>
                <a href="#" id="checkout-button" class="fpd-btn">Checkout</a>
                <a href="#" id="recreation-button" class="fpd-btn">Recreate product</a>
            </div>
            <div class="fpd-right">
                <span class="price badge badge-inverse"><span id="thsirt-price"></span> $</span>
            </div>
        </div>

        <p class="fpd-container">
            Only working on a webserver:<br />
            <span class="fpd-btn" id="save-image-php">Save image with php</span>
            <span class="fpd-btn" id="send-image-mail-php">Send image to mail</span>
        </p>

    </div>
</body>

</html>