<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
?>
<!-- <?php
    if (isset($banner) && $banner != null) {
    ?>
    <div class="section banner-page" data-background="<?= base_url(CAT_SUB_CAT_BANNER . $banner[0]['banner' . get_language_front()]); ?>">
        <div class="content-wrap pos-relative">
            <div class="d-flex justify-content-center bd-highlight mb-2">
                <div class="title-page"><?= $banner[0]['name' . get_language_front()]; ?></div>
            </div>
            <p class="text-center text-white">Your pet's health and well-being are our top priority.</p>
        </div>
    </div>
<?php } ?> -->
<div class="section banner-page" data-background="<?=base_url('assets/images/statistic_bg.jpg')?>">
    <div class="content-wrap pos-relative">
        <div class="d-flex justify-content-center bd-highlight mb-2">
            <div class="title-page">Foods</div>
        </div>
        <p class="text-center text-white">Your pet's health and well-being are our top priority.</p>
    </div>
</div>
<!-- Breadcrumb Start -->
<div class="section bg-breadcrumb">
    <div class="content-wrap py-0 pos-relative">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="shop-list.html">Dogs</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Foods</li>
                </ol>
            </nav>                  
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<!-- Product List Start -->
<div id="class" class="product-list">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-3 col-md-3">
                    <p class="title-heading text-secondary">
                        <?=get_line_front('filter_by')?>
                    </p>
                    <div class="accordion rs-accordion" id="accordionExample">
                        <!-- Item 1 -->
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h3 class="title">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <?=get_line_front('price')?>
                                    </button>
                                </h3>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div id="slider-range"></div>
                                    <div class="row slider-labels">
                                        <div class="col-sm-6 caption">
                                            <span id="slider-range-value1"></span>
                                        </div>
                                        <div class="col-sm-6 text-right caption">
                                            <span id="slider-range-value2"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <form>
                                                <input type="hidden" name="min-value" value="">
                                                <input type="hidden" name="max-value" value="">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Item 2 -->
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h3 class="title">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <?=get_line_front('category')?>
                                    </button>
                                </h3>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample" >
                                <div class="card-body">
                                    <?php
                                    function get_category_tree_select($first_category_id, $sub_mark = '')
                                    {
                                        $CI = &get_instance();
                                        $details = get_details('category', array('parent_id' => $first_category_id));
                                        if (isset($details) && $details != null) {
                                            foreach ($details as $key => $value) {
                                                $selected = $GLOBALS['current_category'] == $value['id'] ? 'selected="selected"' : '';
                                                // echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($sub_mark) . trim($value['name' . get_language_front()]) . '</option>';
                                                // echo trim($sub_mark);

                                                ?>
                                                    <label class="cust-check" style="margin-left: 30px">
                                                        <?=trim($value['name' . get_language_front()])?>
                                                        <input type="checkbox" name="category" value="<?=$value['id']?>" <?=$checked?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                <?php 
                                                get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                            }
                                        }
                                    }
                                    //echo '<option value="">' . get_line('select') . '</option>';
                                    $sub_mark = '';
                                    $temp = get_details('category', array('id' => $first_id));
                                    if (isset($temp) && $temp != null) {
                                        foreach ($temp as $key => $value) {
                                            $checked = $GLOBALS['current_category'] == $value['id'] ? 'checked="checked"' : '';
                                            // echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($value['name' . get_language_front()]) . '</option>';
                                            ?>
                                            <label class="cust-check">
                                                <?=trim($value['name' . get_language_front()])?>
                                                <input type="checkbox" name="category" value="<?=$value['id']?>" <?=$checked?>>
                                                <span class="checkmark"></span>
                                            </label>
                                            <?php
                                            get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                        }
                                    }
                                    $sub_mark = '';
                                    $info = $this->common_model->select_data('category', array('WHERE' => array('parent_id' => 0)));
                                    if ($info['row_count'] > 0) {
                                        foreach ($info['data'] as $key => $value) { ?>
                                            <label class="cust-check">
                                                <?=trim($value['name' . get_language_front()])?>
                                                <input type="checkbox" name="category" value="<?=$value['id']?>" >
                                                <span class="checkmark"></span>
                                            </label>
                                            <?php

                                            // echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($value['name' . get_language_front()]) . '</option>';
                                            get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                        }
                                    }
                                    ?>
                                    <!-- <label class="cust-check">
                                        Dry grain food
                                        <input type="checkbox" checked>
                                        <span class="checkmark"></span>
                                    </label> -->
                                        <!-- <label class="cust-check">
                                            Dry grain free(72)
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cust-check">
                                            Wet chunks in gravy(11)
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cust-check">
                                            Wet gain food(5)
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cust-check">
                                            Wet gain free(25)
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label> -->
                                </div>
                            </div>
                        </div>
                        <!-- Item 3 -->
                        <?php 
                            $conditions = array('SELECT' => 'id_unique,attribute_name', 'WHERE' => array('id_language' => get_language_id()));
                            $attribute_info = $this->common_model->select_data('attributes', $conditions);

                            // echo "<pre>";print_r($attribute_info);exit;
                            foreach ($attribute_info['data'] as $key => $value) { ?>
                                <div class="card">
                                    <div class="card-header" id="<?='attr'.$value['id_unique']?>">
                                        <h3 class="title">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#<?='attr'.$value['id_unique'].'1'?>" aria-expanded="false" aria-controls="<?='attr'.$value['id_unique'].'1'?>">
                                                <?=$value['attribute_name']?>
                                            </button>
                                        </h3>
                                    </div>
                                    <div id="<?='attr'.$value['id_unique'].'1'?>" class="collapse" aria-labelledby="<?='attr'.$value['id_unique']?>" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <?php
                                                $conditions = array('SELECT' => 'id_unique,attribute_value', 'WHERE' => array('id_language' => get_language_id(),'id_unique_attributes'=>$value['id_unique']));
                                                $attribute_val = $this->common_model->select_data('attribute_values', $conditions);
                                                // echo "<pre>";print_r($attribute_val_info);exit;
                                                foreach ($attribute_val['data'] as $attr_key => $attribute_value) {
                                                    ?>
                                                        <label class="cust-check">
                                                            <?=$attribute_value['attribute_value']?>
                                                            <input type="checkbox" name="attribute" value="<?=$attribute_value['id_unique']?>">
                                                                <span class="checkmark"></span>
                                                        </label>                                    
                                                    <?php
                                                }
                                            ?>
                                                
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                        
                        <!-- Item 4 -->
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h3 class="title">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Brands
                                    </button>
                                </h3>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="form-group">
                                        <input type="text" name="search_brand" class="form-control" placeholder="Search Brand">
                                    </div>
                                    <div class="filter-brand">
                                        <label class="cust-check">
                                            Pedigree
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <!-- <label class="cust-check">
                                        Himalaya
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="cust-check">
                                        Gnawlers
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="cust-check">
                                        Goodies
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="cust-check">
                                        First Bark
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="cust-check">
                                        Royal Canin
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end sidebar -->

                <div class="col-12 col-sm-9 col-md-9">

                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <p class="title-heading text-secondary">
                                <?=get_line_front('food')?> <?=get_line_front('products')?>
                            </p>
                            <p>Showing <strong class="text-black">1-2</strong> of 15 <?=get_line_front('products')?></p>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="sort-div">
                                <p class="title-heading text-secondary">
                                    Sort By:
                                </p>
                                <div class="select-wrap">
                                    <select class="form-control-plaintext sorting_filter" name="">
                                        <option value="">Sort</option>
                                        <option value="id desc">New</option>
                                        <!--<option value="price_asc">Price : low to high</option>
                                        <option value="price_desc">Price : high to low</option>-->
                                        <option value="title asc">Name : A to Z</option>
                                        <option value="title desc">Name : Z to A</option>
                                    </select>
                                    <!-- <select class="form-control">
                                        <option>Price low to high</option>
                                        <option>Price high to low</option>
                                        <option>Popularity</option>
                                        <option>Discount</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row filter-result">
                        <?php
                            if (isset($details) && $details != null) {
                                foreach ($details as $key => $value) {
                                    $id = $value['id_unique'];
                                    $id_unique = $value['id_unique'];
                                    ?>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="rs-shop-box mb-5">
                                            <?php
                                            $get_pro_id = get_details('wishlist', array('id_product' => $id, 'login_id' => get_front_login_id()));
                                            if (isset($get_pro_id) && $get_pro_id != null) { ?>
                                                <a href="javascript:void(0)" class="link-wish wishlist" id="<?= $id ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                            <?php } else { ?>
                                                <a href="javascript:void(0)" class="link-wish wishlist" id="<?= $id ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                            <?php } ?>
                                            
                                            <div class="media">
                                                <a href="<?= $current_url . '/' . $value['slug'] . '/' . $id_unique; ?>"><img src="<?= $value['main_image']; ?>" alt="" class="img-fluid"></a>
                                            </div>
                                            <div class="body-text">
                                                <h4 class="title"><a href="#">Grass fed lamb 6Kg</a></h4>
                                                <div class="meta">
                                                    <div class="price">$29</div>
                                                    <div class="rating">
                                                        <a href="#" class="btn btn-cart">Add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                    <!-- end shop -->

                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <nav aria-label="Page navigation">
                              <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                              </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Product List End -->
<section class="mens-clothes-section">
    <div class="container">
        <div class="row no-breadcrumbs">
            <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                <div class="mens-clothes-section-sidebar category">
                    <div class="sidebar-link sidebar-link-new">
                        <?php
                        function get_category_tree($first_category_id, $display = true)
                        {
                            $CI = &get_instance();
                            $details = get_details('category', array('parent_id' => $first_category_id));
                            if (isset($details) && $details != null) {
                                foreach ($details as $key => $value) {
                                    $angle_class = 'right';
                                    $angle_class = $value['id'] <= $GLOBALS['first_id'] ?  'right' : 'down';                                    
                                    $style = '';
                                    if ($display == false) {
                                        $style = 'display:none;';
                                    } else {
                                        $style = $value['id'] <= $GLOBALS['first_id']  ?  'display:none;' : '';
                                    }
                        ?>
                                    <ul style="<?php echo $style; ?>">
                                        <?php
                                        $li_class = "";
                                        $temp = $CI->common_model->select_data('category', array('where' => array('parent_id' => $value['id'])));
                                        if ($temp['row_count'] > 0) {
                                            $li_class = '<i class="fa fa-angle-' . $angle_class . '"></i>';
                                        }
                                        ?>
                                        <li data-category-id="<?php echo $value['id']; ?>"><span><?php echo $value['name' . get_language_front()]; ?></span> <?php echo $li_class; ?></li>
                                        <?php
                                        get_category_tree($value['id'], $display);
                                        ?>
                                    </ul>
                            <?php
                                }
                            }
                        }
                        if (isset($first_category_info) && !empty($first_category_info)) {
                            ?>
                            <ul>
                                <li data-category-id="<?php echo $first_category_info['id']; ?>"><span><?php echo $first_category_info['name' . get_language_front()] ?></span> <i class="fa fa-angle-down"></i></li>
                                <?php
                                get_category_tree($first_id);
                                ?>
                            </ul>
                            <?php
                        }
                        $info = $this->common_model->select_data('category', array('WHERE' => array('parent_id' => 0, 'id != ' => $first_id)));
                        if ($info['row_count'] > 0) {
                            foreach ($info['data'] as $key => $value) {
                            ?>
                                <ul>
                                    <li data-category-id="<?php echo $value['id']; ?>"><span><?php echo $value['name' . get_language_front()] ?></span> <i class="fa fa-angle-right"></i></li>
                                    <?php
                                    get_category_tree($value['id'], $display = false);
                                    ?>
                                </ul>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="main-content col-md-9 col-lg-9 col-sm-12 col-xs-12">
                <div class="filter">
                    <form>
                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext " name="category" data-size="7" data-live-search="true" data-title="<?php echo get_line('select'); ?>" id="state_list" data-width="100%">
                                    <?php
                                    
                                    //echo '<option value="">' . get_line('select') . '</option>';
                                    $sub_mark = '';
                                    $temp = get_details('category', array('id' => $first_id));
                                    if (isset($temp) && $temp != null) {
                                        foreach ($temp as $key => $value) {
                                            $selected = $GLOBALS['current_category'] == $value['id'] ? 'selected="selected"' : '';
                                            echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($value['name' . get_language_front()]) . '</option>';
                                            get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                        }
                                    }
                                    $sub_mark = '';
                                    $info = $this->common_model->select_data('category', array('WHERE' => array('parent_id' => 0, 'id != ' => $first_id)));
                                    if ($info['row_count'] > 0) {
                                        foreach ($info['data'] as $key => $value) {
                                            echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($value['name' . get_language_front()]) . '</option>';
                                            get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                        }
                                    }
                                    ?>
                                </select>

                            </div>
                            <?php
                            if (isset($attribute_info) && !empty($attribute_info)) {
                                foreach ($attribute_info as $value) {
                            ?>
                                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                        <select class="form-control-plaintext attribute-filter" name="<?php echo $value['id_unique']; ?>">
                                            <option value=""><?php echo $value['attribute_name']; ?></option>
                                            <?php
                                            foreach ($value['attribute_info'] as $a_value) {
                                                echo '<option value="' . $a_value['id_unique'] . '">' . $a_value['attribute_value'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                            <?php
                                }
                            }
                            ?>

                            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext sorting_filter" name="">
                                    <option value="">Sort</option>
                                    <option value="id desc">New</option>
                                    <!--<option value="price_asc">Price : low to high</option>
                                    <option value="price_desc">Price : high to low</option>-->
                                    <option value="title asc">Name : A to Z</option>
                                    <option value="title desc">Name : Z to A</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    
                </div>
                <div class="row filter-result">
                    <?php
                    if (isset($details) && $details != null) {
                        foreach ($details as $key => $value) {
                            $id = $value['id_unique'];
                            $id_unique = $value['id_unique'];
                    ?>
                            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                                <div class="mens-clothes-box">
                                    <ul>
                                        <li>
                                            <?php
                                            $get_pro_id = get_details('wishlist', array('id_product' => $id, 'login_id' => get_front_login_id()));
                                            if (isset($get_pro_id) && $get_pro_id != null) { ?>
                                                <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                            <?php } else { ?>
                                                <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                            <?php } ?>
                                        </li>
                                        <?php
                                        if (isset($value['is_new']) && $value['is_new'] == '1') {
                                        ?>
                                            <li class="new"><?php echo get_line('new'); ?></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                    <a href="<?= $current_url . '/' . $value['slug'] . '/' . $id_unique; ?>" style="color: #000;">
                                        <div class="mens-clothes-img text-center img-effect">
                                            <img src="<?= $value['main_image']; ?>">
                                        </div>
                                        <div class="mens-clothes-name text-center">
                                            <h4 style="font-size: 18px;"><b><?= $value['title']; ?></b></h4>
                                            <h4><?= $value['other_info']['final_price'][0]; ?> SAR</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                    <?php }
                    } else {
                        echo '<div class="col-md-12">';
                        echo "<h3>" . get_line_front('product_not_available') . "</h3>";
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var current_url = '<?php echo $current_url; ?>';
</script>
<?php
$footer_js = array(base_url('assets/js/bootstrap-select.js'),base_url('assets/js/range-slider.js'));
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(window).scroll(function() {

            // if ($(window).scrollTop() > 200) {
            //     $('.mens-clothes-section-sidebar').css('position', 'fixed');
            //     $('.mens-clothes-section-sidebar').css('top', '0');
            //     $('.mens-clothes-section-sidebar').css('width', '20%');
            // } else if ($(window).scrollTop() <= 200) {
            //     $('.mens-clothes-section-sidebar').css('position', '');
            //     $('.mens-clothes-section-sidebar').css('top', '');
            //     $('.mens-clothes-section-sidebar').css('width', '');
            // }
            // if ($('.mens-clothes-section-sidebar').offset().top + $(".mens-clothes-section-sidebar").height() > $(".footer-wrapper").offset().top) {
            //     $('.mens-clothes-section-sidebar').css('top', -($(".mens-clothes-section-sidebar").offset().top + $(".mens-clothes-section-sidebar").height() - $("#footer").offset().top));
            // }
        });

        $('.sidebar-link-new li i').click(function() {
            //$(this).find('i').toggleClass('fa fa-angle-right fa fa-angle-down');
            //$(this).nextAll('ul').slideToggle('slow');
            $(this).toggleClass('fa fa-angle-right fa fa-angle-down');
            $(this).closest('li').nextAll('ul').slideToggle('slow');
        });
    });
</script>
<?php $this->load->view('include/footer'); ?>