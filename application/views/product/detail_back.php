<?php
defined('BASEPATH') or exit('No direct script access allowed');
$header_css = array(base_url('assets/custom-design/css/main.css'), base_url('assets/custom-design/css/FancyProductDesigner-all.min.css'),base_url('assets/css/zoomy.css'));
$this->load->view('include/header', array('header_css' => $header_css));
$this->load->view('include/messages');
$is_product_available = true;
?>

<!-- Breadcrumb Start -->
<div class="section bg-breadcrumb">
	<div class="content-wrap py-0 pos-relative">
		<div class="container">
		    <nav aria-label="breadcrumb">
			  	<ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
				    <li class="breadcrumb-item"><a href="<?=base_url()?>">Dogs</a></li>
				    <li class="breadcrumb-item"><a href="<?=base_url()?>">Foods</a></li>
		    		<?php
				    	if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {
							$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();
							$attribute_value = isset($info['other_info']['attribute_info'][0]['attribute_values'][0]['attribute_value'])?$info['other_info']['attribute_info'][0]['attribute_values'][0]['attribute_value']:'';
						}
					?>
				    <li class="breadcrumb-item active" aria-current="page"><?= isset($info['title']) ? $info['title'] : ''; ?> <?=(isset($attribute_value))?$attribute_value:''?></li>
			  	</ol>
			</nav>					
		</div>
	</div>
</div>
<!-- Breadcrumb End -->

<?php //echo "<pre>"; print_r($info); ?>
<section class="product-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div id='el'></div>
			</div>
			<div class="col-md-6">
				<div class="product-des">
					<?php
						
						$total_price = isset($info['other_info']['prices'][0]) ? $info['other_info']['prices'][0] : null;
						$discount = isset($info['other_info']['discount'][0]) ? $info['other_info']['discount'][0] : null;
						$total_discount = calculate_discount(0, $total_price, $discount);
						$discounted_price = $total_price - $total_discount;
						$price_text = '';
						if ($discounted_price == $total_price) {
							$price_text = PRICE_KEY .' '.$discounted_price ;
						} else {
							$price_text = PRICE_KEY .' '.$discounted_price .' <strike>' .PRICE_KEY .' '.$total_price . '</strike>';
						}
					?>
					<?php
						$class = "hidden";
						if (isset($discount) && $discount > 0) {
							$class = "";
						}
						$class = "hidden";
						echo '<h5 class="' . $class . '">' . get_line_front('discount') . ': <span data-discount="true">' . $discount . '</span>%' . '</h5>';
					?>
					<h3>
						<?= isset($info['title']) ? $info['title'] : ''; ?>
					</h3>
					<h3 class="<?php echo $price_class; ?>" data-discounted-price="true">
						<?php
							if (isset($info['other_info']['quantity'][0]) && $info['other_info']['quantity'][0] > 0) {
								$price_class = "";
							}else{
								$price_class = "hidden";
							}
							?>
							
							<?php echo $price_text; ?>
					</h3>
					<?php
						// echo "<pre>";print_r($info['other_info']);exit;
						if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {
							$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();
							if (isset($info['other_info']['attribute_info']) && !empty($info['other_info']['attribute_info'])) {
								?>
								<?php
								foreach ($info['other_info']['attribute_info'] as $value) {
									$temp = array();

									
									?>
										<label><?php echo $value['attribute_name']; ?></label>
										<div class="select-wrap">
											<select class="form-control">
												<?php
													foreach ($value['attribute_values'] as $key => $value_row) {
														$temp[] = $value_row['attribute_value'];
														$selected = ''; 
														if (in_array($value_row['id_unique'], $default_selected)) {
															$selected = 'selected';
														}?>
														<option <?=$selected?>><?=$value_row['attribute_value']?></option>	
														<?php 
													}
												?>
											</select>
										</div>	
									<?php
								}
							}
						}
					?>
					<div class="row">
						<div class="col-md-6">
							<a href="javascript:void(0);" class="btn btn-cart">add to cart</a>
						</div>
						<div class="col-md-6">
							<a href="javascript:void(0);" class="btn btn-cart">buy now</a>
						</div>
					</div>
					<div class="stock">
						<p>
							Available: 
							<span>
								<i class="fa fa-check"></i>
								In Stock
							</span>
						</p>
					</div>
					<div class="accordion rs-accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h3 class="title">
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Description
									</button>
								</h3>
							</div>
							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
									<?php echo isset($info['description']) ? $info['description'] : ''; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="product-detail-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="product-detail-page-content">
					<div class="row">
						<div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 new-glass-case">
							<div class="">
								<ul id="glasscase" class="gc-start">
									<?php
									if (isset($info['main_image']) && !empty($info['main_image'])) {
									?>
										<li><img src="<?= $info['main_image']; ?>"></li>
										<?php
									}
									if (isset($info['other_info']['image_info']) && !empty($info['other_info']['image_info'])) {
										foreach ($info['other_info']['image_info'] as $key => $value) {
										?>
											<li><img src="<?= $value['image_name']; ?>"></li>
									<?php }
									}
									?>
									<?php
									if (isset($info['other_info']['attribute_images']) && !empty($info['other_info']['attribute_images'])) {
										foreach ($info['other_info']['attribute_images'] as $key => $value) {
									?>
											<li><img src="<?= $value['image_name']; ?>"></li>
									<?php }
									}
									?>
								</ul>
							</div>
						</div>
						<div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
							<?php
								$total_price = isset($info['other_info']['prices'][0]) ? $info['other_info']['prices'][0] : null;
								$discount = isset($info['other_info']['discount'][0]) ? $info['other_info']['discount'][0] : null;
								$total_discount = calculate_discount(0, $total_price, $discount);
								$discounted_price = $total_price - $total_discount;
								$price_text = '';
								if ($discounted_price == $total_price) {
									$price_text = $discounted_price . PRICE_KEY . '';
								} else {
									$price_text = $discounted_price . PRICE_KEY . '<strike>' . $total_price . PRICE_KEY . '</strike>';
								}
							?>
							<div class="product-detail-page-title">
								<h3><b><?= isset($info['title']) ? $info['title'] : ''; ?></b></h3>
								<?php
								$class = "hidden";
								if (isset($discount) && $discount > 0) {
									$class = "";
								}
								$class = "hidden";
								echo '<h5 class="' . $class . '">' . get_line_front('discount') . ': <span data-discount="true">' . $discount . '</span>%' . '</h5>';
								?>
								<?php /*<h4><b data-discounted-price="true"><?php echo $discounted_price; ?></b><strike data-total-price="true"><?php echo !empty($total_price) ? '/' . $total_price : ''; ?></strike> SAR</h4>*/ ?>
								
								<?php
								if (isset($info['other_info']['quantity'][0]) && $info['other_info']['quantity'][0] > 0) {
									$price_class = "";
								}else{
									$price_class = "hidden";
								}
								?>
								
								<h4 class="<?php echo $price_class; ?>"><b data-discounted-price="true"><?php echo $price_text; ?></b></h4>
								
									
								
							</div>
							<div class="unavailable">
								<h3 data-availability="true" available-text="<?php echo get_line_front('available'); ?>" not-available-text="<?php echo get_line_front('not_available'); ?>">
									<?php
									if (isset($info['by_quantity_or_availability']) && $info['by_quantity_or_availability'] == '0') {
										//Availability
										if (isset($info['other_info']['is_product_available'][0]) && $info['other_info']['is_product_available'][0] == '1') {
											//echo get_line_front('available');
										} else {
											echo get_line_front('not_available');
											$is_product_available = false;
										}
									} else {
										//By Quantity
										if (isset($info['other_info']['quantity'][0]) && $info['other_info']['quantity'][0] > 0) {
											//echo get_line_front('available');
										} else {
											echo get_line_front('not_available');
											$is_product_available = false;
										}
									}
									?>
								</h3>
							</div>
							<div class="row">
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="review-star">
										<ul>
											<?php
											for ($i = 1; $i <= 5; $i++) {
												$class = '';
												if ($i <= $info['average_review']) {
													$class = 'fa fa-star';
												} else if ($info['average_review'] == ($i - 0.5)) {
													$class = 'fa fa-star-half-o';
												} else {
													$class = 'fa fa-star-o';
												}
												echo '<li><i class="' . $class . '"></i></li>';
											}
											?>

										</ul>
										<p class="review"><a href="javascript:void(0);" style="color: #999;">(<?php echo $info['total_review'] > 0 ? $info['total_review'] : get_line('no_reviews_yet'); ?>)</a></p>
									</div>
								</div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="add-to-favourite">
										<h5>
											<?php
											$get_pro_id = get_details('wishlist', array('id_product' => $info['id_unique'], 'login_id' => get_front_login_id()));
											if (isset($get_pro_id) && $get_pro_id != null) {
											?>
												<a href="javascript:void(0)" class="wishlist" id="<?= $info['id_unique'] ?>"><i class="fa fa-heart"></i> <span><?= get_line_front('remove_favorite'); ?></span></a>
											<?php } else {
											?>
												<a href="javascript:void(0)" class="wishlist" id="<?= $info['id_unique'] ?>"><i class="fa fa-heart-o"></i> <span><?= get_line_front('add_to_favorite'); ?></span></a>
											<?php
											}
											?>
										</h5>
									</div>
								</div>
							</div>
							<div class="product-detail-page-content">
							<?php
							if (isset($info['other_info']['quantity'][0]) && $info['other_info']['quantity'][0] > 0) {
								$sku_class = "";
							}else{
								$sku_class = "hidden";
							}
							?>
								<span class="sku <?php echo $sku_class;?>"><b>SKU: <?php echo isset($info['sku']) ? $info['sku'] : ''; ?></b><br /></span>
							
							
							
								<?php echo isset($info['description']) ? $info['description'] : ''; ?>
								
								<!--<button type="button" class="btn btn-customize" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-edit"></i> &nbsp; Customize </button>-->
							</div>
							<?php
							if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {
								$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();
								if (isset($info['other_info']['attribute_info']) && !empty($info['other_info']['attribute_info'])) {
									foreach ($info['other_info']['attribute_info'] as $value) {
										$attribute_main_class = '';
										$ul_class = '';
										foreach ($value['attribute_values'] as $key => $value_row) {
											$selected = '';
											if (in_array($value_row['id_unique'], $default_selected)) {
												$selected = 'active';
											}
											if ($value_row['image'] != '') {
												$attribute_main_class = 'choose-color';
											} else {
												$attribute_main_class = 'choose-size2';
												$ul_class = 'filter-item-list';
											}
										}
							?>
										<div class="<?php echo $attribute_main_class; ?>">
											<div class="choose-color-title">
												<p class="text-uppercase" style="color: #000;"><b><?php echo get_line('choose'); ?> <?php echo $value['attribute_name']; ?>:</b></p>
											</div>

											<ul class="<?php echo $ul_class;  ?> choose-attribute">
												<?php
												foreach ($value['attribute_values'] as $key => $value_row) {
													$selected = '';
													if (in_array($value_row['id_unique'], $default_selected)) {
														$selected = 'active-new-design';
													}
												?>

													<li data-id-unique="<?php echo $value_row['id_unique']; ?>" class=" <?php echo $selected; ?> <?php echo $value_row['image'] != '' ? 'variable-item color-variable-item color-variable-item-pink' : ''; ?>">
														<?php
														if ($value_row['image'] != '') {
															echo '<img class="attribute-image" src="' . $value_row['image'] . '" />';
														} else {
															echo  '<a href="javascript:void(0);" class="filter-item enabled  ">' . $value_row['attribute_value'] . '</a>';
														}
														?>
													</li>
												<?php } ?>
											</ul>
										</div>
							<?php }
								}
							}
							?>
							<?php /*<div class="choose-color">
								<div class="choose-color-title">
									<p class="text-uppercase" style="color: #000;"><b>Choose Color :</b></p>
								</div>
								<ul>
									<li class="variable-item color-variable-item color-variable-item-pink ">
										<img src="http://myemojiart.com/customizer/uploads/attribute/1f162e058a8cb1e78590191087938521.png">
									</li>
									<li class="variable-item color-variable-item color-variable-item-pink ">
										<img src="http://myemojiart.com/customizer/uploads/attribute/00501b3ccfe5a02489e8d73e9aea0847.png">
									</li>
									<li class="variable-item color-variable-item color-variable-item-pink ">
										<img src="http://myemojiart.com/customizer/uploads/attribute/47e1207ba89b9dc066098e3ab5bcbcb5.png">
									</li>
								</ul>
							</div> *//* ?>
							<div class="choose-size2">
								<div class="choose-color-title">
									<p class="text-uppercase" style="color: #000;"><b>Choose Size :</b></p>
								</div>
								<ul class="filter-item-list">
									<li>
										<a href="javascript:void(0);" class="filter-item enabled">Extra Large</a>
									</li>
									<li>
										<a href="javascript:void(0);" class="filter-item enabled">Extra Small</a>
									</li>
									<li>
										<a href="javascript:void(0);" class="filter-item enabled">Large</a>
									</li>
								</ul>
							</div>
							<?php *//*
							<div class="choose-size2">
								<div class="choose-color-title">
									<p class="text-uppercase" style="color: #000;"><b>Choose Barand :</b></p>
								</div>
								<ul class="filter-item-list">
									<li>
										<a href="javascript:void(0);" class="filter-item enabled">apple</a>
									</li>
									<li>
										<a href="javascript:void(0);" class="filter-item enabled">mi</a>
									</li>
									<li>
										<a href="javascript:void(0);" class="filter-item enabled">Adidas</a>
									</li>
								</ul>
							</div> */ ?>
							<hr>
							<!-- <div class="choose-design">
											<div class="form-group">
											    <label for="exampleFormControlSelect1"><b>Example select</b></label>
											    <select class="form-control" id="exampleFormControlSelect1">
											      <option><b>Design for school spongebob</b></option>
											      <option><b>Design for school spongebob</b></option>
											      <option><b>Design for school spongebob</b></option>
											      <option><b>Design for school spongebob</b></option>
											    </select>
										  	</div>
									  	</div> -->

							<div class="product-detail-button-new">
								<?php
								if (isset($info['is_customizable']) && $info['is_customizable'] == '1') {
								?>
									<a href="javascript:void(0);" class="custom-design-btn"><i class="fa fa-pencil"></i> <?php echo get_line('custom_design'); ?></a>
								<?php
								}
								$class = "";
								if ($is_product_available == false) {
									$class = "disabled";
								}
								?>
								<a href="javascript:void(0)" data-custom-image="" data-id="<?php echo $info['id_unique']; ?>" class="buy-now-btn add-to-cart <?php echo $class; ?>"><i class="fa fa-cart"></i> <?= get_line_front('add'); ?></a>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 9%;">
						<div class="col-md-12 ">
							<ul class="nav nav-tabs" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#description" role="tab"><?php echo get_line('description'); ?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#additional-information" role="tab"><?php echo get_line('additional_description'); ?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#reviews" role="tab"><?php echo get_line('reviews'); ?> (<?php echo $info['total_review']; ?>)</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#view-360" role="tab"><?php echo get_line('view_360'); ?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#view-wrap-gift" role="tab"><?php echo get_line('view_wrap_gift'); ?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#view-video" role="tab"><?php echo get_line('video'); ?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#view-printed-image" role="tab"><?php echo get_line('view_printed_image'); ?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#view-printed-video" role="tab"><?php echo get_line('view_printed_video'); ?></a>
								</li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="description" role="tabpanel">
									<?php echo isset($info['long_description']) ? '<p>' . $info['long_description'] . '</p>' : ''; ?>
								</div>
								<div class="tab-pane" id="additional-information" role="tabpanel">
									<?php echo isset($info['additional_description']) ? '<p>' . $info['additional_description'] . '</p>' : ''; ?>
									<?php
									if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {
										$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();
										if (isset($info['other_info']['attribute_info']) && !empty($info['other_info']['attribute_info'])) {
											?>
											<div class="table-responsive">
												<table class="table table-striped">
													<tbody>
														<?php
														foreach ($info['other_info']['attribute_info'] as $value) {
															$temp = array();
															foreach ($value['attribute_values'] as $key => $value_row) {
																$temp[] = $value_row['attribute_value'];
															}
														?>
															<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--weight">
																<th class="woocommerce-product-attributes-item__label"><?php echo $value['attribute_name']; ?></th>
																<td class="woocommerce-product-attributes-item__value"><?php echo implode(', ', $temp); ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
									<?php
										}
									}
									?>
								</div>
								<div class="tab-pane" id="reviews" role="tabpanel">
									<div id="comments">
										<h2 class="woocommerce-Reviews-title"><?php echo get_line('reviews'); ?></h2>
										<?php
										$reviews = get_reviews($info['id_unique']);
										if (!empty($reviews)) {
											foreach ($reviews as $r_value) {
										?>
												<ol class="commentlist">
													<li class="review even thread-even depth-1" id="li-comment-1426">
														<div class="comment_container">
															<div class="img-thumbnail">
																<img alt="" src="http://1.gravatar.com/avatar/1aedb8d9dc4751e229a335e371db8058?s=80&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/1aedb8d9dc4751e229a335e371db8058?s=160&amp;d=mm&amp;r=g 2x" class="avatar avatar-80 photo" height="80" width="80">
															</div>
															<div class="comment-text">
																<div class="star-rating" title="" data-original-title="4">
																	<span style="width:<?php echo ($r_value['rating'] * 20) ?>%">
																		<strong><?php echo ($r_value['rating']); ?></strong> out of 5 </span>
																</div>

																<p class="meta">
																	<?php
																	if ($r_value['is_approved'] == 1) {
																	} else {
																	?>
																		<em class="woocommerce-review__awaiting-approval"><?php echo get_line('awaiting_approval'); ?></em>

																	<?php
																	}
																	?>
																</p>
																<div class="description">
																	<p><?php echo nl2br($r_value['comment']); ?></p>
																</div>
															</div>
														</div>
													</li>
												</ol>
											<?php
											}
										} else {
											?>
											<p><?php echo get_line('no_reviews_yet'); ?></p>
										<?php
										}
										?>

										<hr class="tall">
										<h3 id="reply-title" class="comment-reply-title"><?php echo get_line('add_a_review'); ?></h3>
										<form action="#" class="comment-form" id="commentform" onsubmit="return false;">
											<input type="hidden" name="rating" value="0" />
											<input type="hidden" name="id_unique" value="<?php echo $info['id_unique'];	 ?>" />
											<div class="comment-form-rating">
												<section class='rating-widget row'>
													<div class="rating-review-title">
														<h5 class="rating-title"><?php echo get_line('your_rating') ?></h5>
													</div>
													<!-- Rating Stars Box -->
													<div class='rating-stars'>
														<ul id='stars'>
															<li class='star' title='Poor' data-value='1'>
																<i class='fa fa-star fa-fw'></i>
															</li>
															<li class='star' title='Fair' data-value='2'>
																<i class='fa fa-star fa-fw'></i>
															</li>
															<li class='star' title='Good' data-value='3'>
																<i class='fa fa-star fa-fw'></i>
															</li>
															<li class='star' title='Excellent' data-value='4'>
																<i class='fa fa-star fa-fw'></i>
															</li>
															<li class='star' title='WOW!!!' data-value='5'>
																<i class='fa fa-star fa-fw'></i>
															</li>
														</ul>
													</div>
												</section>
											</div>
											<div class="form-group">
												<label for="comment"><?php echo get_line('comment'); ?> *</label>
												<textarea id="comment" name="comment" cols="45" rows="8" required></textarea>
											</div>
											<?php
											$read_only = '';
											if ($this->session->userdata('username') != null) {
												$read_only = 'readonly="readonly"';
											}
											?>
											<div class="form-group">
												<label for="comment"><?php echo get_line('name'); ?> * </label>
												<input type="text" name="first_name" value="<?php echo $this->session->userdata('username') != null ? $this->session->userdata('username')  : ''; ?>" <?php echo $read_only; ?>>
											</div>
											<div class="form-group">
												<label for="comment"><?php echo get_line('email_address'); ?> *</label>
												<input type="text" name="email_address" value="<?php echo $this->session->userdata('useremail') != null ? $this->session->userdata('useremail')  : ''; ?>"  <?php echo $read_only; ?>>
										</div>
										<div class=" form-group">
												<input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes"> <label for="wp-comment-cookies-consent">Save my name, email, and website in this browser for the next time I comment.</label>
											</div>
											<div class="form-group">
												<input name="submit" type="submit" id="submit" class="submit" value="Submit">
											</div>
										</form>
									</div>
								</div>
								<div class="tab-pane" id="view-360" role="tabpanel">
									<p><?php echo get_line('view_360'); ?></p>
								</div>
								<div class="tab-pane" id="view-wrap-gift" role="tabpanel">
									<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner">
											<div class="carousel-item active">
												<img class="d-block w-100" src="http://myemojiart.com/customizer/assets/uploads/home_slider/0acca4040e7362a273458be608c49839.png" alt="First slide">
											</div>
											<div class="carousel-item">
												<img class="d-block w-100" src="http://myemojiart.com/customizer/assets/uploads/home_slider/0d7820069d9c60561af0e749cdbf12fc.jpg" alt="Second slide">
											</div>
										</div>
										<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
											<span class="carousel-control-next-icon" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									</div>
								</div>
								<div class="tab-pane" id="view-video" role="tabpanel">
									<?php /*<p><iframe src="https://www.youtube.com/embed/sxl1j8qqqm4" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>*/ ?>
									<?php
									if (isset($info['product_video']) && !empty($info['product_video'])) {
									?>
										<div class="col-md-12">
											<video style="width:100%;height:250px;" controls="">
												<source src="<?php echo $info['product_video']; ?>">
											</video>
										</div>
									<?php
									}
									?>
								</div>
								<div class="tab-pane" id="view-printed-image" role="tabpanel">
									<p>View Printed Image</p>
								</div>
								<div class="tab-pane" id="view-printed-video" role="tabpanel">
									<p><iframe src="https://www.youtube.com/embed/sxl1j8qqqm4" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Customize this product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" enctype="multipart/form-data">
			<div class="form-group text-center">
				<input type="radio" name="customize_radio" class="custom_radio" value="0" checked /> <span style="margin-top:-5px;">Design it by Yourself</span> &nbsp; 
				<input type="radio" name="customize_radio" class="custom_radio" value="1" /> <span style="margin-top:-5px;"> Let us design it for you.</span>
			</div>
			<div class="customize-form hidden">
			<hr style="height:2px;border-width:0;color:gray;background-color:gray">
				<div class="form-group">
					<label>Customization Text :</label>
					<input type="text" class="form-control" name="customization_text" required/>
				</div>
				<div class="form-group">
					<label>Upload Images :</label>
					<input type="file" class="form-control" name="customization_images" multiple required/>
				</div>
				<div class="form-group">
					
					<input type="submit" class="btn btn-success" value="Submit"/>
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

</section>
<!-- Product Detail Section Strat Here -->
<div class="clearfix"></div>
<?php
if (isset($related_product) && !empty($related_product)) {
	// echo "<pre>";print_r($related_product);exit;
?>
	<section class="related-product">
		<div class="container">
			<h1 class="related-head">
				Related Products
			</h1>
			<div class="row">
				<?php 
					foreach ($related_product as $key => $value) {  ?>
						<div class="col-sm-4 col-md-3">
							<div class="rs-shop-box mb-5">
								<?php
                                    $get_pro_id = get_details('wishlist', array('id_product' =>$value['id_unique'], 'login_id' => get_front_login_id()));
                                if (isset($get_pro_id) && $get_pro_id != null) { ?>
                                    <a href="javascript:void(0)" class="link-wish wishlist fill-heart" id="<?= $value['id_unique'] ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" class="link-wish wishlist " id="<?= $value['id_unique'] ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                <?php } ?>
								<div class="media">
									<a href="<?=base_url('/product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>">
										<img src="<?php echo $value['main_image'];?>" alt="" class="img-fluid">
									</a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#"><?= $value['title']; ?></a></h4>
									<div class="meta">
										<div class="price">$<?= $value['other_info']['final_price'][0]; ?></div>
										<div class="rating">
											<a href="javascript:void(0)" data-custom-image="" data-id="<?php echo $value['id_unique']; ?>" class="btn btn-cart add-to-cart"><?= get_line_front('add_to_cart'); ?></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
					}
				?>
				<!-- <div class="col-sm-4 col-md-3">
					<div class="rs-shop-box mb-5">
						<a href="javascript:void(0);" class="link-wish">
							<i class="fa fa-heart"></i>
						</a>
						<div class="media">
							<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="body-text">
							<h4 class="title"><a href="#">Original 6kg</a></h4>
							<div class="meta">
								<div class="price">$29</div>
								<div class="rating">
									<a href="#" class="btn btn-cart">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-3">
					<div class="rs-shop-box mb-5">
						<a href="javascript:void(0);" class="link-wish">
							<i class="fa fa-heart"></i>
						</a>
						<div class="media">
							<a href="#"><img src="images/pro-2.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="body-text">
							<h4 class="title"><a href="#">Grass fed lamb 6kg</a></h4>
							<div class="meta">
								<div class="price">$29</div>
								<div class="rating">
									<a href="#" class="btn btn-cart">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-3">
					<div class="rs-shop-box mb-5">
						<a href="javascript:void(0);" class="link-wish">
							<i class="fa fa-heart"></i>
						</a>
						<div class="media">
							<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="body-text">
							<h4 class="title"><a href="#">Original 6kg</a></h4>
							<div class="meta">
								<div class="price">$29</div>
								<div class="rating">
									<a href="#" class="btn btn-cart">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</section>
	<section class="related-product">
		<div class="container">
			<div class="related-product">
				<div class="related-product-title">
					<h3><?php echo get_line('related_product'); ?></h3>
				</div>
			</div>
			<div id="blogCarousel" class="carousel slide" data-ride="carousel">

				<ol class="carousel-indicators">
					<?php
					$count = 0;
					foreach ($related_product as $key => $value) {
						if ($key == 0) {
					?>
							<li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
						<?php
						}
						if ($key != 0 && ($key) % 3 == 0) {
							$count++;
						?>
							<li data-target="#blogCarousel" data-slide-to="<?php echo $count; ?>"></li>
						<?php
						}
						?>
					<?php
					}
					?>
				</ol>
				<div class="carousel-inner">
					<?php
					foreach ($related_product as $key => $value) {
						if ($key == 0) {
					?>
							<div class="carousel-item active">
								<div class="row">
								<?php
							}
							if ($key != 0 && ($key) % 3 == 0) {
								echo '</div></div><div class="carousel-item"><div class="row">';
							}
								?>
								<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
									<div class="mens-clothes-box">
										<ul>
											<li>
												<?php
												$get_pro_id = get_details('wishlist', array('id_product' => $value['id_unique'], 'login_id' => get_front_login_id()));
												if (isset($get_pro_id) && $get_pro_id != null) { ?>
													<a href="javascript:void(0)" class="wishlist" id="<?= $value['id_unique'] ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
												<?php } else { ?>
													<a href="javascript:void(0)" class="wishlist" id="<?= $value['id_unique'] ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
												<?php } ?>
											</li>
											<?php
											if (isset($value['is_new']) && $value['is_new'] == '1') {
											?>
												<li class="new"><?php echo get_line('new'); ?></li>
											<?php
											}
											?>
										</ul>
										<div class="mens-clothes-img text-center">
											<a href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>"><img src="<?php echo $value['main_image']; ?>"></a>
										</div>
										<div class="mens-clothes-name text-center">
											<h4 style="font-size: 18px;"><b><a class="theme-link" href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>"><?php echo $value['title']; ?></a></b></h4>
											<h4><?php echo isset($value['other_info']['final_price'][0]) ? $value['other_info']['final_price'][0] : 0; ?> <?php echo PRICE_KEY; ?></h4>
										</div>
									</div>
								</div>
							<?php
						}
							?></div>
							</div>
				</div>
	</section>
<?php
}
?>
<script type="text/javascript">
	<?php
	unset($info['description'], $info['additional_description'], $info['long_description']);
	?>
	var info = '<?php echo isset($info) ? json_encode($info) : ''; ?>';
	if (info != '') {
		info = JSON.parse(info);
	}
</script>
<?php
$footer_js = array(
	base_url('assets/js/image-zoom.js'),
	base_url('assets/js/product.js')
);
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
	$(document).ready(function() {

		//Customize product
			$(".custom_radio").click(function(){
				var r_val = $(this).val();

				if(r_val==0){
					$(".customize-form").addClass("hidden");
				}else{
					$(".customize-form").removeClass("hidden");
				}
			});


		//If your <ul> has the id "glasscase"
		$('#glasscase').glassCase({
			'thumbsPosition': 'bottom',
			'widthDisplay': 560
		});
		try {
			$('#blogCarousel').carousel({
				interval: 5000
			});
		} catch (err) {}

		$('.comment-form').submit(function() {
			loading(true);
			$.ajax({
				url: BASE_URL + 'ajax/save_comment',
				method: 'POST',
				data: $('.comment-form').serialize(),
				dataType: 'json',
				success: function(response) {
					toastr.remove();
					if (response.status) {
						toastr.success(response.message);
						setTimeout(function() {
							window.location = window.location.href;
						}, 250);
					} else {
						toastr.error(response.message);
					}
				}
			})
		});

		/* 1. Visualizing things on Hover - See next part for action on click */
		$('#stars li').on('mouseover', function() {
			var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

			// Now highlight all the stars that's not after the current hovered star
			$(this).parent().children('li.star').each(function(e) {
				if (e < onStar) {
					$(this).addClass('hover');
				} else {
					$(this).removeClass('hover');
				}
			});

		}).on('mouseout', function() {
			$(this).parent().children('li.star').each(function(e) {
				$(this).removeClass('hover');
			});
		});


		/* 2. Action to perform on click */
		$('#stars li').on('click', function() {
			var onStar = parseInt($(this).data('value'), 10); // The star currently selected
			var stars = $(this).parent().children('li.star');

			for (i = 0; i < stars.length; i++) {
				$(stars[i]).removeClass('selected');
			}

			for (i = 0; i < onStar; i++) {
				$(stars[i]).addClass('selected');
			}

			// JUST RESPONSE (Not needed)
			var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
			$('[name="rating"]').val(ratingValue);
		});

	});
</script>

<script src="<?php echo base_url('assets/js/zoomy.js')?>"></script>

	<script>

		var image_urls = info.other_info.image_info;
		var img_url =[];
		$.each(image_urls, function( index, value ) {
		 	img_url[index] = value.image_name;
		});
		img_url.push(info.main_image); 
		img_url.reverse();
		console.log(img_url)
	    
	    var options = {
	    };
	    jQuery('#el').zoomy(img_url,options);
 	</script>
<?php
if (isset($info['is_customizable']) && $info['is_customizable'] == '1') {
?>
	<script src="<?php echo base_url('assets/custom-design/js/jquery-ui.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/custom-design/js/fabric.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/custom-design/js/FancyProductDesigner-all.min.js'); ?>"></script>
	<script>
		jQuery(document).ready(function() {
			$('.custom-design-btn,#cancel').click(function() {
				$('[data-custom-image]').attr('data-custom-image', '');
				$('.customize-section').slideToggle();
				$('.product-detail-page').slideToggle();
			});

			<?php
			$count = 0;
			$products = array();
			if (isset($info['main_image']) && !empty($info['main_image'])) {
				$products[$count][0]['title'] = $info['title'];
				$products[$count][0]['thumbnail'] = $info['main_image'];
				$products[$count][0]['elements'] = array();
				$products[$count][0]['elements'][0]['type'] = 'image';
				$products[$count][0]['elements'][0]['title'] = $info['title'];
				$products[$count][0]['elements'][0]['source'] = $info['main_image'];
				$products[$count][0]['elements'][0]['parameters'] = array('left' => 332, 'top' => '311');
				$count++;
			}
			if (isset($info['other_info']['image_info']) && !empty($info['other_info']['image_info'])) {
				foreach ($info['other_info']['image_info'] as $key => $value) {
					// $products['elements'][$count]['type'] = 'image';
					// $products['elements'][$count]['title'] = $info['title'];
					// $products['elements'][$count]['source'] = $info['image_name'];
					// $products['elements'][$count]['parameters'] = array('left' => 332, 'top' => '311');
					$products[$count][0]['title'] = $info['title'];
					$products[$count][0]['thumbnail'] = $value['image_name'];
					$products[$count][0]['elements'] = array();
					$products[$count][0]['elements'][0]['type'] = 'image';
					$products[$count][0]['elements'][0]['title'] = $info['title'];
					$products[$count][0]['elements'][0]['source'] = $value['image_name'];
					$products[$count][0]['elements'][0]['parameters'] = array('left' => 332, 'top' => '311');
					$count++;
				}
			}
			?>
			<?php
			if (isset($info['other_info']['attribute_images']) && !empty($info['other_info']['attribute_images'])) {
				foreach ($info['other_info']['attribute_images'] as $key => $value) {
					// $products['elements'][$count]['type'] = 'image';
					// $products['elements'][$count]['title'] = $info['title'];
					// $products['elements'][$count]['source'] = $info['image_name'];
					// $products['elements'][$count]['parameters'] = array('left' => 332, 'top' => '311');
					$products[$count][0]['title'] = $info['title'];
					$products[$count][0]['thumbnail'] = $value['image_name'];
					$products[$count][0]['elements'] = array();
					$products[$count][0]['elements'][0]['type'] = 'image';
					$products[$count][0]['elements'][0]['title'] = $info['title'];
					$products[$count][0]['elements'][0]['source'] = $value['image_name'];
					$products[$count][0]['elements'][0]['parameters'] = array('left' => 332, 'top' => '311');
					$count++;
				}
			}
			?>

			var $yourDesigner = $('#clothing-designer'),
				pluginOpts = {
					templatesDirectory: BASE_URL + 'assets/custom-design/html/',
					productsJSON: <?php echo json_encode($products); ?>,
					designsJSON: [{
							"source": BASE_URL + 'assets/custom-design/images/designs/swirl.png',
							"title": "Swirl",
							"parameters": {
								"zChangeable": true,
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"price": 10,
								"boundingBox": "Base",
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/swirl2.png',
							"title": "Swirl 2",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"price": 5,
								"boundingBox": "Base",
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/swirl3.png',
							"title": "Swirl 3",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/heart_blur.png',
							"title": "Heart Blur",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#bf0200",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"price": 5,
								"boundingBox": "Base",
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/converse.png',
							"title": "Converse",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/crown.png',
							"title": "Crown",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/men_women.png',
							"title": "Men hits Women",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"boundingBox": "Base",
								"autoCenter": true
							}
						}
					],
					langJSON: {
						"toolbar": {
							"fill": "Fill",
							"font_family_search": "Search Fonts",
							"transform": "Transform",
							"position": "Position",
							"move_up": "Move Up",
							"move_down": "Move Down",
							"reset": "Reset",
							"font_size": "Font Size",
							"line_height": "Line Height",
							"bold": "Bold",
							"italic": "Italic",
							"underline": "Underline",
							"text_align": "Text Alignment",
							"stroke": "Stroke",
							"curved_text": "Curved Text",
							"edit_text": "Edit Text",
							"color": "Color",
							"patterns": "Patterns",
							"transparency": "Transparency",
							"align_left": "Align Left",
							"align_top": "Align Top",
							"align_right": "Align Right",
							"align_bottom": "Align Bottom",
							"center_h": "Center Horizontal",
							"center_v": "Center Vertical",
							"flip_h": "Flip Horizontal",
							"flip_v": "Flip Vertical",
							"curved_text_switch": "Switch",
							"curved_text_reverse": "Reverse",
							"radius": "Radius",
							"spacing": "Spacing",
							"letter_spacing": "Letter Spacing",
							"advanced_editing": "Advanced Editing",
							"text_transform": "Text Transform",
							"stroke_width": "Width",
							"back": "Back",
							"shadow": "Shadow",
							"shadow_blur": "Blur",
							"shadow_offset_x": "Offset X",
							"shadow_offset_y": "Offset Y"
						},
						"actions": {
							"reset_product": "Reset",
							"zoom": "Zoom",
							"magnify_glass": "Magnify Glass",
							"download": "Download",
							"download_current_view": "Only export current showing view",
							"info": "Info",
							"info_content": "Content for the Info Action Button. Change content via language JSON.",
							"print": "Print",
							"save": "Save",
							"load": "Load Designs",
							"manage_layers": "Manage Layers",
							"qr_code": "Add QR-Code",
							"qr_code_input": "Enter a URL, some text...",
							"qr_code_add_btn": "Add QR-Code",
							"undo": "Undo",
							"redo": "Redo",
							"snap": "Center Snap",
							"preview_lightbox": "Preview",
							"save_placeholder": "Optional: Enter a title",
							"ruler": "Toggle Ruler",
							"previous_view": "Previous View",
							"next_view": "Next View",
							"start_guided_tour": "Start Guided Tour",
							"load_designs_empty": "No Designs saved yet!"
						},
						"modules": {
							"products": "Swap Product",
							"products_confirm_replacement": "Are you sure to replace the product?",
							"products_confirm_button": "Yes, please!",
							"images": "Add Image",
							"upload_zone": "Click or drop images here",
							"facebook_select_album": "Select An Album",
							"text": "Add Text",
							"text_input_placeholder": "Enter some text",
							"enter_textbox_width": "Optional: Enter a fixed width",
							"text_add_btn": "Add Text",
							"designs": "Choose From Designs",
							"designs_search_in": "Search in",
							"manage_layers": "Manage Layers",
							"images_agreement": "I have the rights to use the images.",
							"images_confirm_button": "Confirm",
							"images_pdf_upload_info": "Creating images from PDF...",
							"pixabay_search": "Search in Pixabay library",
							"depositphotos_search": "Search in depositphotos.com",
							"depositphotos_search_category": "Search In ",
							"text_layers_clear": "Clear",
							"layouts": "Layouts",
							"layouts_confirm_replacement": "Are you sure to replace all elements in the current view?",
							"layouts_confirm_button": "Yes, got it!"
						},
						"image_editor": {
							"mask": "Mask",
							"filters": "Filters",
							"color_manipulation": "Color Manipulation",
							"custom_cropping": "Custom Cropping",
							"filter_none": "None",
							"brightness": "Brightness",
							"contrast": "Contrast",
							"remove_white": "Remove White",
							"remove_white_distance": "Distance",
							"restore": "Restore Original",
							"save": "Save"
						},
						"misc": {
							"initializing": "Initializing Product Designer",
							"out_of_bounding_box": "Move element in its containment!",
							"product_saved": "Product Saved!",
							"loading_image": "Loading Image...",
							"uploaded_image_size_alert": "Sorry! But the uploaded image size does not conform our indication of size.<br />Minimum Width: %minW pixels<br />Minimum Height: %minH pixels<br />Maximum Width: %maxW pixels<br />Maximum Height: %maxH pixels",
							"modal_done": "Done",
							"minimum_dpi_info": "The JPEG image does not have the required DPI of %dpi.",
							"maximum_size_info": "The image %filename exceeds the maximum file size of %mbMB.",
							"customization_required_info": "You need to customize the default design!",
							"not_supported_device_info": "Sorry! But the product designer can not be displayed on your device. Please use a device with a larger screen!",
							"image_added": "Image Added!",
							"reset_confirm": "Are you sure to reset everything?",
							"popup_blocker_alert": "Please disable your pop-up blocker and try again.",
							"view_optional_unlock": "Unlock view",
							"guided_tour_back": "Back",
							"guided_tour_next": "Next"
						},
						"plus": {
							"names_numbers": "Names & Numbers",
							"names_numbers_add_new": "Add New",
							"drawing": "Free Drawing",
							"drawing_brush_type": "Brush Type",
							"drawing_pencil": "Pencil",
							"drawing_circle": "Circle",
							"drawing_spray": "Spray",
							"drawing_color": "Color",
							"drawing_line_width": "Line Width",
							"drawing_draw_here": "Draw Here",
							"drawing_clear": "Clear",
							"drawing_add": "Add",
							"bulk_add_variations_title": "Bulk Order",
							"bulk_add_variations_add": "Add",
							"dynamic_views": "Manage Views",
							"dynamic_views_add_view_info": "Add new view",
							"dynamic_views_add_blank": "Blank",
							"dynamic_views_add_from_layouts": "From Layouts"
						}
					},
					stageWidth: 1200,
					editorMode: false,
					smartGuides: true,
					fonts: [{
							name: 'Helvetica'
						},
						{
							name: 'Times New Roman'
						},
						{
							name: 'Pacifico',
							url: BASE_URL + 'assets/custom-design/fonts/Pacifico.ttf',
						},
						{
							name: 'Arial'
						},
						{
							name: 'Lobster',
							url: 'google'
						}
					],
					customTextParameters: {
						colors: false,
						removable: true,
						resizable: true,
						draggable: true,
						rotatable: true,
						autoCenter: true,
						boundingBox: "Base"
					},
					customImageParameters: {
						draggable: true,
						removable: true,
						resizable: true,
						rotatable: true,
						colors: '#000',
						autoCenter: true,
						boundingBox: "Base"
					},
					actions: {
						'top': ['download', 'print', 'snap', 'preview-lightbox'],
						'right': ['magnify-glass', 'zoom', 'reset-product', 'qr-code', 'ruler'],
						'bottom': ['undo', 'redo'],
						'left': ['manage-layers', 'info', 'save', 'load']
					}
				},

				yourDesigner = new FancyProductDesigner($yourDesigner, pluginOpts);

			//print button
			$('#print-button').click(function() {
				yourDesigner.print();
				return false;
			});

			//create an image
			$('#image-button').click(function() {
				var image = yourDesigner.createImage();
				return false;
			});

			//checkout button with getProduct()
			$('#checkout-button').click(function() {
				var product = yourDesigner.getProduct();
				console.log(product);
				return false;
			});
			
			//event handler when the price is changing
			$yourDesigner.on('priceChange', function(evt, price, currentPrice) {
				$('#thsirt-price').text(currentPrice);
			});

			//save image on webserver
			$('#save-image-php').click(function() {

				yourDesigner.getProductDataURL(function(dataURL) {
					/*$.post(BASE_URL + "cart/save_customized_product", {
							base64_image: dataURL
						}).done(function(response) {
							response = JSON.parse(response);
							if (response.status) {
								//toastr.success(response.message);
								$('[data-custom-image]').attr('data-custom-image', response.data);
								$('.customize-section').slideToggle();
								$('.product-detail-page').slideToggle();
								$(window).trigger('resize');
								$('.add-to-cart').trigger('click');
							} else {
								toastr.error(response.messagesuccess);
							}
						})
						.fail(function() {
							window.location = window.location.href;
						});*/


					// Get the form element withot jQuery
					var form = document.getElementById("commentform");

					var ImageURL = dataURL;
					// Split the base64 string in data and contentType
					var block = ImageURL.split(";");
					// Get the content type of the image
					var contentType = block[0].split(":")[1]; // In this case "image/gif"
					// get the real base64 content of the file
					var realData = block[1].split(",")[1]; // In this case "R0lGODlhPQBEAPeoAJosM...."

					// Convert it to a blob to upload
					var blob = b64toBlob(realData, contentType);
					// Create a FormData and append the file with "image" as parameter name
					var formDataToUpload = new FormData(form);
					formDataToUpload.append("image", blob);

					$.ajax({
						url: BASE_URL + "cart/save_customized_product",
						data: formDataToUpload,
						type: "POST",
						contentType: false,
						processData: false,
						cache: false,
						dataType: "json",
						error: function(err) {
							console.error(err);
						},
						success: function(response) {
							if (response.status) {
								//toastr.success(response.message);
								$('[data-custom-image]').attr('data-custom-image', response.data);
								$('.customize-section').slideToggle();
								$('.product-detail-page').slideToggle();
								$(window).trigger('resize');
								$('.add-to-cart').trigger('click');
							} else {
								toastr.error(response.messagesuccess);
							}
						},
						complete: function() {
							console.log("Request finished.");
						}
					});
				});

			});
		});

		/**
		 * Convert a base64 string in a Blob according to the data and contentType.
		 * 
		 * @param b64Data {String} Pure base64 string without contentType
		 * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
		 * @param sliceSize {Int} SliceSize to process the byteCharacters
		 * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
		 * @return Blob
		 */
		function b64toBlob(b64Data, contentType, sliceSize = 512) {
			contentType = contentType || '';
			sliceSize = sliceSize || 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, {
				type: contentType
			});
			return blob;
		}

		

		
	</script>
<?php
}
$this->load->view('include/footer'); ?>