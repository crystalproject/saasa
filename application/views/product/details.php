<?php
defined('BASEPATH') or exit('No direct script access allowed');
$header_css = array(base_url('assets/custom-design/css/main.css'), base_url('assets/custom-design/css/FancyProductDesigner-all.min.css'),base_url('assets/css/zoomy.css'));
$this->load->view('include/header', array('header_css' => $header_css));
$this->load->view('include/messages');
$is_product_available = true;
?>

<!-- <style>
	.nav-tabs .nav-item .nav-link {
		font-weight: 700;
		color: #818692 !important;
		text-transform: uppercase;
		background: 0 0 !important;
		border: none !important;
		border-bottom: 2px solid transparent !important;
		padding: 7px 0 !important;
		border-radius: 0;
		margin-right: 35px;
		font-size: 13px
	}

	.nav-tabs .nav-item .nav-link.active,
	.nav-tabs .nav-item .nav-link:hover {
		border-color: #222529 !important;
		color: #222529 !important;
		font-weight: 700;
		font-size: 13px
	}

	.tab-content .tab-pane {
		border: none;
		border-top: solid 1px #e7e7e7;
		box-shadow: none;
		padding: 30px 0 15px
	}

	.woocommerce-product-attributes-item__label {
		width: 20%
	}

	.table td,
	.table th {
		padding: .75rem;
		vertical-align: top;
		border-top: 1px solid #ddd
	}

	.tab-content .tab-pane p {
		font-size: 14px;
		font-weight: 400;
		letter-spacing: .005em;
		line-height: 1.9;
		color: #000
	}

	.tab-content h2 {
		font-size: 1.4286em;
		line-height: 1.4;
		font-weight: 400;
		margin-bottom: 1.0714em
	}

	.tab-content .tab-pane #carouselExampleControls {
		width: 600px;
		margin-left: 22%
	}

	.tab-content .tab-pane #carouselExampleControls .carousel-item:nth-child(2) img {
		height: 290px
	}

	hr.tall {
		border-top: 1px solid rgba(0, 0, 0, .08)
	}

	.comment-form {
		padding: 30px 30px 10px;
		background: #f7f7f7;
		border-radius: 5px
	}

	* Rating Star Widgets Style */ .rating-stars ul {
		list-style-type: none;
		padding: 0;
		-moz-user-select: none;
		-webkit-user-select: none
	}

	.rating-stars ul {
		padding-left: 5px
	}

	.rating-stars ul>li.star {
		display: inline-block
	}

	.rating-stars ul>li.star>i.fa {
		font-size: 14px;
		color: #ccc
	}

	.rating-stars ul>li.star.hover>i.fa {
		color: #706f6c;
		cursor: pointer
	}

	.rating-stars ul>li.star.selected>i.fa {
		color: #706f6c
	}

	.rating-title {
		font-weight: 700;
		color: #212529;
		margin-right: 10px;
		margin-bottom: 0
	}

	.comment-form .form-group {
		margin-left: -1%
	}

	#comments h2 {
		margin-top: 0
	}

	.commentlist {
		list-style: none;
		margin: 15px 0;
		padding: 0;
		*zoom: 1
	}

	.commentlist:before,
	.commentlist:after {
		content: " ";
		display: table
	}

	.commentlist:after {
		clear: both
	}

	.commentlist li {
		clear: both;
		margin-bottom: 20px
	}

	.commentlist li:last-child {
		margin-bottom: 0
	}

	@media (max-width: 575px) {
		.commentlist li .comment_container {
			padding: 10px 10px 15px
		}
	}

	.commentlist li .comment-text {
		padding: 20px;
		position: relative
	}

	@media (max-width: 575px) {
		.commentlist li .comment-text {
			padding: 0;
			background: transparent
		}
	}

	.commentlist li .comment-text p {
		font-size: 0.9em;
		line-height: 21px;
		margin: 0;
		padding: 0
	}

	.commentlist li .comment-text .meta {
		font-size: 1em;
		margin: 0
	}

	.commentlist li .comment-text .meta strong {
		display: inline-block;
		line-height: 21px;
		margin: 0;
		padding: 0 0 5px 0
	}

	@media (max-width: 575px) {
		.commentlist li .comment-text .meta strong {
			display: block;
			padding-bottom: 0
		}
	}

	.commentlist li .comment-text .meta time {
		color: #999;
		font-size: 0.9em
	}

	.comment-reply-title {
		font-size: 1.4em;
		font-weight: 400;
		line-height: 27px;
		margin: 0 0 14px 0
	}

	.comment-form-rating label {
		display: inline-block;
		margin-right: 10px;
		margin-bottom: 0
	}

	.comment-form-rating .stars {
		display: inline-block;
		position: relative;
		top: -.5em;
		white-space: nowrap
	}

	.comment-form-rating .stars span a {
		position: absolute;
		top: 0;
		left: 0;
		font-size: 14px;
		text-indent: -9999em;
		transition: all 0.2s;
		text-decoration: none
	}

	.comment-form-rating .stars span a:before {
		color: #706f6c;
		content: "";
		position: absolute;
		left: 0;
		height: 24px;
		text-indent: 0;
		letter-spacing: 1px
	}

	.comment-form-rating .stars span a:hover:before {
		color: #706f6c
	}

	.comment-form-rating .stars .star-1 {
		z-index: 10
	}

	.comment-form-rating .stars .star-1:before {
		width: 17px
	}

	.comment-form-rating .stars .star-1:hover:before,
	.comment-form-rating .stars .star-1.active:before {
		content: ""
	}

	.comment-form-rating .stars .star-2 {
		z-index: 9
	}

	.comment-form-rating .stars .star-2:before {
		width: 34px
	}

	.comment-form-rating .stars .star-2:hover:before,
	.comment-form-rating .stars .star-2.active:before {
		content: """"
	}


	#reviews .commentlist li {
		position: relative;
		padding-left: 115px
	}

	@media (max-width: 575px) {
		#reviews .commentlist li {
			padding-left: 0
		}
	}

	#reviews .commentlist li .img-thumbnail {
		position: absolute;
		left: 0;
		top: 0;
		border: none;
	}

	#reviews .commentlist li .img-thumbnail img {
		max-width: 80px;
		height: auto
	}

	@media (max-width: 575px) {
		#reviews .commentlist li .img-thumbnail {
			position: static;
			margin: 0 12px 10px 0;
			float: left
		}

		#reviews .commentlist li .img-thumbnail img {
			max-width: 60px
		}
	}

	#reviews .commentlist li .comment-text {
		min-height: 90px
	}

	#reviews .commentlist li .comment-text:before {
		content: "";
		border-bottom: 15px solid transparent;
		left: -15px;
		border-top: 15px solid transparent;
		height: 0;
		position: absolute;
		top: 28px;
		width: 0
	}

	@media (max-width: 575px) {
		#reviews .commentlist li .comment-text:before {
			display: none
		}
	}

	#reviews .commentlist li .star-rating {
		float: right
	}

	@media (max-width: 575px) {
		#reviews .commentlist li .star-rating {
			float: none
		}
	}

	div.products {
		margin-bottom: 1.875rem
	}

	.products.related {
		margin-bottom: 1rem
	}

	.summary-before {
		position: relative
	}

	@media (max-width: 991px) {
		.summary-before {
			margin-left: auto;
			margin-right: auto
		}
	}

	.summary-before .labels {
		position: absolute;
		line-height: 1;
		color: #fff;
		font-weight: 700;
		text-transform: uppercase;
		margin: 0;
		z-index: 10;
		top: .8em;
		left: .8em
	}

	.summary-before .labels .onhot,
	.summary-before .labels .onsale {
		font-size: .8571em;
		padding: .5833em .6333em;
		margin-bottom: 5px;
		display: block
	}

	.summary-before .labels .onhot {
		background: #62b959
	}

	.summary-before .labels .onsale {
		background: #e27c7c
	}

	.product-images {
		position: relative;
		margin-bottom: 6px
	}

	.product-images .zoom {
		border-radius: 100%;
		bottom: 4px;
		cursor: pointer;
		color: #FFF;
		display: block;
		height: 30px;
		padding: 0;
		position: absolute;
		right: 4px;
		text-align: center;
		width: 30px;
		opacity: 0;
		transition: all 0.1s;
		z-index: 1000
	}



	.commentlist li .comment-text {
		background: #f5f7f7;
	}

	#reviews .commentlist li .comment-text:before {
		border-right: 15px solid #f5f7f7;
	}

	#reviews .commentlist li .comment-text:before {
		content: "";
		border-bottom: 15px solid transparent;
		left: -15px;
		border-top: 15px solid transparent;
		height: 0;
		position: absolute;
		top: 28px;
		width: 0;
	}

	.star-rating:before {
		color: rgba(0, 0, 0, .16)
	}

	.product-detail-button-new {
		width: 100%;
		position: relative;
		float: left;
	}

	.product-detail-button-new .custom-design-btn {
		width: 100%;
		text-align: center;
		float: left;
		padding: 10px 0px;
		background: #687cf9;
		color: #FFF;
		margin-bottom: 5px;
	}

	.product-detail-button-new .buy-now-btn {
		width: 100%;
		text-align: center;
		float: left;
		padding: 10px 0px;
		background: #222529;
		color: #FFF;
		font-weight: bold;
	}

	.btn-customize{
		color: #fff;
    	background-color: #007bff;
    	border-color: #007bff;
	}
</style> -->
<!-- Breadcrumb Start -->
<div class="section bg-breadcrumb">
	<div class="content-wrap py-0 pos-relative">
		<div class="container">
		    <nav aria-label="breadcrumb">
			  	<ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
				    <li class="breadcrumb-item"><a href="shop-list.html">Dogs</a></li>
				    <li class="breadcrumb-item"><a href="shop-list.html">Foods</a></li>
		    		<?php
				    	if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {
							$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();
							$attribute_value = isset($info['other_info']['attribute_info'][0]['attribute_values'][0]['attribute_value'])?$info['other_info']['attribute_info'][0]['attribute_values'][0]['attribute_value']:'';
						}
					?>
				    <li class="breadcrumb-item active" aria-current="page"><?= isset($info['title']) ? $info['title'] : ''; ?> <?=(isset($attribute_value))?$attribute_value:''?></li>
			  	</ol>
			</nav>					
		</div>
	</div>
</div>
<!-- Breadcrumb End -->

<?php //echo "<pre>"; print_r($info); ?>
<section class="product-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div id='el'></div>
			</div>
			<div class="col-md-6">
				<div class="product-des">
					<?php
						
						$total_price = isset($info['other_info']['prices'][0]) ? $info['other_info']['prices'][0] : null;
						$discount = isset($info['other_info']['discount'][0]) ? $info['other_info']['discount'][0] : null;
						$total_discount = calculate_discount(0, $total_price, $discount);
						$discounted_price = $total_price - $total_discount;
						$price_text = '';
						if ($discounted_price == $total_price) {
							$price_text = PRICE_KEY .' '.$discounted_price ;
						} else {
							$price_text = PRICE_KEY .' '.$discounted_price .' <strike>' .PRICE_KEY .' '.$total_price . '</strike>';
						}
					?>
					<?php
						$class = "hidden";
						if (isset($discount) && $discount > 0) {
							$class = "";
						}
						$class = "hidden";
						echo '<h5 class="' . $class . '">' . get_line_front('discount') . ': <span data-discount="true">' . $discount . '</span>%' . '</h5>';
					?>
					<h3>
						<?= isset($info['title']) ? $info['title'] : ''; ?>
					</h3>
					<h3 class="<?php echo $price_class; ?>" data-discounted-price="true">
						<?php
							if (isset($info['other_info']['quantity'][0]) && $info['other_info']['quantity'][0] > 0) {
								$price_class = "";
							}else{
								$price_class = "hidden";
							}
							?>
							
							<?php echo $price_text; ?>
					</h3>
					<?php
						// echo "<pre>";print_r($info['other_info']);exit;
						if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {
							$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();
							if (isset($info['other_info']['attribute_info']) && !empty($info['other_info']['attribute_info'])) {
								?>
								<?php
								foreach ($info['other_info']['attribute_info'] as $value) {
									$temp = array();

									
									?>
										<label><?php echo $value['attribute_name']; ?></label>
										<div class="select-wrap">
											<select class="form-control">
												<?php
													foreach ($value['attribute_values'] as $key => $value_row) {
														$temp[] = $value_row['attribute_value'];
														$selected = ''; 
														if (in_array($value_row['id_unique'], $default_selected)) {
															$selected = 'selected';
														}?>
														<option <?=$selected?>><?=$value_row['attribute_value']?></option>	
														<?php 
													}
												?>
											</select>
										</div>	
									<?php
								}
							}
						}
					?>
					<!-- -->
					<?php
					if (isset($info['has_attributes']) && $info['has_attributes'] == '1') {
						$default_selected = isset($info['other_info']['attribute_combinations'][0]) ? $info['other_info']['attribute_combinations'][0] : array();
						if (isset($info['other_info']['attribute_info']) && !empty($info['other_info']['attribute_info'])) {
							foreach ($info['other_info']['attribute_info'] as $value) {
								$attribute_main_class = '';
								$ul_class = '';
								foreach ($value['attribute_values'] as $key => $value_row) {
									$selected = '';
									if (in_array($value_row['id_unique'], $default_selected)) {
										$selected = 'active';
									}
									if ($value_row['image'] != '') {
										$attribute_main_class = 'choose-color';
									} else {
										$attribute_main_class = 'choose-size2';
										$ul_class = 'filter-item-list';
									}
								}
					?>
								<div class="<?php echo $attribute_main_class; ?>">
									<div class="choose-color-title">
										<p class="text-uppercase" style="color: #000;"><b><?php echo get_line('choose'); ?> <?php echo $value['attribute_name']; ?>:</b></p>
									</div>

									<ul class="<?php echo $ul_class;  ?> choose-attribute">
										<?php
										foreach ($value['attribute_values'] as $key => $value_row) {
											$selected = '';
											if (in_array($value_row['id_unique'], $default_selected)) {
												$selected = 'active-new-design';
											}
										?>

											<li data-id-unique="<?php echo $value_row['id_unique']; ?>" class=" <?php echo $selected; ?> <?php echo $value_row['image'] != '' ? 'variable-item color-variable-item color-variable-item-pink' : ''; ?>">
												<?php
												if ($value_row['image'] != '') {
													echo '<img class="attribute-image" src="' . $value_row['image'] . '" />';
												} else {
													echo  '<a href="javascript:void(0);" class="filter-item enabled  ">' . $value_row['attribute_value'] . '</a>';
												}
												?>
											</li>
										<?php } ?>
									</ul>
								</div>
					<?php }
						}
					}
					?>
					<!-- -->
					<div class="row">
						<div class="col-md-6">
							<a href="javascript:void(0);" class="btn btn-cart add-to-cart" data-custom-image="" data-id="<?php echo $info['id_unique']; ?>" data-attr-values="<?=$default_selected?>"><?=get_line_front('add_to_cart')?></a>

						</div>
						<div class="col-md-6">
							<a href="javascript:void(0);" class="btn btn-cart"><?=get_line_front('buy_now')?></a>
						</div>
					</div>
					<div class="stock">
						<p>
							Available: 
							<span>
								<i class="fa fa-check"></i>
								In Stock
							</span>
						</p>
					</div>
					<div class="accordion rs-accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h3 class="title">
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Description
									</button>
								</h3>
							</div>
							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
									<?php echo isset($info['description']) ? $info['description'] : ''; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Product Detail Section Strat Here -->
<div class="clearfix"></div>
<?php
if (isset($related_product) && !empty($related_product)) {
	// echo "<pre>";print_r($related_product);exit;
?>
	<section class="related-product">
		<div class="container">
			<h1 class="related-head">
				Related Products
			</h1>
			<div class="row">
				<?php 
					foreach ($related_product as $key => $value) {  ?>
						<div class="col-sm-4 col-md-3">
							<div class="rs-shop-box mb-5">
								<?php
                                    $get_pro_id = get_details('wishlist', array('id_product' =>$value['id_unique'], 'login_id' => get_front_login_id()));
                                if (isset($get_pro_id) && $get_pro_id != null) { ?>
                                    <a href="javascript:void(0)" class="link-wish wishlist fill-heart" id="<?= $value['id_unique'] ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" class="link-wish wishlist " id="<?= $value['id_unique'] ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                <?php } ?>
								<div class="media">
									<a href="<?=base_url('/product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>">
										<img src="<?php echo $value['main_image'];?>" alt="" class="img-fluid">
									</a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#"><?= $value['title']; ?></a></h4>
									<div class="meta">
										<div class="price">$<?= $value['other_info']['final_price'][0]; ?></div>
										<div class="rating">
											<a href="javascript:void(0)" data-custom-image="" data-id="<?php echo $value['id_unique']; ?>" class="btn btn-cart add-to-cart"><?= get_line_front('add_to_cart'); ?></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
					}
				?>
				<!-- <div class="col-sm-4 col-md-3">
					<div class="rs-shop-box mb-5">
						<a href="javascript:void(0);" class="link-wish">
							<i class="fa fa-heart"></i>
						</a>
						<div class="media">
							<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="body-text">
							<h4 class="title"><a href="#">Original 6kg</a></h4>
							<div class="meta">
								<div class="price">$29</div>
								<div class="rating">
									<a href="#" class="btn btn-cart">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-3">
					<div class="rs-shop-box mb-5">
						<a href="javascript:void(0);" class="link-wish">
							<i class="fa fa-heart"></i>
						</a>
						<div class="media">
							<a href="#"><img src="images/pro-2.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="body-text">
							<h4 class="title"><a href="#">Grass fed lamb 6kg</a></h4>
							<div class="meta">
								<div class="price">$29</div>
								<div class="rating">
									<a href="#" class="btn btn-cart">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-3">
					<div class="rs-shop-box mb-5">
						<a href="javascript:void(0);" class="link-wish">
							<i class="fa fa-heart"></i>
						</a>
						<div class="media">
							<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="body-text">
							<h4 class="title"><a href="#">Original 6kg</a></h4>
							<div class="meta">
								<div class="price">$29</div>
								<div class="rating">
									<a href="#" class="btn btn-cart">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</section>
	<section class="related-product">
		<div class="container">
			<div class="related-product">
				<div class="related-product-title">
					<h3><?php echo get_line('related_product'); ?></h3>
				</div>
			</div>
			<div id="blogCarousel" class="carousel slide" data-ride="carousel">

				<ol class="carousel-indicators">
					<?php
					$count = 0;
					foreach ($related_product as $key => $value) {
						if ($key == 0) {
					?>
							<li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
						<?php
						}
						if ($key != 0 && ($key) % 3 == 0) {
							$count++;
						?>
							<li data-target="#blogCarousel" data-slide-to="<?php echo $count; ?>"></li>
						<?php
						}
						?>
					<?php
					}
					?>
				</ol>
				<div class="carousel-inner">
					<?php
					foreach ($related_product as $key => $value) {
						if ($key == 0) {
					?>
							<div class="carousel-item active">
								<div class="row">
								<?php
							}
							if ($key != 0 && ($key) % 3 == 0) {
								echo '</div></div><div class="carousel-item"><div class="row">';
							}
								?>
								<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
									<div class="mens-clothes-box">
										<ul>
											<li>
												<?php
												$get_pro_id = get_details('wishlist', array('id_product' => $value['id_unique'], 'login_id' => get_front_login_id()));
												if (isset($get_pro_id) && $get_pro_id != null) { ?>
													<a href="javascript:void(0)" class="wishlist" id="<?= $value['id_unique'] ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
												<?php } else { ?>
													<a href="javascript:void(0)" class="wishlist" id="<?= $value['id_unique'] ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
												<?php } ?>
											</li>
											<?php
											if (isset($value['is_new']) && $value['is_new'] == '1') {
											?>
												<li class="new"><?php echo get_line('new'); ?></li>
											<?php
											}
											?>
										</ul>
										<div class="mens-clothes-img text-center">
											<a href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>"><img src="<?php echo $value['main_image']; ?>"></a>
										</div>
										<div class="mens-clothes-name text-center">
											<h4 style="font-size: 18px;"><b><a class="theme-link" href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>"><?php echo $value['title']; ?></a></b></h4>
											<h4><?php echo isset($value['other_info']['final_price'][0]) ? $value['other_info']['final_price'][0] : 0; ?> <?php echo PRICE_KEY; ?></h4>
										</div>
									</div>
								</div>
							<?php
						}
							?></div>
							</div>
				</div>
	</section>
<?php
}
?>
<script type="text/javascript">
	<?php
	unset($info['description'], $info['additional_description'], $info['long_description']);
	?>
	var info = '<?php echo isset($info) ? json_encode($info) : ''; ?>';
	if (info != '') {
		info = JSON.parse(info);
	}
</script>
<?php
$footer_js = array(
	base_url('assets/js/image-zoom.js'),
	base_url('assets/js/product.js')
);
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
	$(document).ready(function() {

		//Customize product
			$(".custom_radio").click(function(){
				var r_val = $(this).val();

				if(r_val==0){
					$(".customize-form").addClass("hidden");
				}else{
					$(".customize-form").removeClass("hidden");
				}
			});


		//If your <ul> has the id "glasscase"
		$('#glasscase').glassCase({
			'thumbsPosition': 'bottom',
			'widthDisplay': 560
		});
		try {
			$('#blogCarousel').carousel({
				interval: 5000
			});
		} catch (err) {}

		$('.comment-form').submit(function() {
			loading(true);
			$.ajax({
				url: BASE_URL + 'ajax/save_comment',
				method: 'POST',
				data: $('.comment-form').serialize(),
				dataType: 'json',
				success: function(response) {
					toastr.remove();
					if (response.status) {
						toastr.success(response.message);
						setTimeout(function() {
							window.location = window.location.href;
						}, 250);
					} else {
						toastr.error(response.message);
					}
				}
			})
		});

		/* 1. Visualizing things on Hover - See next part for action on click */
		$('#stars li').on('mouseover', function() {
			var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

			// Now highlight all the stars that's not after the current hovered star
			$(this).parent().children('li.star').each(function(e) {
				if (e < onStar) {
					$(this).addClass('hover');
				} else {
					$(this).removeClass('hover');
				}
			});

		}).on('mouseout', function() {
			$(this).parent().children('li.star').each(function(e) {
				$(this).removeClass('hover');
			});
		});


		/* 2. Action to perform on click */
		$('#stars li').on('click', function() {
			var onStar = parseInt($(this).data('value'), 10); // The star currently selected
			var stars = $(this).parent().children('li.star');

			for (i = 0; i < stars.length; i++) {
				$(stars[i]).removeClass('selected');
			}

			for (i = 0; i < onStar; i++) {
				$(stars[i]).addClass('selected');
			}

			// JUST RESPONSE (Not needed)
			var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
			$('[name="rating"]').val(ratingValue);
		});

	});
</script>

<script src="<?php echo base_url('assets/js/zoomy.js')?>"></script>

	<script>

		var image_urls = info.other_info.image_info;
		var img_url =[];
		$.each(image_urls, function( index, value ) {
		 	img_url[index] = value.image_name;
		});
		img_url.push(info.main_image); 
		img_url.reverse();
		console.log(img_url)
	    
	    var options = {
	    };
	    jQuery('#el').zoomy(img_url,options);
 	</script>
<?php
if (isset($info['is_customizable']) && $info['is_customizable'] == '1') {
?>
	<script src="<?php echo base_url('assets/custom-design/js/jquery-ui.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/custom-design/js/fabric.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/custom-design/js/FancyProductDesigner-all.min.js'); ?>"></script>
	<script>
		jQuery(document).ready(function() {
			$('.custom-design-btn,#cancel').click(function() {
				$('[data-custom-image]').attr('data-custom-image', '');
				$('.customize-section').slideToggle();
				$('.product-detail-page').slideToggle();
			});

			<?php
			$count = 0;
			$products = array();
			if (isset($info['main_image']) && !empty($info['main_image'])) {
				$products[$count][0]['title'] = $info['title'];
				$products[$count][0]['thumbnail'] = $info['main_image'];
				$products[$count][0]['elements'] = array();
				$products[$count][0]['elements'][0]['type'] = 'image';
				$products[$count][0]['elements'][0]['title'] = $info['title'];
				$products[$count][0]['elements'][0]['source'] = $info['main_image'];
				$products[$count][0]['elements'][0]['parameters'] = array('left' => 332, 'top' => '311');
				$count++;
			}
			if (isset($info['other_info']['image_info']) && !empty($info['other_info']['image_info'])) {
				foreach ($info['other_info']['image_info'] as $key => $value) {
					// $products['elements'][$count]['type'] = 'image';
					// $products['elements'][$count]['title'] = $info['title'];
					// $products['elements'][$count]['source'] = $info['image_name'];
					// $products['elements'][$count]['parameters'] = array('left' => 332, 'top' => '311');
					$products[$count][0]['title'] = $info['title'];
					$products[$count][0]['thumbnail'] = $value['image_name'];
					$products[$count][0]['elements'] = array();
					$products[$count][0]['elements'][0]['type'] = 'image';
					$products[$count][0]['elements'][0]['title'] = $info['title'];
					$products[$count][0]['elements'][0]['source'] = $value['image_name'];
					$products[$count][0]['elements'][0]['parameters'] = array('left' => 332, 'top' => '311');
					$count++;
				}
			}
			?>
			<?php
			if (isset($info['other_info']['attribute_images']) && !empty($info['other_info']['attribute_images'])) {
				foreach ($info['other_info']['attribute_images'] as $key => $value) {
					// $products['elements'][$count]['type'] = 'image';
					// $products['elements'][$count]['title'] = $info['title'];
					// $products['elements'][$count]['source'] = $info['image_name'];
					// $products['elements'][$count]['parameters'] = array('left' => 332, 'top' => '311');
					$products[$count][0]['title'] = $info['title'];
					$products[$count][0]['thumbnail'] = $value['image_name'];
					$products[$count][0]['elements'] = array();
					$products[$count][0]['elements'][0]['type'] = 'image';
					$products[$count][0]['elements'][0]['title'] = $info['title'];
					$products[$count][0]['elements'][0]['source'] = $value['image_name'];
					$products[$count][0]['elements'][0]['parameters'] = array('left' => 332, 'top' => '311');
					$count++;
				}
			}
			?>

			var $yourDesigner = $('#clothing-designer'),
				pluginOpts = {
					templatesDirectory: BASE_URL + 'assets/custom-design/html/',
					productsJSON: <?php echo json_encode($products); ?>,
					designsJSON: [{
							"source": BASE_URL + 'assets/custom-design/images/designs/swirl.png',
							"title": "Swirl",
							"parameters": {
								"zChangeable": true,
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"price": 10,
								"boundingBox": "Base",
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/swirl2.png',
							"title": "Swirl 2",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"price": 5,
								"boundingBox": "Base",
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/swirl3.png',
							"title": "Swirl 3",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/heart_blur.png',
							"title": "Heart Blur",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#bf0200",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"price": 5,
								"boundingBox": "Base",
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/converse.png',
							"title": "Converse",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/crown.png',
							"title": "Crown",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"autoCenter": true
							}
						},
						{
							"source": BASE_URL + 'assets/custom-design/images/designs/men_women.png',
							"title": "Men hits Women",
							"parameters": {
								"left": 215,
								"top": 200,
								"colors": "#000000",
								"removable": true,
								"draggable": true,
								"rotatable": true,
								"resizable": true,
								"boundingBox": "Base",
								"autoCenter": true
							}
						}
					],
					langJSON: {
						"toolbar": {
							"fill": "Fill",
							"font_family_search": "Search Fonts",
							"transform": "Transform",
							"position": "Position",
							"move_up": "Move Up",
							"move_down": "Move Down",
							"reset": "Reset",
							"font_size": "Font Size",
							"line_height": "Line Height",
							"bold": "Bold",
							"italic": "Italic",
							"underline": "Underline",
							"text_align": "Text Alignment",
							"stroke": "Stroke",
							"curved_text": "Curved Text",
							"edit_text": "Edit Text",
							"color": "Color",
							"patterns": "Patterns",
							"transparency": "Transparency",
							"align_left": "Align Left",
							"align_top": "Align Top",
							"align_right": "Align Right",
							"align_bottom": "Align Bottom",
							"center_h": "Center Horizontal",
							"center_v": "Center Vertical",
							"flip_h": "Flip Horizontal",
							"flip_v": "Flip Vertical",
							"curved_text_switch": "Switch",
							"curved_text_reverse": "Reverse",
							"radius": "Radius",
							"spacing": "Spacing",
							"letter_spacing": "Letter Spacing",
							"advanced_editing": "Advanced Editing",
							"text_transform": "Text Transform",
							"stroke_width": "Width",
							"back": "Back",
							"shadow": "Shadow",
							"shadow_blur": "Blur",
							"shadow_offset_x": "Offset X",
							"shadow_offset_y": "Offset Y"
						},
						"actions": {
							"reset_product": "Reset",
							"zoom": "Zoom",
							"magnify_glass": "Magnify Glass",
							"download": "Download",
							"download_current_view": "Only export current showing view",
							"info": "Info",
							"info_content": "Content for the Info Action Button. Change content via language JSON.",
							"print": "Print",
							"save": "Save",
							"load": "Load Designs",
							"manage_layers": "Manage Layers",
							"qr_code": "Add QR-Code",
							"qr_code_input": "Enter a URL, some text...",
							"qr_code_add_btn": "Add QR-Code",
							"undo": "Undo",
							"redo": "Redo",
							"snap": "Center Snap",
							"preview_lightbox": "Preview",
							"save_placeholder": "Optional: Enter a title",
							"ruler": "Toggle Ruler",
							"previous_view": "Previous View",
							"next_view": "Next View",
							"start_guided_tour": "Start Guided Tour",
							"load_designs_empty": "No Designs saved yet!"
						},
						"modules": {
							"products": "Swap Product",
							"products_confirm_replacement": "Are you sure to replace the product?",
							"products_confirm_button": "Yes, please!",
							"images": "Add Image",
							"upload_zone": "Click or drop images here",
							"facebook_select_album": "Select An Album",
							"text": "Add Text",
							"text_input_placeholder": "Enter some text",
							"enter_textbox_width": "Optional: Enter a fixed width",
							"text_add_btn": "Add Text",
							"designs": "Choose From Designs",
							"designs_search_in": "Search in",
							"manage_layers": "Manage Layers",
							"images_agreement": "I have the rights to use the images.",
							"images_confirm_button": "Confirm",
							"images_pdf_upload_info": "Creating images from PDF...",
							"pixabay_search": "Search in Pixabay library",
							"depositphotos_search": "Search in depositphotos.com",
							"depositphotos_search_category": "Search In ",
							"text_layers_clear": "Clear",
							"layouts": "Layouts",
							"layouts_confirm_replacement": "Are you sure to replace all elements in the current view?",
							"layouts_confirm_button": "Yes, got it!"
						},
						"image_editor": {
							"mask": "Mask",
							"filters": "Filters",
							"color_manipulation": "Color Manipulation",
							"custom_cropping": "Custom Cropping",
							"filter_none": "None",
							"brightness": "Brightness",
							"contrast": "Contrast",
							"remove_white": "Remove White",
							"remove_white_distance": "Distance",
							"restore": "Restore Original",
							"save": "Save"
						},
						"misc": {
							"initializing": "Initializing Product Designer",
							"out_of_bounding_box": "Move element in its containment!",
							"product_saved": "Product Saved!",
							"loading_image": "Loading Image...",
							"uploaded_image_size_alert": "Sorry! But the uploaded image size does not conform our indication of size.<br />Minimum Width: %minW pixels<br />Minimum Height: %minH pixels<br />Maximum Width: %maxW pixels<br />Maximum Height: %maxH pixels",
							"modal_done": "Done",
							"minimum_dpi_info": "The JPEG image does not have the required DPI of %dpi.",
							"maximum_size_info": "The image %filename exceeds the maximum file size of %mbMB.",
							"customization_required_info": "You need to customize the default design!",
							"not_supported_device_info": "Sorry! But the product designer can not be displayed on your device. Please use a device with a larger screen!",
							"image_added": "Image Added!",
							"reset_confirm": "Are you sure to reset everything?",
							"popup_blocker_alert": "Please disable your pop-up blocker and try again.",
							"view_optional_unlock": "Unlock view",
							"guided_tour_back": "Back",
							"guided_tour_next": "Next"
						},
						"plus": {
							"names_numbers": "Names & Numbers",
							"names_numbers_add_new": "Add New",
							"drawing": "Free Drawing",
							"drawing_brush_type": "Brush Type",
							"drawing_pencil": "Pencil",
							"drawing_circle": "Circle",
							"drawing_spray": "Spray",
							"drawing_color": "Color",
							"drawing_line_width": "Line Width",
							"drawing_draw_here": "Draw Here",
							"drawing_clear": "Clear",
							"drawing_add": "Add",
							"bulk_add_variations_title": "Bulk Order",
							"bulk_add_variations_add": "Add",
							"dynamic_views": "Manage Views",
							"dynamic_views_add_view_info": "Add new view",
							"dynamic_views_add_blank": "Blank",
							"dynamic_views_add_from_layouts": "From Layouts"
						}
					},
					stageWidth: 1200,
					editorMode: false,
					smartGuides: true,
					fonts: [{
							name: 'Helvetica'
						},
						{
							name: 'Times New Roman'
						},
						{
							name: 'Pacifico',
							url: BASE_URL + 'assets/custom-design/fonts/Pacifico.ttf',
						},
						{
							name: 'Arial'
						},
						{
							name: 'Lobster',
							url: 'google'
						}
					],
					customTextParameters: {
						colors: false,
						removable: true,
						resizable: true,
						draggable: true,
						rotatable: true,
						autoCenter: true,
						boundingBox: "Base"
					},
					customImageParameters: {
						draggable: true,
						removable: true,
						resizable: true,
						rotatable: true,
						colors: '#000',
						autoCenter: true,
						boundingBox: "Base"
					},
					actions: {
						'top': ['download', 'print', 'snap', 'preview-lightbox'],
						'right': ['magnify-glass', 'zoom', 'reset-product', 'qr-code', 'ruler'],
						'bottom': ['undo', 'redo'],
						'left': ['manage-layers', 'info', 'save', 'load']
					}
				},

				yourDesigner = new FancyProductDesigner($yourDesigner, pluginOpts);

			//print button
			$('#print-button').click(function() {
				yourDesigner.print();
				return false;
			});

			//create an image
			$('#image-button').click(function() {
				var image = yourDesigner.createImage();
				return false;
			});

			//checkout button with getProduct()
			$('#checkout-button').click(function() {
				var product = yourDesigner.getProduct();
				console.log(product);
				return false;
			});
			
			//event handler when the price is changing
			$yourDesigner.on('priceChange', function(evt, price, currentPrice) {
				$('#thsirt-price').text(currentPrice);
			});

			//save image on webserver
			$('#save-image-php').click(function() {

				yourDesigner.getProductDataURL(function(dataURL) {
					/*$.post(BASE_URL + "cart/save_customized_product", {
							base64_image: dataURL
						}).done(function(response) {
							response = JSON.parse(response);
							if (response.status) {
								//toastr.success(response.message);
								$('[data-custom-image]').attr('data-custom-image', response.data);
								$('.customize-section').slideToggle();
								$('.product-detail-page').slideToggle();
								$(window).trigger('resize');
								$('.add-to-cart').trigger('click');
							} else {
								toastr.error(response.messagesuccess);
							}
						})
						.fail(function() {
							window.location = window.location.href;
						});*/


					// Get the form element withot jQuery
					var form = document.getElementById("commentform");

					var ImageURL = dataURL;
					// Split the base64 string in data and contentType
					var block = ImageURL.split(";");
					// Get the content type of the image
					var contentType = block[0].split(":")[1]; // In this case "image/gif"
					// get the real base64 content of the file
					var realData = block[1].split(",")[1]; // In this case "R0lGODlhPQBEAPeoAJosM...."

					// Convert it to a blob to upload
					var blob = b64toBlob(realData, contentType);
					// Create a FormData and append the file with "image" as parameter name
					var formDataToUpload = new FormData(form);
					formDataToUpload.append("image", blob);

					$.ajax({
						url: BASE_URL + "cart/save_customized_product",
						data: formDataToUpload,
						type: "POST",
						contentType: false,
						processData: false,
						cache: false,
						dataType: "json",
						error: function(err) {
							console.error(err);
						},
						success: function(response) {
							if (response.status) {
								//toastr.success(response.message);
								$('[data-custom-image]').attr('data-custom-image', response.data);
								$('.customize-section').slideToggle();
								$('.product-detail-page').slideToggle();
								$(window).trigger('resize');
								$('.add-to-cart').trigger('click');
							} else {
								toastr.error(response.messagesuccess);
							}
						},
						complete: function() {
							console.log("Request finished.");
						}
					});
				});

			});
		});

		/**
		 * Convert a base64 string in a Blob according to the data and contentType.
		 * 
		 * @param b64Data {String} Pure base64 string without contentType
		 * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
		 * @param sliceSize {Int} SliceSize to process the byteCharacters
		 * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
		 * @return Blob
		 */
		function b64toBlob(b64Data, contentType, sliceSize = 512) {
			contentType = contentType || '';
			sliceSize = sliceSize || 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, {
				type: contentType
			});
			return blob;
		}

		

		
	</script>
<?php
}
$this->load->view('include/footer'); ?>