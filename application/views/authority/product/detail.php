<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->view('authority/common/header');
$this->view('authority/common/sidebar');
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo isset($page_title) ? $page_title : ''; ?></li>
                    </ol>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug); ?>" class="btn btn-sm btn-info text-white cursor-pointer"><i class="fa fa-angle-left"></i> <?php echo get_line('back'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo isset($page_title) ? $page_title : ''; ?></h3>
                            <?php /* <div class="card-tools">
                              <div class="input-group input-group-sm" style="width: 150px;">
                              <input type="text" name="search_value" class="form-control float-right" placeholder="<?php echo get_line('search'); ?>" autocomplete="off">
                              <div class="input-group-append">
                              <button type="button" class="btn btn-default search-btn"><i class="fas fa-search"></i></button>
                              </div>
                              </div>
                              </div> */ ?>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="product-tabs-tab" role="tablist">
                                <li class="nav-item ">
                                    <a class="nav-link active" id="product-tabs-category-tab" href="#product-tabs-category" data-toggle="pill" role="tab" aria-controls="product-tabs-category" aria-selected="false"><?php echo get_line('category'); ?></a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" id="product-tabs-category-tab" href="#product-tabs-category-desc" data-toggle="pill" role="tab" aria-controls="product-tabs-category" aria-selected="false"><?php echo get_line('description'); ?></a>
                                </li>
                            </ul>
                            <div class="tab-content" id="product-tabs-tabContent">
                                <div class="tab-pane fade active show" id="product-tabs-category" role="tabpanel" aria-labelledby="product-tabs-category-tab">
                                    <div class="row mt-3 ml-2">
                                        <div class="col-lg-6">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td><b><?php echo get_line('product'); ?> <?php echo get_line('category'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            $info = get_records('category', array('SELECT' => 'name', 'WHERE' => array('id' => $product_info['id_category'])));
                                                            echo isset($info[0]['name']) ? $info[0]['name'] : '';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo get_line('is_new'); ?> <?php echo get_line('product'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            echo isset($product_info['is_new']) && $product_info['is_new'] == '1' ? get_line('yes') : get_line('no');
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo get_line('is_gift_wraping'); ?> <?php echo get_line('product'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            echo isset($product_info['is_gift_wraping']) && $product_info['is_gift_wraping'] == '1' ? get_line('yes') : get_line('no');
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo get_line('is_customizable'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            echo isset($product_info['is_customizable']) && $product_info['is_customizable'] == '1' ? get_line('yes') : get_line('no');
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo get_line('title'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            echo isset($product_info['title']) ? $product_info['title'] : '';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    if (isset($product_info['has_attributes']) && $product_info['has_attributes'] == '0') {
                                                    ?>
                                                        <tr>
                                                            <td><b><?php echo get_line('price'); ?>:</b></td>
                                                            <td>
                                                                <?php
                                                                echo isset($product_info['price']) ? $product_info['price'] : '';
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b><?php echo get_line('discount'); ?> (%):</b></td>
                                                            <td>
                                                                <?php
                                                                echo isset($product_info['discount']) ? $product_info['discount'] . ' %' : '';
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        if (isset($product_info['by_quantity_or_availability']) && $product_info['by_quantity_or_availability'] == '0') {
                                                        ?>
                                                            <tr>
                                                                <td><b><?php echo get_line('availability'); ?>:</b></td>
                                                                <td>
                                                                    <?php
                                                                    if ($product_info['is_product_available'] == '1') {
                                                                        echo 'Available';
                                                                    } else {
                                                                        echo 'Not Available';
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        } else if (isset($product_info['by_quantity_or_availability']) && $product_info['by_quantity_or_availability'] == '1') {
                                                        ?>
                                                            <tr>
                                                                <td><b><?php echo get_line('quantity'); ?>:</b></td>
                                                                <td>
                                                                    <?php
                                                                    echo isset($product_info['quantity']) ? $product_info['quantity'] : 'Quantity N/A';
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <?php
                                                if (isset($product_images) && !empty($product_images)) {
                                                    foreach ($product_images as $image_name) {
                                                ?>
                                                        <div class="col-md-4">
                                                            <div class="img-wrap">
                                                                <?php /* <span data-id-unique="<?php echo isset($product_info['id_unique']) ? $product_info['id_unique'] : ''; ?>" data-image="<?php echo $image_name['image_name']; ?>" class="delete-icon"><i class="fa fa-trash"></i></span> */ ?>
                                                                <img style="max-width:100px;max-height: 100px;" class="img img-thumbnail" src="<?php echo base_url('uploads/product/' . $image_name['image_name']); ?>">
                                                            </div>
                                                        </div>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                        if (isset($product_info['has_attributes']) && $product_info['has_attributes'] == '1') {
                                        ?>
                                            <div class="col-lg-12">
                                                <h4><?php echo get_line('product'); ?> <?php echo get_line('attribute'); ?></h4>
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <?php
                                                            $sql = 'SELECT `attribute_name` FROM `attributes` WHERE `id_language` = "' . $this->_language_default['id'] . '" AND `id_unique` IN ("' . implode('","', explode(',', $product_info['id_attributes'])) . '") ';
                                                            $info = $this->common_model->get_data_with_sql($sql);
                                                            if ($info['row_count'] > 0) {
                                                                foreach ($info['data'] as $value) {
                                                                    echo '<th>' . $value['attribute_name'] . '</th>';
                                                                }
                                                            }
                                                            ?>
                                                            <th><?php echo get_line('price'); ?></th>
                                                            <th>Discount (%)</th>
                                                            <?php
                                                            if (isset($product_info['by_quantity_or_availability']) && $product_info['by_quantity_or_availability'] == '0') {
                                                            ?>
                                                                <th><?php echo get_line('availability'); ?></th>
                                                            <?php
                                                            } else if (isset($product_info['by_quantity_or_availability']) && $product_info['by_quantity_or_availability'] == '1') {
                                                            ?>
                                                                <th><?php echo get_line('quantity'); ?></th>
                                                            <?php
                                                            }
                                                            ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($product_attributes as $attributes) {
                                                            echo '<tr>';
                                                            $price = $discount = 0;
                                                            foreach ($attributes as $a_value) {
                                                                $price = $a_value['attribute_price'];
                                                                $discount = $a_value['attribute_discount'];
                                                                $attribute_is_product_available = $a_value['attribute_is_product_available'];
                                                                $attribute_quantity = $a_value['attribute_quantity'];
                                                                $info = get_records('attribute_values', array('SELECT' => 'attribute_value', 'WHERE' => array('id_unique' => $a_value['id_unique_attribute_values'], 'id_language' => $this->_language_default['id'])));
                                                                if (!empty($info)) {
                                                                    echo '<td>' . $info[0]['attribute_value'] . '</td>';
                                                                } else {
                                                                    echo '<td></td>';
                                                                }
                                                            }
                                                            echo '<td>' . $price . '</td>';
                                                            echo '<td>' . $discount . ' %</td>';
                                                            if (isset($product_info['by_quantity_or_availability']) && $product_info['by_quantity_or_availability'] == '0') {
                                                                echo '<td>';
                                                                if ($attribute_is_product_available == '1') {
                                                                    echo get_line('available');
                                                                } else {
                                                                    echo get_line('not_available');
                                                                }
                                                                echo '</td>';
                                                            } else if (isset($product_info['by_quantity_or_availability']) && $product_info['by_quantity_or_availability'] == '1') {
                                                                echo '<td>' . $attribute_quantity . '</td>';
                                                            }
                                                            echo '</tr>';
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="product-tabs-category-desc" role="tabpanel" aria-labelledby="product-tabs-category-tab">
                                    <div class="row mt-3 ml-2">
                                        <div class="col-lg-12">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td><b><?php echo get_line('description'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            echo isset($product_info['description']) ? $product_info['description'] : '';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo get_line('long_description'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            echo isset($product_info['long_description']) ? $product_info['long_description'] : '';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo get_line('additional_description'); ?>:</b></td>
                                                        <td>
                                                            <?php
                                                            echo isset($product_info['additional_description']) ? $product_info['additional_description'] : '';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$footer_js = array();
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<?php $this->view('authority/common/footer'); ?>