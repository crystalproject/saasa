<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<table class="table table-bordered">
    <thead>
        <tr>
            <th><?php echo get_line('name'); ?></th>
            <th><?php echo get_line('email_address'); ?></th>
            <th><?php echo get_line('comment'); ?></th>
            <th><?php echo get_line('rating'); ?></th>
            <th><?php echo get_line('action'); ?></th>
        </tr>
    </thead>
    <tbody>

        <?php
        if (isset($reviews) && !empty($reviews)) {
            foreach ($reviews as $value) {
        ?>
                <tr>
                    <td><?php echo $value['first_name']; ?></td>
                    <td><?php echo $value['email_address']; ?></td>
                    <td><?php echo $value['comment']; ?></td>
                    <td><?php echo $value['rating']; ?></td>                    
                    <td>                        
                        <?php
                        if ($value['is_approved'] == '1') {
                            echo '<span title="Status" class="btn bg-gradient-success btn-xs change-status" data-table="product_review" data-id="' . $value['id'] . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                        } else {
                            echo '<span title="Status" class="btn bg-gradient-danger btn-xs change-status" data-table="product_review" data-id="' . $value['id'] . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                        }
                        ?>
                    </td>
                </tr>
            <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="5" class="text-center"><?php echo get_line('record_not_available'); ?></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>