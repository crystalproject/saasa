<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->view('authority/common/header');
$this->view('authority/common/sidebar');
$discount_type = '';
$discount_value = 0;
$total_product_price = 0;
$total_bag_price = 0;
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo get_line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo isset($page_title) ? $page_title : ''; ?></li>
                    </ol>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug); ?>" class="btn btn-sm btn-info text-white cursor-pointer"><i class="fa fa-angle-left"></i> <?php echo get_line('back'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('order_detail'); ?></h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                <tr>
                                    <th><?php echo get_line('order_id'); ?></th>
                                    <td><?php echo isset($order_info['order_id']) ? $order_info['order_id'] : ''; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo get_line('order_date'); ?></th>
                                    <td><?php echo isset($order_info['order_date_time']) ? date('d-m-Y h:i A', strtotime($order_info['order_date_time'])) : ''; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo get_line('payment_mode'); ?></th>
                                    <td><?php echo isset($order_info['payment_mode']) ? get_payment_mode($order_info['payment_mode'], $this->_language_default['id']) : ''; ?></td>
                                </tr>
                                <?php /* <tr>
                                  <th>Payment Status</th>
                                  <td><?php echo isset($order_info['payment_status']) ? get_payment_status($order_info['payment_status'])['name'] : ''; ?></td>
                                  </tr> */ ?>
                                <tr>
                                    <th><?php echo get_line('order_status'); ?></th>
                                    <td>
                                        <?php
                                        $selected = isset($order_info['order_status']) ? $order_info['order_status'] : '';
                                        if ($selected == '6') {
                                            echo get_order_status(6)['name'];
                                        } else if ($selected == '3') {
                                            echo get_order_status(3)['name'];
                                        } else {
                                            $order_status = get_order_status();
                                            $options = array();
                                            foreach ($order_status as $key => $value) {
                                                if ($key == $selected or !in_array($key, $removed_status)) {
                                                    $options[$key] = $value['name'];
                                                }
                                            }
                                            echo form_dropdown('order_status', $options, $selected, 'class="form-control form-control-sm"');
                                        ?>
                                            <button type="button" class="btn btn-xs btn-primary pull-right change-order-status" data-id="<?php echo $order_info['id']; ?>">SAVE</button>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('user_info'); ?></h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                <tr>
                                    <th><?php echo get_line('name'); ?></th>
                                    <td><?php echo isset($order_info['full_name']) ? $order_info['full_name'] : ''; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo get_line('email_address'); ?></th>
                                    <td><?php echo isset($order_info['email']) ? $order_info['email'] : ''; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo get_line('mobile_number'); ?></th>
                                    <td><?php echo isset($order_info['mobile_number']) ? $order_info['mobile_number'] : ''; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo get_line('address'); ?></th>
                                    <td><?php echo isset($order_info['address']) ? $order_info['address'] : ''; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('coupon_info'); ?></h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                <?php
                                if (isset($order_info['id_coupon']) && !empty($order_info['id_coupon'])) {
                                    $discount_type = isset($order_info['discount_type']) ? $order_info['discount_type'] : '';
                                    $discount_value = isset($order_info['discount_value']) ? $order_info['discount_value'] : '';
                                ?>
                                    <tr>
                                        <th><?php echo get_line('coupon_code'); ?></th>
                                        <td><?php echo isset($order_info['coupon_code']) ? $order_info['coupon_code'] : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <th><?php echo get_line('discount_type'); ?></th>
                                        <td><?php echo get_discount_type($discount_type)['name']; ?></td>
                                    </tr>
                                    <tr>
                                        <th><?php echo get_line('discount_value'); ?></th>
                                        <td><?php echo isset($order_info['discount_value']) ? $order_info['discount_value'] : ''; ?></td>
                                    </tr>
                                <?php
                                } else {
                                ?>
                                    <tr>
                                        <td colspan="2"><?php echo get_line('record_not_available'); ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('product_detail'); ?></h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?php echo get_line('product_title'); ?></th>
                                        <th><?php echo get_line('image'); ?></th>
                                        <th><?php echo get_line('attribites'); ?></th>
                                        <th class="text-right"><?php echo get_line('unit_price'); ?></th>
                                        <th class="text-right"><?php echo get_line('discount'); ?></th>
                                        <th class="text-right"><?php echo get_line('quantity'); ?></th>
                                        <th class="text-right"><?php echo get_line('final_price'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($product_info) && !empty($product_info)) {
                                        foreach ($product_info as $value) {
                                            $discount = calculate_discount(0, $value['price'] * $value['quantity'], $value['discount']);
                                            $price = ($value['price'] * $value['quantity']) - $discount;
                                            $total_product_price += $price;
                                            echo '<tr>';
                                            echo '<td>' . $value['title'] . '</td>';
                                            if (isset($value['customized_image']) && !empty($value['customized_image'])) {
                                                echo '<td><img src="' . base_url('uploads/product/' . $value['customized_image']) . '" class="img img-thumbnail img-listing"/></td>';
                                            } else {
                                                echo '<td><img src="' . base_url('uploads/product/' . $value['main_image']) . '" class="img img-thumbnail img-listing"/></td>';
                                            }
                                            echo '<td>';
                                            if (isset($value['attributes']) && !empty($value['attributes'])) {
                                                $headers = array();
                                                echo '<table class="table table-bordered table-minimal mb-0">';
                                                foreach ($value['attributes'] as $key => $a_value) {
                                                    echo '<tr>';
                                                    echo '<th class="p-1">' . $a_value['attribute_name'] . '</th>';
                                                    echo '<td class="p-1">' . $a_value['attribute_value'] . '</td>';
                                                    echo '</tr>';
                                                }
                                                echo '</table>';
                                            } else {
                                                echo 'N/A';
                                            }
                                            echo '</td>';
                                            echo '<td class="text-right">' . $value['price'] . '</td>';
                                            echo '<td class="text-right">' . $value['discount'] . ' %</td>';
                                            echo '<td class="text-right">' . $value['quantity'] . '</td>';
                                            echo '<td class="text-right">' . $price . '</td>';
                                            echo '</tr>';
                                        }
                                        echo '<tr>';
                                        echo '<td colspan="6" class="text-right"><b>' . get_line('total') . '</b></td>';
                                        echo '<td class="text-right">' . $total_product_price . '</td>';
                                        echo '</tr>';
                                        $coupon_discount = calculate_discount($discount_type, $total_product_price, $discount_value);
                                        $discount = 0;
                                        if ($discount_value > 0) {
                                            $discount = $coupon_discount;
                                            $total_product_price = $total_product_price - $discount;
                                        }
                                        $sign = '';
                                        if ($discount > 0) {
                                            $sign = '- ';
                                        } else {
                                            $discount = 0;
                                        }
                                        echo '<tr>';
                                        echo '<td colspan="6" class="text-right"><b>' . get_line('coupon_discount') . '</b></td>';
                                        echo '<td class="text-right">' . $sign . $discount . '</td>';
                                        echo '</tr>';
                                        echo '<tr>';
                                        echo '<td colspan="6" class="text-right"><b>' . get_line('product') . ' Total</b></td>';
                                        echo '<td class="text-right">' . $total_product_price . '</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php /*<div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('bag_detail'); ?></h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?php echo get_line('title'); ?></th>                                        
                                        <th class="text-right"><?php echo get_line('unit_price'); ?></th>
                                        <th class="text-right"><?php echo get_line('quantity'); ?></th>
                                        <th class="text-right"><?php echo get_line('final_price'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($bag_info) && !empty($bag_info)) {
                                        foreach ($bag_info as $value) {
                                            $price = ($value['price'] * $value['quantity']);
                                            $total_product_price += $price;
                                            $total_bag_price += $price;
                                            echo '<tr>';
                                            echo '<td>' . $value['title'] . '</td>';
                                            echo '<td class="text-right">' . $value['price'] . '</td>';
                                            echo '<td class="text-right">' . $value['quantity'] . '</td>';
                                            echo '<td class="text-right">' . $price . '</td>';
                                            echo '</tr>';
                                        }
                                        echo '<tr>';
                                        echo '<td colspan="3" class="text-right"><b>' . get_line('bag') . ' Total</b></td>';
                                        echo '<td class="text-right">' . $total_bag_price . '</td>';
                                        echo '</tr>';
                                        echo '<tr>';
                                        echo '<td colspan="3" class="text-right"><b>Final (' . get_line('product') . ' + ' . get_line('bag') . ') Total</b></td>';
                                        echo '<td class="text-right">' . $total_product_price . '</td>';
                                        echo '</tr>';
                                    } else {
                                        echo '<tr>';
                                        echo '<td colspan="4" class="text-center">'.get_line('record_not_available').'</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> */ ?>
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('billing_info'); ?></h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                <?php
                                $billing_info = json_decode($order_info['billing_info'], true);
                                foreach ($billing_info as $key => $value) {
                                ?>
                                    <tr>
                                        <th><?php echo get_line(str_replace('billing_', '', $key)); ?></th>
                                        <td><?php echo $value; ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('order_history'); ?></h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?php echo get_line('state'); ?></th>
                                        <th><?php echo get_line('date_time'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($history_info) && !empty($history_info)) {
                                        foreach ($history_info as $value) {
                                            echo '<tr>';
                                            echo '<td>' . get_order_status($value['order_status'])['name'] . '</td>';
                                            echo '<td>' . date('d-m-Y h:i A', strtotime($value['created_at'])) . '</td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$footer_js = array();
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<script>
    $(document).ready(function() {
        $('.change-order-status').click(function() {
            loading(true);
            var current = $(this);
            var id_product_order = current.attr('data-id');
            var order_status = $('[name="order_status"]').val();
            $.ajax({
                url: BASE_URL + 'authority/product_order/change_status',
                method: 'POST',
                data: {
                    'id_product_order': id_product_order,
                    'order_status': order_status
                },
                dataType: 'json',
                success: function(response) {
                    loading(false);
                    if (response.status) {
                        toastr.success(response.message);
                        window.location = window.location.href;
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });
    });
</script>
<?php
$this->view('authority/common/footer');
