<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="modal" id="add-edit-value-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo get_line('add_attribute_value'); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                echo form_open('', array('id' => 'add-value-form', 'method' => 'POST', 'onsubmit' => 'return false;', 'class' => ''));
                echo form_hidden('id_unique', '');
                echo form_hidden('id_unique_attributes', '');
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (isset($this->_language_info) && !empty($this->_language_info)) {
                            foreach ($this->_language_info as $if_value) {
                                $attributes = array(
                                    'type' => 'text',
                                    'name' => 'attribute_value_' . $if_value['id'],
                                    'class' => 'form-control',
                                    'autocomplete' => 'off',
                                    'value' => '',
                                );
                        ?>
                                <div class="form-group">
                                    <label><?php echo get_line('value_in_language'); ?> (<?php echo $if_value['name']; ?>) <span class="required">*</span></label>
                                    <?php
                                    echo form_input($attributes);
                                    ?>
                                </div>
                        <?php
                            }
                        }
                        ?>
                        <!--<div class="form-group">
                            <?php
                            $attributes = array(
                                'type' => 'file',
                                'name' => 'image',
                                'accept' => 'image/*',
                                'class' => 'custom-file-input',
                                'id' => 'custom-file'
                            );
                            ?>
                            <label><?php echo get_line('image'); ?> </label>
                            <div class="custom-file">
                                <?php
                                echo form_input($attributes);
                                ?>
                                <label class="custom-file-label" for="custom-file"><?php echo get_line('choose_file'); ?></label>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <?php
                            $attributes = array(
                                'type' => 'submit',
                                'name' => '',
                                'class' => 'btn btn-sm btn-info text-white cursor-pointer',
                                'value' => get_line('submit'),
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>