<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo isset($page_title) ? $page_title : ''; ?></li>
                    </ol>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug); ?>" class="btn btn-sm btn-info text-white cursor-pointer"><?php echo get_line('view'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo isset($page_title) ? $page_title : ''; ?></h3>
                        </div>
                        <?php
                        echo form_open('', array('id' => 'add-form', 'method' => 'POST', 'onsubmit' => 'return false;', 'class' => ''));
                        ?>
                        <div class="card-body">
                            <?php
                            if (isset($this->_language_info) && !empty($this->_language_info)) {
                                foreach ($this->_language_info as $if_value) {
                                    $attributes = array(
                                        'type' => 'text',
                                        'name' => 'title_' . $if_value['id'],
                                        'class' => 'form-control',
                                        'autocomplete' => 'off',
                                        'value' => isset($old_info[$if_value['id']]['title']) ? $old_info[$if_value['id']]['title'] : '',
                                        'placeholder' => get_line('title')
                                    );
                            ?>
                                    <div class="form-group">
                                        <label><?php echo get_line('title'); ?> (<?php echo $if_value['name']; ?>) <span class="required">*</span></label>
                                        <?php
                                        echo form_input($attributes);
                                        ?>
                                    </div>
                                    <?php
                                    $attributes = array(
                                        'type' => 'text',
                                        'name' => 'description_' . $if_value['id'],
                                        'class' => 'form-control',
                                        'autocomplete' => 'off',
                                        'value' => isset($old_info[$if_value['id']]['description']) ? $old_info[$if_value['id']]['description'] : '',
                                        'rows' => '5',
                                        'placeholder' => get_line('description')
                                    );
                                    ?>
                                    <div class="form-group">
                                        <label><?php echo get_line('description'); ?> (<?php echo $if_value['name']; ?>) <span class="required">*</span></label>
                                        <?php
                                        echo form_textarea($attributes);
                                        ?>
                                    </div>
                                    <?php
                                    $attributes = array(
                                        'type' => 'file',
                                        'name' => 'image_' . $if_value['id'],
                                        'accept' => 'image/*',
                                    );
                                    ?>
                                    <div class="form-group">
                                        <label><?php echo get_line('image'); ?> (<?php echo $if_value['name']; ?>) <span class="required">*</span></label>
                                        <br />
                                        <?php
                                        echo form_input($attributes);
                                        ?>
                                    </div>
                            <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info"><?php echo get_line('submit'); ?></button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$footer_js = array(base_url('assets/authority/js/delete_record.js'), base_url('assets/authority/js/change_status.js'));
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<script>
    var slug = '<?php echo $this->_slug; ?>';
    $(document).ready(function() {
        $('#add-form').submit(function() {
            var current = $(this);
            loading(true);
            var $form = $('#add-form');
            var form_data = new FormData($form[0]);
            $.ajax({
                url: window.location.href,
                method: 'POST',
                data: form_data,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function(response) {
                    loading(false);
                    if (response.status) {
                        current.find('input:visible').val('');
                        toastr.success(response.message);
                        setTimeout(function() {
                            window.location = BASE_URL + 'authority/' + slug;
                        }, 2000);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>