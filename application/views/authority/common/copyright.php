<footer class="main-footer">
    <strong><?php echo get_line('copyright'); ?> &copy; <?= date("Y"); ?> <a href="<?= site_url(); ?>authority/dashboard"><?= SITE_TITLE; ?></a></strong>
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="<?= base_url() ?>assets/authority/js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url() ?>assets/authority/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/authority/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="<?= base_url() ?>assets/authority/js/demo.js"></script>-->
<script src="<?= base_url() ?>assets/authority/js/bs-custom-file-input.min.js"></script>
<!-- Toastr -->
<script src="<?= base_url() ?>assets/authority/js/toastr.min.js"></script>
<!-- Summernote -->
<!--<script src="<?= base_url() ?>assets/authority/js/summernote-bs4.min.js"></script>-->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/authority/js/select2.full.min.js"></script>
<script src="<?= base_url(); ?>assets/validation/jquery.validate.js"></script>
<script src="<?= base_url(); ?>assets/validation/common.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-confirm/jquery-confirm.min.js"></script>

<?php
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
if (isset($footer_js) && $footer_js != null) {
    foreach ($footer_js as $key => $js) {
        echo '<script src=' . $js . '></script>';
    }
}
?>
<script>
    $(document).ready(function () {
        loading(false);
        bsCustomFileInput.init();
        var success = '<?php echo $success; ?>';
        var error = '<?php echo $error; ?>';
        if (success != '') {
            toastr.success(success);
        }
        if (error != '') {
            toastr.error(error);
        }
        $('.change-language').change(function () {
            var current = $(this);
            loading(true);
            $.ajax({
                url: BASE_URL + 'authority/account/change-language',
                method: 'POST',
                data: {'id_language': current.val()},
                dataType: 'json',
                success: function (response) {
                    loading(false);
                    if (response.status) {
                        window.location = window.location.href;
                    }
                }
            });
        });

        $('.select2').select2();
        $('#select_all').on('click', function (e) {
            if ($(this).is(':checked', true)) {
                $(".chk_all").prop('checked', true);
            } else {
                $(".chk_all").prop('checked', false);
            }
        });
    });
</script>