<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 text-center">
            <?php
            if ($this->session->user_info['profile_photo'] != '') {
                $path = base_url() . PROFILE_PICTURE . $this->session->user_info['profile_photo'];
            } else {
                $path = base_url() . 'assets/uploads/default_img.png';
            }
            ?>
            <div class="">
                <a href="<?= base_url('authority/dashboard'); ?>">
                    <img src="<?= $path; ?>" class="img-circle elevation-2" alt="User <?php echo get_line('image'); ?>" style="height: 50px;">
                </a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview <?= get_active_class("my-account")['main_class']; ?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>
                            <?php echo get_line('my_account'); ?>
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?= site_url(); ?>authority/administrator/edit/<?= $this->session->user_info['id']; ?>" class="nav-link <?= get_active_class("profile")['sub_class']; ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo get_line('contact_details'); ?></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= site_url(); ?>authority/account/change-password" class="nav-link <?= get_active_class("change-password")['sub_class']; ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo get_line('change_password'); ?></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('authority/login/logout'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p><?php echo get_line('sign_out'); ?></p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview <?= get_active_class("website")['main_class']; ?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-crosshairs"></i>
                        <p>
                            <?php echo get_line('website'); ?>
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item <?= get_active_class("users")['main_class']; ?>">
                            <a href="<?= base_url('authority/users'); ?>" class="nav-link">
                                <i class="nav-icon far fa-user"></i>
                                <p><?php echo get_line('customer'); ?></p>
                            </a>
                        </li>
                        <!--<li class="nav-item <?= get_active_class("contact-us")['main_class']; ?>">
                            <a href="<?= base_url('authority/contact-us'); ?>" class="nav-link">
                                <i class="nav-icon far fa-user"></i>
                                <p><?php echo get_line('contact_us'); ?></p>
                            </a>
                        </li>-->
                        <li class="nav-item <?= get_active_class("home-slider")['main_class']; ?>">
                            <a href="<?= base_url('authority/home-slider'); ?>" class="nav-link">
                                <i class="nav-icon far fa-image"></i>
                                <p><?php echo get_line('home_slider'); ?></p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview <?= get_active_class("main")['main_class']; ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-list-alt"></i>
                                <p>
                                    <?php echo get_line('manage'); ?> <?php echo get_line('category'); ?>
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <?php /*
                                <li class="nav-item">
                                    <a href="<?= base_url('authority/category-banner'); ?>" class="nav-link <?= get_active_class("category-banner")['sub_class']; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p><?php echo get_line('category'); ?> <?php echo get_line('banner'); ?></p>
                                    </a>
                                </li> */ ?>
                                <li class="nav-item">
                                    <a href="<?= base_url('authority/category'); ?>" class="nav-link <?= get_active_class("category")['sub_class']; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p><?php echo get_line('category'); ?></p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= base_url('authority/sub-category'); ?>" class="nav-link <?= get_active_class("sub-category")['sub_class']; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p><?php echo get_line('sub_category'); ?></p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview <?= get_active_class('product-main')['main_class']; ?>">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-icon fab fa-product-hunt"></i>
                                <p> <?php echo get_line('product'); ?> <i class="right fas fa-angle-left"></i></p>
                            </a>
                           <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?= base_url('authority/attributes'); ?>" class="nav-link <?= get_active_class('attributes')['sub_class']; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p><?php echo get_line('attribute'); ?></p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?= base_url('authority/brand'); ?>" class="nav-link <?= get_active_class('brand')['sub_class']; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p><?php echo get_line('brand'); ?></p>
                                    </a>
                                </li>
                                
                                <li class="nav-item">
                                    <a href="<?= base_url('authority/product'); ?>" class="nav-link <?= get_active_class('product')['sub_class']; ?>">
                                        <i class="fab fa-product-hunt nav-icon"></i>
                                        <p><?php echo get_line('product'); ?></p>
                                    </a>
                                </li>
                                
                                <!--<li class="nav-item">
                                    <a href="<?= base_url('authority/gift-wraping'); ?>" class="nav-link <?= get_active_class('gift-wraping')['sub_class']; ?>">
                                        <i class="fa fa-gift nav-icon"></i>
                                        <p><?php echo get_line('gift_wraping'); ?></p>
                                    </a>
                                </li>-->
                                <li class="nav-item">
                                    <a href="<?= base_url('authority/product-order'); ?>" class="nav-link <?= get_active_class('product-order')['sub_class']; ?>">
                                        <i class="fa fa-shopping-cart nav-icon"></i>
                                        <p><?php echo get_line('order'); ?></p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php /*<li class="nav-item <?= get_active_class("coupons")['main_class']; ?>">
                            <a href="<?= base_url('authority/bag'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-shopping-bag"></i>
                                <p><?php echo get_line('bag'); ?></p>
                            </a>
                        </li> */ ?>
                        <?php /* <li class="nav-item <?= get_active_class("offers")['main_class']; ?>">
                          <a href="<?= base_url('authority/offers'); ?>" class="nav-link">
                          <i class="nav-icon fas fa-tree"></i>
                          <p>Offers</p>
                          </a>
                          </li> */ ?>
                        <!--<li class="nav-item <?= get_active_class("coupons")['main_class']; ?>">
                            <a href="<?= base_url('authority/coupons'); ?>" class="nav-link">
                                <i class="nav-icon fas fa fa-gift"></i>
                                <p><?php echo get_line('coupon'); ?></p>
                            </a>
                        </li>-->
                        <li class="nav-item <?= get_active_class("payment-mode")['main_class']; ?>">
                            <a href="<?= base_url('authority/payment-mode'); ?>" class="nav-link">
                                <i class="nav-icon fa fa-credit-card"></i>
                                <p><?php echo get_line('payment_mode'); ?></p>
                            </a>
                        </li>
                        <?php /*<li class="nav-item <?= get_active_class("keywords")['main_class']; ?>">
                            <a href="<?= base_url('authority/keywords'); ?>" class="nav-link">
                                <i class="nav-icon fa fa-tag"></i>
                                <p><?php echo get_line('keywords'); ?></p>
                            </a>
                        </li> */ ?>
                        <li class="nav-item <?= get_active_class("static-page")['main_class']; ?>">
                            <a href="<?= base_url('authority/static-page'); ?>" class="nav-link">
                                <i class="nav-icon far fa-image"></i>
                                <p><?php echo get_line('cms_page'); ?></p>
                            </a>
                        </li>
                        <!--<li class="nav-item <?= get_active_class("why-choose-us")['main_class']; ?>">
                            <a href="<?= base_url('authority/why-choose-us'); ?>" class="nav-link">
                                <i class="nav-icon fa fa-tag"></i>
                                <p><?php echo get_line('why_choose_us'); ?></p>
                            </a>
                        </li>-->
                        <li class="nav-item <?= get_active_class("happy-clients")['main_class']; ?>">
                            <a href="<?= base_url('authority/happy-clients'); ?>" class="nav-link">
                                <i class="nav-icon fa fa-snowflake-o"></i>
                                <p><?php echo get_line('happy_clients'); ?></p>
                            </a>
                        </li>
                        <li class="nav-item <?= get_active_class("faq")['main_class']; ?>">
                            <a href="<?= base_url('authority/faq'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-shopping-bag"></i>
                                <p><?php echo get_line('faq'); ?></p>
                            </a>
                        </li>
                        <li class="nav-item <?= get_active_class("menus")['main_class']; ?>">
                            <a href="<?= base_url('authority/menus'); ?>" class="nav-link">
                                <i class="nav-icon fa fa-snowflake-o"></i>
                                <p><?php echo get_line('menus'); ?></p>
                            </a>
                        </li>
                        <li class="nav-item <?= get_active_class("settings")['main_class']; ?>">
                            <a href="<?= base_url('authority/settings'); ?>" class="nav-link">
                                <i class="nav-icon far fas fa-cog"></i>
                                <p><?php echo get_line('settings'); ?></p>
                            </a>
                        </li>
                    </ul>
                </li>
                          
            </ul>
        </nav>
    </div>
</aside>