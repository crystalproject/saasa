<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= SITE_TITLE; ?></title>        
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <link rel="stylesheet" href="<?= base_url() ?>assets/authority/css/all.min.css">
        <?php /*<!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/ionicons/css/ionicons.min.css"> */ ?>        
        <link rel="stylesheet" href="<?= base_url() ?>assets/authority/css/adminlte.min.css">        
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">        
        <link rel="stylesheet" href="<?= base_url() ?>assets/authority/css/toastr.min.css">        
<!--        <link rel="stylesheet" href="<?= base_url() ?>assets/authority/css/summernote-bs4.css">        -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/authority/css/select2.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/authority/css/select2-bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/jquery-confirm/jquery-confirm.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css">
        <?php
        if (isset($header_css) && $header_css != null) {
            foreach ($header_css as $css) {
                echo '<link rel="stylesheet" href="' . $css . '">';
            }
        }
        ?>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/authority/css/developer.css">        
        <script type="text/javascript">
            var BASE_URL = '<?= base_url(); ?>';
        </script>
    </head>
    <body class="hold-transition sidebar-mini">
        <div id="cover-spin"></div>
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>                    
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="<?= base_url('authority/dashboard'); ?>" class="nav-link"><?php echo get_line('dashboard'); ?></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="javascript:void(0)" class="nav-link"><?php echo get_line('welcome'); ?>: <?php echo $this->session->user_info['full_name']; ?></a>
                    </li>
                </ul>
            </nav>
            <!-- <div>
                
            </div> -->
            <div class="pull-right" style="margin: -49px 8px 3px 4px;position: relative; z-index: 1111;">
                <ul class="nav navbar-nav">
                    <?php /* <li class="dropdown user user-menu" style="margin: 5px 0 -21px -30px;position: absolute;">
                      <a href="#" style="margin: 0 10px 0 0; color: #000;">
                      <i class="fa fa-bell-o"></i>
                      <span class="label label-warning count_notification" style="font-size: 12px; padding: 0 0 0 3px;">0</span>
                      </a>
                      </li> */ ?>
                    <li class="dropdown user user-menu">
                        <?php
                        $default_language = isset($this->session->default_language) ? $this->session->default_language : DEFAULT_LANGUAGE;
                        ?>
                        <select class="form-control form-control-sm change-language">
                            <option value="1" <?php echo $default_language == '1' ? 'selected="selected"' : ''; ?>><?php echo get_line('english'); ?></option>
                            <option value="2" <?php echo $default_language == '2' ? 'selected="selected"' : ''; ?>><?php echo get_line('hindi'); ?></option>
                        </select>
                    </li>
                </ul>
            </div>
            <!-- /.navbar -->