<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $this->lang->line('change_password'); ?></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="card card-primary card-outline">                                    
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="tab-content">
                                    <?php
                                        $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                        echo form_open("", $attributes);
                                    ?>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-3 col-form-label"><?php echo $this->lang->line('current_password'); ?> <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <?php
                                                    $attributes = array(
                                                        'type' => 'password',
                                                        'class' => 'form-control',
                                                        'name' => 'current_password',
                                                        'placeholder' => $this->lang->line('enter_current_password'),
                                                        'value' => (isset($current_password) ? $current_password : ""),
                                                    );
                                                    echo form_input($attributes);
                                                    echo form_error("current_password", "<div class='error'>", "</div>");

                                                    if (isset($current_password_error) && $current_password_error != "") {
                                                        echo "<div class='error'>" . $current_password_error . "</div>";
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail" class="col-sm-3 col-form-label"><?php echo $this->lang->line('new_password'); ?> <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <?php
                                                    $attributes = array(
                                                        'type' => 'password',
                                                        'class' => 'form-control',
                                                        'name' => 'new_password',
                                                        'id' => 'new_password',
                                                        'placeholder' => $this->lang->line('enter_new_password'),
                                                        'value' => (isset($new_password) ? $new_password : ""),
                                                    );
                                                    echo form_input($attributes);
                                                    echo form_error("new_password", "<div class='error'>", "</div>");
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-3 col-form-label"><?php echo $this->lang->line('confirm_password'); ?> <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <?php
                                                    $attributes = array(
                                                        'type' => 'password',
                                                        'class' => 'form-control',
                                                        'name' => 'confirm_new_password',
                                                        'id' => 'confirm_new_password',
                                                        'placeholder' => $this->lang->line('confirm_password'),
                                                        'value' => (isset($confirm_new_password) ? $confirm_new_password : ""),
                                                    );
                                                    echo form_input($attributes);
                                                    echo form_error("confirm_new_password", "<div class='error'>", "</div>");
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="offset-sm-3 col-sm-9">
                                                <?php
                                                    $attributes = array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-success',
                                                        'value' => $this->lang->line('submit'),
                                                    );
                                                    echo form_input($attributes);
                                                ?>
                                            </div>
                                        </div>
                                    <?= form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function () {
        /*FORM VALIDATION*/
        $("#123form").validate({
            rules: {
                current_password: {required: true, minlength: 6},
                new_password: {required: true, minlength: 6},
                confirm_new_password: {required: true, minlength: 6, equalTo: "#new_password"},
            },
            messages: {
                current_password: {required: "Please enter current password", minlength: "Please enter more than character"},
                new_password: {required: "Please enter new password", minlength: "Please enter more than character"},
                confirm_new_password: {required: "Please enter confirm password", minlength: "Please enter more than character", equalTo: "Password and confirm password should be same"},
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>