<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('payment_mode'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('payment_mode'); ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0" style="overflow-x: auto;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?php echo get_line('payment_mode'); ?></th>
                                        <th><?php echo get_line('description'); ?></th>
                                        <?php /*<th style="min-width: 100px;"><?php echo get_line('action'); ?></th>*/?>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                    <?php
                                    if (isset($info) && $info != null) {
                                        foreach ($info as $key => $value) {
                                            $id = $value['id_unique'];
                                    ?>
                                            <tr>
                                                <td><?= $value['payment_mode']; ?></td>
                                                <td>
                                                    <textarea class="form-control" data-id-unique="<?php echo $value['id_unique']; ?>" name="description" rows="5"><?php echo $value['description']; ?></textarea>
                                                    <button class="btn btn-default btn-sm update-description"><?php echo get_line('update'); ?></button>
                                                </td>
                                                <?php /*<td>
                                                    <?php
                                                    if ($value['is_active'] == '1') {
                                                        echo '<span title="Status" class="btn bg-gradient-success btn-xs change-status" data-table="product" data-id="' . $value['id_unique'] . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                                                    } else {
                                                        echo '<span title="Status" class="btn bg-gradient-danger btn-xs change-status" data-table="product" data-id="' . $value['id_unique'] . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                                    }
                                                    ?>
                                                </td> */ ?>
                                            </tr>
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <tr data-expanded="true">
                                            <td colspan="7" align="center"><?php echo $this->lang->line('records_not_found'); ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$footer_js = array();
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.change-status', function() {
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/payment-mode/change_status',
                data: post_data,
                async: false,
                beforeSend: function() {
                    loading(true);
                },
                success: function(response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('bg-gradient-danger bg-gradient-success');
                        if (current_element.hasClass('bg-gradient-success')) {
                            current_element.html('<i class="fa fa-check" aria-hidden="true"></i>');
                            current_element.attr('data-current-status', '1');
                        } else {
                            current_element.html('<i class="fa fa-times" aria-hidden="true"></i>');
                            current_element.attr('data-current-status', '0');
                        }
                    } else {
                        toastr.remove();
                        toastr.error(response.message);
                    }
                },
                complete: function() {
                    setTimeout(function() {
                        loading(false);
                    }, 100);
                },
            });
        });

        $(document).on('click', '.update-description', function() {
            var current_element = jQuery(this).closest('tr');
            var id_unique = current_element.find('textarea').attr('data-id-unique');
            var description = current_element.find('textarea').val();

            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id_unique': id_unique,
                'description': description
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/payment-mode/update_description',
                data: post_data,
                async: false,
                dataType: 'json',
                beforeSend: function() {
                    loading(true);
                },
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                },
                complete: function() {
                    setTimeout(function() {
                        loading(false);
                    }, 100);
                },
            });
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>