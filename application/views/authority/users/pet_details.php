<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<table class="table table-bordered m-0">
    <thead>
        <tr>
            <th><?php echo get_line('pet_name'); ?></th>
            <th><?php echo get_line('pet_type'); ?></th>
            <th><?php echo get_line('pet_gender'); ?></th>
            <th><?php echo get_line('pet_dob'); ?></th>
            <th><?php echo get_line('pet_breed'); ?></th>
            <th><?php echo get_line('pet_weight'); ?></th>
            
            <!--<th><?php echo get_line('action'); ?></th>-->
        </tr>
    </thead>
    <tbody>
        <?php
        
        if (isset($data) && !empty($data)) {
            foreach ($data as $value) {
                ?>
                <tr>            
                    <td>
                        <p class="name m-0"><?php echo $value['pet_name']; ?></p>                        
                    </td>
                    <td>
                        <p class="m-0"><?php echo $value['pet_type']; ?></p>                        
                    </td>
                    <td>
                        <p class="m-0"><?php echo $value['pet_gender']; ?></p>                        
                    </td>
                    <td>
                        <p class="m-0"><?php echo date('d-m-Y',strtotime($value['pet_dob'])); ?></p>                        
                    </td>
                    <td>
                        <p class="m-0"><?php echo $value['pet_breed']; ?></p>                        
                    </td>
                    <td>
                        <p class="m-0"><?php echo $value['pet_weight']; ?></p>                        
                    </td>
                    
                   <!-- <td>
                    <a href="javascript:void(0)" data-table="pets" data-image-path="" class="btn bg-gradient-danger btn-xs delete_record" id="<?php echo $value['pet_id']; ?>"><i class="fa fa-trash-o"></i></a>
                        <!--<a href="javascript:void(0);" class="btn bg-gradient-danger btn-xs delete-pet-details" id="<?php echo $value['pet_id']; ?>"><i class="fa fa-trash-o"></i></a>-->
                    <!--</td>-->
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="7" class="text-center"><?php echo get_line('record_not_available'); ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>