<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo SITE_TITLE; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/authority/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/authority/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/authority/dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/authority/css/style.css">
    </head>
    <body class="hold-transition login-page">
        <div id="cover-spin"></div>
        <div class="login-box">
            <div class="login-logo">
                <?php
                    $profile_photo = $this->production_model->get_all_with_where('administrator','','',array());
                ?>   
                <img src="<?= $profile_photo !=null ? base_url(PROFILE_PICTURE).$profile_photo[0]['profile_photo'] : base_url('assets/uploads/default_img.png')?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" width="100">
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">                    
                    <p class="login-box-msg">To Reset your password, Please enter email address and hit Reset Password</p>
                    <?php
                    $attributes = array('method' => 'POST', 'id' => 'form');
                    echo form_open('', $attributes);
                    ?>
                    <div class="input-group mb-3">
                        <?php
                        $attributes = array(
                            'type' => 'email',
                            'class' => 'form-control',
                            'name' => 'email_address',
                            'placeholder' => 'Enter email address',
                            'autocomplete' => 'off',
                            'value' => (isset($email_address) ? $email_address : ""),
                        );
                        echo form_input($attributes);
                        ?>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="social-auth-links text-center mb-3">                        
                        <button type="submit" class="btn btn-block btn-primary">RESET PASSWORD</button>
                    </div>
                    <?php echo form_close(); ?>
                    <!-- /.social-auth-links -->
                    <p class="mb-1 text-right">
                        <a href="<?php echo base_url(); ?>authority/login">Sign In</a>
                    </p>      
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/authority/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<?php echo base_url(); ?>assets/authority/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/authority/dist/js/adminlte.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/authority/plugins/jquery-validate/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/authority/js/functions.js"></script>
        <script>
            $(function () {
                $("#form").validate({
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.next('div'));
                    },
                    rules: {
                        email_address: {required: true, email: true},
                    },
                    messages: {
                        email_address: {required: "Please enter email address", email: 'Please enter correct email address'},
                    },
                    errorElement: 'div',
                });
                $('#form').submit(function () {
                    cover_spin(true);
                    if (!$('#form').valid()) {
                        cover_spin(false);
                    }
                });
            });
        </script>
    </body>
</html>
