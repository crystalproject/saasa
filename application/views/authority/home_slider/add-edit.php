<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('home_slider'); ?> </li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open('', $attributes);
                                ?>
                                    <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : '';?>">
                                    
                                    <?php
                                    $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('is_active' => 1));
                                    if (isset($language) && $language != null) {
                                        foreach ($language as $key => $value) {
                                            $id = $value['id'];
                                            $newtitle = strtolower('title_' . $value['slug']);
                                            $newdescription = strtolower('description_' . $value['slug']);
                                            $img_name = strtolower('image_' . $value['slug']);
                                            $newlink = strtolower('link_' . $value['slug']);
                                            ?>
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('title'); ?> (<?= $value['name']; ?>): </label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="<?= $id == 1 ? 'title' : (isset($newtitle) && $newtitle != null ? $newtitle : ''); ?>" class="form-control" placeholder="<?php echo get_line('title'); ?>" value="<?= isset($id) && $id != null && $id == 1 ? (isset($title) && $title != null ? $title : '') : (isset($$newtitle) && $$newtitle != null ? $$newtitle : ''); ?>" maxlength="50" autocomplete="off">
                                                    <?= form_error($id == 1 ? 'title' : $newtitle, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('link'); ?>(<?= $value['name']; ?>) : </label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="<?= $id == 1 ? 'link' : (isset($newlink) && $newlink != null ? $newlink : ''); ?>" class="form-control" placeholder="<?php echo get_line('link'); ?>" value="<?= isset($id) && $id != null && $id == 1 ? (isset($link) && $link != null ? $link : '') : (isset($$newlink) && $$newlink != null ? $$newlink : ''); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label"><?php echo get_line('description'); ?> (<?= $value['name']; ?>): </label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" placeholder="<?php echo get_line('description'); ?>" name="<?= $id == 1 ? 'description' : (isset($newdescription) && $newdescription != null ? $newdescription : ''); ?>" rows="3" autocomplete="off"><?= isset($id) && $id != null && $id == 1 ? (isset($description) && $description != null ? $description : '') : (isset($$newdescription) && $$newdescription != null ? $$newdescription : ''); ?></textarea>
                                                    <?= form_error($id == 1 ? 'description' : $newdescription, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="profile_photo" class="col-sm-2 col-form-label"><?php echo get_line('image'); ?> (<?= $value['name']; ?>): <span class="required">*</span> (<?php echo get_line('upload_size'); ?> 1440&#215;699)</label>
                                                <div class="col-sm-10">
                                                    <input type="file" name="<?= $id == 1 ? 'image' : (isset($img_name) && $img_name != null ? $img_name : ''); ?>" class="form-control" accept="image/*">
                                                    <?= form_error($id == 1 ? 'image' : $img_name, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div><?= $key == 0 ? '<hr class="card card-primary card-outline">' : '';?>
                                            <?php
                                        }
                                    }
                                    ?>                                        
                                    <?php
                                        if (isset($image) && $image != null) {
                                        ?>
                                            <div class="form-group row">
                                                <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('current_image'); ?></label>
                                                <div class="col-sm-9">
                                                    <img src="<?= base_url(HOME_SLIDER.'thumbnail/').$image;?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">                
                                                </div>
                                            </div>
                                        <?php } 
                                        if (isset($image_hindi) && $image_hindi != null) {
                                        ?>
                                            <div class="form-group row">
                                                <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('current_image'); ?></label>
                                                <div class="col-sm-9">
                                                    <img src="<?= base_url(HOME_SLIDER.'thumbnail/').$image_hindi;?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">                
                                                </div>
                                            </div>
                                        <?php } 
                                    ?>
                                    
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => get_line('submit'),
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>