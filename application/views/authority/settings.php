<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Settings</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>authority/dashboard"><?php echo get_line('home'); ?></a></li>
                        <li class="breadcrumb-item active">Settings</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Settings</h3>
                        </div>
                        <?php
                        $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data");
                        echo form_open("", $attributes);
                        ?>
                        <div class="card-body">
                            <?php
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> Success</h4>
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                        if (isset($site_settings['row_count']) && $site_settings['row_count'] > 0) {
                            foreach ($site_settings['data'] as $value) {
                                ?>
                                <div class="form-group">
                                    <label for="<?php echo $value['setting_key'] ?>"><?php echo $value['setting_title'] ?>: <?php echo ($value['is_required'] == 1) ? "<span class='required'>*</span>" : ""; ?></label>
                                    <input class="form-control" name="<?php echo $value['setting_key'] ?>" id="<?php echo $value['setting_key'] ?>" type="text" value="<?php echo $value['setting_value']; ?>">
                                    <?php echo form_error($value['setting_key'], "<div class='error'>", "</div>"); ?>
                                </div>
                                <?php
                            }
                        }
                        ?>                         
                        </div>                       

                        <div class="card-footer">
                            <?php
                            $attributes = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary',
                                'value' => 'Submit',
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/authority/plugins/jquery-validate/jquery.validate.js"></script>
<script>    
//    $(document).ready(function(){
//    /*FORM VALIDATION*/
//    $("#form1").validate({
//        rules: {
//            designation: "required",
//        },
//        messages: {
//            designation: "Please enter designation",
//        }
//    });
//    });
</script>
<?php $this->view('authority/common/footer'); ?>