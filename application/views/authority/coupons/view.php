<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('coupon'); ?></li>
                    </ol>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?= base_url('authority/coupons/add-edit'); ?>" class="btn btn-sm btn-info text-white cursor-pointer"><?php echo get_line('add'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('coupon'); ?></h3>
                            <div class="card-tools">
                                <?php
                                    $name = isset($this->session->coupons_info['name']) && $this->session->coupons_info['name'] != null ? $this->session->coupons_info['name'] : '';
                                ?>
                                <div class="input-group input-group-sm" style="width: 200px;">
                                    <input type="text" name="name" value="<?= $name; ?>" class="form-control float-right" placeholder="<?php echo get_line('search'); ?>">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default btn_filter"><i class="fas fa-search"></i></button>
                                    </div>&nbsp;&nbsp;
                                    <a href="<?= base_url('authority/coupons?clear-search=1') ?>" class="btn btn-info btn-sm" type="button"><?php echo get_line('clear'); ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" id="select_all">
                                                <label for="select_all" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th><?php echo get_line('coupon_code'); ?></th>
                                        <th><?php echo get_line('discount_type'); ?></th>
                                        <th><?php echo get_line('discount'); ?></th>
                                        <th style="min-width: 100px;"><?php echo get_line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody class="attribute-info data-response">
                                    <?php
                                        if (isset($details) && $details != null) {
                                            foreach ($details as $key => $value) {
                                                $id = $value['id'];
                                                ?>
                                                <tr>
                                                    <td style="width: 10px;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id; ?>" value="<?= $id ?>">
                                                            <label for="customCheckbox<?= $id; ?>" class="custom-control-label"></label>
                                                        </div>
                                                    </td>
                                                    <td><?= $value['coupon_code']; ?></td>
                                                    <td><?= $value['discount_type']; ?></td>
                                                    <td>
                                                        <?php 
                                                            if ($value['discount_type'] == 'percentage') {
                                                                echo $value['discount'].'%'; 
                                                            }
                                                            if ($value['discount_type'] == 'amount') {
                                                                echo ''.$value['discount']; 
                                                            }
                                                        ?>   
                                                    </td>
                                                    <td>
                                                        <a href="<?= base_url('authority/coupons/add-edit/' . $id); ?>" class="btn bg-gradient-primary btn-xs"><i class="fas fa-edit"></i></a>

                                                        <a href="javascript:void(0)" data-table="coupons" data-image-path="" class="btn bg-gradient-danger btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a>

                                                        <?php
                                                        if ($value['is_active'] == '1') {
                                                            echo '<span class="btn bg-gradient-success btn-xs change-status-tmp" data-table="coupons" data-id="' . $id . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                                                        } else {
                                                            echo '<span class="btn bg-gradient-danger btn-xs change-status-tmp" data-table="coupons" data-id="' . $id . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                                <tr>
                                                    <td style="width: 10px;">
                                                        <button type="button" class="btn btn-sm btn-danger delete_all" data-table="coupons" data-image-path=""><?php echo get_line('delete'); ?></button>
                                                    </td>
                                                    <td colspan="4">
                                                        <?php
                                                            if (isset($pagination) && $pagination != null) {
                                                                echo $pagination;
                                                            }
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr data-expanded="true">
                                                <td colspan="5" align="center"><?php echo get_line('record_not_available'); ?></td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$footer_js = array(base_url('assets/authority/js/delete_record.js'), base_url('assets/authority/js/change_status.js'));
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    $(document).ready(function () {
        /*Pagination filter*/
        $(document).on('click', '.pagination_filter a', function (e) {
            var url = $(this).attr('href');
            var split_url = url.split('/');
            var page_no = split_url[7];
            e.preventDefault();

            var name = $('input[name="name"]').val();
            get_filtered_data(page_no, name);
        });

        $(document).on('click', '.btn_filter', function () {
            var name = $('input[name="name"]').val();
            get_filtered_data('', name);
        });
    });
    function get_filtered_data(page_no = '', name = '') {
        var post_data = {'page_no': page_no, 'name': name};
        $.ajax({
            url: '<?= base_url('authority/coupons/filter/') ?>' + page_no,
            type: 'POST',
            dataType: 'json',
            data: post_data,
            beforeSend: function () {
                loading(true);
            },
            success: function (response) {
                if (response.success) {
                    $('.data-response').html(response.details);
                    $('.pagination_filter').html(response.pagination);
                } else if (response.error && response.data_error) {
                    $('.data-response').html(response.data_error);
                    $('.pagination_filter').html(response.pagination);
                }
            },
            complete: function () {
                setTimeout(function () {
                    loading(false);
                }, 1000);
            },
        });
    }
</script>
<?php $this->view('authority/common/footer'); ?>									