<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$header_css = array();
$this->view('authority/common/header', array('header_css' => $header_css));
$this->view('authority/common/sidebar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('coupon'); ?></li>
                    </ol>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?= base_url('authority/coupons'); ?>" class="btn btn-sm btn-info text-white cursor-pointer"><?php echo get_line('view'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-12">                    
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('coupon'); ?></h3>
                        </div>
                        <?php
                        $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                        echo form_open('', $attributes);
                        ?>
                        <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : ''; ?>">
                        <div class="card-body">                                
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label"><?php echo get_line('coupon_code'); ?> <span class="required">*</span> </label>
                                <div class="col-sm-9">
                                    <input type="text" name="coupon_code" class="form-control" placeholder='<?php echo get_line('coupon_code'); ?>' value="<?= isset($coupon_code) && $coupon_code != null ? $coupon_code : ''; ?>">
                                    <?= form_error('coupon_code', "<label class='error'>", "</label>"); ?>
                                </div>
                            </div>

                            <!-- <div class="form-group row">
                                <label class="col-sm-3 col-form-label"><?php echo get_line('description'); ?></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="description" placeholder="<?php echo get_line('description'); ?>"><?= isset($description) && $description != null ? $description : ''; ?></textarea>
                                </div>
                            </div> -->

                            <div class="form-group row">
                                <label for="exampleInputPassword1" class="col-sm-3 col-form-label"><?php echo get_line('discount_type'); ?> :<span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <?php
                                    $options = array('' => get_line('select'));
                                    $details = array(
                                        'percentage' => get_line('percentage'),
                                        'amount' => get_line('amount'),
                                    );
                                    foreach ($details as $key => $value) {
                                        $options[$key] = $value;
                                    }
                                    $selected = (isset($discount_type) && $discount_type != null ? $discount_type : "");
                                    echo form_dropdown('discount_type', $options, $selected, 'class="form-control "');
                                    echo form_error('discount_type', "<label class='error'>", "</label>");
                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label"><?php echo get_line('discount');?> (% <?php echo get_line('or');?> <?php echo get_line('amount');?>): <span class="required">*</span> </label>
                                <div class="col-sm-9">
                                    <input type="text" name="discount" class="form-control" placeholder='<?php echo get_line('discount');?>' value="<?= isset($discount) && $discount != null ? $discount : ''; ?>">
                                    <?= form_error('discount', "<label class='error'>", "</label>"); ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleInputPassword1" class="col-sm-3 col-form-label"><?php echo get_line('minimum_cart_status');?><span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <?php
                                    $details = array(
                                        'false' => 'False',
                                        'true' => 'True',
                                    );
                                    foreach ($details as $key => $value) {
                                        $options_type[$key] = $value;
                                    }
                                    $selected = (isset($minimum_amount_cart_status) && $minimum_amount_cart_status != null ? $minimum_amount_cart_status : "");
                                    echo form_dropdown('minimum_amount_cart_status', $options_type, $selected, 'class="form-control"');
                                    echo form_error('minimum_amount_cart_status', "<label class='error'>", "</label>");
                                    ?>
                                </div>
                            </div>

                            <div class="minimum-amount-cart" style="display: none;">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 col-form-label"><?php echo get_line('minimum_cart_amount');?> <span class="required">*</span> </label>
                                    <div class="col-sm-9">
                                        <input type="text" name="minimum_amount_cart" class="form-control" placeholder='<?php echo get_line('amount');?>' value="<?= isset($minimum_amount_cart) && $minimum_amount_cart != null ? $minimum_amount_cart : ''; ?>">
                                        <?= form_error('minimum_amount_cart', "<label class='error'>", "</label>"); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label"><?php echo get_line('maximum_coupon_user');?> <span class="required">*</span> </label>
                                <div class="col-sm-9">
                                    <input type="text" name="maximum_coupon_used" class="form-control" placeholder='<?php echo get_line('maximum_coupon_user');?>' value="<?= isset($maximum_coupon_used) && $maximum_coupon_used != null ? $maximum_coupon_used : ''; ?>">
                                    <?= form_error('maximum_coupon_used', "<label class='error'>", "</label>"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <?php
                            $attributes = array(
                                'type' => 'submit',
                                'class' => 'btn btn-info',
                                'value' => get_line('submit'),
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>                
            </div>
        </div>
    </section>
</div>
<?php
$this->load->view('authority/common/copyright', array('footer_js' => array()));
?>
<script>
    $(document).ready(function () {
        $(document).on('change', '[name=minimum_amount_cart_status]', function () {
            var cart_status = $(this).val();
            if (cart_status == 'true') {
                $('.minimum-amount-cart').css({'display': 'block'});
            } else if (cart_status == 'false') {
                $('.minimum-amount-cart').css({'display': 'none'});
            }
        });
        
        $(document).find('[name=minimum_amount_cart_status]').trigger('change');

        $('[name="coupon_code"]').keypress(function (e) {
            if (e.which === 32 || e.which === 45) {
                return false;
            }
            if (!/[0-9a-zA-Z-]/.test(String.fromCharCode(e.which))) {
                return false;
            }
            if ($(this).val().length >= 15) {
                return false;
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>                                    