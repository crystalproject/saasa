<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$header_css = array(base_url('assets/authority/summernote/summernote.css'), base_url('assets/authority/bootstrap-select/css/bootstrap-select.min.css'));
$this->view('authority/common/header', array('header_css' => $header_css));
?>

<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('category'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php


                                $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                echo form_open('', $attributes);
                                ?>
                                <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : ''; ?>">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('menu'); ?>:<span class="required">*</span> </label>
                                    <div class="col-sm-10">

                                        <?php
                                        $options = array();
                                        foreach ($menu_arr as $menu) {
                                            $options[$menu['id']] = $menu['name'];
                                        }

                                        $selected = isset($menu_id) && !empty($menu_id) ? explode(',', $menu_id) : array();
                                        echo form_multiselect('menu_id[]', $options, $selected, 'class="form-control"');

                                        ?>
                                        <!--<select class="form-control select2" name="menu_id" multiple="">
                                            
                                            <?php
                                                foreach ($menu_arr as $menu) {
                                            ?>
                                                <option value="<?php echo $menu['id'];?>" ><?php echo $menu['name'];?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>-->
                                        <?= form_error("menu_id[]", "<label class='error'>", "</label>"); ?>
                                    </div>
                                </div>

                                <?php
                                    $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('is_active' => 1));
                                    if (isset($language) && $language != null) {
                                        foreach ($language as $key => $value) {
                                            $id = $value['id'];
                                            $newname = strtolower('name_' . $value['slug']);
                                            $img_name = strtolower('image_' . $value['slug']);
                                            $banner_name = strtolower('banner_' . $value['slug']);
                                            ?>                                        


                                            <div class="form-group row">
                                                <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('name'); ?> (<?= $value['name']; ?>):<span class="required">*</span> </label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="<?= $id == 1 ? 'name' : (isset($newname) && $newname != null ? $newname : ''); ?>" class="form-control" placeholder="<?php echo get_line('name'); ?>" value="<?= isset($id) && $id != null && $id == 1 ? (isset($name) && $name != null ? $name : '') : (isset($$newname) && $$newname != null ? $$newname : ''); ?>" maxlength="50" autocomplete="off">
                                                    <?= form_error($id == 1 ? 'name' : $newname, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row">
                                                <label class="col-sm-2 col-form-label"><?php echo get_line('description'); ?> (<?= $value['name']; ?>):<span class="required">*</span> </label>
                                                <?php
                                                $description = '';
                                                if (isset($details_description) && !empty($details_description)) {
                                                    foreach($details_description as $d_value){
                                                        if($d_value['id_language'] == $id){
                                                            $description = $d_value['description'];
                                                            break;
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" placeholder="<?php echo get_line('description'); ?>" name="description_<?php echo $id; ?>" rows="3" autocomplete="off"><?php echo $description; ?></textarea>
                                                </div>
                                            </div> -->
                                            <div class="form-group row">
                                                <label for="profile_photo" class="col-sm-2 col-form-label"><?php echo get_line('image'); ?> (<?= $value['name']; ?>): <?= ($id == 1) ? '<span class="required">*</span>' : '';?> (<?php echo get_line('upload_size'); ?> 370&#215;442)</label>
                                                <div class="col-sm-10">
                                                    <input type="file" name="<?= $id == 1 ? 'image' : (isset($img_name) && $img_name != null ? $img_name : ''); ?>" class="form-control" accept="image/*">
                                                    <?= form_error($id == 1 ? 'image' : $img_name, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div>                                            
                                            <div class="form-group row">
                                                <label for="profile_photo" class="col-sm-2 col-form-label"><?php echo get_line('banner'); ?> (<?= $value['name']; ?>): <?= ($id == 1) ? '<span class="required">*</span>' : '';?>  (<?php echo get_line('upload_size'); ?> 1171&#215;242)</label>
                                                <div class="col-sm-10">
                                                    <input type="file" name="<?= $id == 1 ? 'banner' : (isset($banner_name) && $banner_name != null ? $banner_name : ''); ?>" class="form-control" accept="image/*">
                                                    <?= form_error($id == 1 ? 'banner' : $banner_name, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div><?= $key == 0 ? '<hr class="card card-primary card-outline">' : '';?>
                                            <?php
                                        }
                                    }
                                ?>                                
                                <?php
                                    if (isset($image) && $image != null) {
                                        ?>
                                        <div class="form-group row">
                                            <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('current_image').' '.get_line('english'); ?></label>
                                            <div class="col-sm-4">
                                                <img src="<?= base_url(CATEGORY_IMAGE . 'thumbnail/') . $image; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">                
                                            </div>
                                        </div>
                                    <?php }
                                    if (isset($image_hindi) && $image_hindi != null) {
                                    ?>
                                        <div class="form-group row">
                                            <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('current_image').' '.get_line('hindi'); ?></label>
                                            <div class="col-sm-9">
                                                <img src="<?= base_url(CATEGORY_IMAGE.'thumbnail/').$image_hindi;?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">                
                                            </div>
                                        </div>
                                    <?php } 
                                    if (isset($banner) && $banner != null) {
                                        ?>
                                        <div class="form-group row">
                                            <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('banner').' '.get_line('english'); ?></label>
                                            <div class="col-sm-4">
                                                <img src="<?= base_url(CAT_SUB_CAT_BANNER . 'thumbnail/') . $banner; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">                
                                            </div>
                                        </div>
                                    <?php }
                                    if (isset($banner_hindi) && $banner_hindi != null) {
                                        ?>
                                        <div class="form-group row">
                                            <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('banner').' '.get_line('hindi'); ?></label>
                                            <div class="col-sm-4">
                                                <img src="<?= base_url(CAT_SUB_CAT_BANNER . 'thumbnail/') . $banner_hindi; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">                
                                            </div>
                                        </div>
                                    <?php }
                                ?>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <?php
                                        $attributes = array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-success',
                                            'value' => get_line('submit'),
                                        );
                                        echo form_input($attributes);
                                        ?>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<?php
$this->view('authority/common/copyright', array('footer_js' => array(base_url('assets/authority/summernote/summernote.js'), base_url('assets/authority/bootstrap-select/js/bootstrap-select.min.js'))));
?>

<script type="text/javascript">
    var menu_elements = $('[name="menu_id[]"]');
        menu_elements.selectpicker();
</script>

<?php
$this->view('authority/common/footer');
?>
