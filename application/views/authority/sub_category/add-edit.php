<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('sub_category'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                echo form_open('', $attributes);
                                ?>
                                <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : ''; ?>">
                                <input type="hidden" name="data_level" value="0">

                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label"><?php echo get_line('category'); ?> <span class="required">*</span></label>
                                    <div class="col-sm-10">
                                        <?php
                                        $temp = isset($id) ? $id  : '';
                                        $GLOBALS['selected_id'] = isset($parent_id) && $parent_id > 0 ? $parent_id : $temp;
                                        function cateSubcatTree($parentid = 0, $sub_mark = '', $ids = '')
                                        {
                                            $category_details = get_details('category', array('parent_id' => $parentid, 'is_active' => 1));
                                            if (isset($category_details) && $category_details != null) {
                                                foreach ($category_details as $key => $value) {
                                                    $selected = $GLOBALS['selected_id'] == $value['id'] ? 'selected="selected"' : '';
                                                    echo '<option data-level="' . $sub_mark . '" value="' . $value['id'] . '" ' . $selected . '>' . $sub_mark . $value['name' . get_language()] . '</option>';
                                                    cateSubcatTree($value['id'], $sub_mark . '--&nbsp;', $parentid);
                                                }
                                            }
                                        }
                                        ?>
                                        <select name="parent_id" class="form-control select2">
                                            <option value="">Select</option>
                                            <?php cateSubcatTree($parentid = 0, $sub_mark = '', (isset($id) && $id != null ? $id : 0)); ?>
                                        </select>
                                        <span id="parent_id_error"></span>
                                        <?= form_error("parent_id", "<label class='error'>", "</label>"); ?>
                                    </div>
                                </div>
                                <?php
                                $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('is_active' => 1));
                                if (isset($language) && $language != null) {
                                    foreach ($language as $key => $value) {
                                        $id = $value['id'];
                                        $newname = strtolower('name_' . $value['name']);
                                        $img_name = strtolower('image_' . $value['slug']);
                                        $banner_name = strtolower('banner_' . $value['slug']);
                                ?>
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('name'); ?> (<?= $value['name']; ?>):<span class="required">*</span> </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="<?= $id == 1 ? 'name' : (isset($newname) && $newname != null ? $newname : ''); ?>" class="form-control" placeholder="<?php echo get_line('name'); ?>" value="<?= isset($id) && $id != null && $id == 1 ? (isset($name) && $name != null ? $name : '') : (isset($$newname) && $$newname != null ? $$newname : ''); ?>" maxlength="50">
                                                <?= form_error($id == 1 ? 'name' : $newname, "<label class='error'>", "</label>"); ?>
                                            </div>
                                        </div>
                                        
                                        <!--<div class="form-group row cat-img-sec">
                                            <label for="profile_photo" class="col-sm-2 col-form-label"><?php echo get_line('image'); ?> (<?= $value['name']; ?>): <span class="required">*</span> (<?php echo get_line('upload_size'); ?> 270&#215;274)</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="<?= $id == 1 ? 'image' : (isset($img_name) && $img_name != null ? $img_name : ''); ?>" class="form-control" accept="image/*">
                                                <?= form_error($id == 1 ? 'image' : $img_name, "<label class='error'>", "</label>"); ?>
                                            </div>
                                        </div>-->
                                        <?php /*
                                        <div class="form-group row cat-img-sec">
                                            <label for="profile_photo" class="col-sm-2 col-form-label"><?php echo get_line('banner'); ?> (<?= $value['name']; ?>): <span class="required">*</span> (<?php echo get_line('upload_size'); ?> 1171&#215;242)</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="<?= $id == 1 ? 'banner' : (isset($banner_name) && $banner_name != null ? $banner_name : ''); ?>" class="form-control" accept="image/*">
                                                <?= form_error($id == 1 ? 'banner' : $banner_name, "<label class='error'>", "</label>"); ?>
                                            </div>
                                        </div>
                                        <?= $key == 0 ? '<hr class="card card-primary card-outline">' : ''; */?>
                                <?php
                                    }
                                }
                                ?>
                                <?php
                               /* if (isset($image) && $image != null) {
                                ?>
                                    <div class="form-group row cat-img-sec">
                                        <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('current_image') . ' ' . get_line('english'); ?></label>
                                        <div class="col-sm-10">
                                            <img src="<?= base_url(CATEGORY_IMAGE . 'thumbnail/') . $image; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                                        </div>
                                    </div>
                                <?php }
                                if (isset($image_arabic) && $image_arabic != null) {
                                ?>
                                    <div class="form-group row cat-img-sec">
                                        <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('current_image') . ' ' . get_line('hindi); ?></label>
                                        <div class="col-sm-9">
                                            <img src="<?= base_url(CATEGORY_IMAGE . 'thumbnail/') . $image_arabic; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                                        </div>
                                    </div>
                                <?php }
                                if (isset($banner) && $banner != null) {
                                ?>
                                    <div class="form-group row cat-img-sec">
                                        <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('banner') . ' ' . get_line('english'); ?></label>
                                        <div class="col-sm-4">
                                            <img src="<?= base_url(CAT_SUB_CAT_BANNER . 'thumbnail/') . $banner; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                                        </div>
                                    </div>
                                <?php }
                                if (isset($banner_arabic) && $banner_arabic != null) {
                                ?>
                                    <div class="form-group row cat-img-sec">
                                        <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('banner') . ' ' . get_line('hindi); ?></label>
                                        <div class="col-sm-4">
                                            <img src="<?= base_url(CAT_SUB_CAT_BANNER . 'thumbnail/') . $banner_arabic; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                                        </div>
                                    </div>
                                <?php }*/
                                ?>
                                <div class="form-group row ">
                                    <div class="offset-sm-2 col-sm-10">
                                        <?php
                                        $attributes = array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-success',
                                            'value' => get_line('submit'),
                                        );
                                        echo form_input($attributes);
                                        ?>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function() {
        $('[name="parent_id"]').change(function() {
            var data_level = $(this).find('option:selected').attr('data-level');
            data_level = data_level.trim();
            $('[name="data_level"]').val(data_level.split('--').length);
            if (data_level.split('--').length >= 2) {
                $('.cat-img-sec').hide();
            } else {
                $('.cat-img-sec').show();
            }
        }).trigger('change');
    });
</script>
<?php $this->view('authority/common/footer'); ?>