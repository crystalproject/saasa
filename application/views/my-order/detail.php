<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
?>
<section class="our-story-section">
    <div class="container">
        <form method="POST" name="add-order" action="POST" onsubmit="return false;" />
        <div class="row">
            <div class="col-lg-12">
                <div class="payment-information">
                    <div class="title">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <h3><b><?php echo get_line('order_detail'); ?> (<?php echo $data['order_info']['order_id']; ?>)</b></h3>
                            </div>
                            <div class="col-md-4 col-sm-12 text-right">
                                <a href="<?php echo base_url('my-order'); ?>" class=""><?php echo get_line('back'); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th><?php echo get_line('order_date_time'); ?></th>
                                            <th><?php echo get_line('payment_mode'); ?></th>
                                            <th><?php echo get_line('order_status'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo date('d-m-y h:i A', strtotime($data['order_info']['order_date_time'])); ?></td>
                                            <td><?php
                                                $payment_mode = get_payment_mode_static($data['order_info']['payment_mode']);
                                                echo isset($payment_mode['name']) ? get_line($payment_mode['name']) : '';
                                                ?></td>
                                            <td><?php echo get_order_status($data['order_info']['order_status'])['name']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <tr>
                        <th colspan="2"><?php echo get_line('billing_info'); ?></th>
                    </tr>
                    <?php
                    $billing_info = json_decode($data['order_info']['billing_info'], true);
                    foreach ($billing_info as $key => $value) {
                    ?>
                        <tr>
                            <th><?php echo get_line(str_replace('billing_', '', $key)); ?></th>
                            <td><?php echo $value; ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
            <?php /*<div class="col-lg-6 col-md-6 col-sm-12">
                <table class="table table-bordered">
                    <tr>
                        <th colspan="2"><?php echo get_line('shipping_info'); ?></th>
                    </tr>
                    <?php
                    $shipping_info = json_decode($data['order_info']['shipping_info'], true);
                    foreach ($shipping_info as $key => $value) {
                    ?>
                        <tr>
                            <th><?php echo get_line(str_replace('shipping_', '', $key)); ?></th>
                            <td><?php echo $value; ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div> */ ?>
            <?php
            if (isset($data['product_info']) && !empty($data['product_info'])) {
            ?>
                <div class="col-lg-12">
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="6"><?php echo get_line('product'); ?></th>
                        </tr>
                        <tr>
                            <th><?php echo get_line('title'); ?></th>
                            <th><?php echo get_line('image'); ?></th>
                            <th><?php echo get_line('attributes'); ?></th>
                            <th class="text-right"><?php echo get_line('price'); ?></th>
                            <th class="text-right"><?php echo get_line('quantity'); ?></th>
                            <th class="text-right"><?php echo get_line('total'); ?></th>
                        </tr>
                        <?php
                        foreach ($data['product_info'] as $key => $value) {
                        ?>
                            <tr>
                                <td><?php echo $value['title']; ?></td>
                                <td>
                                    <?php
                                    if (isset($value['customized_image']) && !empty($value['customized_image'])) {
                                    ?>
                                        <img src="<?php echo base_url('uploads/product/' . $value['customized_image']); ?>" class="order-detail-image" />
                                    <?php
                                    } else {
                                    ?>
                                        <img src="<?php echo $value['image_name']; ?>" class="order-detail-image" />
                                    <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (isset($value['attributes']) && !empty($value['attributes'])) {
                                        echo '<table class="table table-bordered">';
                                        foreach ($value['attributes'] as $a_value) {
                                            echo '<tr>';
                                            echo '<td>' . $a_value['attribute_name'] . '</td>';
                                            if (isset($a_value['image']) && !empty($a_value['image'])) {
                                                echo '<td><img src="' . $a_value['image'] . '" class="order-detail-attr-img"/></td>';
                                            } else {
                                                echo '<td>' . $a_value['attribute_value'] . '</td>';
                                            }
                                            echo '</tr>';
                                        }
                                        echo '</table>';
                                    }
                                    ?>
                                </td>
                                <td class="text-right"><?php echo $value['discounted_price']; ?></td>
                                <td class="text-right"><?php echo $value['quantity']; ?></td>
                                <td class="text-right"><?php echo $value['total_product_price']; ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr>
                            <td colspan="5" class="text-right"><?php echo get_line('total'); ?></td>
                            <td class="text-right"><?php echo $data['order_price']['final_total']; ?></td>
                        </tr>
                    </table>
                </div>
            <?php } ?>
        </div>
        </form>
    </div>
</section>
<?php
$footer_js = array(base_url('assets/js/cart.js'));
$this->load->view('include/copyright', array('footer_js' => $footer_js));
$this->load->view('include/footer'); ?>