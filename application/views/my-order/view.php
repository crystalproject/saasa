<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
?>
<section class="our-story-section">
    <div class="container">
        <form method="POST" name="add-order" action="POST" onsubmit="return false;" />
        <div class="row">
            <?php
            if (isset($success_message) && !empty($success_message)) {
            ?>
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <?php echo $success_message; ?>
                    </div>
                </div>
            <?php
            }
            ?>
            <div class="col-lg-12">
                <div class="payment-information">
                    <div class="title">
                        <h3><b><?php echo get_line('my_orders'); ?> (<?php echo $total_order; ?>)</b></h3>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th><?php echo get_line('order_id'); ?></th>
                                            <th><?php echo get_line('order_date_time'); ?></th>
                                            <th><?php echo get_line('payment_mode'); ?></th>
                                            <th><?php echo get_line('order_status'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($data['info']) && !empty($data['info'])) {
                                            foreach ($data['info'] as $key => $value) {
                                                $payment_mode = get_payment_mode_static($value['order_info']['payment_mode']);
                                        ?>
                                                <tr>
                                                    <td><a href="<?php echo base_url('my-order/detail/' . $value['order_info']['order_id']); ?>"><?php echo $value['order_info']['order_id']; ?></a></td>
                                                    <td><?php echo date('d-m-y h:i A', strtotime($value['order_info']['order_date_time'])); ?></td>
                                                    <td><?php echo isset($payment_mode['name']) ? get_line($payment_mode['name']) : ''; ?></td>
                                                    <td><?php echo get_order_status($value['order_info']['order_status'])['name']; ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </form>
    </div>
</section>
<?php
$footer_js = array(base_url('assets/js/cart.js'));
$this->load->view('include/copyright', array('footer_js' => $footer_js));
$this->load->view('include/footer'); ?>