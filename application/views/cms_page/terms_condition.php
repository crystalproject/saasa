<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>
<div class="section banner-page" data-background="<?= base_url() ?>assets/images/statistic_bg.jpg">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-2">
			<div class="title-page">Terms & Conditions</div>
		</div>
		<!-- <p class="text-center text-white">Your wishlisht products.</p> -->
	</div>
</div>
<section class="about-us">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about-des">
					<h2>
						Terms & Conditions
					</h2>
					<?= isset($details) && $details !=null ? $details[0]['description'.get_language_front()] : '';?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>