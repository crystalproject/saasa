<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
?>
<!-- Banner Start -->
<div class="section banner-page" data-background="<?= base_url() ?>assets/images/statistic_bg.jpg">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-2">
			<div class="title-page"><?php echo get_line('wishlist'); ?></div>
		</div>
		<p class="text-center text-white"><?php echo get_line('your_wishlist_products'); ?></p>
	</div>
</div>
<!-- Banner End -->
<div id="class" class="product-list">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<div class="col-md-12">
					
					<div class="row">
						<?php
							$available = true;
							if (isset($details) && !empty($details)) {
								foreach ($details as $key => $value) {
									$combinations = isset($value['other_info']['attribute_combinations'][0]) && !empty($value['other_info']['attribute_combinations']) ? implode(',', $value['other_info']['attribute_combinations']) : '';
									?>
									<div class="col-sm-4 col-md-4 single-wish-item">
										<div class="rs-shop-box mb-5">
											<a href="javascript:void(0);" class="link-wish" id="<?php echo $value['id_unique']; ?>" value="<?php echo $value['id_unique']; ?>">
												<i class="fa fa-heart wishlist from-wishlist" id="<?php echo $value['id_unique']; ?>"></i>
											</a>
											<!-- <button type="button" name="tinvwl-remove" id="<?php echo $value['id_unique']; ?>" value="<?php echo $value['id_unique']; ?>" title="" style="border-radius: 50%;">
												<i class="fa fa-times wishlist from-wishlist" id="<?php echo $value['id_unique']; ?>" style="color: #000;"></i>
											</button> -->
											<div class="media">
												<a href="#"><img src="<?php echo $value['main_image']; ?>" alt="" class="img-fluid"></a>
											</div>
											<div class="body-text">
												<h4 class="title"><a href="#">Grass fed lamb 6Kg</a></h4>
												<div class="meta">
													<div class="price">$29</div>
													<div class="rating">
														<a href="#" class="btn btn-cart">Add to cart</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
							} else {
								$available = false;
								echo '<div class="col-md-12"' . get_line_front('wishlist_is_empty') . '</div>';
							}
						?>
						<!-- <div class="col-sm-4 col-md-4">
							<div class="rs-shop-box mb-5">
								<a href="javascript:void(0);" class="link-wish">
									<i class="fa fa-heart"></i>
								</a>
								<div class="media">
									<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#">Original 6kg</a></h4>
									<div class="meta">
										<div class="price">$29</div>
										<div class="rating">
											<a href="#" class="btn btn-cart">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="rs-shop-box mb-5">
								<a href="javascript:void(0);" class="link-wish">
									<i class="fa fa-heart"></i>
								</a>
								<div class="media">
									<a href="#"><img src="images/pro-2.jpg" alt="" class="img-fluid"></a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#">Grass fed lamb 6kg</a></h4>
									<div class="meta">
										<div class="price">$29</div>
										<div class="rating">
											<a href="#" class="btn btn-cart">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="rs-shop-box mb-5">
								<a href="javascript:void(0);" class="link-wish">
									<i class="fa fa-heart"></i>
								</a>
								<div class="media">
									<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#">Original 6kg</a></h4>
									<div class="meta">
										<div class="price">$29</div>
										<div class="rating">
											<a href="#" class="btn btn-cart">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="rs-shop-box mb-5">
								<a href="javascript:void(0);" class="link-wish">
									<i class="fa fa-heart"></i>
								</a>
								<div class="media">
									<a href="#"><img src="images/pro-2.jpg" alt="" class="img-fluid"></a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#">Grass fed lamb 6Kg</a></h4>
									<div class="meta">
										<div class="price">$29</div>
										<div class="rating">
											<a href="#" class="btn btn-cart">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="rs-shop-box mb-5">
								<a href="javascript:void(0);" class="link-wish">
									<i class="fa fa-heart"></i>
								</a>
								<div class="media">
									<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#">Original 6kg</a></h4>
									<div class="meta">
										<div class="price">$29</div>
										<div class="rating">
											<a href="#" class="btn btn-cart">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="rs-shop-box mb-5">
								<a href="javascript:void(0);" class="link-wish">
									<i class="fa fa-heart"></i>
								</a>
								<div class="media">
									<a href="#"><img src="images/pro-2.jpg" alt="" class="img-fluid"></a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#">Grass fed lamb 6kg</a></h4>
									<div class="meta">
										<div class="price">$29</div>
										<div class="rating">
											<a href="#" class="btn btn-cart">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="rs-shop-box mb-5">
								<a href="javascript:void(0);" class="link-wish">
									<i class="fa fa-heart"></i>
								</a>
								<div class="media">
									<a href="#"><img src="images/pro-1.jpg" alt="" class="img-fluid"></a>
								</div>
								<div class="body-text">
									<h4 class="title"><a href="#">Original 6kg</a></h4>
									<div class="meta">
										<div class="price">$29</div>
										<div class="rating">
											<a href="#" class="btn btn-cart">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div> -->
					</div>
					<!-- end shop -->

					<div class="row">
						<div class="col-sm-12 col-md-12">
							<nav aria-label="Page navigation">
							  <ul class="pagination">
							    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
							    <li class="page-item active"><a class="page-link" href="#">1</a></li>
							    <li class="page-item"><a class="page-link" href="#">2</a></li>
							    <li class="page-item"><a class="page-link" href="#">3</a></li>
							    <li class="page-item"><a class="page-link" href="#">Next</a></li>
							  </ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <section class="mens-clothes-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				<div class="porto-u-main-heading">
					<h2 style="text-transform: capitalize;font-size: 35px;"><?php echo get_line('your_wishlist'); ?></h2>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<form action="#" method="post" autocomplete="off">
			<table class="tinvwl-table-manage-list">
				<thead>
					<tr>
						<th class="product-cb">
							<input type="checkbox" class="all-checkbox" title="">
						</th>
						<th class="product-thumbnail">&nbsp;</th>
						<th class="product-name">
							<span class="tinvwl-full"><?php echo get_line('product'); ?></span>
						</th>
						<th class="product-price"><?php echo get_line('price'); ?></th>
						<th class="product-date"><?php echo get_line('date'); ?></th>
						<th class="product-stock"><?php echo get_line('stock_status'); ?></th>
						<th class="product-action">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$available = true;
					if (isset($details) && !empty($details)) {
						foreach ($details as $key => $value) {
							$combinations = isset($value['other_info']['attribute_combinations'][0]) && !empty($value['other_info']['attribute_combinations']) ? implode(',', $value['other_info']['attribute_combinations']) : '';
					?>
							<tr class="wishlist_item">
								<td class="product-cb">
									<input type="checkbox" class="single-checkbox" value="<?php echo $value['id_unique']; ?>" data-attr-values="<?php echo $combinations; ?>" title="">
								</td>
								<td class="product-remove">
									<button type="button" name="tinvwl-remove" id="<?php echo $value['id_unique']; ?>" value="<?php echo $value['id_unique']; ?>" title="" style="border-radius: 50%;">
										<i class="fa fa-times wishlist from-wishlist" id="<?php echo $value['id_unique']; ?>" style="color: #000;"></i>
									</button>
								</td>
								<td class="product-thumbnail">
									<a href="<?php echo base_url('product/details/'.$value['slug'].'/'.$value['id_unique']); ?>">
										<img width="100" height="100" src="<?php echo $value['main_image']; ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" />
									</a>
									<a href="<?php echo base_url('product/details/'.$value['slug'].'/'.$value['id_unique']); ?>" style="color: #000;text-decoration: none;"><?php echo $value['title']; ?></a>
								</td>
								<?php
								//print_r($value);exit;
								?>
								<td class="product-price">
									<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">SAR</span><?php echo $value['other_info']['final_price'][0]; ?> <strike>SAR<?php echo $value['other_info']['prices'][0]; ?></strike></span>
								</td>
								<td class="product-date">
									<time class="entry-date" datetime="2020-04-07 12:08:52"><?php echo date('F-d, Y', strtotime($value['created_at'])); ?></time>
								</td>
								<td class="product-stock">
									<p class="stock in-stock"><span><i class="ftinvwl ftinvwl-check"></i></span><span class="tinvwl-txt"><?php echo $value['stock']; ?></span></p>
								</td>
								<td class="product-action">
									<button type="button" class="button alt add-to-cart-from-wishlist" name="tinvwl-add-to-cart" value="" title="" style="margin-top: -64%">
										<i class="ftinvwl ftinvwl-shopping-cart"></i><span class="tinvwl-txt"><?php echo get_line('add'); ?></span>
									</button>
								</td>
							</tr>
					<?php
						}
					} else {
						$available = false;
						echo '<tr><td colspan="7" class="text-center">' . get_line_front('wishlist_is_empty') . '</td></tr>';
					}
					?>
				</tbody>
				<?php
				if ($available == true) {
				?>
					<tfoot>
						<tr>
							<td colspan="100">
								<div class="tinvwl-to-left look_in">
									<input type="hidden" name="lists_per_page" value="10" id="tinvwl_lists_per_page" />
									<select name="product_actions" id="tinvwl_product_actions" class="tinvwl-break-input-filed" style="width: 44%;margin-right: 3%;">
										<option value="" selected="selected"><?php echo get_line('action'); ?></option>
										<option value="add_selected"><?php echo get_line('add_to_cart'); ?></option>
										<option value="remove"><?php echo get_line('remove'); ?></option>
									</select>
									<button type="button" style="margin-top: -6%;" type="submit" class="enable-disable apply-button button tinvwl-break-input tinvwl-break-checkbox" name="tinvwl-action" value="product_apply" title=""><?php echo get_line('apply'); ?></button>
								</div>
								<div class="tinvwl-to-right look_in" style="width: 100%;">
									<button type="button" class="button enable-disable add-to-cart-selected-from-wishlist" name="tinvwl-action" value="product_selected" title=""><?php echo get_line('add_selected_to_cart'); ?></button>
									<button type="button" class="button add-to-cart-all-from-wishlist" name="tinvwl-action" value="product_all" title=""><?php echo get_line('add_all_to_cart'); ?></button>
								</div>
								<input type="hidden" id="wishlist_nonce" name="wishlist_nonce" value="e5cfff201d" />
								<input type="hidden" name="_wp_http_referer" value="/" />
							</td>
						</tr>
					</tfoot>
				<?php
				}
				?>
			</table>
		</form>
	</div>
</section> -->
<?php $this->load->view('include/copyright'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		var $tblChkBox = $(".single-checkbox");
		$(".all-checkbox").on("click", function() {
			$($tblChkBox).prop('checked', $(this).prop('checked'));
			check_enable_disable();
		});
		$(".single-checkbox").on("change", function() {
			if (!$(this).prop("checked")) {
				$(".all-checkbox").prop("checked", false);				
			} else if ($(".single-checkbox:checked").length == $(".single-checkbox").length) {
				$(".all-checkbox").prop("checked", true);				
			}
			check_enable_disable();
		});

		function check_enable_disable() {			
			if ($(".single-checkbox:checked").length > 0) {
				$('.enable-disable').removeAttr('disabled');
				$('.enable-disable').removeClass('disabled');
			} else {
				$('.enable-disable').attr('disabled', 'disabled');
				$('.enable-disable').addClass('disabled');
			}
		}
		check_enable_disable();
	});
</script>
<?php $this->load->view('include/footer'); ?>