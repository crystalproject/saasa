<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<section class="login">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="login-form">
					<h3>
						<?= get_line_front('sign_up'); ?>
					</h3>
					<form id="form" method="post">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label><?php echo get_line('full_name'); ?><span>*</span></label>
									<input type="text" name="full_name" value="<?=isset($full_name)?$full_name:''?>" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><?= get_line_front('email_address'); ?><span>*</span></label>
									<input type="text" name="email" value="<?=isset($email)?$email:''?>" class="form-control">
									<?= form_error('email', "<label class='error'>", "</label>"); ?>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><?= get_line_front('password'); ?><span>*</span></label>
									<input type="password" id="password" name="password"  class="form-control">
									<?= form_error('password', "<label class='error'>", "</label>"); ?>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><?= get_line_front('confirm'); ?> <?= get_line_front('password'); ?><span>*</span></label>
									<input type="password" name="confirm_password" class="form-control">
									<?= form_error('confirm_password', "<label class='error'>", "</label>"); ?>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><?= get_line_front('mobile_number'); ?><span>*</span></label>
									<input type="number" name="mobile_number" value="<?=isset($mobile_number)?$mobile_number:''?>" class="form-control">
									<?= form_error('mobile_number', "<label class='error'>", "</label>"); ?>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><?= get_line_front('city'); ?><span>*</span></label>
									<input type="text" name="city" value="<?=isset($city)?$city:''?>" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 clone-section-main">
								<div class="row clone_section clone-section-sub">
									<div class="col-lg-12">
										<h3 class="pet-detail-head">
											<?= get_line_front('pet_details'); ?>
										</h3>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><?= get_line_front('pet_name'); ?><span>*</span></label>
											<input type="text" name="pet_name[]" class="form-control">
											<?= form_error('pet_name[]', "<label class='error'>", "</label>"); ?>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><?= get_line_front('pet_type'); ?><span>*</span></label>
											<div class="select-wrap">
												
													<?php 
														$pet_types = get_pet_type();
			                                            if (isset($pet_types) && $pet_types !=null) {
			                                                foreach ($pet_types as $key => $value) {
			                                                    $options[$key] = $value;
			                                                }
			                                            }
			                                            echo form_dropdown('pet_type[]',$options,'','class="form-control"'); 
			                                            ?>
			                                        <?= form_error('pet_type[]', "<label class='error'>", "</label>"); ?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><?php echo get_line_front('pet_gender'); ?><span>*</span></label>
											<div class="select-wrap">
												<?php 
													$pet_gender = get_gender();
		                                            if (isset($pet_gender) && $pet_gender !=null) {
		                                                foreach ($pet_gender as $key => $value) {
		                                                    $genderoptions[$key] = $value;
		                                                }
		                                            }
		                                            echo form_dropdown('pet_gender[]',$genderoptions,'','class="form-control"'); 

		                                        ?>
		                                        <?= form_error('pet_gender[]', "<label class='error'>", "</label>"); ?>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><?php echo get_line_front('pet_dob'); ?><span>*</span></label>
											<input type="date" name="pet_dob[]" class="form-control">
											<?= form_error('pet_dob[]', "<label class='error'>", "</label>"); ?>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><?php echo get_line_front('pet_breed'); ?><span>*</span></label>
											<input type="text" name="pet_breed[]" class="form-control">
											<?= form_error('pet_breed[]', "<label class='error'>", "</label>"); ?>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><?php echo get_line_front('pet_weight'); ?><span>*</span></label>
											<input type="text" name="pet_weight[]" class="form-control">
											<?= form_error('pet_weight[]', "<label class='error'>", "</label>"); ?>
										</div>
									</div>
									<div class="col-lg-12">
									<div class="form-group pull-right">
											<button type="button" class="btn pull-right btn-add btn-login remove-more"><?php echo get_line_front('remove'); ?></button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<button type="button" class="btn pull-left btn-add btn-login add-more"><?php echo get_line_front('add_another_pet_details'); ?></button>
								</div>
							</div>
							
						</div>
							<div class="col-lg-12">
								<label class="cust-checkbox">
									<?php echo get_line_front('i_would_like_to_receive_offers'); ?>
									<!-- I would like to receive offers, promotions and order related information via Email/SMS/Phone Call -->.
								  	<input type="checkbox" name="news_leter">
								  	<span class="checkmark"></span>
								</label>
							</div>
							<button class="btn btn-login"><?php echo get_line_front('register'); ?></button>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
</section>
<?php
$footer_js = array(base_url('assets/authority/js/toastr.min.js'), 'https://code.jquery.com/ui/1.12.1/jquery-ui.js','https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js');
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script>
	$(document).ready(function() {

		var i = 1;
		$(document).on('change', 'input[name="news_leter"]', function() {
			if($('input[name="news_leter"]').is(":checked")){
				news_leter = $(this).val('1');
			}else{
				news_leter = $(this).val('0');
			}
			console.log($(this).val());
		});
        $(document).on('click', '.add-more', function() {
          if (check_clone_section()){

              $clone = $('.clone-section-sub:last').clone();
              $('.clone-section-main').append($clone);
              $('.clone-section-sub:last').find('input[type="text"]').val('');
                            
          }
        });
        $(document).on('click', '.remove-more', function() {
            if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
                $(this).closest('.clone-section-sub').remove();
            }
            else{
                $.alert({
                title: '<?=get_line_front('sorry')?>!!',
                content: '<?=get_line_front('you_can_not_remove_the_current_section')?>'
            });
            }
        });
		// $('.send-otp').click(function() {
		// 	var current = $(this);
		// 	loading(true);
		// 	var data = $('#form').serialize();
		// 	$.ajax({
		// 		url: BASE_URL + 'ajax/send_otp',
		// 		method: 'POST',
		// 		data: data,
		// 		dataType: 'json',
		// 		success: function(response) {
		// 			setTimeout(function() {
		// 				loading(false);
		// 			}, 150);
		// 			toastr.remove();
		// 			if (response.status) {
		// 				toastr.success(response.message);
		// 			} else {
		// 				toastr.error(response.message);
		// 			}
		// 		}
		// 	});
		// });

		// $('.signup-with-mobile').click(function() {
		// 	var current = $(this);
		// 	loading(true);
		// 	var data = $('#form').serialize();
		// 	$.ajax({
		// 		url: BASE_URL + 'ajax/verify_user',
		// 		method: 'POST',
		// 		data: data,
		// 		dataType: 'json',
		// 		success: function(response) {
		// 			toastr.remove();
		// 			if (response.status) {
		// 				toastr.success(response.message);
		// 				setTimeout(function() {
		// 					window.location = response.redirect_url;
		// 				}, 1000);
		// 			} else {
		// 				setTimeout(function() {
		// 					loading(false);
		// 				}, 150);
		// 				toastr.error(response.message);
		// 			}
		// 		}
		// 	});
		// });


		var today = new Date();
		$(".date_of_birth").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0",
			dateFormat: 'dd-mm-yy',
			endDate: "today",
			maxDate: today,
			showButtonPanel: true,
		});

		/*FORM VALIDATION*/
		$("#form").validate({
			rules: {
				full_name: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				password: {
					required: true
				},
				confirm_password: {
					equalTo: "#password"
				},
				mobile_number: {
					required: true
				},
				city: {
					required: true
				},
				'pet_name[]': {
					required: true
				},
				'pet_type[]': {
					required: true
				},
				'pet_gender[]': {
					required: true
				},
				'pet_breed[]': {
					required: true
				},
				'pet_weight[]': {
					required: true
				},
			},
			messages: {
				full_name: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				email: {
					required: "<?= get_line_front('this_field_is_required') ?>",
					email: "<?= get_line_front('please_enter_valid_email_address') ?>"
				},
				password: {
					required: '<?= get_line_front('this_field_is_required') ?>'
				},
				confirm_password: {
					equalTo: '<?= get_line_front('confirm_password_not_mached') ?>'
				},
				mobile_number: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				city: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				'pet_name[]': {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				'pet_type[]': {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				'pet_gender[]': {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				'pet_breed[]': {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				'pet_weight[]': {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
			}
		});
	});
	function check_clone_section(){
        if ($('.clone-section-main').find('.clone-section-sub').length < 6){
            return true;
        }else{
            $.alert('You can add 5 items only.');
            return false;
        }
    }
</script>
<?php $this->load->view('include/footer'); ?>