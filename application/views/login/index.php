<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<section class="login">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="login-form">
					<h3>
						<?= get_line_front('login'); ?>
					</h3>
					<form action="<?= base_url('login/dologin') ?>" method="post">
						<div class="form-group">
							<label><?= get_line_front('email_address'); ?></label>
							<input type="email" name="email" placeholder="<?= get_line_front('email_address'); ?>" class="form-control">
						</div>
						<div class="form-group">
							<label>password</label>
							<input type="password" name="password" placeholder="<?= get_line_front('password'); ?>" class="form-control">
						</div>
						<div class="form-group">
							<a href="<?=base_url()?>"><?= get_line_front('forgot_password'); ?>?</a>
							<a href="<?=base_url()?>" class="pull-right"><?= get_line_front('new_user'); ?>? <?= get_line_front('register'); ?></a>
						</div>
						<button class="btn btn-login"><?= get_line_front('login'); ?></button>
					</form>
				</div>
			</div>	
		</div>
	</div>
</section>
<?php
$footer_js = array(base_url('assets/authority/js/toastr.min.js'), 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script>
	$(document).ready(function() {
		$('.send-otp').click(function() {
			var current = $(this);
			loading(true);
			var data = $('#form').serialize();
			$.ajax({
				url: BASE_URL + 'ajax/send_otp',
				method: 'POST',
				data: data,
				dataType: 'json',
				success: function(response) {
					setTimeout(function() {
						loading(false);
					}, 150);
					toastr.remove();
					if (response.status) {
						toastr.success(response.message);
					} else {
						toastr.error(response.message);
					}
				}
			});
		});

		$('.signup-with-mobile').click(function() {
			var current = $(this);
			loading(true);
			var data = $('#form').serialize();
			$.ajax({
				url: BASE_URL + 'ajax/verify_user',
				method: 'POST',
				data: data,
				dataType: 'json',
				success: function(response) {
					toastr.remove();
					if (response.status) {
						toastr.success(response.message);
						setTimeout(function() {
							window.location = response.redirect_url;
						}, 1000);
					} else {
						setTimeout(function() {
							loading(false);
						}, 150);
						toastr.error(response.message);
					}
				}
			});
		});


		var today = new Date();
		$(".date_of_birth").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0",
			dateFormat: 'dd-mm-yy',
			endDate: "today",
			maxDate: today,
			showButtonPanel: true,
		});

		/*FORM VALIDATION*/
		$("#form").validate({
			rules: {
				email: {
					required: true,
					email: true
				},
				password: {
					required: true
				},
				first_name: {
					required: true
				},
				last_name: {
					required: true
				},
				gender: {
					required: true
				},
				date_of_birth: {
					required: true
				},
			},
			messages: {
				email: {
					required: "<?= get_line_front('this_field_is_required') ?>",
					email: "<?= get_line_front('please_enter_valid_email_address') ?>"
				},
				password: {
					required: '<?= get_line_front('this_field_is_required') ?>'
				},
				first_name: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				last_name: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				gender: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
				date_of_birth: {
					required: "<?= get_line_front('this_field_is_required') ?>"
				},
			}
		});
	});
</script>
<?php $this->load->view('include/footer'); ?>