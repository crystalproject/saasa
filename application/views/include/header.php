<!DOCTYPE html>
<?php
$set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
?>
<html>
<head>
    <!-- Required meta tags --> 

    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= SITE_TITLE; ?></title>
    <meta name="description" content="Pets is responsive multi-purpose HTML5 template compatible with Bootstrap 4. Take your Startup business website to the next level. it is designed for pet care, clinic, veterinary, shop, store, adopt, food, pets, businesses or any type of person or business who wants to showcase their work, services and professional way">
    <meta name="keywords" content="animals, business, cats, dogs, ecommerce, modern, pet care, pet services, pet shop, pet sitting, pets, shelter animals, store, veterinary">
    <meta name="author" content="rometheme.net">
    

    <?php /*
    if ($set_language == 1) {
    ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/plugins.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/theme.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/shortcodes.css"> -->
    <?php }
    if ($set_language == 2) {
    ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/rtl/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/rtl/style.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/rtl/style_rtl.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/theme.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/theme_rtl.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/plugins.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/shortcodes.css">
        <style type="text/css">
            .footer-main #text-16 {
                margin-left: -7px !important;
            }
        </style>
    <?php
    } */
    ?>
    <!-- Favicon Start -->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/images/favicon.ico">
    <link rel="apple-touch-icon" href="<?= base_url() ?>assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>assets/images/apple-touch-icon-114x114.png">
    <!-- Favicon End -->
    
    <!-- Common Css Start -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/vendor/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/vendor/animate.min.css">
    <!-- Common Css End -->
    
    <!-- Custom Css Start -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/new-custom.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/new-custom-responsive.css" />
    <!-- Custom Css End -->

    <!-- Fonts Css Start -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap" rel="stylesheet"> 
    <!-- Fonts Css End -->
    
    <script src="<?= base_url() ?>assets/js/vendor/modernizr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
    <!--New add-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <?php
    if (isset($header_css) && !empty($header_css)) {
        foreach ($header_css as $key => $value) {
            echo '<link rel="stylesheet" href="' . $value . '">';
        }
    }
    ?>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/developer.css" />
    <style type="text/css">
        /*@import '~@fortawesome/fontawesome-free/css/all.css';*/
    </style>
    <script>
        var BASE_URL = '<?= base_url(); ?>';
        var toastr_success = '';
        var toastr_error = '';
    </script>
</head>

<body>
    <!-- Loader Start -->
    <div class="animationload">
        <div class="loader"></div>
    </div>
    <!-- Loader End -->
    
    <!-- Back To Top Start -->
    <a href="#0" class="cd-top cd-is-visible cd-fade-out">Top</a>
    <!-- Back To Top End -->

    <!-- Header Start -->
    <header class="header header-1">
        <!-- Top Bar Start -->
        <div class="topbar d-none d-md-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-sm-5 col-md-5">
                        <p class="mb-0"><?=get_line_front('welcome_slogun')?></p>
                    </div>

                    <div class="col-sm-6 col-md-6">
                        <div class="sosmed-icon d-inline-flex pull-right">
                            <a href="#"><i class="fa fa-facebook"></i></a> 
                            <a href="#"><i class="fa fa-twitter"></i></a> 
                            <a href="#"><i class="fa fa-instagram"></i></a> 
                            <a href="#"><i class="fa fa-pinterest"></i></a> 
                        </div>
                    </div>
                    <div class="dropdown user user-menu">
                        
                        <?php
                        $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
                        ?>
                        <select class="form-control form-control-sm change-language">
                            <option value="1" <?php echo $set_language == '1' ? 'selected="selected"' : ''; ?>><?php echo get_line('english'); ?></option>
                            <option value="2" <?php echo $set_language == '2' ? 'selected="selected"' : ''; ?>><?php echo get_line('hindi'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Bar End -->

        <!-- Middle Bar Start -->
        <div class="middlebar d-none d-sm-block">
            <div class="container">
                <div class="contact-info">
                    <div class="box-icon-1">
                        <div class="icon">
                            <div class="fa fa-clock-o"></div>
                        </div>
                        <div class="body-content">
                            <div class="heading"><?=get_line_front('open_hours')?> :</div>
                            <?=get_line_front('mon_-_fri')?> : 09:00 - 20:00
                        </div>
                    </div>

                    <div class="box-icon-1">
                        <div class="icon">
                            <div class="fa fa-phone"></div>
                        </div>
                        <?php 
                            $details = get_details('administrator',array('role' => 'Admin'));   
                        ?>
                        <div class="body-content">
                            <div class="heading"><?=get_line_front('call_today')?>  :</div>
                            <?=isset($details) && $details !=null ? nl2br($details[0]['mobile_number_1']) : '';?> / <?=isset($details) && $details !=null ? nl2br($details[0]['mobile_number_2']) : '';?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Middle Bar End -->

        <!-- Navbar Start -->
        <div class="navbar-main">
            <?php

                $menu = get_details('menus', array('is_active' => '1'),'asc');
                foreach ($menu as $key => $value) {
                    $menus[$value['name']] = get_details('menus', array('is_active' => '1', 'id' => $value['id']))[0];
                }
                // echo "<pre>";print_r($menus);

                // print_r($menus['Home']);exit;
                
                // $home = get_details('menus', array('is_active' => '1', 'id' => 1));
                // $shop = get_details('menus', array('is_active' => '1', 'id' => 4));
                // $about_us = get_details('menus', array('is_active' => '1', 'id' => 2));
                // $contact_us = get_details('menus', array('is_active' => '1', 'id' => 5));
                // $return_policy = get_details('menus', array('is_active' => '1', 'id' => 6));
                // $privacy_policy = get_details('menus', array('is_active' => '1', 'id' => 8));
                // $terms_condition = get_details('menus', array('is_active' => '1', 'id' => 10));
            ?>
            <div class="container">
                <nav id="navbar-example" class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="<?= base_url() ?>">
                        <?php
                            
                            $profile_picture = isset($details) && $details !=null ? base_url(PROFILE_PICTURE.$details[0]['profile_photo']) : '';
                        ?>
                        <img src="<?=$profile_picture?>" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <?php
                                foreach ($menus as $key => $value) {
                                    if (isset($key) && $key != null) {
                                        if($key=="Home"){
                                        ?>

                                        <li class="care-img active nav-item">
                                            <a class="mkdf-banner-title-holder" href="<?= base_url() ?>" target="_self">
                                                <span class="mkdf-active-hover">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                        <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                    </svg>
                                                    <span class="mkdf-active-hover-middle"></span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                        <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                    </svg>
                                                </span>
                                                <h3 class="mkdf-banner-title">
                                                    <?=$value['name'. get_language()]?>
                                                </h3>
                                            </a>
                                        </li>
                                    <?php 
                                        }else{ ?>
                                            <li class="care-img nav-item dropdown dmenu">
                                                <a class="mkdf-banner-title-holder" href="javascript:void(0);" target="_self">
                                                    <span class="mkdf-active-hover">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                        </svg>
                                                        <span class="mkdf-active-hover-middle"></span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                        </svg>
                                                    </span>
                                                    <h3 class="mkdf-banner-title">
                                                        <?=$value['name'. get_language()]?>
                                                    </h3>
                                                </a>
                                                    <?php
                                                        $this->db->group_start();
                                                        $this->db->where("find_in_set(".$value['id'].", menu_id)");
                                                        $this->db->group_end();
                                                        $where['parent_id']=0;
                                                        $where['is_active']=1;
                                                        $category_details = get_details('category', $where);
                                                        if (isset($category_details) && $category_details != null) { ?>
                                                            <div class="cust-dropdown-menu">
                                                                <div class="row">
                                                                    <?php
                                                                        foreach ($category_details as $key1 => $value1) {
                                                                            if (!empty($value1)) { ?>
                                                                                <div class="col-md-4">
                                                                                    <div class="drop-links">
                                                                                        <a href="<?php echo base_url('category/' . $value1['slug' . get_language()] . '/' . $value1['id']); ?>" class="category-list-head">
                                                                                            <span class="item_outer">
                                                                                                <span class="mkdf-active-hover">
                                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                                                        <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                                                                    </svg>
                                                                                                    <span class="mkdf-active-hover-middle"></span>
                                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                                                        <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                                                                    </svg>
                                                                                                </span>
                                                                                              <span class="item_text"><?= $value1['name' . get_language_front()]; ?></span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            <?php }
                                                                        ?>
                                                                            
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php     
                                                        }
                                                    ?>                                            
                                            </li>
                                   <?php  }
                                    } 
                                }?>
                            

                            <!-- <li class="care-img nav-item dropdown dmenu">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        Cat
                                    </h3>
                                </a>
                                <div class="cust-dropdown-menu">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 1</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 2</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 3</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="care-img nav-item dropdown dmenu">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        Birds
                                    </h3>
                                </a>
                                <div class="cust-dropdown-menu">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 1</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 2</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 3</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="care-img nav-item dropdown dmenu">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        Hygiene
                                    </h3>
                                </a>
                                <div class="cust-dropdown-menu">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 1</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 2</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 3</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="care-img nav-item dropdown dmenu">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        Shop by brands
                                    </h3>
                                </a>
                                <div class="cust-dropdown-menu">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Pedigree</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Himalaya</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Gnawlers</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Goodies</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">First Bark</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Royal Canin</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="care-img nav-item dropdown dmenu">
                                <a class="mkdf-banner-title-holder" href="#" target="_self">
                                    <span class="mkdf-active-hover">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                            <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                        </svg>
                                        <span class="mkdf-active-hover-middle"></span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                            <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                        </svg>
                                    </span>
                                    <h3 class="mkdf-banner-title">
                                        wholesale
                                    </h3>
                                </a>
                                <div class="cust-dropdown-menu">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 1</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 2</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="drop-links">
                                                <a href="#" class="category-list-head">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Category 3</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="">
                                                    <span class="item_outer">
                                                        <span class="mkdf-active-hover">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
                                                                <path d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
                                                            </svg>
                                                            <span class="mkdf-active-hover-middle"></span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
                                                                <path d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
                                                            </svg>
                                                        </span>
                                                      <span class="item_text">Link 1</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                        <a href="javascript:void(0);" class="btn btn-search-i btn-secondary btn-nav btn-rect ml-auto">
                            <i class="fa fa-search"></i>
                        </a>
                        <div class="search-form">
                            <form>
                                <div class="form-group">
                                    <input type="text" name="search" class="form-control" placeholder="Search">
                                    <button class="btn btn-secondary btn-search">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <a href="<?=base_url('wishlist')?>" class="btn btn-secondary btn-nav btn-rect">
                            <i class="fa fa-heart"></i>
                            <span class="cart-number wishlist-counter"><?php echo get_total_wishlist(); ?></span>
                        </a>
                        <a href="javascript:void(0);" class="btn btn-user btn-secondary btn-nav btn-rect">
                            <i class="fa fa-user"></i>
                        </a>
                        <div class="login-pop">
                            
                            <?php
                                if ($this->session->userdata('login_id') == null) {
                                ?>
                                    <a href="<?= base_url('login') ?>"><?= get_line_front('login'); ?></a>
                                    <a href="<?= base_url('register') ?>"><?= get_line_front('new_user'); ?></a></a>
                                <?php }else{ ?>
                                    <label>Welcome</label>
                                    <a href="<?= base_url('account') ?>"><?= get_line_front('my_account'); ?></a>
                                    <a href="<?= base_url('login/logout') ?>"><?= get_line_front('sign_out'); ?></a>
                                <?php }?>
                        </div>
                        <a href="<?= base_url('cart') ?>" class="btn btn-secondary btn-nav btn-rect">
                            <i class="fa fa-shopping-cart"></i>
                                <span class="cart-number cart-items" <?=(get_total_cart_product() < 1)?'show':''?>><?php echo get_total_cart_product(); ?></span>
                        </a>
                    </div>
                </nav>

            </div>
        </div>
        <!-- Navbar End -->
    </header>
    <!-- Header End -->
    