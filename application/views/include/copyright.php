<!-- Footer Section Strat Here -->
<!-- <div class="footer-wrapper">
    <div id="footer" class="footer-1">
        <div class="footer-main">
            <div class="container">
                <?php
                $website = get_details('menus', array('is_active' => '1', 'id' => 7));
                $home = get_details('menus', array('is_active' => '1', 'id' => 3));
                $about_us = get_details('menus', array('is_active' => '1', 'id' => 2));
                $privacy_policy = get_details('menus', array('is_active' => '1', 'id' => 8));
                $contact_us = get_details('menus', array('is_active' => '1', 'id' => 5));
                $return_policy = get_details('menus', array('is_active' => '1', 'id' => 6));
                $terms_condition = get_details('menus', array('is_active' => '1', 'id' => 10));

                $categories = get_details('menus', array('is_active' => '1', 'id' => 11));
                $clothes = get_details('menus', array('is_active' => '1', 'id' => 23));
                $accessories = get_details('menus', array('is_active' => '1', 'id' => 12));
                $devices = get_details('menus', array('is_active' => '1', 'id' => 13));
                $decor = get_details('menus', array('is_active' => '1', 'id' => 14));
                $stationary = get_details('menus', array('is_active' => '1', 'id' => 15));

                $other = get_details('menus', array('is_active' => '1', 'id' => 16));
                $faq = get_details('menus', array('is_active' => '1', 'id' => 17));
                $site_map = get_details('menus', array('is_active' => '1', 'id' => 18));

                $registration = get_details('menus', array('is_active' => '1', 'id' => 19));
                $sign_in = get_details('menus', array('is_active' => '1', 'id' => 20));
                $sign_up = get_details('menus', array('is_active' => '1', 'id' => 21));
                $my_account = get_details('menus', array('is_active' => '1', 'id' => 22));
                $forgot_password = get_details('menus', array('is_active' => '1', 'id' => 24));
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <aside id="text-20" class="widget widget_text">
                            <h3 class="widget-title"><?= get_line_front('subscribe_to_our_newsletter'); ?></h3>
                            <div class="textwidget">
                                <form action="#" class="wpcf7-form">
                                    <div class="widget_wysija_cont widget_wysija">
                                        <div class="wysija-paragraph d-table-cell">
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <input type="email" name="your-email" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control wysija-input" placeholder="<?= get_line_front('your_email'); ?>">
                                            </span>
                                        </div>
                                        <div class="d-table-cell">
                                            <input type="submit" value=">" class="wpcf7-form-control wpcf7-submit btn btn-quaternary wysija-submit font-weight-bold">
                                        </div>
                                    </div>
                                </form>
                                <div id="header" class="D_sharelinks footer-sharelinks">
                                    <div class="share-links">
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-facebook" style="color: #000">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-twitter" style="color: #000">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-instagram" style="color: #000">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-linkedin" style="color: #000">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <aside id="text-16" class="widget widget_text">
                            <?php
                            if (isset($website) && $website != null) {
                            ?>
                                <h3 class="widget-title"><?= $website[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        if (isset($home) && $home != null) {
                                        ?>
                                            <li><a href="<?= base_url(); ?>"><?= $home[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($about_us) && $about_us != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/about-us'); ?>"><?= $about_us[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($privacy_policy) && $privacy_policy != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/privacy-policy'); ?>"><?= $privacy_policy[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($return_policy) && $return_policy != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/return-policy'); ?>"><?= $return_policy[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($terms_condition) && $terms_condition != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/terms-condition'); ?>"><?= $terms_condition[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <aside id="text-23" class="widget widget_text">
                            <?php
                            if (isset($categories) && $categories != null) {
                            ?>
                                <h3 class="widget-title"><?= $categories[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        $conditions = array(
                                            'where' => array('parent_id' => 0, 'is_active' => '1'),
                                            'limit' => '5'
                                        );
                                        $info = $this->common_model->select_data('category', $conditions);
                                        if ($info['row_count'] > 0) {
                                            foreach ($info['data'] as $key => $value) {
                                        ?>
                                                <li><a href="<?php echo base_url('category/' . $value['slug' . get_language()] . '/' . $value['id']); ?>"><?= $value['name' . get_language()]; ?></a></li>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <aside id="text-23" class="widget widget_text">
                            <?php
                            if (isset($other) && $other != null) {
                            ?>
                                <h3 class="widget-title"><?= $other[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        if (isset($contact_us) && $contact_us != null) {
                                        ?>
                                            <li><a href="<?= base_url('contact-us'); ?>"><?= $contact_us[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($faq) && $faq != null) {
                                        ?>
                                            <li><a href="<?= base_url('faq'); ?>"><?= $faq[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($site_map) && $site_map != null) {
                                        ?>
                                            <li><a href="#"><?= $site_map[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <aside id="text-23" class="widget widget_text">
                            <?php
                            if (isset($registration) && $registration != null) {
                            ?>
                                <h3 class="widget-title"><?= $registration[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        if (isset($sign_up) && $sign_up != null) {
                                            if ($this->session->userdata('login_id') == null) {
                                        ?>
                                                <li><a href="<?= base_url('register'); ?>"><?= $sign_up[0]['name' . get_language_front()]; ?></a></li>
                                            <?php }
                                        }
                                        if (isset($my_account) && $my_account != null) {
                                            if ($this->session->userdata('login_id') != null) {
                                            ?>
                                                <li><a href="<?= base_url('account'); ?>"><?= $my_account[0]['name' . get_language_front()]; ?></a></li>
                                            <?php }
                                        }
                                        if (isset($forgot_password) && $forgot_password != null && $this->session->userdata('login_id') == null) {
                                            ?>
                                            <li><a href="<?= base_url('forgot-password'); ?>"><?= $forgot_password[0]['name' . get_language_front()]; ?></a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Newsletter Start -->
<section class="section cta bgi-cover-center" data-background="<?= base_url() ?>assets/images/pattern_bg.jpg">
    <div class="content-wrap py-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-12 col-md-8">
                    <div class="cta-1">
                        <div class="body-text text-white mb-3">
                            <h3 class="my-1">Lorem Ipsum is simply dummy text of the industry!</h3>
                            <p class="uk18 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="cta-1">
                        <div class="news-form">
                            <form>
                                <ul>
                                    <li>
                                        <input type="email" name="email" placeholder="Email Address" class="form-control">
                                    </li>
                                    <li>
                                        <a href="#" class="btn btn-secondary">Subscribe</a>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Newsletter End -->

<!-- Footer Start -->
<footer class="footer bg-overlay-secondary" data-background="<?= base_url() ?>assets/images/statistic_bg.jpg">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="footer-item">
                        <?php
                            $details = get_details('administrator',array('role' => 'Admin'));
                            $profile_picture = isset($details) && $details !=null ? base_url(PROFILE_PICTURE.$details[0]['profile_photo']) : '';
                        ?>
                        <img src="<?=$profile_picture?>" alt="logo bottom" class="logo-bottom">
                        <div class="spacer-20"></div>
                        <p><?=get_line_front('health_and_well_being')?><?=get_line_front('health_and_well_being')?></p>
                        <div class="spacer-20"></div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="footer-item">
                        <div class="footer-title">
                            <?=get_line_front('useful_links')?>
                        </div>
                        <ul class="list">
                            <li>
                                <a href="<?= base_url('cms-page/about-us'); ?>"><?=get_line_front('about_us')?></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><?=get_line_front('contact_us')?></a>
                            </li>
                            <li>
                                <a href="<?= base_url('wishlist'); ?>"><?=get_line_front('wishlist')?></a>
                            </li>
                            <?php
                                if ($this->session->userdata('login_id') !== null) {
                                ?>
                                <li>
                                    <a href="<?= base_url('account') ?>"><?=get_line_front('my_account')?></a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="footer-item">
                        <div class="footer-title">
                            <?=get_line_front('policies')?>
                        </div>
                        <ul class="list">

                            <li>
                                <a href="<?= base_url('cms-page/privacy-policy'); ?>"><?=get_line_front('privacy_policy')?></a>
                            </li>
                            <li>
                                <a href="<?= base_url('cms-page/terms-condition'); ?>"><?=get_line_front('terms_condition')?></a>
                            </li>
                            <li>
                                <a href="<?= base_url('cms-page/privacy-policy'); ?>"><?=get_line_front('website_disclaimer_policy')?></a>
                            </li>
                            <li>
                                <a href="<?= base_url('cms-page/privacy-policy'); ?>"><?=get_line_front('replacement_and_refund_policy')?></a>
                            </li>
                            <li>
                                <a href="<?= base_url('cms-page/privacy-policy'); ?>"><?=get_line_front('shipment_and_delivery_policy')?></a>
                            </li>
                            <li>
                                <a href="<?= base_url('cms-page/privacy-policy'); ?>"><?=get_line_front('pricing_information')?></a>
                            </li>
                            <li>
                                <a href="<?= base_url('cms-page/privacy-policy'); ?>"><?=get_line_front('disclaimer')?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="footer-item">
                        <div class="footer-title">
                            <?=get_line_front('contact_info')?>
                        </div>
                        
                        <p>
                            <?=isset($details) && $details !=null ? nl2br($details[0]['address']) : '';?></p>
                        <ul class="list-info">
                            <!-- <li>
                                <div class="info-icon text-primary">
                                    <span class="fa fa-map-marker"></span>
                                </div>
                                <div class="info-text">99 S.t Jomblo Park Pekanbaru 28292. Indonesia</div> 
                            </li> -->
                            <li>
                                <div class="info-icon text-primary">
                                    <span class="fa fa-phone"></span>
                                </div>
                                <div class="info-text"><?=isset($details) && $details !=null ? nl2br($details[0]['mobile_number_1']) : '';?></div>
                            </li>
                            <li>
                                <div class="info-icon text-primary">
                                    <span class="fa fa-phone"></span>
                                </div>
                                <div class="info-text"><?=isset($details) && $details !=null ? nl2br($details[0]['mobile_number_2']) : '';?></div>
                            </li>
                            <li>
                                <div class="info-icon text-primary">
                                    <span class="fa fa-envelope"></span>
                                </div>
                                <div class="info-text"><?=isset($details) && $details !=null ? nl2br($details[0]['email_address_website']) : '';?></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="fcopy">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p class="ftex">&copy; <?=date('Y')?> <?=get_line_front('star_pets')?>. <?=get_line_front('all_rights_reserved')?>. <!-- Designed By <a href="#" target="_blank" class="text-primary">Crystall Infoway</a> --></p> 
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="sosmed-icon d-inline-flex float-right">
                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a> 
                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a> 
                        <a href="#" target="_blank"><i class="fa fa-instagram"></i></a> 
                        <a href="#" target="_blank"><i class="fa fa-pinterest"></i></a> 
                    </div>
                </div>
            </div>
        </div>
    </div>  
</footer>
<!-- Footer End -->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<!-- <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script> -->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.tr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<?php /*<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>*/ ?>

<!-- Common Js Start -->
<script src="<?= base_url(); ?>assets/js/vendor/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/vendor/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/vendor/owl.carousel.js"></script>
<script src="<?= base_url(); ?>assets/js/vendor/jquery.magnific-popup.min.js"></script>
<!-- Common Js End -->

<!-- Sendmail Js Start -->
<script src="<?= base_url(); ?>assets/js/vendor/validator.min.js"></script>
<script src="<?= base_url(); ?>assets/js/vendor/form-scripts.js"></script>
<!-- Sendmail Js End -->

<!-- Custom Js Start -->
<script src="<?= base_url(); ?>assets/js/script.js"></script>
<script src="<?= base_url(); ?>assets/js/custom.js"></script>
<!-- Custom Js End -->

<script src="<?= base_url(); ?>assets/validation/common.js"></script>
<script src="<?= base_url('assets/validation/jquery.validate.js'); ?>"></script>
<script src="<?= base_url() ?>assets/js/toastr.min.js"></script>
<script src="<?= base_url() ?>assets/js/cart.js"></script>

<?php
if (isset($footer_js) && $footer_js != null) {
    foreach ($footer_js as $key => $js) {
        echo '<script src=' . $js . '></script>';
    }
}
?>
<script type="text/javascript">
    "use strict"; // Start of use strict
    (function($) {
        function bootstrapAnimatedLayer() {
            function doAnimations(elems) {
                //Cache the animationend event in a variable
                var animEndEv = "webkitAnimationEnd animationend";
                elems.each(function() {
                    var $this = $(this),
                        $animationType = $this.data("animation");
                    $this.addClass($animationType).one(animEndEv, function() {
                        $this.removeClass($animationType);
                    });
                });
            }

            //Variables on page load
            var $myCarousel = $("#minimal-bootstrap-carousel"),
                $firstAnimatingElems = $myCarousel
                .find(".carousel-item:first")
                .find("[data-animation ^= 'animated']");

            //Initialize carousel
            try {
                $myCarousel.carousel();
            } catch (err) {

            }

            //Animate captions in first slide on page load
            doAnimations($firstAnimatingElems);

            //Other slides to be animated on carousel slide event
            $myCarousel.on("slide.bs.carousel", function(e) {
                var $animatingElems = $(e.relatedTarget).find(
                    "[data-animation ^= 'animated']"
                );
                doAnimations($animatingElems);
            });
        }

        bootstrapAnimatedLayer();



    })(jQuery);

    function openNav() {
        document.getElementById("mySidenav").style.width = "300px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    function openNav1() {
        document.getElementById("mySidenav1").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("mySidenav1").style.width = "0";
        document.body.style.backgroundColor = "white";
    }

    function closeNav1() {
        document.getElementById("mySidenav1").style.width = "0";
    }

    //  $( "li.fade" ).hover(function() {
    //   $( this ).fadeOut( 100 );
    //   $( this ).fadeIn( 500 );
    // });

    $(document).ready(function() {

        if (toastr_success != '') {
            toastr.success(toastr_success);
        }
        if (toastr_error != '') {
            toastr.error(toastr_error);
        }

        $('.send-otp-login').click(function() {
            var current = $(this);
            $('.active-user').addClass('active');
            loading(true);
            var data = $('#login_form').serialize();
            $.ajax({
                url: BASE_URL + 'ajax/send_otp_login',
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(response) {
                    setTimeout(function() {
                        loading(false);
                    }, 150);
                    toastr.remove();
                    if (response.status) {
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });
        $('.mobile-signin').click(function() {
            var current = $(this);
            loading(true);
            var data = $('#login_form').serialize();
            $.ajax({
                url: BASE_URL + 'ajax/mobile-signin',
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(response) {
                    setTimeout(function() {
                        loading(false);
                    }, 150);
                    toastr.remove();
                    if (response.status) {
                        toastr.success(response.message);
                        window.location = window.location.href;
                    } else {
                        $('.active-user').removeClass('active');
                        toastr.error(response.message);
                    }
                }
            });
        });

        loading(false);
        // $('.change-language').click(function() {
        //     var language = $(this).attr('data-id');
        //     loading(true);
        //     $.ajax({
        //         url: '<?= base_url('home/change_language') ?>',
        //         type: 'POST',
        //         data: {
        //             'id_language': language
        //         },
        //         dataType: 'json',
        //         success: function(response) {
        //             loading(false);
        //             if (response.status) {
        //                 window.location = window.location.href;
        //             }
        //         }
        //     });
        // });
        $('.change-language').change(function () {
            var current = $(this);
            loading(true);
            $.ajax({
                url: BASE_URL + 'home/change_language',
                method: 'POST',
                data: {'id_language': current.val()},
                dataType: 'json',
                success: function (response) {
                    loading(false);
                    if (response.status) {
                        window.location = window.location.href;
                    }
                }
            });
        });
        $(document).on('click', '.wishlist', function() {
            var id_product = $(this).attr('id');
            var current = $(this);
            loading(true);
            $.ajax({
                url: "<?php echo base_url('wishlist/add_wishlist') ?>",
                data: {
                    id_product: id_product
                },
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    loading(false);
                    toastr.remove();
                    if (data.success) {
                        current.addClass('available');
                        current.find('i').addClass('fa-heart');
                        current.find('i').removeClass('fa-heart-o');
                        current.find('span').text('<?= get_line_front('remove_favorite'); ?>');
                        toastr.success('<?= get_line_front('wishlist_added_successfully') ?>');
                        $('.wishlist-counter').text(data.data.total_product);
                    }
                    if (data.delete) {
                        if (current.hasClass('from-wishlist')) {
                            current.closest('.single-wish-item').remove();
                        } else {
                            current.removeClass('available');
                            current.find('i').removeClass('fa-heart');
                            current.find('i').addClass('fa-heart-o');
                            current.find('span').text('<?= get_line_front('add_to_favorite'); ?>');
                        }
                        toastr.success('<?= get_line_front('wishlist_removed_successfully') ?>');
                        $('.wishlist-counter').text(data.data.total_product);
                    }
                    if (data.error == false) {
                        toastr.error('<?= get_line_front('please_login_after_add_wishlist') ?>');
                    }
                }
            });
        });
    });
</script>