<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
?>
<!-- banner Section Strat Here -->
<?php
    if (isset($banner) && $banner != null) {
    ?>
    <div class="section banner-page" data-background="<?= base_url(CAT_SUB_CAT_BANNER . $banner[0]['banner' . get_language_front()]); ?>">
        <div class="content-wrap pos-relative">
            <div class="d-flex justify-content-center bd-highlight mb-2">
                <div class="title-page"><?= $banner[0]['name' . get_language_front()]; ?></div>
            </div>
            <p class="text-center text-white">Your pet's health and well-being are our top priority.</p>
        </div>
    </div>
<?php } ?>
<div class="section bg-breadcrumb">
    <div class="content-wrap py-0 pos-relative">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('category/' . $value1['slug' . get_language()] . '/' . $value1['id']); ?>">Dogs</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Foods</li>
                </ol>
            </nav>                  
        </div>
    </div>
</div>
<!-- banner Section End Here -->
<!-- Clothes Section Strat Here -->
<section class="clothes-section">
    <div class="clothes-section-content">
        <div class="container">
            <div class="porto-u-main-heading-2">
                <h2><?= isset($banner) && $banner != null ? $banner[0]['name' . get_language_front()] : ''; ?></h2>
            </div>
            <div class="row">
                <?php
                if (isset($sub_category) && $sub_category != null) {
                    foreach ($sub_category as $key => $value) {
                        $id = $value['id'];
                ?>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                            <div class="clothes-section-box text-center">
                                <a href="<?php echo $value['base_url']; ?>" class="new-link">
                                    <img src="<?= base_url(CATEGORY_IMAGE . $value['image' . get_language_front()]); ?>">
                                    <div class="product-box-title">
                                        <h4><b><?= $value['name' . get_language_front()]; ?></b></h4>
                                    </div>
                                </a>
                                <?php /*<a href="<?php echo $value['base_url']; ?>" class="discover-btn"><?= get_line_front('discover'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>*/ ?>
                            </div>
                        </div>
                <?php }
                }
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Clothes Section End Here -->
<?php $this->load->view('include/copyright'); ?>
<?php $this->load->view('include/footer'); ?>