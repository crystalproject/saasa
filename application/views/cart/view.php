<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
?>
<div class="section banner-page" data-background="<?= base_url() ?>assets/images/statistic_bg.jpg">
    <div class="content-wrap pos-relative">
        <div class="d-flex justify-content-center bd-highlight mb-2">
            <div class="title-page">Cart</div>
        </div>
        <!-- <p class="text-center text-white">Your wishlisht products.</p> -->
    </div>
</div>
<!-- Cart Start -->
<section class="cart">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="cart-table">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                    </th>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if (isset($data) && !empty($data)) {
                                        foreach ($data as $value) { ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>">
                                                        <?php
                                                            if (isset($value['customized_image']) && !empty($value['customized_image'])) {
                                                            ?>
                                                                <img src="<?php echo $value['customized_image']; ?>" />
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <img src="<?php echo $value['main_image']; ?>" />
                                                            <?php
                                                            }
                                                        ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="theme-link" href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>"><?php echo $value['title']; ?></a>
                                                    
                                                </td>
                                                <td>
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                            <span class="fa fa-minus"></span>
                                                        </button>
                                                    </span>
                                                    <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                                                            <span class="fa fa-plus"></span>
                                                        </button>
                                                    </span>
                                                </td>
                                                <td>
                                                    <i class="fa fa-rupee"></i> 1790
                                                </td>
                                                <td>
                                                    <button class="btn btn-delete">
                                                        <i class="fa fa-trash-o remove-from-cart"  data-id="<?php echo $value['id']; ?>" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php 
                                        }
                                    }else{ ?>
                                        <tr><td colspan="5" align="center"><h3>Cart is empty</h3></td></tr>
                                        <?php
                                    }
                                ?>
                                <!-- <tr>
                                    <td>
                                        <a href="#">
                                            <img src="<?= base_url() ?>assets/images/shop-img-9.jpg" alt="images">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#">
                                            Lorem Ipsum Simple dummy text
                                        </a>
                                    </td>
                                    <td>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                        </span>
                                        <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </span>
                                    </td>
                                    <td>
                                        <i class="fa fa-rupee"></i> 1790
                                    </td>
                                    <td>
                                        <button class="btn btn-delete">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">
                                            <img src="<?= base_url() ?>assets/images/shop-img-9.jpg" alt="images">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#">
                                            Lorem Ipsum Simple dummy text
                                        </a>
                                    </td>
                                    <td>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[2]">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                        </span>
                                        <input type="text" name="quant[2]" class="form-control input-number" value="1" min="1">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[2]">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </span>
                                    </td>
                                    <td>
                                        <i class="fa fa-rupee"></i> 1790
                                    </td>
                                    <td>
                                        <button class="btn btn-delete">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">
                                            <img src="<?= base_url() ?>assets/images/shop-img-9.jpg" alt="images">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#">
                                            Lorem Ipsum Simple dummy text
                                        </a>
                                    </td>
                                    <td>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[3]">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                        </span>
                                        <input type="text" name="quant[3]" class="form-control input-number" value="1" min="1">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[3]">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </span>
                                    </td>
                                    <td>
                                        <i class="fa fa-rupee"></i> 1790
                                    </td>
                                    <td>
                                        <button class="btn btn-delete">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr> -->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <a href="#" class="btn btn-continue">
                                            Continue Shopping
                                        </a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="checkout-des">
                    <p class="checkout-head">
                        Product Details
                    </p>
                    <?php
                        if (get_cart_total(get_front_login_id()) > 0) {
                        ?>
                        <ul class="sub-total">
                            <li>
                                Cart sub total:
                            </li>
                            <li>
                                <i class="fa fa-rupee"></i> 5350
                            </li>
                        </ul>
                        <ul>
                            <li>
                                Amount Payable
                            </li>
                            <li>
                                <i class="fa fa-rupee"></i> <?php echo get_cart_total(get_front_login_id()); ?>
                            </li>
                        </ul>
                        <?php }
                    ?>
                </div>
            </div>
            
        </div>
    </div>
</section>
<!-- Cart End -->

<!-- <section class="mens-clothes-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-lg-9 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <div class="porto-u-main-heading cart">
                            <h2 style="text-transform: capitalize;font-size: 35px;">Cart</h2>
                        </div>
                    </div>
                </div>
                <div class="cart-content">
                    <?php
                    if (isset($data) && !empty($data)) {
                        foreach ($data as $value) {
                    ?>
                            <ul>
                                <li class="cancel-btn">
                                    <div class="cancel ">
                                        <i class="fa fa-times remove-from-cart" data-id="<?php echo $value['id']; ?>" aria-hidden="true"></i>
                                    </div>
                                </li>
                                <li class="cart-product">
                                    <div class="cart-product-img">
                                        <a href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>">
                                            <?php
                                            if (isset($value['customized_image']) && !empty($value['customized_image'])) {
                                            ?>
                                                <img src="<?php echo $value['customized_image']; ?>" />
                                            <?php
                                            } else {
                                            ?>
                                                <img src="<?php echo $value['main_image']; ?>" />
                                            <?php
                                            }
                                            ?>
                                        </a>
                                    </div>
                                </li>
                                <li class="cart-product-content">
                                    <div class="cart-product-title">
                                        <h3><b><a class="theme-link" href="<?php echo base_url('product/details/' . $value['slug'] . '/' . $value['id_unique']); ?>"><?php echo $value['title']; ?></a></b></h3>
                                    </div>
                                    <?php
                                    if (isset($value['other_info']['attribute_info']) && !empty($value['other_info']['attribute_info'])) {
                                        echo '<dl class="variation">';
                                        foreach ($value['other_info']['attribute_info'] as $a_value) {
                                    ?>
                                            <dt class="variation-Color"><?php echo $a_value['attribute_name'] ?>:</dt>
                                            <dd class="variation-Color">
                                                <p><?php
                                                    if (isset($a_value['attribute_values'][0]['image']) && !empty($a_value['attribute_values'][0]['image'])) {
                                                        echo '<img src="' . $a_value['attribute_values'][0]['image'] . '" style="max-width:50px;max-height:25px;"/>';
                                                    } else if (isset($a_value['attribute_values'][0]['attribute_value']) && !empty($a_value['attribute_values'][0]['attribute_value'])) {
                                                        echo $a_value['attribute_values'][0]['attribute_value'];
                                                    }
                                                    ?></p>
                                            </dd>
                                    <?php
                                        }
                                        echo '</dl>';
                                    }
                                    ?>
                                    <div class="table-reponsive">
                                        <table>
                                            <tr>
                                                <td class="product-price" style="text-align: left !important; padding: 0 10px;">
                                                    <label style="color: #aaa;margin-bottom: 0px;"><?php echo get_line('price'); ?></label><br>
                                                    <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY;  ?></span><?php echo isset($value['other_info']['final_price'][0]) ? $value['other_info']['final_price'][0] : 0; ?></span> </td>
                                                <td class="product-quantity" style="text-align: left !important; padding: 0 10px;">
                                                    <label style="color: #aaa;margin-bottom: 0px;">&nbsp;</label><br>
                                                    <div class="quantity">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default btn-number" data-type="minus" data-field="quant[<?php echo $value['id']; ?>]">
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                            </span>
                                                            <input type="text" name="quant[<?php echo $value['id']; ?>]" class="form-control input-number quantity-input " readonly="readonly" data-id="<?php echo $value['id']; ?>" value="<?php echo $value['quantity']; ?>" min="1" max="11000">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[<?php echo $value['id']; ?>]">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="product-subtotal" style="padding: 0 10px;text-align: center;" data-title="Subtotal">
                                                    <label style="color: #aaa;margin-bottom: 0px;">Price</label><br>
                                                    <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY;  ?></span><?php echo isset($value['other_info']['final_price'][0]) ? ($value['other_info']['final_price'][0] * $value['quantity']) : 0; ?></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </li>

                            </ul>
                        <?php
                        }
                        ?>
                        <div class="cart_totals_toggle">
                            <div class="card card-default">
                                <div class="card-header arrow">
                                    <h2 class="card-title"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#panel-cart-discount"><?php echo get_line('discount_code'); ?></a></h2>
                                </div>
                                <div id="panel-cart-discount" class="accordion-body collapse">
                                    <div class="card-body">
                                        <div class="coupon">
                                            <label for="coupon_code"><?php echo get_line('apply_coupon_desc'); ?></label><br>
                                            <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" />
                                            <button type="submit" class="btn btn-primary" name="apply_coupon" style="background-color: #000;"><?php echo get_line('apply_coupon'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    <?php
                        $available = true;
                    } else {
                        $available = false;
                        echo '<h3>' . get_line('your_cart_is_empty') . '</h3>';
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <div class="continue-pay-btn">
                                <a href="<?php echo base_url(); ?>" class="continue-pay">Continue Shopping </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-12">
                <div class="cart_totals calculated_shipping">
                    <div class="cart_totals_toggle">
                        <?php
                        if (get_cart_total(get_front_login_id()) > 0) {
                        ?>
                            <?php /*<div class="card card-default woocommerce-shipping-totals shipping" style="margin-top: 16px;">
                                <div class="card-header arrow">
                                    <h2 class="card-title" style="text-align: left;"><a class="accordion-toggle collapsed out" data-toggle="collapse" href="#panel-cart-shipping" aria-expanded="false">ESTIMATE SHIPPING AND TAX</a></h2>
                                </div>
                                <div id="panel-cart-shipping" class="accordion-body collapse">
                                    <div class="card-body">
                                        <ul id="shipping_method" class="woocommerce-shipping-methods">
                                            <li>
                                                <input type="hidden" name="shipping_method[0]" data-index="0" id="shipping_method_0_flat_rate2" value="flat_rate:2" class="shipping_method" /><label for="shipping_method_0_flat_rate2">Flat rate: <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY;  ?></span>5.00</span></label> </li>
                                        </ul>
                                        <p class="woocommerce-shipping-destination">
                                            Shipping to <strong>test, address 2, rajkot 360001, Gujarat, India</strong>. </p>
                                    </div>
                                </div>
                            </div> */ ?>

                            <div class="card card-default" style="margin-top: 16px;">
                                <div class="card-header arrow">
                                    <h2 class="card-title" style="text-align: left;"><a class="accordion-toggle" data-toggle="collapse" href="#panel-cart-total"><?php echo get_line('cart_total'); ?></a></h2>
                                </div>
                                <div id="panel-cart-total" class="accordion-body collapse show">
                                    <div class="card-body">
                                        <table class="responsive cart-total" cellspacing="0">
                                            <tr class="cart-subtotal">
                                                <th><?php echo get_line('subtotal'); ?></th>
                                                <td><span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY;  ?></span><span class="woocommerce-Price-amount amount"><?php echo get_cart_total(get_front_login_id()); ?></span></td>
                                            </tr>
                                            <tr class="tax-total">
                                                <th><?php echo get_line('discount'); ?></th>
                                                <td data-title="VAT"><span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY;  ?></span><span class="woocommerce-Price-amount amount" data-discount="true">0.00</span></td>
                                            </tr>
                                            <tr class="tax-total">
                                                <th><?php echo get_line('VAT'); ?></th>
                                                <td data-title="VAT"><span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY;  ?></span><span class="woocommerce-Price-amount amount"  data-vat="true">0.00</span></td>
                                            </tr>
                                            <tr class="order-total">
                                                <th><?php echo get_line('grand_total'); ?></th>
                                                <td><strong><span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY;  ?></span><span class="woocommerce-Price-amount amount" data-cart-total="true"><?php echo get_cart_total(get_front_login_id()); ?></span></strong> </td>
                                            </tr>
                                        </table>
                                        <div class="wc-proceed-to-checkout">
                                            <a href="<?php echo base_url('cart/checkout'); ?>" class="btn btn-primary btn-block"><?php echo get_line('proceed_to_checkout'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</section>
<?php
$footer_js = array();
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    //plugin bootstrap minus and plus
    //http://jsfiddle.net/laelitenetwork/puJ6G/
    $('.btn-number').click(function(e) {
        var current = $(this);
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }
            } else if (type == 'plus') {
                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }
            }
        } else {
            input.val(0);
        }
        current.closest('.input-group').find('.quantity-input').trigger('focusout');
    });
    $('.input-number').focusin(function() {
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        $('.quantity-input').trigger('focusout');
    });
</script>
<?php
$this->load->view('include/footer'); ?>