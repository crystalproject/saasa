<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Coming_Soon';
//$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

$route['authority'] = 'authority/dashboard';
$route['authority'] = 'authority/login';
$route['authority/login'] = 'authority/login';
$route['authority/login/forgot-password'] = 'authority/login/forgot_password';
/* ACCOUNT */
$route['authority/account/change-password'] = 'authority/account/changepassword';

$route['sub-category/(:num)'] = 'sub_category/index/$1';

/*$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13/$14/$15';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13/$14';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8/$9';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7/$8';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6/$7';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5/$6';
$route['product/details/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4/$5';
$route['product/details/(:any)/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3/$4';
$route['product/details/(:any)/(:any)/(:any)'] = 'product/details/index/$1/$2/$3';
$route['product/details/(:any)/(:any)'] = 'product/details/index/$1/$2';
$route['product/details/(:any)'] = 'product/details/index/$1';

$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13/$14/$15';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13/$14';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8/$9';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7/$8';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6/$7';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5/$6';
$route['product/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4/$5';
$route['product/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3/$4';
$route['product/(:any)/(:any)/(:any)'] = 'product/index/$1/$2/$3';
$route['product/(:any)/(:any)'] = 'product/index/$1/$2';
$route['product/(:any)'] = 'product/index/$1';

$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13/$14/$15';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13/$14';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8/$9';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7/$8';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6/$7';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5/$6';
$route['category/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4/$5';
$route['category/(:any)/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3/$4';
$route['category/(:any)/(:any)/(:any)'] = 'category/index/$1/$2/$3';
$route['category/(:any)/(:any)'] = 'category/index/$1/$2';
$route['category/(:any)'] = 'category/index/$1';*/
