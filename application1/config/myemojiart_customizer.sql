-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 09, 2020 at 06:04 PM
-- Server version: 5.7.29-cll-lve
-- PHP Version: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myemojiart_customizer`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `mobile_number_1` varchar(15) NOT NULL,
  `mobile_number_2` varchar(15) NOT NULL,
  `address` mediumtext NOT NULL,
  `gender` enum('Male','Female','Transgender') NOT NULL,
  `role` enum('Admin','Subadmin') NOT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `is_enable` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `full_name`, `username`, `email_address`, `mobile_number_1`, `mobile_number_2`, `address`, `gender`, `role`, `profile_photo`, `salt`, `password`, `is_enable`, `created_at`, `updated_at`) VALUES
(1, 'Customizer', 'Customizer', 'customizer@gmail.com', '1234567890', '', 'Address', 'Male', 'Admin', '37e3b0cc7581b428d9579a4b4aa4ca59.png', 'e94049558d21fabbd8124d874355b1ad', 'eb0bc03b321ab5343591e63489b5e475218b531a', '1', '2019-08-29 11:03:49', '2020-05-03 03:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) NOT NULL,
  `id_unique` varchar(15) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `attribute_name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active, 0-Inactive',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `id_unique`, `id_language`, `attribute_name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '5ea6744289b00', 1, 'COLOR', 1, '2020-04-27 11:27:22', '2020-04-27 11:27:22'),
(2, '5ea6744289b00', 2, 'COLOR A', 1, '2020-04-27 11:27:22', '2020-04-27 11:27:22'),
(5, '5eabb3f6c12ce', 1, 'Ram', 1, '2020-05-01 11:00:30', '2020-05-06 11:13:05'),
(6, '5eabb3f6c12ce', 2, 'Ram', 1, '2020-05-01 11:00:30', '2020-05-06 11:13:05'),
(7, '5eb2a7134e9c5', 1, 'size', 1, '2020-05-06 17:31:23', '2020-05-06 17:31:23'),
(8, '5eb2a7134e9c5', 2, 'size-ar', 1, '2020-05-06 17:31:23', '2020-05-06 17:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` bigint(20) NOT NULL,
  `id_unique_attributes` varchar(25) NOT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `attribute_value` varchar(100) NOT NULL,
  `image` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0-InActive, 1-Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `id_unique_attributes`, `id_unique`, `id_language`, `attribute_value`, `image`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '5ea6744289b00', '5ea6744ce3ad6', 1, 'RED', '1f162e058a8cb1e78590191087938521.png', 1, '2020-04-27 11:27:32', '2020-05-06 11:50:51'),
(2, '5ea6744289b00', '5ea6744ce3ad6', 2, 'RED A', '1f162e058a8cb1e78590191087938521.png', 1, '2020-04-27 11:27:32', '2020-05-06 11:50:51'),
(3, '5ea6744289b00', '5ea67456a6e76', 1, 'BLACK', '00501b3ccfe5a02489e8d73e9aea0847.png', 1, '2020-04-27 11:27:42', '2020-05-08 13:16:00'),
(4, '5ea6744289b00', '5ea67456a6e76', 2, 'BLACK A', '00501b3ccfe5a02489e8d73e9aea0847.png', 1, '2020-04-27 11:27:42', '2020-05-08 13:16:00'),
(5, '5ea6744289b00', '5ea7c4e3696b9', 1, 'BROWN', '47e1207ba89b9dc066098e3ab5bcbcb5.png', 1, '2020-04-28 11:23:39', '2020-05-08 13:20:21'),
(6, '5ea6744289b00', '5ea7c4e3696b9', 2, 'BROWN', '47e1207ba89b9dc066098e3ab5bcbcb5.png', 1, '2020-04-28 11:23:39', '2020-05-08 13:20:21'),
(15, '5eabb3f6c12ce', '5eae3f1a54877', 1, '1 GB', '', 1, '2020-05-03 09:18:42', '2020-05-03 09:18:42'),
(16, '5eabb3f6c12ce', '5eae3f1a54877', 2, '1 GB', '', 1, '2020-05-03 09:18:42', '2020-05-03 09:18:42'),
(17, '5eabb3f6c12ce', '5eae3f3ec0f28', 1, '2 GB', '', 1, '2020-05-03 09:19:18', '2020-05-03 09:19:18'),
(18, '5eabb3f6c12ce', '5eae3f3ec0f28', 2, '2 GB', '', 1, '2020-05-03 09:19:18', '2020-05-03 09:19:18'),
(19, '5eb2a7134e9c5', '5eb2a7314ec2c', 1, 'XS', '', 1, '2020-05-06 17:31:53', '2020-05-06 17:31:53'),
(20, '5eb2a7134e9c5', '5eb2a7314ec2c', 2, 'XS-ar', '', 1, '2020-05-06 17:31:53', '2020-05-06 17:31:53'),
(21, '5eb2a7134e9c5', '5eb2a7480e259', 1, 'XL', '', 1, '2020-05-06 17:32:16', '2020-05-06 17:32:16'),
(22, '5eb2a7134e9c5', '5eb2a7480e259', 2, 'XL-ar', '', 1, '2020-05-06 17:32:16', '2020-05-06 17:32:16'),
(23, '5eb2a7134e9c5', '5eb2a75763a7a', 1, 'XXL', '', 1, '2020-05-06 17:32:31', '2020-05-06 17:32:31'),
(24, '5eb2a7134e9c5', '5eb2a75763a7a', 2, 'XXL-ar', '', 1, '2020-05-06 17:32:31', '2020-05-06 17:32:31'),
(25, '5eb2a7134e9c5', '5eb2a769cc491', 1, 'XXXL', '', 1, '2020-05-06 17:32:49', '2020-05-06 17:32:49'),
(26, '5eb2a7134e9c5', '5eb2a769cc491', 2, 'XXXL', '', 1, '2020-05-06 17:32:49', '2020-05-06 17:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `bag`
--

CREATE TABLE `bag` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_turkish` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `image` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `id_unique_product_attributes` varchar(50) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `id_user`, `id_unique_product`, `id_unique_product_attributes`, `quantity`, `created_at`, `updated_at`) VALUES
(3, 1, '5eae9860baa55', NULL, 1, '2020-05-03 17:50:52', '2020-05-03 17:50:52'),
(7, 1, '5eae5a40b8a97', '5eb16353b9c50', 1, '2020-05-05 18:31:44', '2020-05-05 18:31:44'),
(23, 768487, '5eae59914b117', '5eb29f983c3f7', 1, '2020-05-09 08:57:52', '2020-05-09 08:57:52'),
(25, 6, '5eae9860baa55', NULL, 1, '2020-05-09 17:22:30', '2020-05-09 17:22:30');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `name_arabic` varchar(50) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `slug_arabic` varchar(255) NOT NULL,
  `image` varchar(50) NOT NULL,
  `image_arabic` varchar(50) NOT NULL,
  `banner` varchar(50) DEFAULT NULL,
  `banner_arabic` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `name_arabic`, `slug`, `slug_arabic`, `image`, `image_arabic`, `banner`, `banner_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 0, 'Mobile', 'Mobile Section', 'mobile-section', 'يرجى-تحديد-صورة-صالحة', '85d63c0b7cbfdb704d069f32cfb69885.png', '454307fb2295433f5af71234abe7a6c9.png', '39045f430022b35b3d29840436e4ab2c.jpg', '023dfc4da5066ba8ef796ae96822fa04.png', '2020-04-27 08:49:32', '2020-05-03 16:09:21', 1),
(8, 0, 'Men clothes', 'يرجى تحديد صورة صالحة', 'mmen-clothes', 'يرجى-تحديد-صورة-صالحة', 'e1906e13247418788ca7eee7e3914615.png', 'aa0f7af2566592c51a571b09ccfb6459.png', '59301f90f9a3623f04cf79156cd07628.png', '9201e8ee3282aceb28c8dd1fbccc1cff.png', '2020-04-28 13:01:58', '2020-05-03 16:20:37', 1),
(11, 8, 'shirt', 'shirt', 'shirt', 'صالحة-1', '6ac9b13d54a012e7c6c9ff08e7e863dd.png', '9de1b1c2b089f6943611433b740aff1f.png', 'f3e5e69b8caa2bd360212c37974f5d22.png', '6eabd382dc0b9bfd34d2c5e82bb2eacc.png', '2020-04-29 11:38:49', '2020-05-03 16:10:26', 1),
(18, 1, 'Mi', 'Mi', 'mi', 'mi', 'a6cc97e09dc0a2759c01429eccbfe6ae.jpg', '38418ade14fa08717f31bc528cbcfdc3.jpg', '13007290370961db8efa0fb36c79e2d7.png', 'aa481c672453ec13f16137171faa983e.png', '2020-05-03 06:50:18', '2020-05-06 14:32:01', 1),
(19, 18, 'A Series', 'A Series', 'a-series', 'a-series', '038798f595106371c476448f85e85f54.jpg', '08cf6ea31f8090d0ff551a43782a90fd.jpg', '2fc29aefaa27aa639d802988f90d2ce3.png', '6a25d17f1cf05db787bbe38962ba3931.png', '2020-05-03 06:51:03', '2020-05-06 14:37:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_banner`
--

CREATE TABLE `category_banner` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_arabic` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `image` mediumtext NOT NULL,
  `banner_for` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Category, 1- Sub Category, 3- Product',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `category_description`
--

CREATE TABLE `category_description` (
  `id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_description`
--

INSERT INTO `category_description` (`id`, `id_category`, `id_language`, `description`) VALUES
(1, 1, 1, ''),
(2, 1, 2, ''),
(3, 2, 1, ''),
(4, 2, 2, ''),
(5, 3, 1, 'shirt description'),
(6, 3, 2, 'shirt description'),
(7, 4, 1, ''),
(8, 4, 2, ''),
(9, 5, 1, ''),
(10, 5, 2, ''),
(11, 6, 1, ''),
(12, 6, 2, ''),
(13, 7, 1, ''),
(14, 7, 2, ''),
(15, 8, 1, ''),
(16, 8, 2, ''),
(17, 9, 1, ''),
(18, 9, 2, ''),
(19, 18, 1, ''),
(20, 18, 2, ''),
(21, 19, 1, ''),
(22, 19, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `subject`, `message`, `created_at`) VALUES
(12, 'test', 'test@gmail.com', 'test subject', 'test msg', '2020-04-29 08:31:09');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) NOT NULL,
  `coupon_code` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `discount_type` varchar(10) NOT NULL,
  `discount` float NOT NULL COMMENT 'ruppes or percentage',
  `minimum_amount_cart_status` varchar(5) NOT NULL,
  `minimum_amount_cart` float NOT NULL,
  `maximum_coupon_used` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_arabic` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `title`, `title_arabic`, `description`, `description_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(2, 'faq 2', 'faq  2 arabic', 'test description', 'description  arabic', '2020-04-27 14:57:23', '2020-04-29 16:25:21', 1),
(4, 'faq 1', 'faq 1', 'description', 'description', '2020-04-28 07:52:09', '2020-04-29 16:25:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `happy_clients`
--

CREATE TABLE `happy_clients` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(50) NOT NULL,
  `name_arabic` varchar(100) NOT NULL,
  `designation_arabic` varchar(100) NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `image_arabic` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `happy_clients`
--

INSERT INTO `happy_clients` (`id`, `name`, `designation`, `description`, `image`, `name_arabic`, `designation_arabic`, `description_arabic`, `image_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'John Smith', 'sdfsdf', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mipsum dolor sit amet, consectetur elitad adipiscing cas non placerat mi.', '59fd6b8e27083605e7e8a77702b99b4d.png', 'John Smith', 'fgfdg', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mipsum dolor sit amet, consectetur elitad adipiscing cas non placerat mi.', '51165c7c150fb5bef8ee5ccdb8d1be91.png', '2020-04-29 09:35:23', '2020-04-29 10:27:45', 1),
(2, 'John Smith', 'SMARTWAVE CEO', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mipsum dolor sit amet, consectetur elitad adipiscing cas non placerat mi.', '5ed757aab2f62ceb0dd612d1e36d48a5.png', 'John Smith', 'SMARTWAVE seo', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mipsum dolor sit amet, consectetur elitad adipiscing cas non placerat mi.', '512005b75bf47553dc965536d2a4df64.png', '2020-04-29 09:58:49', '2020-04-29 10:29:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_arabic` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `image` mediumtext NOT NULL,
  `image_arabic` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `link_arabic` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`id`, `title`, `title_arabic`, `description`, `description_arabic`, `image`, `image_arabic`, `link`, `link_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(2, 'slider 1', 'slider 1', 'dsfdfgdfgdfg', 'description  arabic', '6f346a7d1ea404506e6d271166fc4f15.jpg', '74f7dc598b7c26d785db9e6c4645b14d.jpg', '', NULL, '2020-04-27 14:57:23', '2020-04-28 12:22:02', 1),
(3, 'test1', 'test11', 'slider 2 description', 'slider 2 description', '0d7820069d9c60561af0e749cdbf12fc.jpg', 'd45b2563135e838d058b27480cf6d90f.png', '', NULL, '2020-04-27 15:04:42', '2020-04-28 12:21:51', 1),
(4, 'create your own design', 'إنشاء التصميم الخاص بك', '', '', '0acca4040e7362a273458be608c49839.png', 'ed7c0682c2dffc4ca67a66934a889bd2.jpg', 'https://google.com', NULL, '2020-04-28 10:47:22', '2020-04-28 16:03:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `default_language` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `slug`, `default_language`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'English', 'english', 1, '2020-02-04 12:51:36', '2020-05-08 13:35:48', 1),
(2, 'Arabic', 'arabic', 0, '2020-02-04 17:35:20', '2020-05-08 13:35:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `name_arabic` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `name_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(2, 'About us', 'معلومات عنا', '2020-04-27 14:57:23', '2020-05-02 08:40:24', 1),
(3, 'Home', 'الصفحة الرئيسية', '2020-04-27 15:04:42', '2020-05-02 08:40:01', 1),
(4, 'Shop', 'متجر', '2020-04-28 10:47:22', '2020-05-02 08:39:34', 1),
(5, 'Contact us', 'اتصل بنا', '2020-05-01 14:49:01', '2020-05-02 08:39:10', 1),
(6, 'Return policy', 'سياسة العائدات', '2020-05-01 14:49:22', '2020-05-02 08:38:25', 1),
(7, 'Website', 'موقع الكتروني', '2020-05-01 14:49:39', '2020-05-02 08:37:56', 1),
(8, 'Privacy policy', 'سياسة الخصوصية', '2020-05-01 14:49:53', '2020-05-02 08:37:25', 1),
(9, 'Return policy', 'سياسة العائدات', '2020-05-01 14:50:07', '2020-05-02 08:23:04', 1),
(10, 'Terms and condition', 'أحكام وشروط', '2020-05-01 14:50:31', '2020-05-02 08:22:41', 1),
(11, 'Categories', 'التصنيفات', '2020-05-01 14:50:47', '2020-05-02 08:22:09', 1),
(12, 'Accessories', 'مستلزمات', '2020-05-01 14:50:58', '2020-05-02 08:21:43', 1),
(13, 'Devices', 'الأجهزة', '2020-05-01 14:51:09', '2020-05-02 08:21:19', 1),
(14, 'Decor', 'ديكور', '2020-05-01 14:51:24', '2020-05-02 08:20:10', 1),
(15, 'Stationary', 'ثابت', '2020-05-01 14:51:32', '2020-05-02 08:19:53', 1),
(16, 'Other', 'آخر', '2020-05-01 14:51:38', '2020-05-02 08:19:30', 1),
(17, 'Faq', 'التعليمات', '2020-05-01 14:51:45', '2020-05-02 08:19:12', 1),
(18, 'Site Map', 'خريطة الموقع', '2020-05-01 14:51:53', '2020-05-02 08:18:54', 1),
(19, 'Registration', 'التسجيل', '2020-05-01 14:52:01', '2020-05-02 08:18:31', 1),
(20, 'Sign In', 'تسجيل الدخول', '2020-05-01 14:52:09', '2020-05-02 08:18:08', 1),
(21, 'Sign Up', 'سجل', '2020-05-01 14:52:16', '2020-05-02 08:17:47', 1),
(22, 'My Account', 'حسابي', '2020-05-01 14:52:23', '2020-05-02 08:17:15', 1),
(23, 'Clothes', 'ملابس', '2020-05-01 15:18:06', '2020-05-02 08:14:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_turkish` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_turkish` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification_status`
--

CREATE TABLE `notification_status` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 - read, 0 - not-read',
  `remove_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 - remove, 0 - not-remove'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification_token`
--

CREATE TABLE `notification_token` (
  `id` int(11) NOT NULL,
  `token` mediumtext NOT NULL,
  `device_id` mediumtext NOT NULL,
  `device_type` varchar(7) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payment_mode`
--

CREATE TABLE `payment_mode` (
  `id` int(11) NOT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-Active, 0-Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_mode`
--

INSERT INTO `payment_mode` (`id`, `id_unique`, `id_language`, `payment_mode`, `is_active`) VALUES
(1, '1', 1, 'Credit Card', 1),
(2, '1', 2, 'Kredi kartı', 1),
(3, '2', 1, 'Credit Card on Delivery', 0),
(4, '2', 2, 'Teslimatta Kredi Kartı', 0),
(5, '3', 1, 'Cash On Delivery', 0),
(6, '3', 2, 'Kapıda ödeme', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `id_language` bigint(20) NOT NULL,
  `title` varchar(500) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `long_description` text NOT NULL,
  `additional_description` text NOT NULL,
  `has_attributes` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No,1-Yes',
  `id_attributes` varchar(500) NOT NULL,
  `by_quantity_or_availability` int(1) NOT NULL DEFAULT '0' COMMENT '1-Quantity, 0-Availability',
  `is_popular` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No, 1-Yes',
  `product_video` varchar(50) DEFAULT NULL,
  `youtube_link` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active,0-Inactive',
  `main_image` varchar(50) DEFAULT NULL,
  `sku` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `id_unique`, `id_category`, `id_language`, `title`, `slug`, `description`, `long_description`, `additional_description`, `has_attributes`, `id_attributes`, `by_quantity_or_availability`, `is_popular`, `product_video`, `youtube_link`, `is_active`, `main_image`, `sku`, `created_at`, `updated_at`) VALUES
(43, '5eae59914b117', 19, 1, 'test', 'test', '<p>test</p>', '', '', 1, '5ea6744289b00,5eabb3f6c12ce', 1, 0, '', '', 1, NULL, '', '2020-05-03 11:11:37', '2020-05-06 16:59:28'),
(44, '5eae59914b117', 19, 2, 'test', 'test', '<p>test</p>', '', '', 1, '5ea6744289b00,5eabb3f6c12ce', 1, 0, '', '', 1, NULL, '', '2020-05-03 11:11:37', '2020-05-06 16:59:28'),
(45, '5eae5a40b8a97', 19, 1, 'Second Mobile', 'second-mobile', '<p>Second Mobile<br></p></br>', '', '', 1, '5ea6744289b00,5eabb3f6c12ce', 1, 0, '', '', 1, NULL, '', '2020-05-03 11:14:32', '2020-05-06 16:40:51'),
(46, '5eae5a40b8a97', 19, 2, 'Second Mobile', 'second-mobile', '<p>Second Mobile<br></p></br>', '', '', 1, '5ea6744289b00,5eabb3f6c12ce', 1, 0, '', '', 1, NULL, '', '2020-05-03 11:14:32', '2020-05-06 16:40:51'),
(49, '5eae9860baa55', 19, 1, 'Without attribute quantity', 'without-attribute-quantity', '<p>Without attribute quantity<br></p></br>', '<p>Without attribute quantity<br></p></br>', '<p>Without attribute quantity<br></p></br>', 0, '', 1, 0, '', '', 1, NULL, '', '2020-05-03 15:39:36', '2020-05-06 16:50:53'),
(50, '5eae9860baa55', 19, 2, 'Without attribute quantity', 'without-attribute-quantity', '<p>Without attribute quantity<br></p></br>', '', '', 0, '', 1, 0, '', '', 1, NULL, '', '2020-05-03 15:39:36', '2020-05-06 16:50:53'),
(51, '5eb4d2fdd6b71', 19, 1, 'Small brand tiger leather bags', 'small-brand-tiger-leather-bags', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p></br>', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p></br>', 1, '5ea6744289b00,5eabb3f6c12ce', 0, 0, 'e78655906a55281c175e3eec417539a9.mp4', NULL, 1, '66f961e917978f89b1e7ef708fdfa784.png', '123456', '2020-05-08 09:03:17', '2020-05-09 10:43:35'),
(52, '5eb4d2fdd6b71', 19, 2, 'Small brand tiger leather bags', 'small-brand-tiger-leather-bags', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p></br>', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p></br>', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p></br>', 1, '5ea6744289b00,5eabb3f6c12ce', 0, 0, 'e78655906a55281c175e3eec417539a9.mp4', NULL, 1, '66f961e917978f89b1e7ef708fdfa784.png', '123456', '2020-05-08 09:03:17', '2020-05-09 10:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) DEFAULT NULL,
  `id_unique` varchar(50) NOT NULL,
  `id_unique_attributes` varchar(50) NOT NULL,
  `id_unique_attribute_values` varchar(50) NOT NULL,
  `is_ordered` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `id_unique_product`, `id_unique`, `id_unique_attributes`, `id_unique_attribute_values`, `is_ordered`) VALUES
(286, NULL, '5eb29b3b56c53', '5ea6744289b00', '5ea6744ce3ad6', 1),
(287, NULL, '5eb29b3b56c53', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(288, NULL, '5eb29b3b56e35', '5ea6744289b00', '5ea6744ce3ad6', 1),
(289, NULL, '5eb29b3b56e35', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(290, NULL, '5eb29b3b56f9f', '5ea6744289b00', '5ea67456a6e76', 1),
(291, NULL, '5eb29b3b56f9f', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(292, NULL, '5eb29b3b57133', '5ea6744289b00', '5ea67456a6e76', 1),
(293, NULL, '5eb29b3b57133', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(294, NULL, '5eb29b3b5728f', '5ea6744289b00', '5ea7c4e3696b9', 1),
(295, NULL, '5eb29b3b5728f', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(296, NULL, '5eb29b3b573eb', '5ea6744289b00', '5ea7c4e3696b9', 1),
(297, NULL, '5eb29b3b573eb', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(322, NULL, '5eb29f983bf5a', '5ea6744289b00', '5ea6744ce3ad6', 1),
(323, NULL, '5eb29f983bf5a', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(324, NULL, '5eb29f983c10a', '5ea6744289b00', '5ea6744ce3ad6', 1),
(325, NULL, '5eb29f983c10a', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(326, NULL, '5eb29f983c27e', '5ea6744289b00', '5ea67456a6e76', 1),
(327, NULL, '5eb29f983c27e', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(328, NULL, '5eb29f983c3f7', '5ea6744289b00', '5ea67456a6e76', 1),
(329, NULL, '5eb29f983c3f7', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(330, NULL, '5eb29f983c564', '5ea6744289b00', '5ea7c4e3696b9', 1),
(331, NULL, '5eb29f983c564', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(332, NULL, '5eb29f983c6b1', '5ea6744289b00', '5ea7c4e3696b9', 1),
(333, NULL, '5eb29f983c6b1', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(334, NULL, '5eb4d2fdee625', '5ea6744289b00', '5ea6744ce3ad6', 1),
(335, NULL, '5eb4d2fdee625', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(336, NULL, '5eb4d2fdeeac1', '5ea6744289b00', '5ea6744ce3ad6', 1),
(337, NULL, '5eb4d2fdeeac1', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(338, NULL, '5eb4d2fdeed2b', '5ea6744289b00', '5ea67456a6e76', 1),
(339, NULL, '5eb4d2fdeed2b', '5eabb3f6c12ce', '5eae3f1a54877', 1),
(340, NULL, '5eb4d2fdeefab', '5ea6744289b00', '5ea67456a6e76', 1),
(341, NULL, '5eb4d2fdeefab', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(342, NULL, '5eb4d2fdef209', '5ea6744289b00', '5ea7c4e3696b9', 1),
(343, NULL, '5eb4d2fdef209', '5eabb3f6c12ce', '5eae3f3ec0f28', 1),
(344, '5eb4d2fdd6b71', '5eb63bff953a7', '5ea6744289b00', '5ea6744ce3ad6', 0),
(345, '5eb4d2fdd6b71', '5eb63bff953a7', '5eabb3f6c12ce', '5eae3f1a54877', 0),
(346, '5eb4d2fdd6b71', '5eb63bffa5c96', '5ea6744289b00', '5ea6744ce3ad6', 0),
(347, '5eb4d2fdd6b71', '5eb63bffa5c96', '5eabb3f6c12ce', '5eae3f3ec0f28', 0),
(348, '5eb4d2fdd6b71', '5eb63bffa5e20', '5ea6744289b00', '5ea67456a6e76', 0),
(349, '5eb4d2fdd6b71', '5eb63bffa5e20', '5eabb3f6c12ce', '5eae3f1a54877', 0),
(350, '5eb4d2fdd6b71', '5eb63bffa5f98', '5ea6744289b00', '5ea67456a6e76', 0),
(351, '5eb4d2fdd6b71', '5eb63bffa5f98', '5eabb3f6c12ce', '5eae3f3ec0f28', 0),
(352, '5eb4d2fdd6b71', '5eb63bffa60f6', '5ea6744289b00', '5ea7c4e3696b9', 0),
(353, '5eb4d2fdd6b71', '5eb63bffa60f6', '5eabb3f6c12ce', '5eae3f3ec0f28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes_image`
--

CREATE TABLE `product_attributes_image` (
  `id` int(11) NOT NULL,
  `id_unique_product_attributes` varchar(25) NOT NULL,
  `image_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_attributes_image`
--

INSERT INTO `product_attributes_image` (`id`, `id_unique_product_attributes`, `image_name`) VALUES
(1, '5eb23ff3ec600', '2efc6341d66a9c0ad8bd4871d691d155.png'),
(2, '5eb2499e596a9', 'c1e29166786a559b1ae3120fc1f70f72.jpg'),
(3, '5eb2499e5d220', '324d30cbf26deb0b9abac439fe2f3aee.jpg'),
(4, '5eb2499e5dbf0', '0212081d4912ad087861f6d0535c1c17.jpg'),
(5, '5eb28f5a5a273', 'b09f839d35715583f306dd8fa57fb0af.jpg'),
(6, '5eb63bff953a7', 'd8844c31042f71fc0386e1d46b92a21c.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `image_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `id_unique_product`, `image_name`) VALUES
(32, '5eae59914b117', '53960eb89af981d003edb82e05a5b6b5.jpg'),
(40, '5eae9860baa55', '0b27d73ce82be7de072a966a19b19489.jpg'),
(41, '5eae9860baa55', 'ea73a2bbfa7de30bdcf8a12ab206db0b.jpg'),
(42, '5eae5a40b8a97', 'ae4a8fa33e81dbd330d049dd4d0dfc80.jpg'),
(43, '5eae5a40b8a97', 'dd61bfd3d5f2dac214f3401ec598bcfb.jpg'),
(44, '5eae5a40b8a97', 'ddba2dddaecde423dc55ee2d93048ffc.jpg'),
(45, '5eae9860baa55', '021160f417a1bf9f3948f3739562e21a.jpg'),
(46, '5eae59914b117', 'babc5976186c193efe745d4e2ee3ed47.jpg'),
(47, '5eae59914b117', '6f9fcd990a18f1d63b5cd7b401c4d03b.jpg'),
(48, '5eb4d2fdd6b71', 'e2c702bbfc6d9cf480c89622267210b6.png'),
(49, '5eb4d2fdd6b71', 'a32c540a53e356bd63b831d9e64f0155.png'),
(50, '5eb4d2fdd6b71', '9c7e928be61a421fa02096519b7f2e56.png');

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `order_date_time` datetime NOT NULL,
  `address` mediumtext,
  `payment_mode` tinyint(1) NOT NULL COMMENT '0-Cash on Delivery, 1-Online',
  `payment_info` varchar(500) DEFAULT NULL,
  `payment_status` tinyint(1) NOT NULL,
  `order_status` tinyint(1) NOT NULL,
  `id_coupon` bigint(20) DEFAULT NULL,
  `coupon_code` varchar(50) DEFAULT NULL,
  `discount_type` tinyint(1) DEFAULT NULL COMMENT '0-Percentage, 1-Amount',
  `discount_value` float DEFAULT NULL,
  `billing_info` text NOT NULL,
  `shipping_info` text NOT NULL,
  `order_notes` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `id_user`, `order_id`, `order_date_time`, `address`, `payment_mode`, `payment_info`, `payment_status`, `order_status`, `id_coupon`, `coupon_code`, `discount_type`, `discount_value`, `billing_info`, `shipping_info`, `order_notes`, `created_at`, `updated_at`) VALUES
(1, 7, '5eb61b0ef4144', '2020-05-09 08:23:02', NULL, 2, NULL, 0, 0, NULL, NULL, NULL, NULL, '{\"first_name\":\"guest\",\"last_name\":\"guest\",\"company_name\":\"guest\",\"country\":\"usa\",\"street\":\"guest\",\"apartment_suite_etc\":\"guest\",\"city\":\"guest\",\"pincode\":\"123456789\",\"phone\":\"1234567890\",\"email_address\":\"guest@gmail.com\"}', '', '', '2020-05-09 08:23:02', '2020-05-09 08:23:02');

-- --------------------------------------------------------

--
-- Table structure for table `product_order_history`
--

CREATE TABLE `product_order_history` (
  `id` bigint(20) NOT NULL,
  `id_product_order` varchar(50) NOT NULL,
  `order_status` tinyint(2) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_order_history`
--

INSERT INTO `product_order_history` (`id`, `id_product_order`, `order_status`, `created_at`) VALUES
(1, '1', 0, '2020-05-09 08:23:02');

-- --------------------------------------------------------

--
-- Table structure for table `product_order_meta`
--

CREATE TABLE `product_order_meta` (
  `id` bigint(20) NOT NULL,
  `id_product_order` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `id_unique_product_attributes` varchar(50) DEFAULT NULL,
  `price` float NOT NULL,
  `discount` float DEFAULT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_order_meta`
--

INSERT INTO `product_order_meta` (`id`, `id_product_order`, `id_unique_product`, `id_unique_product_attributes`, `price`, `discount`, `quantity`) VALUES
(1, 1, '5eb4d2fdd6b71', '5eb4d2fdef209', 9000, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_prices`
--

CREATE TABLE `product_prices` (
  `id` bigint(20) NOT NULL,
  `id_unique_product` varchar(50) NOT NULL,
  `id_unique_product_attributes` varchar(50) DEFAULT NULL,
  `id_currency` bigint(20) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `discount` float NOT NULL,
  `is_product_available` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Available (In Stock),0-Not Available(Out Of Stock)',
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_prices`
--

INSERT INTO `product_prices` (`id`, `id_unique_product`, `id_unique_product_attributes`, `id_currency`, `price`, `discount`, `is_product_available`, `quantity`) VALUES
(179, '5eae5a40b8a97', '5eb29b3b56c53', NULL, 5000, 10, 1, 7),
(180, '5eae5a40b8a97', '5eb29b3b56e35', NULL, 6000, 20, 1, 20),
(181, '5eae5a40b8a97', '5eb29b3b56f9f', NULL, 4000, 30, 0, 5),
(182, '5eae5a40b8a97', '5eb29b3b57133', NULL, 3000, 40, 1, 15),
(183, '5eae5a40b8a97', '5eb29b3b5728f', NULL, 7000, 50, 1, 10),
(184, '5eae5a40b8a97', '5eb29b3b573eb', NULL, 4500, 60, 1, 10),
(185, '5eae9860baa55', NULL, NULL, 100, 10, 1, 50),
(198, '5eae59914b117', '5eb29f983bf5a', NULL, 200, 1, 1, 9),
(199, '5eae59914b117', '5eb29f983c10a', NULL, 300, 1, 1, 10),
(200, '5eae59914b117', '5eb29f983c27e', NULL, 400, 1, 1, 0),
(201, '5eae59914b117', '5eb29f983c3f7', NULL, 556, 1, 1, 10),
(202, '5eae59914b117', '5eb29f983c564', NULL, 33, 1, 1, 10),
(203, '5eae59914b117', '5eb29f983c6b1', NULL, 554, 1, 1, 10),
(209, '5eb4d2fdd6b71', '5eb63bff953a7', NULL, 5000, 10, 1, 0),
(210, '5eb4d2fdd6b71', '5eb63bffa5c96', NULL, 6000, 10, 1, 0),
(211, '5eb4d2fdd6b71', '5eb63bffa5e20', NULL, 7000, 25, 1, 0),
(212, '5eb4d2fdd6b71', '5eb63bffa5f98', NULL, 7000, 5, 1, 0),
(213, '5eb4d2fdd6b71', '5eb63bffa60f6', NULL, 9000, 10, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `date_of_birth` date NOT NULL,
  `password` text NOT NULL,
  `forgot_pwd_tocken` varchar(6) NOT NULL,
  `is_valid_reset_password_link` datetime DEFAULT NULL,
  `is_account_confirm` enum('0','1') NOT NULL COMMENT '0=not-confirm , 1=confirm',
  `language_type` enum('1','2') NOT NULL COMMENT '1=english , 2=turkish',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `first_name`, `last_name`, `email`, `gender`, `date_of_birth`, `password`, `forgot_pwd_tocken`, `is_valid_reset_password_link`, `is_account_confirm`, `language_type`, `created_at`, `modified_at`, `is_active`) VALUES
(5, 'test', 'abc', 'test@gmail.com', 'female', '2020-04-01', '1978e83179fac8840e8a5fdafaa0a282acfee2c3e05bd5c099b999c1d36141d7f0f586217e1b8ebe108078b8aefaa6225abe9ef17e662cebfe019ab1d341cbb69fqIxEDFizPY8zfaAzG80JBMd+Qk1+ZtN9JOvnNW36I=', '', NULL, '0', '1', '2020-04-30 15:33:18', '2020-05-02 15:02:37', '1'),
(6, 'First name', 'Last name', 'customizer@gmail.com', 'male', '2020-05-02', 'b74d5f5f7bdeeb4772ca1e13607b8575922777c5f2cf6aa9a34dbae82782af947be2831393e44e9faf9ea618569983188ee45280c5d4fcdcb4940b3cc6105082np9S/lVZTQAB8tyKNP6rUjEVmS3j75Ra3FmNVT2bbP0=', '', NULL, '0', '1', '2020-05-03 14:54:14', '2020-05-03 14:54:14', '1'),
(7, 'guest', 'guest', 'guest@gmail.com', '', '0000-00-00', '', '', NULL, '0', '1', '2020-05-09 08:23:02', '2020-05-09 08:23:02', '1');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `setting_title` varchar(255) NOT NULL,
  `setting_key` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `is_required` enum('1','0') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `setting_title`, `setting_key`, `setting_value`, `is_required`, `created_at`, `updated_at`) VALUES
(1, 'Site Title', 'SITE_TITLE', 'Customizer', '1', '2018-03-10 17:31:13', '2020-04-27 05:26:07'),
(2, 'From Email', 'FROM_EMAIL', 'customizer@gmail.com', '1', '2018-03-10 17:31:13', '2020-04-27 05:26:12'),
(3, 'From Email Title', 'FROM_EMAIL_TITLE', 'Customizer', '1', '2018-03-10 17:31:32', '2020-04-27 05:26:15'),
(4, 'Admin Email', 'ADMIN_EMAIL', 'customizer@gmail.com', '1', '2018-03-10 17:32:01', '2020-04-27 05:26:31'),
(5, 'SMTP HOST', 'SMTP_HOST', '', '1', '2019-08-31 00:00:00', '2020-04-27 05:26:17'),
(6, 'SMTP PORT', 'SMTP_PORT', '', '1', '2019-08-31 00:00:00', '2020-04-27 05:26:19'),
(7, 'SMTP USER', 'SMTP_USERNAME', '', '1', '2019-08-31 00:00:00', '2020-04-27 05:26:24'),
(8, 'SMTP PASSWORD', 'SMTP_PASSWORD', '', '1', '2019-08-31 00:00:00', '2020-04-27 05:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `static_page`
--

CREATE TABLE `static_page` (
  `id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(50) NOT NULL,
  `title_arabic` varchar(100) NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `image_arabic` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page`
--

INSERT INTO `static_page` (`id`, `title`, `description`, `image`, `title_arabic`, `description_arabic`, `image_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(2, 'Privacy policy', '<div class=\"contact-us-content terms-and-condition-content\">\r\n\r\n							<p>Lorem Ipsum is simply dummy text of the printing and \r\ntypesetting industry. Lorem Ipsum has been the industry\'s standard dummy\r\n text ever since the 1500s, when an unknown printer took a galley of \r\ntype and scrambled it to make a type specimen book. It has survived not \r\nonly five centuries, but also the leap into electronic typesetting, \r\nremaining essentially unchanged. It was popularised in the 1960s with \r\nthe release of Letraset sheets containing Lorem Ipsum passages, and more\r\n recently with desktop publishing software like Aldus PageMaker \r\nincluding versions of Lorem Ipsum.</p><br></div>', '0d8caaf66dd168023b207d42dbbce04b.jpg', 'سياسة الخصوصية', '<p>Lorem Ipsum هو ببساطة نص وهمي لصناعة الطباعة والتنضيد. كان لوريم إيبسوم هو النص الوهمي القياسي للصناعة منذ القرن الخامس عشر ، عندما أخذت طابعة غير معروفة مجموعة من الأنواع وخلطتها لعمل كتاب من نوع العينة. لقد نجا ليس فقط من خمسة قرون ، ولكن أيضًا قفزة في التنضيد الإلكتروني ، وبقي دون تغيير بشكل أساسي. تم تعميمه في الستينيات مع إصدار أوراق Letraset التي تحتوي على مقاطع Lorem Ipsum ، ومؤخرًا مع برامج النشر المكتبي مثل Aldus PageMaker بما في ذلك إصدارات Lorem Ipsum.<br></p>', '', '2020-01-02 18:24:18', '2020-04-28 06:40:54', 1),
(3, 'Terms and conditions', '<div class=\"contact-us-content terms-and-condition-content\">\r\n\r\n							<p>Lorem Ipsum is simply dummy text of the printing and \r\ntypesetting industry. Lorem Ipsum has been the industry\'s standard dummy\r\n text ever since the 1500s, when an unknown printer took a galley of \r\ntype and scrambled it to make a type specimen book. It has survived not \r\nonly five centuries, but also the leap into electronic typesetting, \r\nremaining essentially unchanged. It was popularised in the 1960s with \r\nthe release of Letraset sheets containing Lorem Ipsum passages, and more\r\n recently with desktop publishing software like Aldus PageMaker \r\nincluding versions of Lorem Ipsum.</p></div>', '779d2e4374d2f1ee122ec44a12f8524e.png', 'الأحكام والشروط', '<p>Lorem Ipsum هو ببساطة نص وهمي لصناعة الطباعة والتنضيد. كان لوريم إيبسوم هو النص الوهمي القياسي للصناعة منذ القرن الخامس عشر ، عندما أخذت طابعة غير معروفة مجموعة من الأنواع وخلطتها لعمل كتاب من نوع العينة. لقد نجا ليس فقط من خمسة قرون ، ولكن أيضًا قفزة في التنضيد الإلكتروني ، وبقي دون تغيير بشكل أساسي. تم تعميمه في الستينيات مع إصدار أوراق Letraset التي تحتوي على مقاطع Lorem Ipsum ، ومؤخرًا مع برامج النشر المكتبي مثل Aldus PageMaker بما في ذلك إصدارات Lorem Ipsum.<br></p>', '', '2020-01-02 18:25:14', '2020-04-28 06:41:24', 1),
(4, 'About us', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting \r\nindustry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book. It has survived not only five\r\n centuries, but also the leap into electronic typesetting, remaining \r\nessentially unchanged.</p>\r\n					<p>Lorem Ipsum is simply dummy text of the printing and typesetting\r\n industry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book.</p>\r\n					<p>“ Many desktop publishing packages and web page editors now use \r\nLorem Ipsum as their default model search for evolved over sometimes by \r\naccident, sometimes on purpose ”</p>', 'd1ea3c77ddc94db0096912a92cde0dbc.png', 'معلومات عنا', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting \r\nindustry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book. It has survived not only five\r\n centuries, but also the leap into electronic typesetting, remaining \r\nessentially unchanged.</p>\r\n					<p>Lorem Ipsum is simply dummy text of the printing and typesetting\r\n industry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book.</p>\r\n					<p>Many desktop publishing packages and web page editors now use \r\nLorem Ipsum as their default model search for evolved over sometimes by \r\naccident, sometimes on purpose ....<br></p>', 'fb903e7c6d48284973fdc7b2ed7a4f4e.jpg', '2020-01-27 16:21:26', '2020-04-29 10:49:27', 1),
(5, 'Return policy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting \r\nindustry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book. It has survived not only five\r\n centuries, but also the leap into electronic typesetting, remaining \r\nessentially unchanged.</p>\r\n					<p>Lorem Ipsum is simply dummy text of the printing and typesetting\r\n industry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book.</p>\r\n					<p>Many desktop publishing packages and web page editors now use \r\nLorem Ipsum as their default model search for evolved over sometimes by \r\naccident, sometimes on purpose ....<br></p>', '58e9d428598d8205dc337487669ef23a.jpg', 'سياسة العائدات', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting \r\nindustry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book. It has survived not only five\r\n centuries, but also the leap into electronic typesetting, remaining \r\nessentially unchanged.</p>\r\n					<p>Lorem Ipsum is simply dummy text of the printing and typesetting\r\n industry. Lorem Ipsum has been the industry’s standard dummy text ever \r\nsince the 1500s, when an unknown printer took a galley of type and \r\nscrambled it to make a type specimen book.</p>\r\n					<p>Many desktop publishing packages and web page editors now use \r\nLorem Ipsum as their default model search for evolved over sometimes by \r\naccident, sometimes on purpose ....<br></p>', '0655e8cb9cb6c50e502fbac2d7e72369.jpg', '2020-04-28 06:00:46', '2020-04-29 10:49:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(15) DEFAULT NULL,
  `address` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_cards`
--

CREATE TABLE `user_cards` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `card_holder_name` varchar(500) NOT NULL,
  `card_number` varchar(500) NOT NULL,
  `expiry_month` varchar(500) NOT NULL,
  `expiry_year` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `why_choose_us`
--

CREATE TABLE `why_choose_us` (
  `id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `title_arabic` varchar(100) NOT NULL,
  `description_arabic` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active , 0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `why_choose_us`
--

INSERT INTO `why_choose_us` (`id`, `title`, `description`, `title_arabic`, `description_arabic`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Free Shipping', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', 'الشحن مجانا', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '2020-04-29 09:07:15', '2020-04-29 10:22:27', 1),
(2, '100% money back guarantee.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '100% money back guarantee.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '2020-04-29 09:07:34', '2020-04-29 09:07:34', 1),
(3, 'Online Support 24/7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', 'Online Support 24/7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industr.', '2020-04-29 09:07:57', '2020-04-29 15:38:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `id_product` varchar(50) NOT NULL,
  `login_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `id_product`, `login_id`, `created_at`) VALUES
(32, '5', 5, '2020-04-30 15:35:28'),
(34, '45', 6, '2020-05-08 14:46:58'),
(35, '51', 6, '2020-05-08 14:47:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bag`
--
ALTER TABLE `bag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_banner`
--
ALTER TABLE `category_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_description`
--
ALTER TABLE `category_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `happy_clients`
--
ALTER TABLE `happy_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_status`
--
ALTER TABLE `notification_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_id` (`notification_id`);

--
-- Indexes for table `notification_token`
--
ALTER TABLE `notification_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_mode`
--
ALTER TABLE `payment_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes_image`
--
ALTER TABLE `product_attributes_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order_history`
--
ALTER TABLE `product_order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order_meta`
--
ALTER TABLE `product_order_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_prices`
--
ALTER TABLE `product_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_page`
--
ALTER TABLE `static_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_cards`
--
ALTER TABLE `user_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `why_choose_us`
--
ALTER TABLE `why_choose_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `bag`
--
ALTER TABLE `bag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `category_banner`
--
ALTER TABLE `category_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_description`
--
ALTER TABLE `category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `happy_clients`
--
ALTER TABLE `happy_clients`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_status`
--
ALTER TABLE `notification_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_token`
--
ALTER TABLE `notification_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_mode`
--
ALTER TABLE `payment_mode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;

--
-- AUTO_INCREMENT for table `product_attributes_image`
--
ALTER TABLE `product_attributes_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_order_history`
--
ALTER TABLE `product_order_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_order_meta`
--
ALTER TABLE `product_order_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_prices`
--
ALTER TABLE `product_prices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `static_page`
--
ALTER TABLE `static_page`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_cards`
--
ALTER TABLE `user_cards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `why_choose_us`
--
ALTER TABLE `why_choose_us`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
