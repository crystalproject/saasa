<?php

defined('BASEPATH') or exit('No direct script access allowed');

function delete_product($ids)
{
    $CI = &get_instance();
    $restiction = $CI->common_model->select_data('product_order_meta', array('SELECT' => 'id', 'IN' => array('id_unique_product' => $ids)));
    if ($restiction['row_count'] > 0) {
        return false;
    } else {
        $video = $CI->common_model->select_data('product', array('select' => 'product_video', 'IN' => array('id_unique' => $ids)));
        if ($video['row_count'] > 0) {
            foreach ($video['data'] as $value) {
                if (file_exists(UPLOAD_DIR_PRODUCT . $value['product_video'])) {
                    @unlink(UPLOAD_DIR_PRODUCT . $value['product_video']);
                }
            }
        }

        $images = $CI->common_model->select_data('product_images', array('IN' => array('id_unique_product' => $ids)));
        if ($images['row_count'] > 0) {
            foreach ($images['data'] as $value) {
                if (file_exists(UPLOAD_DIR_PRODUCT . $value['image_name'])) {
                    @unlink(UPLOAD_DIR_PRODUCT . $value['image_name']);
                }
            }
        }
        $attribute_images = $CI->common_model->select_data('product_attributes', array('IN' => array('id_unique_product' => $ids)));
        if ($attribute_images['row_count'] > 0) {
            $id_unique_product_attributes = array_column($attribute_images['data'], 'id_unique');
            $attribute_images = $CI->common_model->select_data('product_attributes_image', array('IN' => array('id_unique_product_attributes' => $id_unique_product_attributes)));
            if ($attribute_images['row_count'] > 0) {
                foreach ($attribute_images['data'] as  $value) {
                    if (file_exists(UPLOAD_DIR_PRODUCT . $value['image_name'])) {
                        @unlink(UPLOAD_DIR_PRODUCT . $value['image_name']);
                    }
                }
            }
        }
        $CI->common_model->delete_data('product_prices', array('IN' => array('id_unique_product' => $ids)));
        $CI->common_model->delete_data('product_attributes', array('IN' => array('id_unique_product' => $ids)));
        $CI->common_model->delete_data('product_images', array('IN' => array('id_unique_product' => $ids)));
        $CI->common_model->delete_data('cart', array('IN' => array('id_unique_product' => $ids)));
        $CI->common_model->delete_data('product', array('IN' => array('id_unique' => $ids)));
    }
    return true;
}

/**
 * When $for == listing then this will return minimum information of the product 
 * When $for == detail then this will return all the information about the product
 */
function get_product_detail($info, $id_language, $for = 'listing', $settings = array())
{
    $CI = &get_instance();
    $data = array();
    $id_unique = $info['id_unique'];
    $data['product_info'] = $info;
    $data['attribute_combinations'] = null;
    $data['prices'] = null;
    $data['attribute_info'] = null;
    $data['image_info'] = null;
    $data['discount'] = null;
    $data['is_product_available'] = null;
    $data['quantity'] = null;
    $data['attribute_images'] = null;
    //getting images
    if ($for == 'detail' && empty($settings)) {
        $temp = $CI->common_model->select_data('product_images', array('SELECT' => 'image_name', 'WHERE' => array('id_unique_product' => $id_unique)));
        if ($temp['row_count'] > 0) {
            $data['image_info'] = array();
            foreach ($temp['data'] as $key => $value) {
                $data['image_info'][] = array('image_name' => base_url('uploads/product/') . $value['image_name']);
            }
        }
    }
    //getting attributes            
    $data['product_attributes'] = array();
    if ($data['product_info']['has_attributes'] == '1') {
        $conditions = array('WHERE' => array('id_unique_product' => $id_unique));
        if (isset($settings['id_unique_product_attributes']) && !empty($settings['id_unique_product_attributes'])) {
            $conditions['WHERE']['id_unique'] = $settings['id_unique_product_attributes'];
        }
        $temp = $CI->common_model->select_data('product_attributes', $conditions);
        if ($temp['row_count'] > 0) {
            $data['product_attributes'] = $temp['data'];
        }
        $temp = array();
        foreach ($data['product_attributes'] as $value) {
            $temp[$value['id_unique']][] = $value;
        }
        $data['product_attributes'] = $temp;
    }
    //Getting prices
    $conditions = array('WHERE' => array('id_unique_product' => $id_unique) , 'ORDER BY' => array('price' => 'DESC'));
    if (isset($settings['id_unique_product_attributes']) && !empty($settings['id_unique_product_attributes'])) {
        $conditions['WHERE']['id_unique_product_attributes'] = $settings['id_unique_product_attributes'];
    }
    $temp = $CI->common_model->select_data('product_prices', $conditions);
    $data['product_prices'] = array();
    if ($temp['row_count'] > 0) {
        $data['product_prices'] = $temp['data'];
    }
    if ($data['product_info']['has_attributes'] == '0') {
        $data['product_info']['price'] = isset($data['product_prices'][0]['price']) ? $data['product_prices'][0]['price'] : 0;
        $data['product_info']['is_product_available'] = isset($data['product_prices'][0]['is_product_available']) ? $data['product_prices'][0]['is_product_available'] : 0;
        $data['product_info']['discount'] = isset($data['product_prices'][0]['discount']) ? $data['product_prices'][0]['discount'] : 0;
        $data['product_info']['quantity'] = isset($data['product_prices'][0]['quantity']) ? $data['product_prices'][0]['quantity'] : 0;
        $data['prices'] = array($data['product_info']['price']);
        $data['is_product_available'] = array($data['product_info']['is_product_available']);
        $data['discount'] = array($data['product_info']['discount']);
        $data['quantity'] = array($data['product_info']['quantity']);
        unset($data['product_prices']);
    } else {
        $data['product_info']['price'] = 0;
        //Adding price to all combination
        $temp = array();
        $attribute_ids = array();
        $attribute_value_ids = array();
        $attribute_combinations = array();
        $attribute_images = array();
        $prices = array();
        $discount = array();
        $is_product_available = array();
        $quantity = array();
        foreach ($data['product_prices'] as $value) {
            if (isset($data['product_attributes'][$value['id_unique_product_attributes']])) {
                foreach ($data['product_attributes'][$value['id_unique_product_attributes']] as $key => $temp) {
                    $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_price'] = $value['price'];
                    $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_discount'] = $value['discount'];
                    $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_is_product_available'] = $value['is_product_available'];
                    $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_quantity'] = $value['quantity'];
                    $attribute_ids[] = $data['product_attributes'][$value['id_unique_product_attributes']][$key]['id_unique_attributes'];
                }
            }
        }
        $temp = array();
        foreach ($data['product_attributes'] as $value) {
            $temp[] = $value;
            foreach ($value as $ai_value) {
                $attribute_value_ids[$ai_value['id_unique_attributes']][] = $ai_value['id_unique_attribute_values'];
                $attribute_combinations[$ai_value['id_unique']][] = $ai_value['id_unique_attribute_values'];
                $prices[$ai_value['id_unique']] = $ai_value['attribute_price'];
                $discount[$ai_value['id_unique']] = $ai_value['attribute_discount'];
                $is_product_available[$ai_value['id_unique']] = $ai_value['attribute_is_product_available'];
                $quantity[$ai_value['id_unique']] = $ai_value['attribute_quantity'];
                if (!in_array($ai_value['id_unique'], $attribute_images)) {
                    $attribute_images[] = $ai_value['id_unique'];
                }
            }
        }
        $data['product_attributes'] = $temp;
        $data['attribute_images'] = $attribute_images;
        $temp = array();
        if (!empty($data['attribute_images'])) {
            $sub_query = ',IF(image_name="","",CONCAT("' . base_url('uploads/product/') . '",image_name)) AS image_name';
            $conditions = array('SELECT' => 'id' . $sub_query, 'IN' => array('id_unique_product_attributes' => $data['attribute_images']));
            $temp = $CI->common_model->select_data('product_attributes_image', $conditions);
            if ($temp['row_count'] > 0) {
                $data['attribute_images'] = $temp['data'];
            } else {
                $data['attribute_images'] = array();
            }
        }

        $data['attribute_combinations'] = array_values($attribute_combinations);
        $data['prices'] = array_values($prices);
        $data['discount'] = array_values($discount);

        if ($data['product_info']['by_quantity_or_availability'] == 0) {
            $data['is_product_available'] = array_values($is_product_available);
            $data['quantity'] = null;
        } else {
            $data['is_product_available'] = null;
            $data['quantity'] = array_values($quantity);
        }

        $data['product_prices'] = array();

        //Getting attribute Names
        $attribute_ids = array_unique($attribute_ids);
        $conditions = array('SELECT' => 'id_unique,attribute_name', 'WHERE' => array('id_language' => $id_language), 'IN' => array('id_unique' => $attribute_ids));
        $data['attribute_info'] = $CI->common_model->select_data('attributes', $conditions);
        if ($data['attribute_info']['row_count'] > 0) {
            $data['attribute_info'] = $data['attribute_info']['data'];
            foreach ($data['attribute_info'] as &$value) {
                $sub_query = ',IF(image="","",CONCAT("' . base_url('uploads/attribute/') . '",image)) AS image';
                $conditions = array('SELECT' => 'id_unique,attribute_value' . $sub_query, 'WHERE' => array('id_language' => $id_language, 'is_active' => '1'), 'IN' => array('id_unique' => $attribute_value_ids[$value['id_unique']]));
                $temp = $CI->common_model->select_data('attribute_values', $conditions);
                if ($temp['row_count'] > 0) {
                    $value['attribute_values'] = $temp['data'];
                } else {
                    $value['attribute_values'] = null;
                }
            }
            unset($value);
        } else {
            $data['attribute_info'] = null;
        }
        unset($data['product_prices']);
    }
    if ($for == 'listing') {
        $data['attribute_combinations'] = isset($data['attribute_combinations']) && !empty($data['attribute_combinations']) ? $data['attribute_combinations'][0] : null;
        $data['prices'] = array($data['prices'][0]);
        $data['attribute_info'] = null;
        $data['discount'] = isset($data['discount'][0]) ? array($data['discount'][0]) : null;
        $data['is_product_available'] = isset($data['is_product_available'][0]) ? array($data['is_product_available'][0]) : null;
        $data['quantity'] = isset($data['quantity'][0]) ? array($data['quantity'][0]) : null;
    }
    unset($data['product_info']);
    unset($data['product_attributes']);
    $data['discounted_amount'] = array();
    $data['final_price'] = array();
    foreach ($data['prices'] as $key => $value) {
        $data['discounted_amount'][$key] = calculate_discount(0, $data['prices'][$key], $data['discount'][$key]);
        $data['final_price'][$key] = $data['prices'][$key] - $data['discounted_amount'][$key];
    }
    return $data;
}

/**
 * This function is used to check quantity or availability of product
 * @param type $id_unique
 * @param type $id_unique_product_attributes
 * @param type $quantity
 * @return boolean
 */
function check_product_available_quantity($id_unique, $id_unique_product_attributes = null, $quantity = null)
{
    $return = false;
    $CI = &get_instance();
    $sql = 'SELECT by_quantity_or_availability,has_attributes FROM product WHERE id_unique = "' . $id_unique . '" LIMIT 1';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        $info = $info['data'][0];
        $sub_query = '';
        if ($info['has_attributes'] == 1) {
            $sub_query = ' AND id_unique_product_attributes = "' . $id_unique_product_attributes . '" ';
        }
        if ($info['by_quantity_or_availability'] == 0) {
            //Check availability
            $sql = 'SELECT is_product_available FROM product_prices WHERE id_unique_product = "' . $id_unique . '" ' . $sub_query . ' ';
            $info = $CI->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                if ($info['data'][0]['is_product_available'] == 1) {
                    $return = true;
                }
            }
        } else {
            $sql = 'SELECT quantity FROM product_prices WHERE id_unique_product = "' . $id_unique . '" ' . $sub_query . ' ';
            $info = $CI->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                if ($info['data'][0]['quantity'] > 0 && $info['data'][0]['quantity'] >= $quantity) {
                    $return = true;
                }
            }
        }
    }
    return $return;
}


/**
 * This function is used for getting product attribute unique id from product attribute values (selected combination)
 * This function is used when user trying to repeat an order
 * $length - Attribute length means total selected attribute like if color, size means total 2
 */
function get_id_unique_attribute_values($id_unique_product, $id_unique_attribute_values, $length = '')
{
    $CI = &get_instance();

    if ($length == '') {
        $sql = 'SELECT id_attributes FROM product WHERE id_unique = "' . $id_unique_product . '" ';
        $info = $CI->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $length = count(explode(',', $info['data'][0]['id_attributes']));
        }
    }

    $id_unique_product_attributes = '';
    $id_unique_attribute_values = explode(',', $id_unique_attribute_values);
    $id_unique_attribute_values = "'" . implode("','", $id_unique_attribute_values) . "'";
    $sql = "SELECT id_unique FROM product_attributes WHERE id_unique_product = '" . $id_unique_product . "' AND id_unique_attribute_values IN (" . $id_unique_attribute_values . ") ";
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        $id_uniques = array();
        foreach ($info['data'] as $value) {
            $id_uniques[$value['id_unique']][] = 1;
        }
        foreach ($info['data'] as $value) {
            if (count($id_uniques[$value['id_unique']]) ==  $length) {
                $id_unique_product_attributes = $value['id_unique'];
            }
        }
        // if ($info['row_count'] == 1) {
        //     $id_unique_product_attributes = $info['data'][0]['id_unique'];
        // } else {
        //     $id_uniques = array();
        //     foreach ($info['data'] as $value) {
        //         $id_uniques[$value['id_unique']][] = 1;
        //     }
        //     $count = 0;
        //     foreach ($info['data'] as $value) {
        //         if (count($id_uniques[$value['id_unique']]) >  $count) {
        //             $count = count($id_uniques[$value['id_unique']]);
        //             $id_unique_product_attributes = $value['id_unique'];
        //         }
        //     }
        // }
    }
    return  $id_unique_product_attributes;
}

/**
 * When admin update a product then need to check if the same attribute has product available or not
 */
function check_if_order_available($id_unique)
{
    $CI = &get_instance();
    $sql = 'SELECT is_ordered FROM product_attributes WHERE id_unique = "' . $id_unique . '"';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        return isset($info['data'][0]['is_ordered']) ? $info['data'][0]['is_ordered'] : 0;
    } else {
        return 0;
    }
}


function get_total_review($id_unique)
{
    $CI = &get_instance();
    $sql = 'SELECT COUNT(id) AS total_review FROM product_review WHERE id_unique_product = "' . $id_unique . '" ';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        return isset($info['data'][0]['total_review']) ? $info['data'][0]['total_review'] : 0;
    } else {
        return 0;
    }
}

/**
 * This function is used for getting all the reviewes added by users
 */
function get_reviews($id_unique)
{
    $CI = &get_instance();
    $sql = 'SELECT * FROM product_review WHERE id_unique_product = "' . $id_unique . '" ';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        return $info['data'];
    } else {
        return null;
    }
}

/**
 * This function is used for getting average review of the product
 */
function get_average_review($id_unique)
{
    $CI = &get_instance();
    $sql = 'SELECT AVG(rating) AS average_rating FROM product_review WHERE id_unique_product = "' . $id_unique . '" ';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        return abs($info['data'][0]['average_rating']);
    } else {
        return 0;
    }
}
