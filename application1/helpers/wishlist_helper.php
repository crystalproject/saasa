<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * This function is used for getting product that are in wishlist
 */
function get_total_wishlist()
{
    $CI = &get_instance();
    $sql = 'SELECT COUNT(id) AS total FROM wishlist WHERE login_id = "' . get_front_login_id() . '" ';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        return isset($info['data'][0]['total']) ? $info['data'][0]['total'] : 0;
    } else {
        return 0;
    }
}
