<?php

defined('BASEPATH') or exit('No direct script access allowed');
if (!function_exists('get_active_class')) {

    function get_active_class($key)
    {

        $ci = &get_instance();
        $link = $ci->router->fetch_class() . '/' . $ci->router->fetch_method();

        $menu = array(
            'administrator/add' => array('my-account', 'profile'),
            'administrator/view' => array('my-account', 'profile'),
            'administrator/edit' => array('my-account', 'profile'),
            'account/changepassword' => array('my-account', 'change-password'),
            'users/index' => array('website', 'users'),
            'user/edit' => array('website', 'user'),
            'user/details' => array('website', 'user'),
            'language/index' => array('website', 'language'),
            'language/add_edit' => array('website', 'language'),
            'category_banner/index' => array('website', 'main', 'category-banner'),
            'category_banner/add_edit' => array('website', 'main', 'category-banner'),
            'category/index' => array('website', 'main', 'category'),
            'category/add_edit' => array('website', 'main', 'category'),
            'sub_category/index' => array('website', 'main', 'sub-category'),
            'sub_category/add_edit' => array('website', 'main', 'sub-category'),
            'static_page/index' => array('website', 'static-page'),
            'static_page/add_edit' => array('website', 'static-page'),
            'settings/index' => array('website', 'settings'),
            'attributes/index' => array('website', 'product-main', 'attributes'),
            'attributes/add_edit' => array('website', 'product-main', 'attributes'),
            'brand/index' => array('website', 'product-main', 'brand'),
            'brand/add_edit' => array('website', 'product-main', 'brand'),
            'product/index' => array('website', 'product-main', 'product'),
            'product/add_edit' => array('website', 'product-main', 'product'),
            'product/detail' => array('website', 'product-main', 'product'),
            'gift_wraping/index' => array('website','product-main', 'gift-wraping'),
            'gift_wraping/add_edit' => array('website','product-main', 'gift-wraping'),
            'product_order/index' => array('website', 'product-main', 'product-order'),
            'product_order/detail' => array('website', 'product-main', 'product-order'),
            'payment_mode/index' => array('website', 'payment-mode'),

            'home_slider/index' => array('website', 'home-slider'),
            'home_slider/add_edit' => array('website', 'home-slider'),
            'contact_us/index' => array('website', 'contact-us'),

            'coupons/index' => array('website', 'coupons'),
            'coupons/add_edit' => array('website', 'coupons'),

            'faq/index' => array('website', 'faq'),
            'faq/add_edit' => array('website', 'faq'),

            'menus/index' => array('website', 'menus'),
            'menus/add_edit' => array('website', 'menus'),

            'why_choose_us/index' => array('website', 'why-choose-us'),
            'why_choose_us/add_edit' => array('website', 'why-choose-us'),
            'happy_clients/index' => array('website', 'happy-clients'),
            'happy_clients/add_edit' => array('website', 'happy-clients'),

            
        );
        if (array_key_exists($link, $menu)) {
            if (in_array($key, $menu[$link])) {
                return array('main_class' => 'menu-open', 'sub_class' => 'active', 'open_class' => 'display:block');
            } else {
                return array('main_class' => '', 'sub_class' => '', 'open_class' => 'display:block');
            }
        } else {
            return array('main_class' => '', 'sub_class' => '', 'open_class' => '');
        }
    }
}
