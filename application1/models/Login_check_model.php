<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_check_model extends CI_model {

    public function __construct() {
        if (!$this->session->loggedin) {
            if ($this->router->fetch_class() != "login") {
                redirect("authority/login");
            }
        }

        if ($this->session->loggedin) {
            /*if ($this->session->loggedin_user_role == "User" && $this->router->fetch_class() != "login") {
                if ($this->uri->segment(1) != "user") {
                    redirect("user/dashboard");
                }
            }*/
            if ($this->session->loggedin_user_role == "Admin" || $this->session->loggedin_user_role == "Subadmin" && $this->router->fetch_class() != "login") {
                if ($this->uri->segment(1) != "authority") {
                    redirect("authority/dashboard");
                }
            }
        }
    }

}
