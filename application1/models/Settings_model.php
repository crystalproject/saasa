<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings_model extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        $this->db->query('SET time_zone = "+03:00"');
        $default_language = isset($this->session->default_language) ? $this->session->default_language : DEFAULT_LANGUAGE;
        load_language($default_language);        
        if (get_cookie('unique_id') != '') {
            define('COOKIE_USER', decrypt_cookie(get_cookie('unique_id')));
        } else {
            $time = time() + 60 * 60 * 24 * 50 * 50;
            $number = '';
            for ($counter = 0; $counter < 6; $counter++) {
                $number .= mt_rand(0, 9);
            }
            delete_cookie('unique_id');
            $cookie = array(
                'name' => 'unique_id',
                'value' => encrypt_cookie($number),
                'expire' => $time,
            );
            set_cookie($cookie);
            define('COOKIE_USER', decrypt_cookie(get_cookie('unique_id')));
        }
        $conditions = array(
            "select" => "*",
        );
        $info = $this->common_model->select_data("site_settings", $conditions);
        if ($info['row_count'] > 0) {
            foreach ($info['data'] as $value) {
                define($value['setting_key'], $value['setting_value']);
            }
        }
    }
}
