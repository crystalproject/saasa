<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . '/libraries/class.phpmailer.php');

class Production_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_record($table_name, $data) {
        $record = $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function get_all_with_where($table_name, $order_by_column = "", $order_by_value = "", $where_array) {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($order_by_column, $order_by_value);
        $this->db->where($where_array);
        $record = $this->db->get();
        return $record->result_array();
    }

    public function get_all_with_where_in($table_name, $order_by_column = "", $order_by_value = "", $column_name, $where_in_array, $where_array = array()) {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($order_by_column, $order_by_value);
        $this->db->where_in($column_name, $where_in_array);
        $this->db->where($where_array);
        $record = $this->db->get();
        return $record->result_array();
    }

    public function get_delete_where_in($table_name, $column_name, $where_in_array) {
        $this->db->where_in($column_name, $where_in_array);
        $result = $this->db->delete($table_name);
        return $result;
    }

    public function get_all_with_where_limit($table_name, $order_by_column = "", $order_by_value = "", $where_array = array(), $limit, $start) {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($order_by_column, $order_by_value);
        $this->db->where($where_array);
        if (strval($limit) != '' && strval($start) != '') {
            $this->db->limit($limit, $start);
        }
        $record = $this->db->get();
        return $record->result_array();
    }

    public function update_record($table_name, $data_array, $where_array) {
        $this->db->where($where_array);
        $this->db->update($table_name, $data_array);
        return $this->db->affected_rows();
    }

    public function delete_record($table_name, $where_array) {
        $result = $this->db->delete($table_name, $where_array);
        return $result;
    }

    function jointable_descending($column = "", $table, $like_array = "", $join = "", $order_by_column = "", $order_by_value = "", $where = "", $or_where = array(), $group_by = array(), $limit = "", $start = "", $where_in_array = "") {
        if ($column == '') {
            $column = '*';
        }
        $this->db->select($column);
        $this->db->from($table);
        if ($like_array != "") {
            $this->db->like($like_array);
        }
        if ($group_by != '') {
            $this->db->group_by($group_by);
        }
        if ($where != "") {
            $this->db->where($where);
        }
        if ($or_where != "") {
            $this->db->or_where($or_where);
        }
        if ($order_by_column != "" || $order_by_value != "") {
            $this->db->order_by($order_by_column, $order_by_value);
        }
        if (!empty($where_in_array)) {
            foreach ($where_in_array as $key => $value) {
                if (!empty($value)) {
                    $this->db->where_in($key, $value);
                }
            }
        }
        if ($join != "") {
            foreach ($join as $join_row) {
                $this->db->join($join_row['table_name'], $join_row['column_name'], $join_row['type']);
            }
        }
        if ($limit != "" && $start != "") {
            $this->db->limit($limit, $start);
        } else if ($limit != '') {
            $this->db->limit($limit);
        }
        $record = $this->db->get();
        return $record->result_array();
    }
    
    public function generate_thumbnail($path, $file_name) {
        if (!empty($file_name) && !file_exists(FCPATH . $path . 'thumbnail/' . $file_name)) {

            $source_path = $path . $file_name;
            $target_path = $path . 'thumbnail/';
            if (!is_dir($target_path)) {
                mkdir($target_path);
                @chmod($target_path, 0777);
            }
            $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'maintain_ratio' => FALSE,
                'create_thumb' => TRUE,
                'thumb_marker' => '',
                'width' => 60,
                'height' => 60
            );
            // $this->load->library('image_lib', $config_manip);
            $this->image_lib->initialize($config_manip);

            if (!$this->image_lib->resize()) {
                $this->image_lib->display_errors();
            }
            // clear //
            $this->image_lib->clear();
        }
    }

    public function only_pagination($settings = array()) {
        $response = array();

        $this->load->library('pagination');
        $config_pagination = array();
        $config_pagination['base_url'] = isset($settings['url']) ? $settings['url'] : site_url();
        $config_pagination['total_rows'] = $settings['total_record'];
        $config_pagination['per_page'] = isset($settings['per_page']) ? $settings['per_page'] : RECORDS_PER_PAGE;        
        $config_pagination["uri_segment"] = 4;        
        // $config_pagination['num_links'] = 4;
        $config_pagination['use_page_numbers'] = TRUE;
        $config_pagination['display_pages'] = TRUE;
        $config_pagination['full_tag_open'] = '<ul class="pagination pagination-sm m-0 float-right">';
        $config_pagination['full_tag_close'] = '</ul>';
        $config_pagination['first_link'] = '<i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i>';
        $config_pagination['first_tag_open'] = '<li class="page-item">';
        $config_pagination['first_tag_close'] = '</li>';
        $config_pagination['last_link'] = '<i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i>';
        $config_pagination['last_tag_open'] = '<li class="page-item">';
        $config_pagination['last_tag_close'] = '</li>';
        $config_pagination['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config_pagination['next_tag_open'] = '<li class="page-item">';
        $config_pagination['next_tag_close'] = '</li>';
        $config_pagination['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config_pagination['prev_tag_open'] = '<li class="page-item">';
        $config_pagination['prev_tag_close'] = '</li>';
        $config_pagination['cur_tag_open'] = '<li class="page-item"><a href="" class="page-link active" style="background:#343A40;color:#fff;">';
        $config_pagination['cur_tag_close'] = '</a></li>';
        $config_pagination['num_tag_open'] = '<li class="page-item">';
        $config_pagination['num_tag_close'] = '</li>';
        $config_pagination['attributes'] = array('class' => "page-link");

        if ($this->uri->segment(3) == 'jd-form' || $this->uri->segment(3) == 'filter_jd_form') {
            $page_number = (isset($settings['page_no']) && $settings['page_no'] != '') ? $settings['page_no'] : (($this->uri->segment(5)) ? $this->uri->segment(5) : 0);
        } else {
            $page_number = (isset($settings['page_no']) && $settings['page_no'] != '') ? $settings['page_no'] : (($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
        }
        $config_pagination['cur_page'] = $page_number;
        $this->pagination->initialize($config_pagination);
        $response['pagination'] = $this->pagination->create_links();

        //$page_number = isset($settings['page_no']) ? $settings['page_no'] : (($this->uri->segment(4)) ? $this->uri->segment(4) : 0);

        if ($page_number > ceil(($config_pagination['total_rows'] / $config_pagination['per_page']))) {
            /* Redirect when page limit exceeded */
            redirect($config_pagination['base_url']);
        }

        /* GETTING A OFFSET */
        $offset = ($page_number == 0) ? 0 : ($page_number * $config_pagination['per_page']) - $config_pagination['per_page'];

        /* FOR SELECTING DATA WITH START AND END LIMIT */
        $response['start'] = $offset;
        $response['limit'] = $settings['per_page'];
        $response['no'] = $response['start'] + 1;
        return $response;
    }

    public function image_upload($image_path, $filename, $table_name = '', $id = '') {
        if (!is_dir($image_path)) {
            mkdir($image_path);
            @chmod($image_path, 0777);
        }
        if ($_FILES[$filename]['name'] != '') {
            if (isset($table_name) && $table_name != null) {
                $get_image = $this->production_model->get_all_with_where($table_name, '', '', array('id' => $id));
                if ($get_image != null && $get_image[0][$filename] != null && !empty($get_image[0][$filename])) {
                    @unlink($image_path . $get_image[0][$filename]);
                    @unlink($image_path . 'thumbnail/' . $get_image[0][$filename]);
                }
            }
            if ($_FILES[$filename]["size"] >= MAX_FILE_SIZE) {
                $this->session->set_flashdata('error', 'Maxfile upload' . (MAX_FILE_SIZE / 1000000) . 'MB');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $config['upload_path'] = $image_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($filename)) {
                $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect($_SERVER['HTTP_REFERER']);
            }
            $imageDetailArray = $this->upload->data();
            $this->production_model->generate_thumbnail($image_path, $imageDetailArray['file_name']);
            return $imageDetailArray['file_name'];
        } else {
            return '';
        }
    }

}
