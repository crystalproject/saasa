<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Faq extends CI_Controller {
        public function __construct() {
            parent::__construct();
        }
        public function index() {
            $condition = array('is_active'=>'1');
            $data['details'] = get_details('faq',$condition);

            $this->load->view('faq/index',$data);
        }
    }
?>