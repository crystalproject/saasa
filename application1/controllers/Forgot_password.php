<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Forgot_password extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $this->load->view('forgot-password', $data);
    }

    public function send_otp()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('temp', '', 'callback_check_data');
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $email = addslashes($this->input->post('email'));
                $mobile_number = addslashes($this->input->post('mobile_number'));
                $sub_query = ' email = "" AND mobile_number = "" ';
                if (strlen(trim($email)) > 0) {
                    $sub_query = ' email = "' . $email . '" ';
                } else {
                    $sub_query = ' mobile_number = "' . $mobile_number . '" ';
                }
                $sql = 'SELECT * FROM register WHERE ' . $sub_query . ' ';
                $info = $this->common_model->get_data_with_sql($sql);
                $info = $info['data'][0];
                $error = false;
                $otp = '';
                for ($i = 0; $i < 6; $i++) {
                    $otp .= mt_rand(0, 9);
                }
                if ($email == $info['email']) {
                    $conditions = array('where' => array('email' => $email));
                    $html = $this->load->view('email-template/forgot-password', array('otp' => $otp), true);
                    if (send_mail(array('to' => $email, 'subject' => SITE_TITLE . ': Reset Password', 'html' => $html))) {
                    } else {
                        $error = true;
                        $response_array['message'] = get_line('mail_not_sent');
                    }
                } else if ($mobile_number == $info['mobile_number']) {
                    $conditions = array('where' => array('mobile_number' => $mobile_number));

                    $cURLConnection = curl_init();
                    $message = urlencode("You One Time Password for reset password is: " . $otp);
                    $url = 'http://meapi.myvaluefirst.com/smpp/sendsms?username=shurfahttp&password=shurf@1181&to=+' . $this->input->post('country_code') . $this->input->post('mobile_number') . '&from=SHURFA&text=' . $message;
                    curl_setopt($cURLConnection, CURLOPT_URL, $url);
                    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
                    $phoneList = curl_exec($cURLConnection);
                    curl_close($cURLConnection);
                } else {
                    $error = true;
                    $response_array['message'] = get_line('something_wrong');
                }
                if (!$error) {
                    $records = array('forgot_pwd_token' => $otp);
                    $this->common_model->update_data('register', $records, $conditions);

                    $response_array['status'] = true;
                    $response_array['message'] = get_line('otp_sent');;
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function verify_otp()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('temp', '', 'callback_check_data');
            $this->form_validation->set_rules('otp', '', 'trim|numeric|required|min_length[6]|max_length[6]');
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $email = addslashes($this->input->post('email'));
                $mobile_number = addslashes($this->input->post('mobile_number'));
                $sub_query = ' email = "" AND mobile_number = "" ';
                if (strlen(trim($email)) > 0) {
                    $sub_query = ' email = "' . $email . '" ';
                } else {
                    $sub_query = ' mobile_number = "' . $mobile_number . '" ';
                }
                $sub_query .= ' AND forgot_pwd_token = "' . $this->input->post('otp') . '" ';
                $sql = 'SELECT * FROM register WHERE ' . $sub_query . ' ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    $response_array['message'] = get_line('otp_verified');
                } else {
                    $response_array['message'] = get_line('incorrect_otp');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function check_data()
    {
        $email = addslashes($this->input->post('email'));
        $mobile_number = addslashes($this->input->post('mobile_number'));
        if (strlen(trim($email)) <= 0 && strlen(trim($mobile_number)) <= 0) {
            $this->form_validation->set_message('check_data', get_line('email_mobile_not_available'));
            return false;
        } else if (strlen(trim($email)) > 0 && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->form_validation->set_message('check_data', get_line('please_enter_valid_email_address'));
            return false;
        } else {
            $sub_query = ' email = "" AND mobile_number = "" ';
            if (strlen(trim($email)) > 0) {
                $sub_query = ' email = "' . $email . '" ';
            } else {
                $sub_query = ' mobile_number = "' . $mobile_number . '" ';
            }
            $sql = 'SELECT id FROM register WHERE ' . $sub_query . ' ';
            $info = $this->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                return true;
            } else {
                $this->form_validation->set_message('check_data', get_line('email_mobile_not_available'));
                return false;
            }
        }
        return true;
    }

    public function reset_password()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('temp', '', 'callback_check_data');
            $this->form_validation->set_rules('otp', '', 'trim|numeric|required|min_length[6]|max_length[6]');
            $this->form_validation->set_rules('new_password', '', 'trim|numeric|required|min_length[6]|max_length[6]');
            $this->form_validation->set_rules('confirm_password', '', 'trim|numeric|required|min_length[6]|max_length[6]|matches[new_password]');
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $email = addslashes($this->input->post('email'));
                $mobile_number = addslashes($this->input->post('mobile_number'));
                $sub_query = ' email = "" AND mobile_number = "" ';
                if (strlen(trim($email)) > 0) {
                    $sub_query = ' email = "' . $email . '" ';
                } else {
                    $sub_query = ' mobile_number = "' . $mobile_number . '" ';
                }
                $sub_query .= ' AND forgot_pwd_token = "' . $this->input->post('otp') . '" ';
                $sql = 'SELECT * FROM register WHERE ' . $sub_query . ' ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $info = $info['data'][0];

                    $records = array();
                    $records['password'] = $this->encryption->encrypt($this->input->post('new_password'));
                    $records['forgot_pwd_token'] = '';
                    $records['modified_at'] = CURRENT_TIME;
                    $this->common_model->update_data('register', $records, array('where' => array('id' => $info['id'])));
                    $response_array['status'] = true;
                    $response_array['message'] = get_line('reset_pwd_successful');
                } else {
                    $response_array['message'] = get_line('incorrect_otp');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }
}
