<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Cart extends REST_Controller {

    private $_default_limit = 2;
    public $_language_default = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper('product_helper');
        $this->load->helper('cart_helper');
    }

    /**
     * Add product to the cart
     */
    public function add_product_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_language', '', 'trim|required|numeric|greater_than[0]', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_unique', '', 'trim|required', array('required' => get_line('something_wrong')));
            //$this->form_validation->set_rules('id_unique_attribute_values', '', 'trim|required', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $response_array = add_to_cart($form_data);
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

    /**
     * get cart product list
     */
    public function get_product_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => 'Call from not available in the request'));
            $this->form_validation->set_rules('id_language', '', 'trim|required|numeric|greater_than[0]', array('required' => 'language id not available in the request'));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => 'id user not available in the request'));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $this->_language_default = get_language_info($form_data['id_language']);
                $slug = '_' . $this->_language_default['slug'];
                if ($this->_language_default['id'] == '1') {
                    $slug = '';
                }
                $form_data['language_slug'] = $slug;
                $response_array = get_cart_products($form_data);
            }
        }
        $this->response($response_array);
    }

    /**
     * Update product quantity
     */
    public function update_quantity_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => 'Call from not available in the request'));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => 'id user not available in the request'));
            $this->form_validation->set_rules('id', '', 'trim|required|numeric|greater_than[0]', array('required' => 'cart id not available in the request'));
            $this->form_validation->set_rules('quantity', '', 'trim|required|numeric|greater_than[0]', array('required' => 'quantity available in the request'));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $sql = 'SELECT id_unique_product,id_unique_product_attributes FROM cart WHERE id = "' . $form_data['id'] . '" ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $info = $info['data'][0];
                    if (check_product_available_quantity($info['id_unique_product'], $info['id_unique_product_attributes'], $form_data['quantity'])) {
                        $conditions = array('where' => array('id' => $form_data['id'], 'id_user' => $form_data['id_user']));
                        $this->common_model->update_data('cart', array('quantity' => $form_data['quantity']), $conditions);
                        $response_array['status'] = true;
                        $response_array['message'] = $this->lang->line('product_quantity_updated');
                    } else {
                        $response_array['message'] = $this->lang->line('product_not_available');
                    }
                } else {
                    $response_array['message'] = $this->lang->line('product_not_available');
                }
            }
        }
        $this->response($response_array);
    }

    /**
     * Delete product from cart
     */
    public function delete_product_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => 'Call from not available in the request'));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => 'id user not available in the request'));
            $this->form_validation->set_rules('id', '', 'trim|required|numeric|greater_than[0]', array('required' => 'cart id not available in the request'));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $conditions = array('where' => array('id' => $form_data['id'], 'id_user' => $form_data['id_user']));
                $this->common_model->delete_data('cart', $conditions);
                $response_array['status'] = true;
                $response_array['message'] = get_line('product_removed_from_cart');
            }
        }
        $this->response($response_array);
    }

    /**
     * Empty whole cart for particular user
     */
    public function empty_cart_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => 'Call from not available in the request'));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => 'id user not available in the request'));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                empty_cart($form_data);
                $response_array['status'] = true;
                $response_array['message'] = get_line('empty_cart');
            }
        }
        $this->response($response_array);
    }

    /**
     * This function is used for getting total count of available product in cart
     */
    public function get_total_product_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => array('total_product' => 0));
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules('call_from', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_user', '', 'trim|required|numeric|greater_than[0]', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                /*$sql = 'SELECT COUNT(id) AS total_product FROM cart WHERE id_user = "' . $form_data['id_user'] . '" ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['data']['total_product'] = isset($info['data'][0]['total_product']) ? (int) $info['data'][0]['total_product'] : 0;
                }*/
                $sql = 'SELECT SUM(quantity) AS total_product FROM `cart` WHERE id_user = "' . $form_data['id_user'] . '" ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['data']['total_product'] = isset($info['data'][0]['total_product']) ? (int) $info['data'][0]['total_product'] : 0;
                }
                $response_array['status'] = true;
                $response_array['message'] = '';
            }
        }
        $this->response($response_array);
    }
}
