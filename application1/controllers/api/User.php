<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class User extends REST_Controller {

    public $_language_default = array();    

    public function __construct() {
        parent::__construct();        
    }

    public function get_token_post() {
        $error = false;
        $response_array = array('status' => false, 'message' => '', 'data' => array('bag_info' => null));
        $request_data = check_headers_and_data();
        if (!$request_data['error']) {
            $form_data = $request_data['form_data'];
            $this->form_validation->set_data($form_data);            
            $this->form_validation->set_rules('id_user', '', 'trim|required|greater_than[0]', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $sql = 'SELECT * FROM notification_token WHERE user_id = "'.$form_data['id_user'].'" ORDER BY id DESC LIMIT 1 '; 
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    $response_array['data'] = array('info' => $info['data']);
                }
            }
        } else {
            $response_array['message'] = $request_data['message'];
        }
        $this->response($response_array);
    }

}