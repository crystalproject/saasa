<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . '/libraries/REST_Controller.php');

class Category extends REST_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index_post() {
        $error = false;
        $response_array = array('result' => false);
        $form_data = array();
        $headers = $this->input->request_headers();
        if (check_api_key($headers)) {
            $form_data = $this->input->post('data');
            $form_data = str_replace('\n', '', $form_data);
            $form_data = str_replace('\"', '"', $form_data);
            $form_data = str_replace('"{', '{', $form_data);
            $form_data = str_replace('}"', '}', $form_data);
            write_log('api/ecommerce/Category.php', 'index_post()', $form_data);

            if (!empty($form_data) && is_json_string($form_data)) {
                $form_data = (array) json_decode($form_data);
            } else {
                $response_array['message'] = 'json data is not correct';
                $error = true;
            }
        } else {
            $response_array['message'] = 'api key is not correct';
            $error = true;
        }
        $set_language = isset($form_data['language_type']) && $form_data['language_type'] != null ? $form_data['language_type'] : DEFAULT_LANGUAGE;
        $language_type = load_language($set_language);
        if (!$error) {
            $required_array = array(
                'language_type' => $this->lang->line('language_type_not_available_request'),
            );
            foreach ($required_array as $key => $value) {
                if (!array_key_exists($key, $form_data) || strlen(trim($form_data[$key])) <= 0) {
                    $error = true;
                    $response_array['message'] = $value;
                    break;
                }
            }
        }
        if (!$error) {
            $banner = array();
            $category = array();

            $get_category = get_details('category', array('parent_id' => 0, 'is_active' => 1));
            $get_banner = get_details('category_banner', array('is_active' => 1,'banner_for'=>'1'));
            if (isset($get_banner) && $get_banner != null) {
                foreach ($get_banner as $key => $value) {
                    $banner_response[$key]['id'] = $value['id'];
                    $banner_response[$key]['title'] = $value['title'];
                    $banner_response[$key]['title_turkish'] = $value['title_turkish'];
                    $banner_response[$key]['description'] = $value['description'];
                    $banner_response[$key]['description_turkish'] = $value['description_turkish'];
                    $banner_response[$key]['image'] = isset($value['image']) && $value['image'] != null ? base_url(CATEGORY_BANNER . $value['image']) : '';
                }
                $response_array['result'] = true;
                $banner['banner'] = $banner_response;
            } else {
                $banner['banner'] = null;
            }

            /* Category details */
            if (isset($get_category) && $get_category != null) {
                foreach ($get_category as $key => $value) {
                    $get_sub_category = count(get_details('category', array('parent_id' => $value['id'], 'is_active' => 1)));
                    $category_response[$key]['id'] = $value['id'];
                    $category_response[$key]['name'] = $value['name'];
                    $category_response[$key]['name_turkish'] = $value['name_turkish'];
                    $category_response[$key]['image'] = isset($value['image']) && $value['image'] != null ? base_url(CATEGORY_IMAGE . $value['image']) : '';
                    $condition = array('id_category' => $value['id'], 'id_language' => $set_language);
                    $details_description = get_details('category_description', $condition);
                    if (!empty($details_description)) {
                        $category_response[$key]['description'] = isset($details_description[0]['description']) ? $details_description[0]['description'] : '';
                    } else {
                        $category_response[$key]['description'] = '';
                    }
                    $category_response[$key]['have_sub_category'] = (isset($get_sub_category) && $get_sub_category != null && $get_sub_category > 0) ? true : false;
                }
                $response_array['result'] = true;
                $category['category'] = $category_response;
            } else {
                $category['category'] = null;
            }
            $response_array['response'] = array_merge($banner, $category);
        }
        $this->response($response_array);
    }

    public function sub_category_post() {
        $error = false;
        $response_array = array('result' => false);
        $form_data = array();
        $headers = $this->input->request_headers();
        if (check_api_key($headers)) {
            $form_data = $this->input->post('data');
            $form_data = str_replace('\n', '', $form_data);
            $form_data = str_replace('\"', '"', $form_data);
            $form_data = str_replace('"{', '{', $form_data);
            $form_data = str_replace('}"', '}', $form_data);
            write_log('api/ecommerce/Category.php', 'index_post()', $form_data);

            if (!empty($form_data) && is_json_string($form_data)) {
                $form_data = (array) json_decode($form_data);
            } else {
                $response_array['message'] = 'json data is not correct';
                $error = true;
            }
        } else {
            $response_array['message'] = 'api key is not correct';
            $error = true;
        }
        $set_language = isset($form_data['language_type']) && $form_data['language_type'] != null ? $form_data['language_type'] : DEFAULT_LANGUAGE;
        $parent_id = isset($form_data['parent_id']) && $form_data['parent_id'] != null ? $form_data['parent_id'] : 0;
        $language_type = load_language($set_language);
        if (!$error) {
            $required_array = array(
                'language_type' => $this->lang->line('language_type_not_available_request'),
                'parent_id' => $this->lang->line('parent_id_not_available_request'),
            );
            foreach ($required_array as $key => $value) {
                if (!array_key_exists($key, $form_data) || strlen(trim($form_data[$key])) <= 0) {
                    $error = true;
                    $response_array['message'] = $value;
                    break;
                }
            }
        }
        if (!$error) {
            $banner = array();
            $category = array();

            $get_category = get_details('category', array('parent_id' => $parent_id, 'is_active' => 1));
            $get_banner = get_details('category_banner', array('is_active' => 1,'banner_for'=>2));
            if (isset($get_banner) && $get_banner != null) {
                foreach ($get_banner as $key => $value) {
                    $banner_response[$key]['id'] = $value['id'];
                    $banner_response[$key]['title'] = $value['title'];
                    $banner_response[$key]['title_turkish'] = $value['title_turkish'];
                    $banner_response[$key]['description'] = $value['description'];
                    $banner_response[$key]['description_turkish'] = $value['description_turkish'];
                    $banner_response[$key]['image'] = isset($value['image']) && $value['image'] != null ? base_url(CATEGORY_BANNER . $value['image']) : '';
                }
                $response_array['result'] = true;
                $banner['banner'] = $banner_response;
            } else {
                $banner['banner'] = null;
            }

            /* Category details */
            if (isset($get_category) && $get_category != null) {
                foreach ($get_category as $key => $value) {
                    $get_sub_category = count(get_details('category', array('parent_id' => $value['id'], 'is_active' => 1)));
                    $category_response[$key]['id'] = $value['id'];
                    $category_response[$key]['name'] = $value['name'];
                    $category_response[$key]['name_turkish'] = $value['name_turkish'];
                    $category_response[$key]['image'] = isset($value['image']) && $value['image'] != null ? base_url(CATEGORY_IMAGE . $value['image']) : '';

                    $condition = array('id_category' => $value['id'], 'id_language' => $set_language);
                    $details_description = get_details('category_description', $condition);
                    if (!empty($details_description)) {
                        $category_response[$key]['description'] = isset($details_description[0]['description']) ? $details_description[0]['description'] : '';
                    } else {
                        $category_response[$key]['description'] = '';
                    }

                    $category_response[$key]['have_sub_category'] = (isset($get_sub_category) && $get_sub_category != null && $get_sub_category > 0) ? true : false;
                }
                $response_array['result'] = true;
                $category['category'] = $category_response;
            } else {
                $category['category'] = null;
            }
            $response_array['response'] = array_merge($banner, $category);
        }
        $this->response($response_array);
    }

}
