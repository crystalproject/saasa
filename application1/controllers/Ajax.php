<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Ajax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_attribute_image()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id_unique', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('id_unique_product', '', 'trim|required', array('required' => get_line('something_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $id_unique_attribute_values = $this->input->post('id_unique');
                $id_unique_attribute_values = explode(',', $id_unique_attribute_values);
                $id_unique_attribute_values = "'" . implode("','", $id_unique_attribute_values) . "'";
                $sql = "SELECT id_unique FROM product_attributes WHERE id_unique_product = '" . $this->input->post('id_unique_product') . "' AND id_unique_attribute_values IN (" . $id_unique_attribute_values . ") ";
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    if ($info['row_count'] == 1) {
                        $id_unique = $info['data'][0]['id_unique'];
                    } else {
                        $id_uniques = array();
                        foreach ($info['data'] as $value) {
                            $id_uniques[$value['id_unique']][] = 1;
                        }
                        $count = 0;
                        foreach ($info['data'] as $value) {
                            if (count($id_uniques[$value['id_unique']]) >  $count) {
                                $count = count($id_uniques[$value['id_unique']]);
                                $id_unique = $value['id_unique'];
                            }
                        }
                    }

                    $sql = 'SELECT image_name FROM product_attributes_image WHERE id_unique_product_attributes = "' . $id_unique . '" ';
                    $info = $this->common_model->get_data_with_sql($sql);

                    if ($info['row_count'] > 0) {
                        /*$html = '<ul id="glasscase" class="gc-start">';
                        foreach ($info['data'] as $value) {
                            $html .= '<li><img src="' . base_url('uploads/product/' . $value['image_name']) . '"></li>';
                        }
                        $html .= '</ul>';*/
                        $response_array['status'] = true;
                        $response_array['data'] = base_url('uploads/product/' . $info['data'][0]['image_name']);
                    }
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function filter_product()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        $this->_default_limit = 12;

        $form_data = $this->input->post();

        $form_data['start'] = isset($form_data['start_from']) ? $form_data['start_from'] : 0;
        $form_data['limit'] = isset($form_data['limit']) ? $form_data['limit'] : $this->_default_limit;

        $form_data['view_by'] = isset($form_data['view_by']) ? $form_data['view_by'] : 'all';
        $slug = get_language_front();
        $sub_query = '';
        if ($form_data['view_by'] == 'discounted') {
            $form_data['is_popular'] = 0;
            $form_data['id_category'] = 0;
            $sub_query .= ' AND id_unique IN (SELECT DISTINCT(id_unique_product) AS id_unique_product FROM product_prices WHERE discount > 0) ';
        }
        if (isset($form_data['search_value']) && $form_data['search_value'] != null) {
            $sub_query .= ' AND (title LIKE "%' . $form_data['search_value'] . '%" OR description LIKE "%' . $form_data['search_value'] . '%") ';
        }
        if (isset($form_data['is_popular']) && $form_data['is_popular'] == '1') {
            $sub_query .= ' AND is_popular = "1" ';
        }
        if (isset($form_data['id_category']) && $form_data['id_category'] > 0) {
            $GLOBALS['child_category_ids'] = array($form_data['id_category']);
            //Uncomment this line when need to display all sub category product
            //get_child_category_ids($GLOBALS['child_category_ids'][0]);
            $child_category_ids = $GLOBALS['child_category_ids'];
            $sub_query .= ' AND id_category IN (' . implode(',', $child_category_ids) . ') ';
        }

        $attribute_value_ids = isset($form_data['attribute_id']) ? $form_data['attribute_id'] : array();
        if (!empty($attribute_value_ids)) {
            $info = $this->common_model->select_data('product_attributes', array('select' => 'id_unique_product', 'where' => array('id_unique_product !=' => null), 'IN' => array('id_unique_attribute_values' => $attribute_value_ids)));
            if ($info['row_count'] > 0) {
                $id_unique_product = array_unique(array_column($info['data'], 'id_unique_product'));
                $id_unique_product = '"' . implode('","', $id_unique_product) . '"';
                $sub_query .= ' AND id_unique IN (' . $id_unique_product . ') ';
            } else {
                $id_unique_product = null;
                $sub_query .= ' AND id_unique IS NULL ';
            }
        }

        $sortby = !empty($form_data['sort']) ? $form_data['sort'] : 'id_unique ASC';

        $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
        $sql = 'SELECT id_unique,is_new,title,main_image,slug,has_attributes,by_quantity_or_availability' . $sub_sub_query . ' FROM product WHERE id_language = "' . get_language_id() . '" ' . $sub_query . ' ORDER BY ' . $sortby . ' LIMIT ' . $form_data['start'] . ',' . $form_data['limit'] . ' ';
        $info = $this->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $response_array['status'] = true;
            foreach ($info['data'] as &$value) {
                if ($value['image_name'] != null) {
                    $value['image_name'] = base_url('uploads/product/') . $value['image_name'];
                }
                if ($value['main_image'] != null) {
                    $value['main_image'] = base_url('uploads/product/') . $value['main_image'];
                }
                $value['other_info'] = get_product_detail($value, get_language_id());
                //unset($value['has_attributes']);
                unset($value['by_quantity_or_availability']);
                unset($value['category_name']);
            }
            $response_array['data'] = $info['data'];
            unset($value);
        } else {
            // $response_array['status'] = true;
            // $response_array['data'] = null;
            // $response_array['message'] = get_line('product_not_available');        
        }
        $response_array['status'] = true;
        $data = array();
        $data['current_url'] = $this->input->post('current_url');
        $data['details'] = $response_array['data'];
        $response_array['data'] = $this->load->view('product/listing', $data, true);
        echo json_encode($response_array);
        exit;
    }

    public function save_comment()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id_unique', '', 'trim|required', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('first_name', '', 'trim|required', array('required' => get_line('name') . ' ' . get_line('required')));
            $this->form_validation->set_rules('email_address', '', 'trim|required|valid_email', array('required' => get_line('email_address') . ' ' . get_line('required')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $records  = array(
                    'id_unique_product' => $this->input->post('id_unique'),
                    'first_name' => $this->input->post('first_name'),
                    'email_address' => $this->input->post('email_address'),
                    'comment' => $this->input->post('comment'),
                    'rating' => $this->input->post('rating'),
                );
                $conditions = array('where' => array('id_unique_product' => $records['id_unique_product'], 'email_address' => $records['email_address']));
                $info = $this->common_model->select_data('product_review', $conditions);
                if ($info['row_count'] > 0) {
                    $this->common_model->update_data('product_review', $records, $conditions);
                } else {
                    $this->common_model->insert_data('product_review', $records);
                }
                $response_array['message'] = get_line('review_add');
                $response_array['status'] = true;
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function send_otp()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('country_code', '', 'trim|required|numeric');
            $this->form_validation->set_rules('mobile_number', '', 'trim|required|numeric|min_length[6]|max_length[15]|is_unique[register.mobile_number]', array('required' => get_line('mobile_number') . ' ' . get_line('required')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $response_array['status'] = true;
                $otp = '';
                for ($i = 0; $i < 6; $i++) {
                    $otp .= mt_rand(0, 9);
                }
                $this->common_model->delete_data('otp_verify', array('where' => array('mobile_number' => $this->input->post('mobile_number'))));
                $this->common_model->insert_data('otp_verify', array('mobile_number' => $this->input->post('mobile_number'), 'otp' => $otp));

                $cURLConnection = curl_init();
                $message = urlencode("You One Time Password for customizer is: " . $otp);

                $url = 'http://meapi.myvaluefirst.com/smpp/sendsms?username=shurfahttp&password=shurf@1181&to=+' . $this->input->post('country_code') . $this->input->post('mobile_number') . '&from=SHURFA&text=' . $message;
                curl_setopt($cURLConnection, CURLOPT_URL, $url);
                curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
                $phoneList = curl_exec($cURLConnection);
                curl_close($cURLConnection);

                $response_array['message'] = get_line('otp_sent');;
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function send_otp_login()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('country_code', '', 'trim|required|numeric');
            $this->form_validation->set_rules('mobile_number', '', 'trim|required|numeric|min_length[6]|max_length[15]', array('required' => get_line('mobile_number') . ' ' . get_line('required')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $conditions = array('where' => array('mobile_number' => $this->input->post('mobile_number')));
                $info = $this->common_model->select_data('register', $conditions);
                if ($info['row_count'] > 0) {
                    $response_array['status'] = true;
                    $otp = '';
                    for ($i = 0; $i < 6; $i++) {
                        $otp .= mt_rand(0, 9);
                    }
                    $this->common_model->delete_data('otp_verify', array('where' => array('mobile_number' => $this->input->post('mobile_number'))));
                    $this->common_model->insert_data('otp_verify', array('mobile_number' => $this->input->post('mobile_number'), 'otp' => $otp));

                    $cURLConnection = curl_init();
                    $message = urlencode("You One Time Password for customizer is: " . $otp);

                    $url = 'http://meapi.myvaluefirst.com/smpp/sendsms?username=shurfahttp&password=shurf@1181&to=+' . $this->input->post('country_code') . $this->input->post('mobile_number') . '&from=SHURFA&text=' . $message;
                    curl_setopt($cURLConnection, CURLOPT_URL, $url);
                    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
                    $phoneList = curl_exec($cURLConnection);
                    curl_close($cURLConnection);

                    $response_array['message'] = get_line('otp_sent');;
                } else {
                    $response_array['message'] = get_line('user_not_available');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function verify_user()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('country_code', '', 'trim|required|numeric');
            $this->form_validation->set_rules('mobile_number', '', 'trim|required|numeric|min_length[6]|max_length[15]|is_unique[register.mobile_number]', array('required' => get_line('mobile_number') . ' ' . get_line('required')));
            $this->form_validation->set_rules('otp', '', 'trim|required|numeric|min_length[6]|max_length[6]|is_unique[register.mobile_number]', array('required' => get_line('otp') . ' ' . get_line('required')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $conditions = array(
                    'where' => array('mobile_number' => $this->input->post('mobile_number'), 'otp' => $this->input->post('otp'))
                );
                $info = $this->common_model->select_data('otp_verify', $conditions);
                if ($info['row_count'] > 0) {
                    $records = array(
                        'mobile_number' => $this->input->post('mobile_number'),
                        'is_account_confirm' => '1',
                        'created_at' => CURRENT_TIME,
                        'modified_at' => CURRENT_TIME,
                    );
                    $last_insert_id = $this->common_model->insert_data('register', $records);
                    if ($last_insert_id > 0) {
                        $this->common_model->delete_data('otp_verify', array('where' => array('mobile_number' => $this->input->post('mobile_number'))));

                        $session = array(
                            'login_id' => $last_insert_id,
                            'username' => '',
                            'useremail' => '',
                            'useremobile' => $this->input->post('mobile_number'),
                        );
                        $this->session->set_userdata($session);

                        $response_array['status'] = true;
                        $response_array['message'] = get_line('registration_success_otp');
                        $response_array['redirect_url'] = base_url();
                    } else {
                        $response_array['message'] = get_line('something_wrong');
                    }
                } else {
                    $response_array['message'] = get_line('incorrect_otp');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function mobile_signin()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('mobile_number', '', 'trim|required|numeric|min_length[6]|max_length[15]', array('required' => get_line('mobile_number') . ' ' . get_line('required')));
            $this->form_validation->set_rules('otp', '', 'trim|required|numeric|min_length[6]|max_length[6]', array('required' => get_line('otp') . ' ' . get_line('required')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $value) {
                    $response_array['message'] = $value;
                    break;
                }
            } else {
                $conditions = array('where' => array('mobile_number' => $this->input->post('mobile_number')));
                $info = $this->common_model->select_data('register', $conditions);
                if ($info['row_count'] > 0) {
                    $user_info = $info['data'][0];
                    $conditions = array('where' => array('mobile_number' => $this->input->post('mobile_number'),'otp' => $this->input->post('otp')));
                    $info = $this->common_model->select_data('otp_verify', $conditions);
                    if ($info['row_count'] > 0) {
                        $response_array['status'] = true;
                        $this->common_model->delete_data('otp_verify', array('where' => array('mobile_number' => $this->input->post('mobile_number'))));
                        
                        $session = array(
                            'login_id' => $user_info['id'],
                            'username' => $user_info['first_name'],
                            'useremail' => $user_info['email'],
                            'useremobile' => $this->input->post('mobile_number'),
                        );
                        $this->session->set_userdata($session);

                        $conditions = array(
							'where' => array('id_user' => COOKIE_USER)
						);
						$this->common_model->update_data('cart', array('id_user' => $session['login_id']), $conditions);

						$conditions = array(
							'where' => array('login_id' => COOKIE_USER)
						);
						$this->common_model->update_data('wishlist', array('login_id' => $session['login_id']), $conditions);
                        $response_array['message'] = get_line('login_success_wait');
                    } else {
                        $response_array['message'] = get_line('incorrect_otp');
                    }
                } else {
                    $response_array['message'] = get_line('user_not_available');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }
}
