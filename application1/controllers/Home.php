<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Home extends CI_Controller {
        public function __construct() {
            parent::__construct();
        }
        public function index() {
        	$condition = array('is_active'=>'1');
            $data['slider_details'] = get_details('home_slider',$condition);

            $condition = array('parent_id'=>0,'is_active'=>'1');
        	$data['category_details'] = get_details('category',$condition);

            $this->load->view('index',$data);
        }

        function change_language() {    
            $response_array = array('status' => false, 'message' => '');
            $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => 'This field is required'));
            $data = array();
            if ($this->form_validation->run() === FALSE) {
                $data = array_merge($data, $_POST);
            } else {              
                if ($this->input->post('id_language') == 1) {
                    $this->session->language = 1;
                }
                else if ($this->input->post('id_language') == 2) {
                    $this->session->language = 2;
                }
                $response_array['status'] = true;
            }
            echo json_encode($response_array);
            exit;
        }
    }
?>