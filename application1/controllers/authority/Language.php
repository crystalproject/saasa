<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->language_info = array();
            redirect(base_url('authority/language'));
        }
        $data = array();
        $tmp_data = get_details('language');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/language/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;

        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('language', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        $this->load->view('authority/language/view', $data);
    }

    function add_edit($id = '') {
        $message = '';
        $data = $this->input->post();
        $details = array();
        if (isset($id) && $id != null) {
            $condition = array('id' => $id);
            $details = get_details('language', $condition);
        }
        if ($this->input->method() == 'post') {
            if ($id == '') { // Add
                if ($this->input->method() == 'post') {
                    $this->form_validation->set_rules('name', 'name', 'required|is_unique[language.name]', array('required' => 'Please enter language name', "is_unique" => "This name is already available"));
                    if ($this->form_validation->run() == FALSE) {
                        $data = array_merge($data, $this->form_validation->error_array());
                    } else {
                        $record = $this->production_model->insert_record('language', $data);
                        $message = 'add';
                    }
                }
            } else { // Edit       
                $this->form_validation->set_rules('name', 'name', 'required|is_unique_with_except_record[language.name.id.' . $data['id'] . ']', array('required' => 'Please enter language name', "is_unique_with_except_record" => "This name is already available"));
                if ($this->form_validation->run() == FALSE) {
                    $data = array_merge($details[0], $this->form_validation->error_array());
                } else {
                    $record = $this->production_model->update_record('language', $data, array('id' => $id));
                    $message = 'edit';
                }
            }
            if (isset($message) && $message != '') {
                if ($message == 'add') {
                    $this->session->set_flashdata('success', $this->lang->line('added_successfully'));
                } elseif ($message == 'edit') {
                    $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
                }
                redirect(base_url('authority/language'));
            }
        }
        $data = isset($details) && $details != null ? $details[0] : $this->input->post();
        $this->load->view('authority/language/add-edit', $data);
    }

    function filter() {
        $this->session->language_info = $_POST;
        $name = isset($this->session->language_info['name']) ? $this->session->language_info['name'] : '';
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('name', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post();
        $tmp_data = get_details('language');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/language/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('name', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('language', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        ob_start();
        if (isset($filteredData) && !empty($filteredData)) {
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                $current_language = get_details('language', array('default_language' => '1'));
                ?>
                <tr>
                    <td style="width: 10px;">
                        <?php
                        if (isset($current_language) && $current_language != null && $current_language[0]['id'] != $id && $id != 1) {
                            ?>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id; ?>" value="<?= $id ?>">
                                <label for="customCheckbox<?= $id; ?>" class="custom-control-label"></label>
                            </div>
                        <?php }
                        ?>
                    </td>
                    <td><?= $value['name']; ?></td>
                    <td>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio<?= $key + 1; ?>" name="default_language" value="<?= $id; ?>" <?= isset($current_language) && $current_language != null && $current_language[0]['id'] == $id ? 'checked' : ''; ?>>
                            <label for="customRadio<?= $key + 1; ?>" class="custom-control-label"></label>
                        </div>
                    </td>
                    <td>
                        <?php
                        if (isset($current_language) && $current_language != null && $id != 1) {
                            ?>
                            <a href="<?= base_url('authority/language_info/edit/' . $id); ?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>
                            <?php
                        }

                        if (isset($current_language) && $current_language != null && $current_language[0]['id'] != $id && $id != 1) {
                            ?>
                            <a href="javascript:void(0)" data-table="language" data-image-path="" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a>
                                <?php
                            }

                            if (isset($current_language) && $current_language != null && $id != 1) {
                                if ($value['is_active'] == '1') {
                                    echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status" data-table="language" data-id="' . $id . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                                } else {
                                    echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status" data-table="language" data-id="' . $id . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                }
                            }
                            ?>
                    </td>
                </tr>
                <?php
            }
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();
            $response_array['pagination'] = $data['pagination'];
        } else {
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">' . $this->lang->line('records_not_found') . '</td>
                                            </tr>';
            $response_array['pagination'] = '';
        }
        echo json_encode($response_array);
        exit;
    }

    function set_language() {
        $data = $this->input->post();
        $update_data = array('default_language' => '1');
        $this->production_model->update_record('language', array('default_language' => '0'), array('default_language' => '1'));

        $record = $this->production_model->update_record('language', $update_data, array('id' => $data['id']));
        if ($record == 1) {
            $response_array['success'] = true;
            $response_array['message'] = $this->lang->line('updated_successfully');
        } else {
            $response_array['error'] = true;
            $response_array['message'] = $this->lang->line('not_updated');
        }
        echo json_encode($response_array);
        exit;
    }

}