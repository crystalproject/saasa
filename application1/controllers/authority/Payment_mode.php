<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * This class is use for all the common ajax request.
 */
class Payment_mode extends CI_Controller
{

    public $_table = 'payment_mode';
    public $_slug = 'payment-mode';
    public $_language_info = array();
    public $_language_default = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');

        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                break;
            }
        }
    }

    public function index()
    {
        $data = array();

        $conditions = array('WHERE' => array('id_language' => $this->_language_default['id']));
        $info = $this->common_model->select_data($this->_table, $conditions);
        $data['info'] = array();
        if ($info['row_count'] > 0) {
            $data['info'] = $info['data'];
        }
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/view', $data);
    }


    public function update_description()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('id_unique', '', 'trim|required|numeric|greater_than[0]', array('required' => get_line('something_wrong')));
            $this->form_validation->set_rules('description', '', 'trim|required|max_length[500]', array('required' => get_line('description') . ' ' . get_line('required')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $error_value) {
                    $response_array['message'] = $error_value;
                    break;
                }
            } else {
                $conditions = array('where' => array('id_unique' => $this->input->post('id_unique'), 'id_language' => $this->_language_default['id']));
                $this->common_model->update_data('payment_mode', array('description' => $this->input->post('description')), $conditions);
                $response_array['status'] = true;
                $response_array['message'] = get_line('updated_successfully');
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function change_status()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $error = false;
        $response_array = array('success' => false);
        $required_fields = array(
            'id' => $this->lang->line('something_wrong'),
            'table' => $this->lang->line('something_wrong'),
            'current_status' => $this->lang->line('something_wrong'),
        );
        foreach ($required_fields as $key => $value) {
            if (strlen(trim($this->input->post($key))) <= 0) {
                $response_array['msg'] = $value;
                $error = true;
                break;
            }
        }
        if (!$error) {
            $conditions = array('WHERE' => array('id_language' => $this->_language_default['id'], 'is_active' => '1'));
            $info = $this->common_model->select_data($this->_table, $conditions);
            $data['info'] = array();
            if ($this->input->post('current_status') == '0') {
                $status = '1';
            }
            if ($this->input->post('current_status') == '1') {
                $status = '0';
            }
            if ($status == '0' && $info['row_count'] > 1) {
                $records = array(
                    'is_active' => $status,
                );
                $conditions = array(
                    "where" => array("id_unique" => $this->input->post('id')),
                );
                $this->common_model->update_data($this->_table, $records, $conditions);
                $response_array['success'] = true;
            } else if ($status == '1') {
                $records = array(
                    'is_active' => $status,
                );
                $conditions = array(
                    "where" => array("id_unique" => $this->input->post('id')),
                );
                $this->common_model->update_data($this->_table, $records, $conditions);
                $response_array['success'] = true;
            } else {
                $response_array['message'] = get_line('one_payment_mode_is_compulsory');
            }
        }
        echo json_encode($response_array);
        exit;
    }
}
