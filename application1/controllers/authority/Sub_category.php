<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sub_Category extends CI_Controller
{

    public $_language_info = array();
    public $_language_default = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_check_model');

        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                break;
            }
        }
    }

    public function index()
    {
        if ($this->input->get('clear-search') == 1) {
            $this->session->sub_category_info = array();
            redirect(base_url('authority/sub-category'));
        }
        $data = array();
        $tmp_data = get_details('category');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/sub-category/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;

        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('category', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        $this->load->view('authority/sub_category/view', $data);
    }

    function add_edit($id = '')
    {
        $message = '';
        $data = $this->input->post();
        $details = array();
        if (isset($id) && $id != null) {
            $condition = array('id' => $id);
            $details = get_details('category', $condition);
            $condition = array('id_category' => $id);
            $details_description = get_details('category_description', $condition);
        }
        if ($this->input->method() == 'post') {
            if ($id == '') { // Add
                if ($this->input->method() == 'post') {
                    $this->form_validation->set_rules('parent_id', 'category', 'required', array('required' => $this->lang->line('please_select_value')));
                    $this->form_validation->set_rules('name', 'Name', 'required|callback_exists_in_name', array('required' => $this->lang->line('this_is_required_field')));
                    if ($this->input->post('data_level') < 2) {
                        //$this->form_validation->set_rules('banner', 'banner', 'callback_banner_selected');
                        //$this->form_validation->set_rules('banner_hindi', 'banner_hindi', 'callback_banner_tmp_selected');
                        //$this->form_validation->set_rules('image', 'image', 'callback_file_selected');
                    }

                    $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('id !=' => 1, 'is_active' => 1));
                    if (isset($language) && $language != null) {
                        foreach ($language as $key => $value) {
                            $newname = strtolower('name_' . $value['slug']);
                            $column_name[] = $newname;
                            $get_table_column = array();
                            $fields = $this->db->field_data('category');
                            if (isset($fields) && $fields != null) {
                                foreach ($fields as $key => $field) {
                                    $get_table_column[] = $field->name;
                                }
                            }
                            $this->form_validation->set_rules($newname, $newname, 'required|max_length[50]', array('required' => $this->lang->line('this_is_required_field') . ' ' . (strtolower($value['name'])) . ''));
                        }
                        if (isset($column_name) && $column_name != null) {
                            foreach ($column_name as $key => $column_row) {
                                if (!in_array($column_row, $get_table_column)) {
                                    /* New column create in table */
                                    $this->db->query('ALTER TABLE `category` ADD ' . $column_row . ' VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER parent_id');
                                }
                            }
                        }
                    }
                    if ($this->form_validation->run() == FALSE) {
                        $data = array_merge($data, $this->form_validation->error_array());
                    } else {
                        $files = array('image', 'image_hindi', 'banner', 'banner_hindi');
                        if ($this->input->post('data_level') < 2) {
                            $data['image'] = $this->production_model->image_upload(CATEGORY_IMAGE, 'image');
                            $data['image_hindi'] = $this->production_model->image_upload(CATEGORY_IMAGE, 'image_hindi');
                            //$data['banner'] = $this->production_model->image_upload(CAT_SUB_CAT_BANNER, 'banner');
                            //$data['banner_hindi'] = $this->production_model->image_upload(CAT_SUB_CAT_BANNER, 'banner_hindi');
                        } else {
                            foreach ($files as $key => $value) {
                                $data[$value] = "";
                            }
                        }
                        // $data['image_hindi'] = $this->production_model->image_upload(CATEGORY_IMAGE, 'image_hindi');
                        // $data['banner'] = $this->production_model->image_upload(CAT_SUB_CAT_BANNER, 'banner');
                        // $data['banner_hindi'] = $this->production_model->image_upload(CAT_SUB_CAT_BANNER, 'banner_hindi');

                        foreach ($language as $key => $value) {
                            if (isset($data['description_' . $value['id']])) {
                                unset($data['description_' . $value['id']]);
                            }
                            if (isset($data['description_1'])) {
                                unset($data['description_1']);
                            }
                        }
                        $data['slug'] = strtolower(url_title($data['name']));
                        $data['slug_hindi'] = strtolower(url_title($data['name_hindi']));
                        unset($data['data_level']);
                        $record = $this->production_model->insert_record('category', $data);
                        if ($record > 0) {
                            $records = array();
                            $records[] = array(
                                'id_category' => $record,
                                'id_language' => 1,
                                'description' => $this->input->post('description_1'),
                            );
                            foreach ($language as $key => $value) {
                                $records[] = array(
                                    'id_category' => $record,
                                    'id_language' => $value['id'],
                                    'description' => $this->input->post('description_' . $value['id']),
                                );
                            }
                            if (!empty($records)) {
                                $this->common_model->insert_data('category_description', $records, true);
                            }
                        }
                        $message = 'add';
                    }
                }
            } else { // Edit    
                $this->form_validation->set_rules('parent_id', 'category', 'required', array('required' => $this->lang->line('please_select_value')));
                $this->form_validation->set_rules('name', 'Name', 'required|callback_exists_in_name', array('required' => $this->lang->line('this_is_required_field')));

                $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('id !=' => 1, 'is_active' => 1));
                if (isset($language) && $language != null) {
                    foreach ($language as $key => $value) {
                        $newname = strtolower('name_' . $value['name']);
                        $column_name[] = $newname;
                        $get_table_column = array();
                        $fields = $this->db->field_data('category');
                        if (isset($fields) && $fields != null) {
                            foreach ($fields as $key => $field) {
                                $get_table_column[] = $field->name;
                            }
                        }
                        $this->form_validation->set_rules($newname, $newname, 'required|max_length[50]', array('required' => $this->lang->line('this_is_required_field') . (strtolower($value['name'])) . ''));
                    }
                    if (isset($column_name) && $column_name != null) {
                        foreach ($column_name as $key => $column_row) {
                            if (!in_array($column_row, $get_table_column)) {
                                /* New column create in table */
                                $this->db->query('ALTER TABLE `category` ADD ' . $column_row . ' VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER parent_id');
                            }
                        }
                    }
                }
                if ($this->form_validation->run() == FALSE) {
                    $data = array_merge($details[0], $this->form_validation->error_array());
                } else {
                    $data['parent_id'] = ($id == $data['parent_id'] ? 0 : $data['parent_id']);
                    /*if ($_FILES['image']['name'] != '') {
                        $data['image'] = $this->production_model->image_upload(CATEGORY_IMAGE, 'image', 'category', $id);
                    }
                    if ($_FILES['image_hindi']['name'] != '') {
                        $data['image_hindi'] = $this->production_model->image_upload(CATEGORY_IMAGE, 'image_hindi', 'category', $id);
                    } 
                    if ($_FILES['banner']['name'] != '') {
                        $data['banner'] = $this->production_model->image_upload(CAT_SUB_CAT_BANNER, 'banner', 'category', $id);
                    }
                    if ($_FILES['banner_hindi']['name'] != '') {
                        $data['banner_hindi'] = $this->production_model->image_upload(CAT_SUB_CAT_BANNER, 'banner_hindi', 'category', $id);
                    }*/
                    /*foreach ($language as $key => $value) {
                        if (isset($data['description_' . $value['id']])) {
                            unset($data['description_' . $value['id']]);
                        }
                        if (isset($data['description_1'])) {
                            unset($data['description_1']);
                        }
                    }*/
                    $data['slug'] = strtolower(url_title($data['name']));
                    $data['slug_hindi'] = strtolower(url_title($data['name_hindi']));
                    unset($data['data_level']);
                    $record = $this->production_model->update_record('category', $data, array('id' => $id));
                    
                    $message = 'edit';
                }
            }
            if (isset($message) && $message != '') {
                if ($message == 'add') {
                    $this->session->set_flashdata('success', $this->lang->line('added_successfully'));
                } elseif ($message == 'edit') {
                    $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
                }
                redirect(base_url('authority/sub-category'));
            }
        }
        $data = isset($details) && $details != null ? $details[0] : $this->input->post();
        $data['details_description'] = isset($details_description) && $details_description != null ? $details_description : array();
        $this->load->view('authority/sub_category/add-edit', $data);
    }
    function file_selected()
    {
        $this->form_validation->set_message('file_selected', $this->lang->line('this_is_required_field'));
        if (empty($_FILES['image']['name'])) {
            return false;
        } else {
            return true;
        }
    }
    function banner_selected()
    {
        $this->form_validation->set_message('banner_selected', $this->lang->line('this_is_required_field'));
        if (empty($_FILES['banner']['name'])) {
            return false;
        } else {
            return true;
        }
    }
    function banner_tmp_selected()
    {
        $this->form_validation->set_message('banner_tmp_selected', $this->lang->line('this_is_required_field') . ' ' . strtolower($this->lang->line('hindi')));
        if (empty($_FILES['banner_hindi']['name'])) {
            return false;
        } else {
            return true;
        }
    }

    public function exists_in_name()
    {
        $post_data = $this->input->post();

        $id = $post_data['id'];
        $parent_id = $post_data['parent_id'];
        $name = $post_data['name'];

        if (isset($id) && $id != null) {
            $where['id !='] = $id;
        }
        $where['parent_id'] = $parent_id;
        $where['name'] = $name;
        $query = $this->db->get_where('category', $where)->result_array();
        if (isset($query) && count($query) > 0) {
            $this->form_validation->set_message('exists_in_name', $this->lang->line('this_record_is_available'));
            return FALSE;
        } else {
            return true;
        }
    }

    function filter()
    {
        $this->session->sub_category_info = $_POST;
        $name = isset($this->session->sub_category_info['name']) ? $this->session->sub_category_info['name'] : '';
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('name', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post();
        $tmp_data = get_details('category');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/sub-category/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('name', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('category', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        ob_start();
        if (isset($filteredData) && !empty($filteredData)) {
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
?>
                <tr>
                    <td style="width: 10px;">
                        <?php
                        if ($value['parent_id'] != 0) {
                        ?>
                            <div class="">
                                <input class="chk_all" type="checkbox" id="customCheckbox<?= $id; ?>" value="<?= $id ?>">
                                <label for="customCheckbox<?= $id; ?>" class=""></label>
                            </div>
                        <?php }
                        ?>
                    </td>
                    <td>
                        <span <?= $value['parent_id'] == 0 ? 'style="font-weight: bold;"' : ''; ?>><?= $value['name']; ?></span>
                    </td>
                    <?php /*?>
                    <td>
                        <?php
                        $condition = array('id_category' => $id, 'id_language' => $this->_language_default['id']);
                        $details_description = get_details('category_description', $condition);
                        if (!empty($details_description)) {
                            echo isset($details_description[0]['description']) ? $details_description[0]['description'] : '';
                        }
                        ?>
                    </td>
                    <?php */ ?>
                    <td>
                        <img src="<?= base_url(CATEGORY_IMAGE . 'thumbnail/') . $value['image' . get_language('admin')] ?>" onerror="this.src='<?= base_url('assets/uploads/img-not-available.png') ?>'" height="50px" width="50px">
                    </td>
                    <!-- <td>
                        <?php
                        if ($value['parent_id'] != 0) {
                        ?>
                            <img src="<?= base_url(CAT_SUB_CAT_BANNER . 'thumbnail/') . $value['banner' . get_language('admin')] ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                        <?php }
                        ?>
                    </td> -->
                    <td>
                        <?php
                        if ($value['parent_id'] != 0) {
                        ?>
                            <a href="<?= base_url('authority/sub-category/edit/' . $id); ?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>

                            <a href="javascript:void(0)" data-image-path="" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a>

                            <?php
                            if ($value['is_active'] == '1') {
                                echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status-subtmp" data-table="category" data-id="' . $id . '" data-parent_id="' . $value['parent_id'] . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                            } else {
                                echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status-subtmp" data-table="category" data-id="' . $id . '" data-parent_id="' . $value['parent_id'] . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                            }
                            ?>
                        <?php }
                        ?>
                    </td>
                </tr>
<?php
            }
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();
            $response_array['pagination'] = $data['pagination'];
        } else {
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">' . $this->lang->line('records_not_found') . '</td>
                                            </tr>';
            $response_array['pagination'] = '';
        }
        echo json_encode($response_array);
        exit;
    }
}
