<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bag extends CI_Controller {

    public $_language_info = array();
    public $_language_default = array();
    public $_page_title = '';
    public $_table = 'bag';
    public $_slug = 'bag';
    public $_upload_path = 'uploads/bag/';
    public $_valid_extensions = array('jpg', 'jpeg', 'png');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->helper('upload_helper');
        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                break;
            }
        }
        $this->_page_title = $this->lang->line('bag');
    }

    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->bag_info = array();
            redirect(base_url('authority/' . $this->_slug));
        }
        $data = array();
        $tmp_data = get_details('bag', array());
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/' . $this->_slug . '/filter');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;

        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('bag', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        $this->load->view('authority/' . $this->_slug . '/view', $data);
    }

    public function add_edit($id = '') {
        $message = '';
        $data = $this->input->post();
        $details = array();
        if (isset($id) && $id != null) {
            $condition = array('id' => $id);
            $details = get_details('bag', $condition);
        }
        if ($this->input->method() == 'post') {
            if ($id == '') { // Add
                $this->set_validation('add');

                if ($this->form_validation->run() == FALSE) {
                    $data = array_merge($data, $this->form_validation->error_array());
                } else {
                    $records = array(
                        'price' => $this->input->post('price'),
                    );
                    foreach ($this->_language_info as $l_value) {
                        if ($l_value['default_language'] == 1) {
                            $records['title'] = $this->input->post('title');
                        } else {
                            $records['title_' . $l_value['slug']] = $this->input->post('title');
                        }
                    }
                    $records['created_at'] = CURRENT_TIME;
                    $records['updated_at'] = CURRENT_TIME;
                    $image_info = upload_file('image', $this->_upload_path);
                    if ($image_info['status'] == true) {
                        $records['image'] = $image_info['file_name'];
                    }
                    $record = $this->production_model->insert_record('bag', $records);
                    $message = 'add';
                }
            } else { // Edit
                $this->set_validation('edit');
                if ($this->form_validation->run() == FALSE) {
                    $data = array_merge($data, $this->form_validation->error_array());
                } else {
                    $records = array(
                        'price' => $this->input->post('price'),
                    );
                    foreach ($this->_language_info as $l_value) {
                        if ($l_value['default_language'] == 1) {
                            $records['title'] = $this->input->post('title');
                        } else {
                            $records['title_' . $l_value['slug']] = $this->input->post('title');
                        }
                    }
                    $records['updated_at'] = CURRENT_TIME;
                    if (!empty($_FILES['image']['name'])) {
                        $image_info = upload_file('image', $this->_upload_path);
                        if ($image_info['status'] == true) {
                            $records['image'] = $image_info['file_name'];
                            if (isset($details[0]['image']) && !empty($details[0]['image'])) {
                                @unlink($this->_upload_path . $details[0]['image']);
                            }
                        }
                    }
                    $record = $this->common_model->update_data('bag', $records, array('where' => array('id' => $id)));
                    $message = 'edit';
                }
            }
            if (isset($message) && $message != '') {
                if ($message == 'add') {
                    $this->session->set_flashdata('success', $this->lang->line('added_successfully'));
                } elseif ($message == 'edit') {
                    $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
                }
                redirect(base_url('authority/' . $this->_slug));
            }
        }

        $data = isset($details) && $details != null ? $details[0] : $this->input->post();
        $this->load->view('authority/' . $this->_slug . '/add-edit', $data);
    }

    public function filter() {
        $this->session->bag_info = $_POST;
        $name = isset($this->session->bag_info['name']) ? $this->session->bag_info['name'] : '';
        $language = get_details('language', array('id !=' => 1, 'is_active' => 1));
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('title', $name);
            if (isset($language) && $language != null) {
                foreach ($language as $key => $value) {
                    $language_name = strtolower('title_' . $value['slug']);
                    $this->db->or_like($language_name, $name);
                }
            }
            $this->db->group_end();
        }
        $data[] = $this->input->post();
        $tmp_data = get_details('bag', array());
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/' . $this->_slug . '/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('title', $name);
            if (isset($language) && $language != null) {
                foreach ($language as $key => $value) {
                    $language_name = strtolower('title_' . $value['slug']);
                    $this->db->or_like($language_name, $name);
                }
            }
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('bag', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        ob_start();
        if (isset($filteredData) && !empty($filteredData)) {
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                <tr>
                    <td style="width: 10px;">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id; ?>" value="<?= $id; ?>">
                            <label for="customCheckbox<?= $id; ?>" class="custom-control-label"></label>
                        </div>
                    </td>
                    <td><?= $value['title' . get_language()]; ?></td>
                    <td><?= $value['price']; ?></td>
                    <td>
                        <img src="<?= base_url('uploads/bag/') . $value['image']; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                    </td>
                    <td>
                        <a href="<?= base_url('authority/' . $this->_slug . '/add-edit/' . $id); ?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0)" data-table="<?php echo $this->_table; ?>" data-image-path="<?= $this->_upload_path; ?>" class="btn bg-gradient-danger btn-flat btn-xs delete_record_cat" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a>
                            <?php
                            /* if ($value['is_active'] == '1') {
                              echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status-tmp" data-table="category" data-id="' . $id . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                              } else {
                              echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status-tmp" data-table="category" data-id="' . $id . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                              } */
                            ?>
                    </td>
                </tr>
                <?php
            }
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();
            $response_array['pagination'] = $data['pagination'];
        } else {
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">' . $this->lang->line('records_not_found') . '</td>
                                            </tr>';
            $response_array['pagination'] = '';
        }
        echo json_encode($response_array);
        exit;
    }

    private function set_validation($mode = 'add') {
        $data = $this->input->post();
        $this->form_validation->set_rules('title', 'Title', 'trim|required', array('required' => $this->lang->line('please_enter_value')));
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric|greater_than_equal_to[0]', array('required' => $this->lang->line('please_enter_value')));
        if ($mode == 'add') {
            $this->form_validation->set_rules('image', 'image', 'callback_file_selected');
        }
        if ($mode == 'edit' && !empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'image', 'callback_file_selected');
        }
        $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('id !=' => 1, 'is_active' => 1));
        if (isset($language) && $language != null) {
            foreach ($language as $key => $value) {
                $newname = strtolower('title_' . $value['name']);
                $column_name[] = $newname;
                $get_table_column = array();
                $fields = $this->db->field_data('bag');
                if (isset($fields) && $fields != null) {
                    foreach ($fields as $key => $field) {
                        $get_table_column[] = $field->name;
                    }
                }
                $this->form_validation->set_rules($newname, $newname, 'trim|required', array('required' => $this->lang->line('please_enter_value') . ' ' . (strtolower($value['name']))));
            }
            if (isset($column_name) && $column_name != null) {
                foreach ($column_name as $key => $column_row) {
                    if (!in_array($column_row, $get_table_column)) {
                        /* New column create in table */
                        $this->db->query('ALTER TABLE `bag` ADD ' . $column_row . ' VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER title');
                    }
                }
            }
        }
        return true;
    }

    public function file_selected() {
        $return = true;
        if (empty($_FILES['image']['name'])) {
            $this->form_validation->set_message('file_selected', $this->lang->line('this_is_required_field'));
            return false;
        } else {
            $input_name = 'image';
            if (!validate_file_size(MAX_IMAGE_UPLOAD_SIZE, $input_name)) {
                $return = false;
                $this->form_validation->set_message('file_selected', $this->lang->line('upload_file_with_size') . (MAX_IMAGE_UPLOAD_SIZE / 1000000) . 'MB');
            } else if (!validate_extensions($this->_valid_extensions, $input_name)) {
                $return = false;
                $this->form_validation->set_message('file_selected', $this->lang->line('upload_file_with_extension') . implode(', ', $this->_valid_extensions));
            }
            return $return;
        }
    }

}
