<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Keywords extends CI_Controller {

    public $_table = 'keywords';
    public $_slug = 'keywords';
    public $_language_info = array();
    public $_language_default = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');

        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                break;
            }
        }
    }

    public function index() {
        $data = array();
        $data['page_title'] = $this->lang->line('keywords');

        $conditions = array('WHERE' => array('app_type' => '1'));
        $info = $this->common_model->select_data('app_keywords', $conditions);
        $data['keywords'] = array();
        if ($info['row_count'] > 0) {
            $data['keywords'] = $info['data'];
        }
        $keywords = $data['keywords'];
        if($this->input->method(true) == 'POST') {
	        if(isset($keywords) && !empty($keywords)) {
		        foreach ($keywords as $key => $value) {
		        	if($this->input->post($value['language_key']) != null) {
		        		$conditions  = array('where'=>array('app_type'=>'1','language_key'=>$value['language_key']));
		        		$records = array('language_value'=>$this->input->post($value['language_key']));
		        		$this->common_model->update_data('app_keywords',$records,$conditions);
		        	}
		        }
		    }
		    $conditions = array('WHERE' => array('app_type' => '1'));
	        $info = $this->common_model->select_data('app_keywords', $conditions);
	        $data['keywords'] = array();
	        if ($info['row_count'] > 0) {
	            $data['keywords'] = $info['data'];
	        }
	        $data['message'] = get_line('updated_successfully');
        }

        $this->load->view(AUTHORITY . '/' . $this->_slug . '/view', $data);
    }
}