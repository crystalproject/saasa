<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {

    public $_page_title = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->_page_title = $this->lang->line('brand');
    }

    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->brands_info = array();
            redirect(base_url('authority/brand'));
        }
        $data = array();
        $tmp_data = get_details('brands');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/brand/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;

        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('brands', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        $this->load->view('authority/brands/view', $data);
    }

    function add_edit($id = '') {
        $message = '';
        $data = $this->input->post();
        $details = array();
        if (isset($id) && $id != null) {
            $condition = array('id' => $id);
            $details = get_details('brands', $condition);
        }
        if ($this->input->method() == 'post') {
            if ($id == '') { // Add
                if ($this->input->method() == 'post') {
                    $this->form_validation->set_rules('name', 'name', 'required', array('required' => $this->lang->line('this_is_required_field')));

                    $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('id !=' => 1, 'is_active' => 1));
                    if (isset($language) && $language != null) {
                        foreach ($language as $key => $value) {
                            $newname = strtolower('name_' . $value['slug']);

                            $this->form_validation->set_rules($newname, $newname, 'required', array('required' => $this->lang->line('this_field_is_required') . ' ' . (strtolower($value['name'])) . ''));
                        }
                    }
                    if ($this->form_validation->run() == FALSE) {
                        $data = array_merge($data, $this->form_validation->error_array());
                    } else {
                        $record = $this->production_model->insert_record('brands', $data);
                        $message = 'add';
                    }
                }
            } else { // Edit   
                $this->form_validation->set_rules('name', 'name', 'required', array('required' => $this->lang->line('this_is_required_field')));

                $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('id !=' => 1, 'is_active' => 1));
                if (isset($language) && $language != null) {
                    foreach ($language as $key => $value) {
                        $newname = strtolower('name_' . $value['slug']);

                        $this->form_validation->set_rules($newname, $newname, 'required', array('required' => $this->lang->line('this_field_is_required') . ' ' . (strtolower($value['name'])) . ''));
                    }
                }
                if ($this->form_validation->run() == FALSE) {
                    $data = array_merge($details[0], $this->form_validation->error_array());
                } else {
                    $record = $this->production_model->update_record('brands', $data, array('id' => $id));
                    $message = 'edit';
                }
            }
            if (isset($message) && $message != '') {
                if ($message == 'add') {
                    $this->session->set_flashdata('success', $this->lang->line('added_successfully'));
                } elseif ($message == 'edit') {
                    $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
                }
                redirect(base_url('authority/brand'));
            }
        }
        $data = isset($details) && $details != null ? $details[0] : $this->input->post();
        $this->load->view('authority/brands/add-edit', $data);
    }

    function filter() {
        
        $this->session->brands_info = $_POST;
        $name = isset($this->session->brands_info['name']) ? $this->session->brands_info['name'] : '';
        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('name', $name);
            $this->db->or_like('name_hindi', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post();
        $tmp_data = get_details('brands');
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/brand/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name != null) {
            $this->db->group_start();
            $this->db->like('name', $name);
            $this->db->or_like('name_hindi', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('brands', 'id', 'desc', array(), $record['limit'], $record['start']);
        $data['pagination'] = $record['pagination'];
        $data['no'] = $record['no'];

        ob_start();
        if (isset($filteredData) && !empty($filteredData)) {
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                <tr>
                    <td><?= $value['name'. get_language()]; ?></td>
                    <td>
                        <a href="<?= base_url('authority/brand/add-edit/' . $id); ?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>
                        <?php
                            if ($value['is_active'] == '1') {
                                echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status" data-table="brands" data-id="' . $id . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                            } else {
                                echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status" data-table="brands" data-id="' . $id . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                            }
                        ?>
                    </td>
                </tr>
                <?php
            }
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();
            $response_array['pagination'] = $data['pagination'];
        } else {
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">' . $this->lang->line('records_not_found') . '</td>
                                            </tr>';
            $response_array['pagination'] = '';
        }
        echo json_encode($response_array);
        exit;
    }

}
