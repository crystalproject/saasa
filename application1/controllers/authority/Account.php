<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
    }

    public function changepassword() {
        $data = array('form_title' => 'Change password');
        $this->form_validation->set_rules('current_password', 'current password', 'required|min_length[6]', array('required' => 'This field is required'));
        $this->form_validation->set_rules('new_password', 'new password', 'required|min_length[6]', array('required' => 'This field is required'));
        $this->form_validation->set_rules('confirm_new_password', 'confirm new password', 'required|min_length[6]|matches[new_password]', array('required' => 'This field is required', "matches" => 'Password and confirm password should be same'));
        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $current_password = $this->input->post("current_password");
            if ($this->session->user_info['password'] != sha1($this->session->user_info['salt'] . $current_password)) {
                $data = array_merge($data, array("current_password_error" => 'Please enter correct old password'));
            } else {
                $new_password = sha1($this->session->user_info['salt'] . $this->input->post("new_password"));
                $records = array(
                    "password" => $new_password,
                );
                $conditions = array(
                    "where" => array("id" => $this->session->user_info['id']),
                );
                $this->common_model->update_data('administrator', $records, $conditions);
                $_POST = array();
                // $data = array_merge($data, array("success" => 'Password changed successfully'));
                $this->session->set_flashdata('success', 'Password changed successfully');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        $this->load->view('authority/account/change-password', $data);
    }

    public function change_language() {        
        $response_array = array('status' => false, 'message' => '');
        $this->form_validation->set_rules('id_language', '', 'trim|required|greater_than[0]', array('required' => 'This field is required'));
        $data = array();
        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $this->session->default_language = $this->input->post('id_language') && in_array($this->input->post('id_language'), array(1, 2)) ? $this->input->post('id_language') : DEFAULT_LANGUAGE;
            $sql = 'UPDATE language SET default_language = "0" ';            
            $this->common_model->simple_query($sql);
            $sql = 'UPDATE language SET default_language = "1" WHERE id = "'.$this->session->default_language.'" ';            
            $this->common_model->simple_query($sql);
            $response_array['status'] = true;
        }
        echo json_encode($response_array);
        exit;
    }

}
