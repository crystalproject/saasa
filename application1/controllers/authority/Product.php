<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{

    public $_table = 'product';
    public $_slug = 'product';
    public $_language_info = array();
    public $_language_default = array();
    public $_upload_path = 'uploads/product/';
    public $_valid_extensions = array('jpg', 'jpeg', 'png');

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->helper('upload_helper');
        $this->load->helper('product_helper');

        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                break;
            }
        }
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = get_line('product');
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/view', $data);
    }

    public function get_records($page_number = '')
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $data = array();
            $sub_query = '';
            if ($this->input->post('search_value') != null) {
                $sub_query = ' AND (title LIKE "%' . trim($this->input->post('search_value')) . '%" OR description LIKE "%' . trim($this->input->post('search_value')) . '%") ';
            }
            $sql = 'SELECT count(id) AS total_records FROM product WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' ';
            $info = $this->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                $total_records = $info['data'][0]['total_records'];
            } else {
                $total_records = 0;
            }
            $settings = array(
                'total_record' => $total_records,
                'url' => base_url(AUTHORITY . '/' . $this->_slug . '/get_records'),
                'per_page' => RECORDS_PER_PAGE,
                'page_number' => $page_number
            );
            $pagination = $this->common_model->only_pagination($settings);
            $data['pagination'] = $pagination['pagination'];
            $slug = '_' . $this->_language_default['slug'];
            if ($this->_language_default['id'] == '1') {
                $slug = '';
            }
            $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = product.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $slug . ' FROM category WHERE id = product.id_category) as category_name';
            $sql = 'SELECT *' . $sub_sub_query . ' FROM product WHERE id_language = "' . $this->_language_default['id'] . '" ' . $sub_query . ' LIMIT ' . $pagination['start'] . ',' . $pagination['limit'] . ' ';
            $info = $this->common_model->get_data_with_sql($sql);
            $data['info'] = array();
            if ($info['row_count'] > 0) {
                foreach ($info['data'] as $key => $value) {
                    $info['data'][$key]['total_review'] = get_total_review($value['id_unique']);
                }
                $data['info'] = $info['data'];
            }
            $response_array['status'] = true;
            $response_array['data'] = $this->load->view(AUTHORITY . '/' . $this->_slug . '/listing', $data, true);
        }
        echo json_encode($response_array);
        exit;
    }

    public function add_edit($id_unique = '')
    {
        $data = array();
        $data['page_title'] = get_line('add_product');
        if ($id_unique != '') {
            $info = $this->get_detail($id_unique);
            $data = array_merge($data, $info);
            $data['page_title'] = get_line('edit_product');
        }
        if ($this->input->is_ajax_request()) {
            $response_array = array('status' => false, 'message' => '');
            $this->form_validation->set_rules('id_category', '', 'trim|required|numeric|greater_than[0]|callback_check_child_category', array('required' => 'Please select category'));

            $this->form_validation->set_rules('id_brand', '', 'trim|required|numeric|greater_than[0]', array('required' => 'Please select brand'));

            $this->form_validation->set_rules('images', '', 'callback_check_images');
            $this->form_validation->set_rules('title_description', '', 'callback_check_title_description');
            $this->form_validation->set_rules('attributes', '', 'callback_check_attributes');
            //$this->form_validation->set_rules('sku', '', 'trim|required', array('required' => get_line('enter_sku')));
            //$this->form_validation->set_rules('is_new', '', 'trim|required|in_list[0,1]', array('required' => get_line('somthing_wrong')));
            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $error_value) {
                    $response_array['message'] = $error_value;
                    break;
                }
            } else {
                $main_image = upload_file('main_image', $this->_upload_path);
                $error = false;
                if ($main_image['status'] == false && $id_unique == '') {
                    $error = true;
                    $response_array['message'] = get_line('image_upload_error');
                }
                $file_names = upload_file_multiple('images', $this->_upload_path);
                $error = false;
                if ($file_names['status'] == false && $id_unique == '') {
                    $error = true;
                    $response_array['message'] = get_line('image_upload_error');
                }
                $file_name_video = upload_file('product_video', $this->_upload_path);
                $error = false;
                if ($file_name_video['status'] == false && $id_unique == '') {
                    //$error = true;
                    //$response_array['message'] = get_line('video_upload_error');
                }
                if (!$error) {
                    $records = array();
                    if ($id_unique == '') {
                        //Adding New Record
                        $id_unique = uniqid();
                        foreach ($file_names['file_name'] as $value) {
                            $records[] = array(
                                'id_unique_product' => $id_unique,
                                'image_name' => $value,
                            );
                        }
                        $this->common_model->insert_data('product_images', $records, true);
                        $records = array();
                        foreach ($this->_language_info as $l_value) {
                            $records[] = array(
                                'id_unique' => $id_unique,
                                'id_category' => $this->input->post('id_category'),
                                'id_brand' => $this->input->post('id_brand'),
                                'id_language' => $l_value['id'],
                                'title' => $this->input->post('title_' . $l_value['id']),
                                'slug' => strtolower(url_title($this->input->post('title_' . $l_value['id']))),
                                'description' => closetags($this->input->post('description_' . $l_value['id'])),
                                'long_description' => closetags($this->input->post('long_description_' . $l_value['id'])),
                                'additional_description' => closetags($this->input->post('additional_description_' . $l_value['id'])),
                                'has_attributes' => $this->input->post('has_attributes'),
                                'id_attributes' => !empty($this->input->post('id_attributes')) ? implode(',', $this->input->post('id_attributes')) : '',
                                'by_quantity_or_availability' => $this->input->post('by_quantity_or_availability'),
                                'is_popular' => $this->input->post('is_popular') != null ? $this->input->post('is_popular') : 0,
                                'product_video' => $file_name_video['status'] == true ? $file_name_video['file_name'] : null,
                                'main_image' => $main_image['status'] == true ? $main_image['file_name'] : null,
                                'youtube_link' => $this->input->post('youtube_link') != null ? $this->input->post('youtube_link') : null,
                               // 'sku' => $this->input->post('sku') != null ? $this->input->post('sku') : null,
                                //'is_new' => $this->input->post('is_new') != null ? $this->input->post('is_new') : 0,
                                //'is_gift_wraping' => $this->input->post('is_gift_wraping') != null ? $this->input->post('is_gift_wraping') : 0,
                                //'is_customizable' => $this->input->post('is_customizable') != null ? $this->input->post('is_customizable') : 0,
                                'is_best_seller' => $this->input->post('is_best_seller') != null ? $this->input->post('is_best_seller') : 0,
                                'created_at' => CURRENT_TIME,
                                'updated_at' => CURRENT_TIME,
                            );
                        }
                        $this->common_model->insert_data('product', $records, true);
                        $this->insert_prices($id_unique);
                        $response_array['status'] = true;
                        $response_array['message'] = get_line('added_successfully');
                    } else {
                        if ($file_names['status'] == true) {
                            //Upload new selected files
                            foreach ($file_names['file_name'] as $value) {
                                $records[] = array(
                                    'id_unique_product' => $id_unique,
                                    'image_name' => $value,
                                );
                            }
                            $this->common_model->insert_data('product_images', $records, true);
                        }
                        $records = array();
                        foreach ($this->_language_info as $l_value) {
                            $conditions = array('where' => array('id_unique' => $id_unique, 'id_language' => $l_value['id']));
                            $duplicate = $this->common_model->select_data('product', $conditions);
                            if ($duplicate['row_count'] > 0) {
                                $records = array(
                                    'id_category' => $this->input->post('id_category'),
                                    'id_brand' => $this->input->post('id_brand'),
                                    'title' => $this->input->post('title_' . $l_value['id']),
                                    'slug' => strtolower(url_title($this->input->post('title_' . $l_value['id']))),
                                    'description' => closetags($this->input->post('description_' . $l_value['id'])),
                                    'long_description' => closetags($this->input->post('long_description_' . $l_value['id'])),
                                    'additional_description' => closetags($this->input->post('additional_description_' . $l_value['id'])),
                                    'has_attributes' => $this->input->post('has_attributes'),
                                    'id_attributes' => !empty($this->input->post('id_attributes')) ? implode(',', $this->input->post('id_attributes')) : '',
                                    'by_quantity_or_availability' => $this->input->post('by_quantity_or_availability'),
                                    'is_popular' => $this->input->post('is_popular') != null ? $this->input->post('is_popular') : 0,
                                    'youtube_link' => $this->input->post('youtube_link') != null ? $this->input->post('youtube_link') : null,
                                    //'sku' => $this->input->post('sku') != null ? $this->input->post('sku') : null,
                                    //'is_new' => $this->input->post('is_new') != null ? $this->input->post('is_new') : 0,
                                    //'is_gift_wraping' => $this->input->post('is_gift_wraping') != null ? $this->input->post('is_gift_wraping') : 0,
                                    //'is_customizable' => $this->input->post('is_customizable') != null ? $this->input->post('is_customizable') : 0,
                                    'is_best_seller' => $this->input->post('is_best_seller') != null ? $this->input->post('is_best_seller') : 0,
                                    'updated_at' => CURRENT_TIME,
                                );
                                if ($file_name_video['status'] == true) {
                                    $records['product_video'] = $file_name_video['file_name'];
                                    if (isset($data['product_info']['product_video']) && !empty($data['product_info']['product_video'])) {
                                        if (file_exists($this->_upload_path . $data['product_info']['product_video'])) {
                                            @unlink($this->_upload_path . $data['product_info']['product_video']);
                                        }
                                        unset($data['product_info']['product_video']);
                                    }
                                }
                                if ($main_image['status'] == true) {
                                    $records['main_image'] = $main_image['file_name'];
                                    if (isset($data['product_info']['main_image']) && !empty($data['product_info']['main_image'])) {
                                        if (file_exists($this->_upload_path . $data['product_info']['main_image'])) {
                                            @unlink($this->_upload_path . $data['product_info']['main_image']);
                                        }
                                        unset($data['product_info']['main_image']);
                                    }
                                }
                                if ($records['has_attributes'] == '0') {
                                    $records['id_attributes'] = '';
                                }
                                $this->common_model->update_data('product', $records, $conditions);
                            } else {
                                $records = array(
                                    'id_unique' => $id_unique,
                                    'id_category' => $this->input->post('id_category'),
                                    'id_brand' => $this->input->post('id_brand'),
                                    'id_language' => $l_value['id'],
                                    'title' => $this->input->post('title_' . $l_value['id']),
                                    'slug' => strtolower(url_title($this->input->post('title_' . $l_value['id']))),
                                    'description' => closetags($this->input->post('description_' . $l_value['id'])),
                                    'long_description' => closetags($this->input->post('long_description_' . $l_value['id'])),
                                    'additional_description' => closetags($this->input->post('additional_description_' . $l_value['id'])),
                                    'has_attributes' => $this->input->post('has_attributes'),
                                    'id_attributes' => !empty($this->input->post('id_attributes')) ? implode(',', $this->input->post('id_attributes')) : '',
                                    'by_quantity_or_availability' => $this->input->post('by_quantity_or_availability'),
                                    'is_popular' => $this->input->post('is_popular') != null ? $this->input->post('is_popular') : 0,
                                    'youtube_link' => $this->input->post('youtube_link') != null ? $this->input->post('youtube_link') : null,
                                    //'sku' => $this->input->post('sku') != null ? $this->input->post('sku') : null,
                                    //'is_new' => $this->input->post('is_new') != null ? $this->input->post('is_new') : 0,
                                    //'is_gift_wraping' => $this->input->post('is_gift_wraping') != null ? $this->input->post('is_gift_wraping') : 0,
                                    //'is_customizable' => $this->input->post('is_customizable') != null ? $this->input->post('is_customizable') : 0,
                                    'is_best_seller' => $this->input->post('is_best_seller') != null ? $this->input->post('is_best_seller') : 0,
                                    'created_at' => CURRENT_TIME,
                                    'updated_at' => CURRENT_TIME,
                                );
                                if ($records['has_attributes'] == '0') {
                                    $records['id_attributes'] = '';
                                }
                                if ($file_name_video['status'] == true) {
                                    $records['product_video'] = $file_name_video['file_name'];
                                }
                                if ($main_image['status'] == true) {
                                    $records['main_image'] = $main_image['file_name'];
                                }
                                $this->common_model->insert_data('product', $records);
                            }
                        }
                        // $this->common_model->delete_data('product_prices', array('IN' => array('id_unique_product' => $id_unique)));
                        // $this->common_model->update_data('product_attributes', array('id_unique_product' => null), array('where' => array('is_ordered' => 1), 'IN' => array('id_unique_product' => $id_unique)));
                        // $this->common_model->delete_data('product_attributes', array('where' => array('is_ordered' => 0), 'IN' => array('id_unique_product' => $id_unique)));
                        $this->insert_prices($id_unique, $update = true);
                        $response_array['status'] = true;
                        $response_array['message'] = get_line('updated_successfully');
                    }
                }
            }
            echo json_encode($response_array);
            exit;
        }

        $this->load->view(AUTHORITY . '/' . $this->_slug . '/add-edit', $data);
    }

    public function get_detail($id_unique)
    {
        $info = $this->common_model->select_data($this->_table, array('WHERE' => array('id_unique' => $id_unique)));
        if ($info['row_count'] > 0) {
            $id_unique = $info['data'][0]['id_unique'];
            $data = array();
            $data['product_info'] = $info['data'][0];
            $data['language_info'] = array();
            foreach ($info['data'] as $value) {
                $data['language_info'][$value['id_language']] = array(
                    'title' => $value['title'],
                    'description' => $value['description'],
                    'long_description' => $value['long_description'],
                    'additional_description' => $value['additional_description']
                );
            }
            //getting images
            $temp = $this->common_model->select_data('product_images', array('SELECT' => 'image_name', 'WHERE' => array('id_unique_product' => $id_unique)));
            $data['product_images'] = array();
            if ($temp['row_count'] > 0) {
                $data['product_images'] = $temp['data'];
            }
            //getting attributes            
            $data['product_attributes'] = array();
            if ($data['product_info']['has_attributes'] == '1') {
                $temp = $this->common_model->select_data('product_attributes', array('WHERE' => array('id_unique_product' => $id_unique), 'ORDER BY' => array('id' => 'ASC')));
                if ($temp['row_count'] > 0) {
                    $data['product_attributes'] = $temp['data'];
                }
                $temp = array();
                foreach ($data['product_attributes'] as $value) {
                    $temp[$value['id_unique']][] = $value;
                }
                $data['product_attributes'] = $temp;
            }
            //getting prices
            $temp = $this->common_model->select_data('product_prices', array('WHERE' => array('id_unique_product' => $id_unique), 'ORDER BY' => array('id' => 'ASC')));
            $data['product_prices'] = array();
            if ($temp['row_count'] > 0) {
                $data['product_prices'] = $temp['data'];
            }
            if ($data['product_info']['has_attributes'] == '0') {
                $data['product_info']['price'] = isset($data['product_prices'][0]['price']) ? $data['product_prices'][0]['price'] : 0;
                $data['product_info']['discount'] = isset($data['product_prices'][0]['discount']) ? $data['product_prices'][0]['discount'] : 0;
                $data['product_info']['is_product_available'] = isset($data['product_prices'][0]['is_product_available']) ? $data['product_prices'][0]['is_product_available'] : 0;
                $data['product_info']['quantity'] = isset($data['product_prices'][0]['quantity']) ? $data['product_prices'][0]['quantity'] : 0;
            } else {
                $data['product_info']['price'] = 0;
                //Adding price to all combination
                foreach ($data['product_prices'] as $value) {
                    if (isset($data['product_attributes'][$value['id_unique_product_attributes']])) {
                        foreach ($data['product_attributes'][$value['id_unique_product_attributes']] as $key => $temp) {
                            $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_price'] = $value['price'];
                            $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_discount'] = $value['discount'];
                            $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_is_product_available'] = $value['is_product_available'];
                            $data['product_attributes'][$value['id_unique_product_attributes']][$key]['attribute_quantity'] = $value['quantity'];
                        }
                    }
                }
            }
            return $data;
        } else {
            redirect(base_url(AUTHORITY . '/' . $this->_slug . ''));
        }
    }

    /**
     * This function is used to validate uploaded images
     * @return boolean
     */
    public function check_images()
    {
        $return = true;
        $input_name = 'images';
        if (isset($_FILES[$input_name]['name']) && !empty($_FILES[$input_name]['name'][0])) {
            if (!validate_file_size(MAX_IMAGE_UPLOAD_SIZE, $input_name)) {
                $return = false;
                $this->form_validation->set_message('check_images', get_line('file_size_less_than') . ' ' . (MAX_IMAGE_UPLOAD_SIZE / 1000000) . 'MB');
            } else if (!validate_extensions($this->_valid_extensions, $input_name)) {
                $return = false;
                $this->form_validation->set_message('check_images', get_line('file_size_extension') . ' ' . implode(', ', $this->_valid_extensions));
            }
        } else {
            if ($this->uri->segment(4) == '') {
                $this->form_validation->set_message('check_images', get_line('select_image'));
                $return = false;
            }
        }
        return $return;
    }

    /**
     * This function is used to validate all language title and description
     * @return boolean
     */
    public function check_title_description()
    {
        $return = true;
        foreach ($this->_language_info as $l_value) {
            if ($this->input->post('title_' . $l_value['id']) == null) {
                $this->form_validation->set_message('check_title_description', get_line('enter_title_in') . ' ' . $l_value['name'] . ' ' . get_line('language'));
                return false;
            }
            if ($this->input->post('description_' . $l_value['id']) == null || strip_tags(trim($this->input->post('description_' . $l_value['id']))) == '') {
                $this->form_validation->set_message('check_title_description', get_line('enter_description_in') . ' ' . $l_value['name'] . ' ' . get_line('language'));
                return false;
            }
        }
        return $return;
    }

    /**
     * This function is used to validate product price and attributes
     * @return boolean
     */
    public function check_attributes()
    {
        $by_quantity_or_availability = $this->input->post('by_quantity_or_availability');
        $return = true;
        if ($this->input->post('has_attributes') == '1') {
            //When product has attribute then need to validate all attributes
            if (empty($this->input->post('id_attributes'))) {
                $this->form_validation->set_message('check_attributes', get_line('select_attribute'));
                $return = false;
            } else {
                $post_data = $this->input->post();
                $attribute_ids = $this->input->post('id_attributes');
                $total_attributes = count($attribute_ids);
                if (isset($post_data['attribute_price'])) {
                    $error = false;
                    foreach ($post_data['attribute_price'] as $key => $value) {
                        if ($by_quantity_or_availability == '1') {
                            if (!isset($post_data['attribute_quantity'][$key]) || $post_data['attribute_quantity'][$key] < 0) {
                                $error = true;
                                $this->form_validation->set_message('check_attributes', get_line('check_quantity'));
                                $return = false;
                            }
                        } else if ($by_quantity_or_availability == '0') {
                            if (!isset($post_data['attribute_is_product_available'][$key]) || !in_array($post_data['attribute_is_product_available'][$key], array(0, 1))) {
                                $error = true;
                                $this->form_validation->set_message('check_attributes', get_line('check_available_status'));
                                $return = false;
                            }
                        }
                    }
                    if (!$error) {
                        $records = array();
                        $duplicate = array();
                        foreach ($post_data['attribute_price'] as $key => $value) {
                            if ($value > 0) {
                                if (isset($post_data['attribute_discount'][$key]) && ($post_data['attribute_discount'][$key] < 0 || $post_data['attribute_discount'][$key] > 100)) {
                                    $this->form_validation->set_message('check_attributes', get_line('check_discount'));
                                    $return = false;
                                    break;
                                }
                                $temp_string = '';
                                for ($i = 0; $i < $total_attributes; $i++) {
                                    if (isset($post_data[$attribute_ids[$i]][$key]) && isset($post_data[$attribute_ids[$i]][$key]) > 0) {
                                        $temp_string .= $post_data[$attribute_ids[$i]][$key];
                                    } else {
                                        $this->form_validation->set_message('check_attributes', get_line('check_attribute'));
                                        $return = false;
                                        break;
                                    }
                                }
                                if ($return == false) {
                                    break;
                                }
                                //$temp_string .= $value;
                                // Checking duplicate records and validating all attribute fields
                                if (!in_array($temp_string, $duplicate)) {
                                    $duplicate[] = $temp_string;
                                } else {
                                    $this->form_validation->set_message('check_attributes', get_line('check_attribute_duplicate'));
                                    $return = false;
                                    break;
                                }
                            } else {
                                //If price lessthan or equal to 0 then that is not valid price
                                $this->form_validation->set_message('check_attributes', get_line('enter_price'));
                                $return = false;
                                break;
                            }
                        }
                    }
                } else {
                    $this->form_validation->set_message('check_attributes', get_line('select_attribute'));
                    $return = false;
                }
            }
        } else {
            if ($this->input->post('price') == null || $this->input->post('price') <= 0) {
                $return = false;
                if ($this->input->post('price') <= 0) {
                    $this->form_validation->set_message('check_attributes', get_line('price_greater_than'));
                } else {
                    //When product not have any attribute then need to check valid price
                    $this->form_validation->set_message('check_attributes', get_line('enter_price'));
                }
            } else if ($this->input->post('discount') < 0 || $this->input->post('discount') > 100) {
                $return = false;
                $this->form_validation->set_message('check_attributes', get_line('correct_discount_price'));
            } else if ($by_quantity_or_availability == '1') {
                if ($this->input->post('available_quantity') < 0) {
                    $return = false;
                    $this->form_validation->set_message('check_attributes', get_line('correct_quantity'));
                }
            } else if ($by_quantity_or_availability == '0') {
                if (!in_array($this->input->post('is_product_available'), array(0, 1))) {
                    $return = false;
                    $this->form_validation->set_message('check_attributes', get_line('check_available_option'));
                }
            }
        }
        return $return;
    }

    /**
     * This function is used to insert product price and attributes
     * @param type $id_unique_product
     */
    public function insert_prices($id_unique_product, $update = false)
    {
        $return = true;
        if ($this->input->post('has_attributes') == '1') {
            $post_data = $this->input->post();
            $attribute_ids = $this->input->post('id_attributes');
            $total_attributes = count($attribute_ids);
            if (isset($post_data['attribute_price'])) {
                $record_price = array();
                $record_attribute = array();
                $attribute_images = array();
                $duplicate = array();
                foreach ($post_data['attribute_price'] as $key => $value) {
                    if ($value > 0) {
                        //Price validation
                        $id_unique_product_attributes = uniqid();
                        $record_price[] = array(
                            'id_unique_product' => $id_unique_product,
                            'id_unique_product_attributes' => $id_unique_product_attributes,
                            'price' => $value,
                            'discount' => isset($post_data['attribute_discount'][$key]) && $post_data['attribute_discount'][$key] != null ? $post_data['attribute_discount'][$key] : 0,
                            'is_product_available' => isset($post_data['attribute_is_product_available'][$key]) && $post_data['attribute_is_product_available'][$key] != null ? $post_data['attribute_is_product_available'][$key] : 0,
                            'quantity' => isset($post_data['attribute_quantity'][$key]) && $post_data['attribute_quantity'][$key] != null ? $post_data['attribute_quantity'][$key] : 0,
                        );
                        if (isset($_FILES['attribute_image']['name'][$key]) && !empty($_FILES['attribute_image']['name'][$key])) {
                            foreach ($_FILES['attribute_image']['name'][$key] as $i_key => $i_value) {
                                $_FILES['temp']['name'] = $_FILES['attribute_image']['name'][$key];
                                $_FILES['temp']['type'] = $_FILES['attribute_image']['type'][$key];
                                $_FILES['temp']['tmp_name'] = $_FILES['attribute_image']['tmp_name'][$key];
                                $_FILES['temp']['error'] = $_FILES['attribute_image']['error'][$key];
                                $_FILES['temp']['size'] = $_FILES['attribute_image']['size'][$key];
                                $attribute_images[] = upload_file_multiple('temp', $this->_upload_path);
                            }
                        } else {
                            $attribute_images[] = array();
                        }
                        $temp_string = '';
                        for ($i = 0; $i < $total_attributes; $i++) {
                            if (isset($post_data[$attribute_ids[$i]][$key]) && $post_data[$attribute_ids[$i]][$key] > 0) {
                                $temp = array();
                                $temp['id_unique_product'] = $id_unique_product;
                                $temp['id_unique'] = $id_unique_product_attributes;
                                $temp['id_unique_attributes'] = $attribute_ids[$i];
                                $temp['id_unique_attribute_values'] = $post_data[$attribute_ids[$i]][$key];
                                //$temp_string .= $post_data[$attribute_ids[$i]][$key];
                                //$temp_string .= $value;                                
                                //if (!in_array($temp_string, $duplicate)) {
                                //$duplicate[] = $temp_string ;
                                $record_attribute[] = $temp;
                                //} else {
                                //                                    $return = false;
                                //                                    //'Please check product attributes. There is some duplicate records available'
                                //                                    break;
                                //}
                            } else {
                                //'Please check product attributes. Some attributes has no values'
                                //$return = false;
                                //break;
                            }
                        }
                        //                        if ($return == false) {
                        //                            //break;
                        //                        }
                        //                        $temp_string .= $value;
                        //                        // Checking duplicate records and validating all attribute fields
                        //                        if (!in_array($temp_string, $duplicate)) {
                        //                            $duplicate[] = $temp_string;
                        //                        } else {
                        //                            $return = false;
                        //                            //'Please check product attributes. There is some duplicate records available'
                        //                            break;
                        //                        }
                    } else {
                        //$return = false;
                        //'Please enter price for all selected attributes'
                    }
                }
                if (!empty($record_price) && !empty($record_attribute)) {
                    $id_unique_product_attribute_values = array();
                    foreach ($record_attribute as $key => $value) {
                        $id_unique_product_attribute_values[$value['id_unique']][] = $value['id_unique_attribute_values'];
                    }
                    $temp_new_attributes = array();
                    foreach ($id_unique_product_attribute_values as $key => $value) {
                        $temp = get_id_unique_attribute_values($id_unique_product, implode(',', $value), $total_attributes);
                        if ($temp == '') {
                            $temp_new_attributes[$key] = $key;
                        } else {
                            //If old attribute id available then we will not update that with new id 
                            $temp_new_attributes[$key] = $temp;
                        }
                    }
                    $id_temp = '';
                    $count = 0;
                    $all_attriute_ids = array_values($temp_new_attributes);
                    $is_ordered = array();
                    foreach ($all_attriute_ids as $key => $value) {
                        $is_ordered[$value] = check_if_order_available($value);
                    }

                    $this->common_model->update_data('product_attributes', array('id_unique_product' => null), array('where' => array('is_ordered' => 1), 'IN' => array('id_unique_product' => $id_unique_product), 'NOT IN' => array('id_unique' => $all_attriute_ids)));
                    $this->common_model->delete_data('product_prices', array('IN' => array('id_unique_product' => $id_unique_product)));
                    $this->common_model->delete_data('product_attributes', array('IN' => array('id_unique_product' => $id_unique_product)));
                    foreach ($record_attribute as $key => $value) {
                        $value['id_unique'] = $temp_new_attributes[$value['id_unique']];
                        $value['is_ordered'] = $is_ordered[$value['id_unique']];

                        $id_unique_product_attribute_images = $this->common_model->insert_data('product_attributes', $value);
                        if ($id_temp != $value['id_unique']) {
                            if ($id_unique_product_attribute_images > 0) {
                                $conditions = array('select' => 'id', 'WHERE' => array('id' => $id_unique_product_attribute_images));
                                $info = $this->common_model->select_data('product_attributes', $conditions);
                                if ($info['row_count'] > 0) {
                                    $id_unique = $info['data'][0]['id_unique'];
                                    $temp = array();
                                    if (isset($attribute_images[$count]) && !empty($attribute_images[$count]['file_name'])) {
                                        foreach ($attribute_images[$count]['file_name'] as $image) {
                                            $temp[] = array(
                                                'id_unique_product_attributes' => $id_unique,
                                                'image_name' => $image,
                                            );
                                        }
                                    }
                                    if (!empty($temp)) {
                                        $this->common_model->insert_data('product_attributes_image', $temp, true);
                                    }
                                }
                            }
                            $count++;
                        }
                        $id_temp = $value['id_unique'];
                    }

                    //Inserting price
                    foreach ($record_price as $key => $value) {
                        $value['id_unique_product_attributes'] = $temp_new_attributes[$value['id_unique_product_attributes']];
                        $this->common_model->insert_data('product_prices', $value);
                    }
                }
            } else {
                //$return = false;
                //'Please enter value of a single attribute'
            }
        } else {
            $this->common_model->delete_data('product_prices', array('IN' => array('id_unique_product' => $id_unique_product)));
            $records = array(
                'id_unique_product' => $id_unique_product,
                'id_unique_product_attributes' => null,
                'price' => $this->input->post('price'),
                'discount' => $this->input->post('discount') != null ? $this->input->post('discount') : 0,
                'is_product_available' => $this->input->post('is_product_available') != null ? $this->input->post('is_product_available') : 0,
                'quantity' => $this->input->post('quantity') != null ? $this->input->post('quantity') : 0,
            );
            $this->common_model->insert_data('product_prices', $records);
        }
        return $return;
    }

    /**
     * This function is used for two operation 
     * product enable and disable and 
     * product review enable disable
     */
    public function change_status()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            $error = false;
            $response_array = array('success' => false);
            $required_fields = array(
                'id' => get_line('something_wrong'),
                'table' => get_line('something_wrong'),
                'current_status' => get_line('something_wrong'),
            );
            foreach ($required_fields as $key => $value) {
                if (strlen(trim($this->input->post($key))) <= 0) {
                    $response_array['msg'] = $value;
                    $error = true;
                    break;
                }
            }
            if (!$error) {
                if ($this->input->post('table') == 'product_review') {
                    if ($this->input->post('current_status') == '0') {
                        $status = '1';
                    }
                    if ($this->input->post('current_status') == '1') {
                        $status = '0';
                    }
                    $records = array(
                        'is_approved' => $status,
                    );
                    $conditions = array(
                        "where" => array("id" => $this->input->post('id')),
                    );
                    $this->common_model->update_data('product_review', $records, $conditions);
                } else {
                    if ($this->input->post('current_status') == '0') {
                        $status = '1';
                    }
                    if ($this->input->post('current_status') == '1') {
                        $status = '0';
                        $this->common_model->delete_data('cart', array('where' => array('id_unique_product' => $this->input->post('id'))));
                    }
                    $records = array(
                        'is_active' => $status,
                    );
                    $conditions = array(
                        "where" => array("id_unique" => $this->input->post('id')),
                    );
                    $this->common_model->update_data($this->_table, $records, $conditions);
                }
                $response_array['success'] = true;
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function delete()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id')) && is_array($this->input->post('id'))) {
                if (delete_product($this->input->post('id'))) {
                    $response_array['status'] = true;
                    $response_array['message'] = $this->lang->line('deleted_successfully');
                } else {
                    $response_array['message'] = $this->lang->line('associated_with_other_records_delete');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function delete_image()
    {
        $response_array = array('status' => false, 'message' => get_line('product_single_image'));
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id_unique_product')) && !empty($this->input->post('image'))) {
                $sql = 'SELECT COUNT(id) AS total_images FROM product_images WHERE id_unique_product = "' . $this->input->post('id_unique_product') . '" ';
                $info = $this->common_model->get_data_with_sql($sql);
                if ($info['row_count'] > 0) {
                    if ($info['data'][0]['total_images'] > 1) {
                        @unlink($this->_upload_path . $this->input->post('image'));
                        $this->common_model->delete_data('product_images', array('where' => array('id_unique_product' => $this->input->post('id_unique_product'), 'image_name' => $this->input->post('image'))));
                        $response_array['status'] = true;
                        $response_array['message'] = $this->lang->line('deleted_successfully');;
                    }
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function delete_video()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id_unique_product')) && !empty($this->input->post('video'))) {
                @unlink($this->_upload_path . $this->input->post('video'));
                $this->common_model->update_data('product', array('product_video' => ''), array('where' => array('id_unique' => $this->input->post('id_unique_product'))));
                $response_array['status'] = true;
                $response_array['message'] = $this->lang->line('deleted_successfully');;
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function detail($id_unique)
    {
        $data = array();
        $data['page_title'] = $this->lang->line('product_detail');
        if ($id_unique != '') {
            $info = $this->get_detail($id_unique);
            $data = array_merge($data, $info);
        }
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/detail', $data);
    }

    public function check_child_category()
    {
        return true; //by passing child category checking
        $id_category = $this->input->post('id_category');
        $sql = 'SELECT id FROM category WHERE id != "' . $id_category . '" AND parent_id = "' . $id_category . '" ';
        $info = $this->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $this->form_validation->set_message('check_child_category', 'Please select child category');
            return false;
        }
        return true;
    }

    public function get_attribute_image()
    {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $id_unique_product_attributes = $this->input->post('id_unique_product_attributes');
            if (strlen($id_unique_product_attributes) > 0) {
                $info = $this->common_model->select_data('product_attributes_image', array('SELECT' => 'id,CONCAT("' . base_url('uploads/product/') . '",image_name) AS image_name', 'where' => array('id_unique_product_attributes' => $id_unique_product_attributes)));
                if ($info['row_count'] > 0) {
                    $response_array['data'] = $info['data'];
                    $response_array['status'] = true;
                } else {
                    $response_array['message'] = get_line('image_not_available');
                }
            }
        }
        echo json_encode($response_array);
        exit;
    }

    public function delete_attribute_image()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id'))) {
                $info = $this->common_model->select_data('product_attributes_image', array('where' => array('id' => $this->input->post('id'))));
                if ($info['row_count'] > 0) {
                    delete_file($this->_upload_path . $info['data'][0]['image_name']);

                    $this->common_model->delete_data('product_attributes_image', array('where' => array('id' => $this->input->post('id'))));
                }
                $response_array['status'] = true;
                $response_array['message'] = $this->lang->line('deleted_successfully');;
            }
        }
        echo json_encode($response_array);
        exit;
    }

    /**
     * This function is used for getting reviews based on id of the product
     */
    public function get_reviews()
    {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            if (!empty($this->input->post('id_unique'))) {
                $temp = array();
                $temp['reviews'] = get_reviews($this->input->post('id_unique'));
                $response_array['data'] = $this->load->view(AUTHORITY . '/' . $this->_slug . '/review-listing', $temp, true);
                $response_array['status'] = true;
                $response_array['message'] = $this->lang->line('deleted_successfully');;
            }
        }
        echo json_encode($response_array);
        exit;
    }
}
