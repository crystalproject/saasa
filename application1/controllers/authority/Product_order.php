<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_order extends CI_Controller {

    public $_language_info = array();
    public $_language_default = array();
    public $_page_title = '';
    public $_table = 'product_order';
    public $_slug = 'product-order';
    public $_column_slug = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->helper('upload_helper');
        //Language Settings
        $conditions = array('WHERE' => array('is_active' => '1'));
        $info = $this->common_model->select_data('language', $conditions);
        if ($info['row_count'] > 0) {
            $this->_language_info = $info['data'];
        }
        foreach ($this->_language_info as $value) {
            if ($value['default_language'] == '1') {
                $this->_language_default = $value;
                if ($value['id'] == '1') {
                    $this->_column_slug = '';
                } else {
                    $this->_column_slug = '_'.$value['slug'];
                }
                break;
            }
        }
        $this->_page_title = $this->lang->line('orders');
    }

    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->product_info = array();
            redirect(base_url('authority/' . $this->_slug));
        }
        $data = array();
        $data = array();
        $data['page_title'] = $this->_page_title;
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/view', $data);
    }

    public function get_records($page_number = '') {
        $response_array = array('status' => false, 'message' => '', 'data' => null);
        if ($this->input->is_ajax_request()) {
            $data = array();
            $sub_query = '';
            if ($this->input->post('search_value') != null) {
                $sub_query = ' AND (u.name LIKE "%' . trim($this->input->post('search_value')) . '%" OR u.email LIKE "%' . trim($this->input->post('search_value')) . '%" OR u.email LIKE "%' . trim($this->input->post('search_value')) . '%") ';
            }
            $sql = 'SELECT u.first_name,u.last_name,u.email,u.email,po.order_date_time,po.payment_mode,po.payment_status,po.order_status FROM product_order po,register u WHERE po.id_user = u.id ' . $sub_query . ' ORDER BY po.id DESC ';
            $sql = 'SELECT po.id AS total_records FROM product_order po,register u WHERE po.id_user = u.id ' . $sub_query . ' ORDER BY po.id DESC ';
            $info = $this->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                $total_records = $info['data'][0]['total_records'];
            } else {
                $total_records = 0;
            }
            $settings = array(
                'total_record' => $total_records,
                'url' => base_url(AUTHORITY . '/' . $this->_slug . '/get_records'),
                'per_page' => RECORDS_PER_PAGE,
                'page_number' => $page_number
            );
            $pagination = $this->common_model->only_pagination($settings);
            $data['pagination'] = $pagination['pagination'];
            $slug = '_' . $this->_language_default['slug'];
            if ($this->_language_default['id'] == '1') {
                $slug = '';
            }
            $sql = 'SELECT u.first_name,u.last_name,u.email,u.email,po.order_date_time,po.id,po.order_id,po.payment_mode,po.order_status,po.payment_status,po.order_status FROM product_order po,register u WHERE po.id_user = u.id ' . $sub_query . ' ORDER BY po.id DESC LIMIT ' . $pagination['start'] . ',' . $pagination['limit'];
            $info = $this->common_model->get_data_with_sql($sql);
            $data['info'] = array();
            if ($info['row_count'] > 0) {
                $data['info'] = $info['data'];
            }
            $response_array['status'] = true;
            $response_array['data'] = $this->load->view(AUTHORITY . '/' . $this->_slug . '/listing', $data, true);
        }
        echo json_encode($response_array);
        exit;
    }

    public function detail($id) {        
        $data = array();
        $data['page_title'] = $this->lang->line('order_detail');
        $sql = 'SELECT u.first_name,u.last_name,u.email,u.email,po.* FROM product_order po,register u WHERE 
                po.id_user = u.id AND po.id = "' . $id . '"';
        $info = $this->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $data['order_info'] = $info['data'][0];
            $id_product_order = $data['order_info']['id'];
            $data['product_info'] = array();
            $sub_sub_query = ' ,(SELECT image_name FROM product_images WHERE id_unique_product = p.id_unique ORDER BY id DESC LIMIT 1) AS image_name, 
                    (SELECT name' . $this->_column_slug . ' FROM category WHERE id = p.id_category) as category_name';
            //Quantity,Price and discount comes from product order meta table
            $sql = 'SELECT id_unique,title,description,main_image,has_attributes,by_quantity_or_availability,pom.*' . $sub_sub_query . ' FROM product p,product_order_meta pom WHERE pom.id_unique_product = p.id_unique AND 
                    pom.id_product_order = "' . $id_product_order . '" AND p.id_language = "' . $this->_language_default['id'] . '"   ';
            $info = $this->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                foreach ($info['data'] as &$value) {
                    $value['attributes'] = array();
                    if ($value['id_unique_product_attributes'] != null) {
                        $sql = 'SELECT attribute_name,attribute_value FROM 
                                attributes a,attribute_values av,product_attributes pa WHERE 
                                pa.id_unique_attributes = a.id_unique AND pa.id_unique_attribute_values = av.id_unique AND 
                                a.id_language = "' . $this->_language_default['id'] . '" AND av.id_language = "' . $this->_language_default['id'] . '" AND pa.id_unique = "' . $value['id_unique_product_attributes'] . '" ';
                        $attributes = $this->common_model->get_data_with_sql($sql);
                        if ($attributes['row_count'] > 0) {
                            //We'll have multiple values for making header take every first (attribute_name) from every array element
                            //For value user attribute_value
                            $value['attributes'] = $attributes['data'];
                        }
                    }
                }
                $data['product_info'] = $info['data'];
            }
            $data['bag_info'] = array();            

            $data['history_info'] = array();
            $data['removed_status'] = array();
            $sql = 'SELECT poh.* FROM product_order_history poh WHERE poh.id_product_order = "' . $id_product_order . '" ORDER BY poh.id ASC ';
            $info = $this->common_model->get_data_with_sql($sql);
            if ($info['row_count'] > 0) {
                $data['history_info'] = $info['data'];
                $data['removed_status'] = array_column($info['data'], 'order_status');
            }
        } else {
            redirect(base_url(AUTHORITY . '/' . $this->_slug));
        }
        $this->load->view(AUTHORITY . '/' . $this->_slug . '/detail', $data);
    }

    public function change_status() {
        $response_array = array('status' => false, 'message' => '');
        if ($this->input->is_ajax_request()) {
            $error = false;
            $response_array = array('success' => false);
            $required_fields = array(
                'id_product_order' => get_line('something_wrong'),
                'order_status' => get_line('something_wrong')
            );
            foreach ($required_fields as $key => $value) {
                if (strlen(trim($this->input->post($key))) <= 0) {
                    $response_array['msg'] = $value;
                    $error = true;
                    break;
                }
            }
            if (!$error) {
                $records = array(
                    'id_product_order' => $this->input->post('id_product_order'),
                    'order_status' => $this->input->post('order_status'),
                    'created_at' => CURRENT_TIME,
                );

                $sql = 'SELECT po.id_user,po.order_id,nt.token,nt.device_type FROM product_order po,notification_token nt,register r WHERE 
                nt.user_id = po.id_user AND r.id = po.id_user AND po.id = "'.$records['id_product_order'].'"';
                $info = $this->common_model->get_data_with_sql($sql);
                if($info['row_count'] > 0) {
                    $info = $info['data'][0];
                    $language_code = $this->_language_info['id'];
                    
                    $this->lang->load('english','english');
                    $info_english = get_order_status($records['order_status']);                    

                    $this->lang->load('turkish','turkish');
                    $info_turkish = get_order_status($records['order_status']);

                    $settings = array(
                        'user_id' => $info['id_user'],
                        'title' => $info_english['name'].', Order ID: #'.$info['order_id'],
                        'title_turkish' => $info_turkish['name'].', Order ID: #'.$info['order_id'],
                        'message' => $info_english['description'],
                        'message_turkish' => $info_turkish['description'],
                        'image' => '',
                        'token' => $info['token'],
                        'type' => 3,
                        'language_code' => $language_code,
                        'device_type' => $info['device_type'],
                        'order_id' => $info['order_id']
                    );
                    //send_push_notification($settings);

                    $records_temp = array(
                        'user_id' => $settings['user_id'],
                        'title' => $settings['title'],
                        'title_turkish' => $settings['title_turkish'],
                        'description' => $settings['message'],
                        'description_turkish' => $settings['message_turkish'],
                        'image' => $settings['image'],
                        'type' => '2',
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $notification_id = $this->common_model->insert_data('notification',$records_temp);
                    $records_temp = array(
                        'notification_id' => $notification_id,
                        'user_id' => $records_temp['user_id'],
                        'read_status' => 0,
                        'remove_status' => 0,
                    );
                    $notification_id = $this->common_model->insert_data('notification_status',$records_temp);
                }
                if($records['order_status'] == 3) {
                    revert_quantity($records['id_product_order']);
                }
                
                $default_language = isset($this->session->default_language) ? $this->session->default_language : DEFAULT_LANGUAGE;
                load_language($default_language);                
                $this->common_model->insert_data('product_order_history', $records);

                $this->common_model->update_data('product_order', array('order_status' => $records['order_status']), array('where' => array('id' => $records['id_product_order'])));

                $response_array['status'] = true;
                $response_array['message'] = get_line('updated_successfully');
            }
        }
        echo json_encode($response_array);
        exit;
    }

}
