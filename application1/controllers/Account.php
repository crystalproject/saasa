<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Account extends CI_Controller {
        public function __construct() {
            parent::__construct();
            if ($this->session->userdata('login_id') == null) {
                redirect(base_url());
            }
        }
        function index() {
            $condition = array('id' => $this->session->userdata('login_id'));
            $data = get_details('register', $condition);

            $condition = array('user_id' => $this->session->userdata('login_id'));
            $address['address'] = get_details('user_address', $condition);
            
            $data = array_merge($data[0],$address);
            $this->load->view('account/index',$data);
        }
        function edit() {
            $data = $this->input->post();
            $condition = array('id' => $this->session->userdata('login_id'));
            $details = get_details('register', $condition);

            if ($this->input->method() == 'post') { 

                $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique_with_except_record[register.email.id.' . $this->session->userdata('login_id') . ']', array('required' => get_line_front('this_field_is_required'), "valid_email" => get_line_front('please_enter_valid_email_address'),'is_unique_with_except_record'=> get_line_front('email_already_exist')));

                $this->form_validation->set_rules('first_name', 'first_name', 'required', array('required' => get_line_front('this_field_is_required'))); 
                $this->form_validation->set_rules('last_name', 'last_name', 'required', array('required' => get_line_front('this_field_is_required'))); 
                $this->form_validation->set_rules('gender', 'gender', 'required', array('required' => get_line_front('this_field_is_required')));
                $this->form_validation->set_rules('date_of_birth', 'date_of_birth', 'required', array('required' => get_line_front('this_field_is_required'))); 

                if ($this->form_validation->run() == FALSE)
                {
                    $data['data'] = array_merge($data, $this->form_validation->error_array());
                } else {
                    $data['date_of_birth'] = date('Y-m-d',strtotime($data['date_of_birth']));
                    $record = $this->production_model->update_record('register', $data, $condition);

                    $this->session->set_flashdata('success', get_line_front('updated_successfully'));
                    redirect(base_url('account'));
                }
            }
            unset($details[0]['password']);
            $data = isset($details) && $details != null ? $details[0] : $this->input->post();
            // echo"<pre>"; print_r($data); exit;
            $this->load->view('account/edit',$data);
        }
        public function address($id = '') {
            $message = '';
            $data = $this->input->post();
            $details = array();
            if (isset($id) && $id != null) {
                $condition = array('id' => $id);
                $details = get_details('user_address', $condition);
            }
            if ($this->input->method() == 'post') {
                if ($id == '') { // Add
                    if ($this->input->method() == 'post') {
                        $this->form_validation->set_rules('name', 'name', 'required', array('required' => get_line_front('this_field_is_required')));
                        $this->form_validation->set_rules('address', 'address', 'required', array('required' => get_line_front('this_field_is_required')));

                        if ($this->form_validation->run() == FALSE) {
                            $data = array_merge($data, $this->form_validation->error_array());
                        } else {
                            $data['user_id'] = $this->session->userdata('login_id');
                            $record = $this->production_model->insert_record('user_address', $data);
                            $message = 'add';
                        }
                    }
                } else { // Edit  
                    $this->form_validation->set_rules('name', 'name', 'required', array('required' => get_line_front('this_field_is_required')));
                    $this->form_validation->set_rules('address', 'address', 'required', array('required' => get_line_front('this_field_is_required')));
                    if ($data['address'] == 'percentage') 

                    if ($this->form_validation->run() == FALSE) {
                        $data = array_merge($details[0], $this->form_validation->error_array());
                    } else {             
                        $record = $this->production_model->update_record('user_address', $data, array('id' => $id));
                        $message = 'edit';
                    }
                }
                if (isset($message) && $message != '') {
                    if ($message == 'add') {
                        $this->session->set_flashdata('success', get_line_front('added_successfully'));
                    } elseif ($message == 'edit') {
                        $this->session->set_flashdata('success', get_line_front('updated_successfully'));
                    }
                    redirect(base_url('account'));
                }
            }
            $data = isset($details) && $details != null ? $details[0] : $this->input->post();
            $this->load->view('account/address', $data);
        }
        function remove_address($id = '') {
            $this->production_model->delete_record('user_address', array('id' => $id));
            $this->session->set_flashdata('success', get_line_front('deleted_successfully'));
            redirect(base_url('account'));
        }
        function changepassword() {
            $data = $this->input->post();
            if ($this->input->method() == 'post') {
                $this->form_validation->set_rules('current_password', 'current password', 'required|min_length[6]', array('required' => get_line_front('please_enter_current_password')));

                $this->form_validation->set_rules('new_password', 'new password', 'required|min_length[6]', array('required' => get_line_front('please_enter_new_password')));
                $this->form_validation->set_rules('confirm_new_password', 'confirm new password', 'required|min_length[6]|matches[new_password]', array('required' => get_line_front('Please_enter_confirm_password'), "matches" => get_line_front('password_and_confirm_password_should_be_same')));

                if ($this->form_validation->run() === FALSE) {
                    // $this->load->view('account/change_password', $data);
                } else {
                    $current_password = $this->input->post("current_password");
                    $get_record = $this->production_model->get_all_with_where('register','','',array('id'=>$this->session->userdata('login_id')));
                    $check = $this->encryption->decrypt($get_record[0]['password']); 
                    if($check != $current_password){           
                        $this->session->set_flashdata('error', get_line_front('please_enter_correct_old_password'));
                        redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $new_password = $this->input->post("new_password");
                        $records = array(
                            "password" => $this->encryption->encrypt($new_password),
                        );
                        $conditions = array(
                            "where" => array("id" => $this->session->userdata('login_id')),
                        );
                        $this->common_model->update_data('register', $records, $conditions);

                        $this->session->set_flashdata('success', get_line_front('password_changed_successfully'));
                        redirect(base_url('account'));
                    }
                }      
            }   
            $this->load->view('account/change_password', $data);
        }
    }
?>