<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Contact_us extends CI_Controller {
        public function __construct() {
            parent::__construct();
        }
        function index() {
            $data = $this->input->post();
            if ($this->input->method() == 'post') {         
                $this->form_validation->set_rules('name', 'name', 'required', array('required' => get_line_front('this_field_is_required'))); 
                $this->form_validation->set_rules('email', 'email', 'required|valid_email', array('required' => get_line_front('this_field_is_required'), "valid_email" => get_line_front('please_enter_valid_email_address')));
                $this->form_validation->set_rules('subject', 'subject', 'required', array('required' => get_line_front('this_field_is_required')));
                $this->form_validation->set_rules('message', 'message', 'required|strip_tags', array('required' => get_line_front('this_field_is_required'))); 
                if ($this->form_validation->run() == FALSE)
                {
                    $data['data'] = array_merge($data, $this->form_validation->error_array());
                } else {
                    // $this->load->view('mail_form/thankyou_page/contact_us',$data);
                    // $this->load->view('mail_form/admin_send_mail/contact_us',$data);
                    $record = $this->production_model->insert_record('contact_us', $data);

                    // $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',array($data['email']),'','mail_form/thankyou_page/contact_us','',''); // user send email thank-you page
                    
                    // $admin_email = get_details('administrator'); 
                    // $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/contact_us',$data,''); // admin send mail
                    $this->session->set_flashdata('success', get_line_front('thank_you_for_your_contact'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
            $this->load->view('contact_us/index',$data);
        }
    }
?>