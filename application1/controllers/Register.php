<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    function index()
    {
        $data = $this->input->post();
        if ($this->input->method() == 'post') {
            $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[register.email]', array('required' => get_line_front('this_field_is_required'), "valid_email" => get_line_front('please_enter_valid_email_address'), 'is_unique' => get_line_front('email_already_exist')));
            $this->form_validation->set_rules('password', 'password', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('first_name', 'first_name', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('last_name', 'last_name', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('gender', 'gender', 'required', array('required' => get_line_front('this_field_is_required')));
            $this->form_validation->set_rules('date_of_birth', 'date_of_birth', 'required', array('required' => get_line_front('this_field_is_required')));
            if ($this->form_validation->run() == FALSE) {
                $data['data'] = array_merge($data, $this->form_validation->error_array());
            } else {
                $data['password'] = $this->encryption->encrypt($data['password']);
                $data['date_of_birth'] = date('Y-m-d', strtotime($data['date_of_birth']));
                if (array_key_exists('otp', $data)) {
                    unset($data['otp']);
                }
                $record = $this->production_model->insert_record('register', $data);

                // $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',array($data['email']),'','mail_form/thankyou_page/contact_us','',''); // user send email thank-you page

                // $admin_email = get_details('administrator'); 
                // $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/contact_us',$data,''); // admin send mail
                $this->session->set_flashdata('success', get_line_front('thanks_for_registration'));
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        $this->load->view('register/index', $data);
    }
}
