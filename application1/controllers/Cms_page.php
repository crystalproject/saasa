<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Cms_page extends CI_Controller {
        public function __construct() {
            parent::__construct();
        }
        public function about_us() {
            $condition = array('id'=>4,'is_active'=>'1');
            $details['details'] = get_details('static_page',$condition);

            $condition = array('is_active'=>'1');            
            $happy_clients['happy_clients'] = get_details('happy_clients',$condition);

            $data = array_merge($details,$happy_clients);
            
            $this->load->view('cms_page/about_us',$data);
        }
        public function return_policy() {
            $condition = array('id'=>5,'is_active'=>'1');
            $data['details'] = get_details('static_page',$condition);

            $this->load->view('cms_page/return_policy',$data);
        }
        public function privacy_policy() {
        	$condition = array('id'=>2,'is_active'=>'1');
            $data['details'] = get_details('static_page',$condition);

            $this->load->view('cms_page/privacy_policy',$data);
        }
        public function terms_condition() {
            $condition = array('id'=>3,'is_active'=>'1');
            $data['details'] = get_details('static_page',$condition);

            $this->load->view('cms_page/terms_condition',$data);
        }
    }
?>