<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function dologin()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$this->form_validation->set_rules('email', 'email', 'trim|required', array('required' => get_line_front('this_field_is_required'), "valid_email" => get_line_front('please_enter_valid_email_address')));
		$this->form_validation->set_rules('password', 'password', 'trim|required', array('required' => get_line_front('this_field_is_required')));
		$user = '';
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', get_line_front('please_enter_email_and_password'));
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			// $usrlogin = $this->production_model->email_or_mobile_login('register',$email);
			$usrlogin = $this->production_model->get_all_with_where('register', '', '', array('email' => $email));
			// $sql = 'SELECT * FROM register WHERE email = "' . addslashes($email) . '" OR mobile_number = "' . addslashes($email) . '" ';
			// $info = $this->common_model->get_data_with_sql($sql);
			// if ($info['row_count'] > 0) {
			// 	$usrlogin = $info['data'];
			// } else {
			// 	$usrlogin = null;
			// }			
			if ($usrlogin != null) {
				$usrpass = $this->encryption->decrypt($usrlogin[0]['password']);
				if ($usrlogin[0]['is_active'] == '1') {
					if ($password == $usrpass) {
						$session = array(
							'login_id' => $usrlogin[0]['id'],
							'username' => $usrlogin[0]['first_name'],
							'useremail' => $usrlogin[0]['email'],
							'useremobile' => $usrlogin[0]['mobile_number'],
						);
						$this->session->set_userdata($session);

						$conditions = array(
							'where' => array('id_user' => COOKIE_USER)
						);
						$this->common_model->update_data('cart', array('id_user' => $session['login_id']), $conditions);

						$conditions = array(
							'where' => array('login_id' => COOKIE_USER)
						);
						$this->common_model->update_data('wishlist', array('login_id' => $session['login_id']), $conditions);
						$user = 'user';
					} else {
						$this->session->set_flashdata('error', get_line_front('password_incorrect'));
						redirect($_SERVER['HTTP_REFERER']);
					}
				} else {
					$this->session->set_flashdata('error', get_line_front('account_not_active'));
					redirect($_SERVER['HTTP_REFERER']);
				}
			} else {
				$this->session->set_flashdata('error', get_line_front('user_not_available'));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
		if ($user == 'user') {
			redirect(base_url('home'));
		} else {
			$this->session->set_flashdata('error', get_line_front('user_not_available'));
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	public function Logout()
	{
		$array_items = array('login_id', 'username', 'useremail');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
