<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
$cart_total = get_cart_total(get_front_login_id());
$discounted_total = $cart_total;
$total_discount = 0;
if ($this->session->coupon_code != '') {
    $form_data = $this->input->post();
    $form_data['id_language'] = get_language_id();
    $form_data['id_user'] = get_front_login_id();
    $form_data['coupon_code'] = $this->session->coupon_code;
    $coupon_info = apply_coupon_code($form_data);
    if ($coupon_info['status'] == true) {
        $total_discount = $coupon_info['data']['total_discount'];
        $discounted_total = $coupon_info['data']['discounted_total'];
    } else {
        $discounted_total = $cart_total;
    }
}
?>
<section class="our-story-section">
    <div class="container">
        <div id="accordion" class="myaccordion">
            <div class="card">
                <div class="card-header" id="headingOne" style="padding-left: 20px!important; padding-top: 6px;">
                    <h2 class="mb-0">
                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <?php echo get_line('discount_code'); ?>
                            <span class="fa-stack fa-sm">
                                <!-- <i class="fas fa-circle fa-stack-2x"></i> -->
                                <i class="fas fa-chevron-down fa-stack-1x fa-inverse" style="color: #000"></i>
                            </span>
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <p><?php echo get_line('discount_code_text'); ?></p>
                        <form class="row">
                            <div class="form-group col-md-4 col-lg-4 col-sm-12">
                                <input type="text" autocomplete="false" class="form-control" value="<?php echo isset($this->session->coupon_code) ? $this->session->coupon_code : ''; ?>" aria-describedby="emailHelp" placeholder="">
                            </div>
                            <button type="button" name="apply_coupon" class="form-submit-btn w-auto " style="height: 40px;"><?php echo get_line('apply_coupon'); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <form action="" method="POST" name="add-order" onsubmit="return false;">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="checkout-tile">
                        <h4><?php echo get_line('billing_info'); ?></h4>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <label for="name"><?php echo get_line('first_name'); ?></label>
                            <input type="text" autocomplete="false" name="first_name" class="form-control" placeholder="<?php echo get_line('first_name'); ?>" value="<?php echo isset($loggeed_in_user_info['first_name']) ? $loggeed_in_user_info['first_name'] : ''; ?>">
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <label for="name"><?php echo get_line('last_name'); ?></label>
                            <input type="text" autocomplete="false" name="last_name" class="form-control" placeholder="<?php echo get_line('last_name'); ?>" value="<?php echo isset($loggeed_in_user_info['last_name']) ? $loggeed_in_user_info['last_name'] : ''; ?>">
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="cname"><?php echo get_line('company_name'); ?></label>
                            <input type="text" autocomplete="false" name="company_name" class="form-control" placeholder="<?php echo get_line('company_name'); ?>" value="<?php echo isset($loggeed_in_user_info['company_name']) ? $loggeed_in_user_info['company_name'] : ''; ?>">
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 theme-dropdown">
                            <label for="cname"><?php echo get_line('country'); ?></label>
                            <select class="form-control form-control-xs selectpicker" name="country" data-size="7" data-live-search="true" data-title="Location" id="state_list" data-width="100%" style="background-color: #f7f7f7!important;border: 1px solid #a2a2a2!important">
                                <option value="" selected><?php echo get_line('select'); ?></option>
                                <option value="india">India</option>
                                <option value="usa">USA</option>
                                <option value="australia">Australia</option>
                                <option value="sri lank">Srilanka</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="street"><?php echo get_line('street'); ?></label>
                            <input type="text" autocomplete="false" name="street" class="form-control" placeholder="<?php echo get_line('street'); ?>">
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="apartment_suite_etc"><?php echo get_line('apartment_suite_etc'); ?></label>
                            <input type="text" autocomplete="false" name="apartment_suite_etc" class="form-control" placeholder="<?php echo get_line('apartment_suite_etc'); ?>">
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="city"><?php echo get_line('city'); ?></label>
                            <input type="text" autocomplete="false" name="city" class="form-control" placeholder="<?php echo get_line('city'); ?>">
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="pincode"><?php echo get_line('pincode'); ?> (optional)</label>
                            <input type="text" autocomplete="false" name="pincode" class="form-control" placeholder="<?php echo get_line('pincode'); ?>">
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="phone"><?php echo get_line('phone'); ?> *</label>
                            <input type="text" autocomplete="false" name="phone" class="form-control" placeholder="<?php echo get_line('phone'); ?>" value="<?php echo isset($loggeed_in_user_info['mobile_number']) ? $loggeed_in_user_info['mobile_number'] : ''; ?>">
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="email_address"><?php echo get_line('email_address'); ?></label>
                            <input type="text" autocomplete="false" name="email_address" class="form-control" placeholder="<?php echo get_line('email_address'); ?>" value="<?php echo isset($loggeed_in_user_info['email']) ? $loggeed_in_user_info['email'] : ''; ?>">
                        </div>
                        <div class="form-check col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-left: 9%;">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1"><?php echo get_line('ship_to_different_address'); ?></label>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlTextarea1"><?php echo get_line('order_notes'); ?></label>
                            <textarea class="form-control" name="order_notes" id="exampleFormControlTextarea1" rows="3" placeholder=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-pmd-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="checkout-tile">
                        <h4><?php echo get_line('order_review'); ?></h4>
                    </div>
                    <div class="table-responsive">
                        <table style="border: 1px solid #a2a2a2;width: 100%;">
                            <thead style="background-color: #f7f7f7f7">
                                <tr>
                                    <th><?php echo get_line('product'); ?></th>
                                    <th><?php echo get_line('total'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && !empty($data)) {
                                    foreach ($data as $value) {
                                ?>
                                        <tr class="text-center">
                                            <td><?php echo $value['title'] ?> × <?php echo $value['quantity']; ?><br>
                                                <?php
                                                if ($value['has_attributes'] == 0) {
                                                ?>
                                                    <?php
                                                } else {
                                                    foreach ($value['other_info']['attribute_info'] as $key => $a_value) {
                                                        foreach ($a_value['attribute_values'] as $key => $a_value_value) {
                                                    ?>
                                                            <?php echo $a_value['attribute_name']; ?>: <b><?php echo $a_value_value['attribute_value']; ?></b>
                                                <?php
                                                        }
                                                    }
                                                }
                                                ?></td>
                                            <td class="text-right"><?php echo PRICE_KEY; ?><?php echo ($value['quantity'] * $value['other_info']['final_price'][0]); ?></td>
                                        </tr>
                                <?php
                                    }
                                }
                                ?>
                                <tr>
                                    <td style="background-color: #f7f7f7"><?php echo get_line('total'); ?></td>
                                    <td class="text-right"><?php echo PRICE_KEY; ?><?php echo $cart_total; ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #f7f7f7"><?php echo get_line('discount'); ?></td>
                                    <td class="text-right"><?php echo PRICE_KEY; ?><?php echo $total_discount; ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #f7f7f7"><?php echo get_line('vat'); ?></td>
                                    <td class="text-right"><?php echo PRICE_KEY; ?>0.00</td>
                                </tr>
                                <tr>
                                    <td style="background-color: #f7f7f7"><?php echo get_line('total'); ?></td>
                                    <td class="text-right"><b><?php echo PRICE_KEY; ?><?php echo $discounted_total; ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-pmd-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="checkout-tile">
                        <h4><?php echo get_line('payment_method'); ?></h4>
                    </div>
                    <div class="form-check" id="show">
                        <input class="form-check-input" type="radio" name="payment_mode" id="exampleRadios1" value="1">
                        <label class="form-check-label" for="exampleRadios1">
                            <?php echo get_line('cheque_payment'); ?>
                        </label>
                    </div>
                    <p class="hide-p">
                        <?php
                        $temp = $this->common_model->select_data('payment_mode', array('where' => array('id_language' => get_language_id(), 'id_unique' => '1')));
                        if ($temp['row_count'] > 0) {
                            echo $temp['data'][0]['description'];
                        }
                        ?>
                    </p>
                    <div class="form-check" id="show-2">
                        <input class="form-check-input" type="radio" name="payment_mode" id="exampleRadios2" value="2" checked>
                        <label class="form-check-label" for="exampleRadios2">
                            <?php echo get_line('cash_on_delivery'); ?>
                        </label>
                    </div>
                    <p class="hide-p2" style="display: none;"><?php echo get_line('cash_upon_delivery'); ?></p>
                    <p style="margin-top: 30px;">
                        <?php
                        $temp = $this->common_model->select_data('payment_mode', array('where' => array('id_language' => get_language_id(), 'id_unique' => '2')));
                        if ($temp['row_count'] > 0) {
                            echo $temp['data'][0]['description'];
                        }
                        ?>
                    </p>
                    <h5><b><?php echo get_line('grand_total'); ?>: <?php echo PRICE_KEY; ?><?php echo $discounted_total; ?></b></h5>
                    <button type="button" class="form-submit-btn add-order-btn" style="float: left;width: 30%;font-size: 15px;"><?php echo get_line('place_order'); ?></button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php /*<section class="our-story-section">
    <div class="container">
        <form method="POST" name="add-order" action="POST" onsubmit="return false;" />
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                <div class="payment-information">
                    <div class="title">
                        <h3><b><?php echo get_line('billing_shipping_information'); ?></b></h3>
                    </div>
                    <div class="payment-information-box">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?php echo get_line('billing_info'); ?>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" autocomplete="false" name="billing_name" class="form-control" placeholder="<?php echo get_line('name'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" autocomplete="false" name="billing_address_line_1" class="form-control" placeholder="<?php echo get_line('address'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" autocomplete="false" name="billing_address_line_2" class="form-control" placeholder="<?php echo get_line('address'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12 pr-0">
                                <input type="text" autocomplete="false" name="billing_state" class="form-control" placeholder="<?php echo get_line('state'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <input type="text" autocomplete="false" name="billing_city" class="form-control" placeholder="<?php echo get_line('city'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <input type="text" autocomplete="false" name="billing_mobile" class="form-control" placeholder="<?php echo get_line('mobile'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <input type="text" autocomplete="false" name="billing_pincode" class="form-control" placeholder="<?php echo get_line('pincode'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-12">
                                <label>
                                    <input type="checkbox" name="is_shipping_same_as_billing" value="1">
                                    <?php echo get_line('is_shipping_same_as_billing'); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                <div class="payment-information">
                    <div class="title">
                        <h3>&nbsp;</h3>
                    </div>
                    <div class="payment-information-box">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?php echo get_line('shipping_info'); ?>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" autocomplete="false" name="shipping_name" class="form-control" placeholder="<?php echo get_line('name'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" autocomplete="false" name="shipping_address_line_1" class="form-control" placeholder="<?php echo get_line('address'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" autocomplete="false" name="shipping_address_line_2" class="form-control" placeholder="<?php echo get_line('address'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12 pr-0">
                                <input type="text" autocomplete="false" name="shipping_state" class="form-control" placeholder="<?php echo get_line('state'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <input type="text" autocomplete="false" name="shipping_city" class="form-control" placeholder="<?php echo get_line('city'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <input type="text" autocomplete="false" name="shipping_mobile" class="form-control" placeholder="<?php echo get_line('mobile'); ?>" style="border: none;height: 50px;">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <input type="text" autocomplete="false" name="shipping_pincode" class="form-control" placeholder="<?php echo get_line('pincode'); ?>" style="border: none;height: 50px;">
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="form-submit-btn add-order" style="float: left;position: absolute;top: 105%;text-decoration: none;"><?php echo get_line('submit'); ?> </a>
                </div>
            </div>
        </div>
        </form>
    </div>
</section> */ ?>
<?php
$footer_js = array(base_url('assets/js/bootstrap-select.js'));
$this->load->view('include/copyright', array('footer_js' => $footer_js));
$this->load->view('include/footer'); ?>