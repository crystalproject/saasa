<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
    $this->load->view('include/messages');
?>
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/shortcodes.css">
<!-- About Us Section Strat Here -->
<section class="our-story-section">
	<div class="container">
		<div class="porto-u-main-heading">
			<h2 style="text-transform: capitalize;"><?= get_line_front('my_account');?></h2>
		</div>
		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-12">
				<div class="myaccount-sidebar">
					<!-- <div class="myaccount-sidebar-header">
						<img src="<?= base_url();?>assets/images/avtar.jpg">
						<div class="name">
							<h3><b><?= isset($first_name) && $first_name !=null ? $first_name : '';?></b></h3>
						</div>
					</div> -->
					<div class="link">
						<ul>
							<li class="active"><a href="javascript:void(0);"><?= get_line_front('my_account');?></a></li>
							<li><a href="<?= base_url('account/changepassword');?>"><?= get_line_front('change_password');?></a></li>
							<li><a href="<?= base_url('my-order');?>">Orders</a></li>
							<li><a href="#">Returns</a></li>
							<li><a href="<?= base_url('wishlist');?>"><?= get_line_front('favorite');?></a></li>
							<li><a href="<?= base_url('cart');?>"><?= get_line_front('cart');?></a></li>
						</ul>
						<ul class="sign-out">
							<li><a href="<?= base_url('login/logout')?>"><?= get_line_front('sign_out');?></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9 col-sm-12">
				<!-- <div class="my-account-content">
					<div class="row border-bottom">
						<div class="col-md-3 col-lg-3">
							<div class="my-account-content-title">
								<h3><?= get_line_front('personal_info');?></h3>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="my-account-content-title">
								<h4><b><?= get_line_front('email_address');?></b> <span><?= isset($email) && $email !=null ? $email : '';?></span></h4>
							</div>
						</div>
						<div class="col-md-2 col-lg-2">
							<div class="my-account-content-title">
								<a href="<?= base_url('account/edit');?>" class="edit-btn"><?= get_line_front('edit');?></a>
							</div>
						</div>
					</div>
					<div class="row border-bottom">
						<div class="col-md-3 col-lg-3">
							<div class="my-account-content-title">
								<h3><?= get_line_front('my_account');?></h3>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="my-account-content-title">
								<h4><b><?= get_line_front('first_name');?></b> <span><?= isset($first_name) && $first_name !=null ? $first_name : '';?></span></h4>
								<h4><b><?= get_line_front('last_name');?></b> <span><?= isset($last_name) && $last_name !=null ? $last_name : '';?></span></h4>
								<h4><b><?= get_line_front('gender');?></b> <span><?= isset($gender) && $gender !=null ? $gender : '';?></span></h4>
								<h4><b><?= get_line_front('d.o.b');?></b> <span><?= isset($date_of_birth) && $date_of_birth !=null ? date('d M Y',strtotime($date_of_birth)) : '';?></span></h4>
							</div>
						</div>
					</div>
					<div class="row border-bottom">
						<div class="col-md-3 col-lg-3">
							<div class="my-account-content-title">
								<h3><?= get_line_front('address');?></h3>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="my-account-content-title">
								<a href="<?= base_url('account/address');?>" class="form-submit-btn" style="float: left;position: absolute;width: 31%;color: #ffffff"><?= get_line_front('add_address');?></a>
							</div>
						</div>
					</div>
					<?php
						if (isset($address) && $address !=null) {
							foreach ($address as $key => $value) {
								$id = $value['id'];
							?>
								<div class="row border-bottom">
									<div class="col-md-3 col-lg-3">
										<div class="my-account-content-title">
											<h3><?= $value['name'];?></h3>
											<h6><?= $value['address'];?></h6>
										</div>
									</div>
									<div class="col-md-6 col-lg-9">
										<div class="my-account-content-title">
											<a href="<?= base_url('account/remove-address/'.$id);?>" class="form-submit-btn" style="width: 5%;" onclick="return confirm('<?= get_line_front('delete_confirm');?>')"><i class="fa fa-trash-o"></i></a>
										</div>
									</div>
								</div>
							<?php }
						}
					?>
				</div> -->
				<p>Hello <strong><?php echo $this->session->userdata('username'); ?></strong> (<?php echo get_line('not') ?> <strong><?php echo $this->session->userdata('username'); ?></strong> <a href="<?php echo base_url('login/logout'); ?>" style="color: #000"><?= get_line_front('sign_out');?></a>)</p>
				<p style="font-size: 14px;">From your account dashboard you can view your recent orders, manage your shipping and billing addresses, and <a href="#" style="color: #000;">edit your password and account details.</a></p>
			</div>
		</div>
	</div>
</section>
<!-- About Us Section End Here -->
<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>