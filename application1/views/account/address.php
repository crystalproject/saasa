<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
    $this->load->view('include/messages');
?>
<section class="new-account-page-section">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-9 col-offset-1">
				<form id="form" method="post" class="registration-form">
					<div class="border-bottom row">
						<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
							<div class="form-title">
								<h4><?= get_line_front('address_info');?></h4>
							</div>
						</div>
						<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
							<div class="form-content">
								<div class="form-group row">
								    <label for="email" class="col-sm-3 col-form-label"><?= get_line_front('title');?></label>
								    <div class="col-sm-9">
								      <input type="name" name="name" class="form-control-plaintext" id="name" value="<?= isset($name) && $name !=null ? $name : '';?>" style="padding-left: 10px !important;">
								      <?= form_error('name', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
							</div>
							<div class="form-content">
								<div class="form-group row">
								    <label for="address" class="col-sm-3 col-form-label"><?= get_line_front('address');?></label>
								    <div class="col-sm-9">
								      <textarea name="address" class="form-control-plaintext" id="address" style="height: 100px !important;"><?= isset($address) && $address !=null ? $address : '';?></textarea>
								      <?= form_error('address', "<label class='error'>", "</label>");?>
								    </div>
							  	</div>
							</div>
						</div>
					</div>
					<button class="form-submit-btn"><?= get_line_front('submit');?> <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
				</form>
			</div>
		</div>
	</div>
</section>
<?php 
    $footer_js = array(base_url('assets/authority/js/toastr.min.js'));
    $this->load->view('include/copyright',array('footer_js'=> $footer_js));
?>
<script>
    $(document).ready(function(){
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {        
                name:{required : true},      
                address:{required : true},     
            },
            messages: {          
                name: {required: "<?= get_line_front('this_field_is_required')?>"},     
                address: {required :"<?= get_line_front('this_field_is_required')?>"},       
            }
        });
    });
</script>
<?php $this->load->view('include/footer');?>