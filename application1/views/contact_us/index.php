<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
    $this->load->view('include/messages');
?>
<section class="contact-us">
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14948644.59494812!2d36.03957103039324!3d23.833834106364563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15e7b33fe7952a41%3A0x5960504bc21ab69b!2sSaudi%20Arabia!5e0!3m2!1sen!2sin!4v1588064312356!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-12">
				<div class="contact-title porto-u-main-heading">
					<h2><?= get_line_front('get_in_touch');?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
				<div class="contact-title porto-u-main-heading" style="align-items: center;vertical-align: middle;">
					<?php
	                    $details = get_details('administrator',array('role'=>'Admin'));
	                    $mobile_number = isset($details) && $details !=null ? $details[0]['mobile_number_1'] : '';
	                    $email_address = isset($details) && $details !=null ? $details[0]['email_address_website'] : '';
	                    $address = isset($details) && $details !=null ? $details[0]['address'] : '';
	                ?>  
					<h2><?= get_line_front('the_office');?></h2>
					<ul>
						<li>
							<div class="porto-icon circle " style="color:#ffffff;background:#0088cc;font-size:14px;display:inline-block;margin-right: 2%;">
								<i class="fas fa-map-marker-alt"></i>
							</div>
						</li>
						<li>
							<div class="porto-sicon-header"style="margin-top: 3%;">
								<h3 class="porto-sicon-title" style="font-size:14px;color:#777777;">
									<strong><?= get_line_front('address');?>:</strong>
									<span><?= $address;?></span>
								</h3>
							</div>
						</li>
					</ul>
					<ul>
						<li>
							<div class="porto-icon circle " style="color:#ffffff;background:#0088cc;font-size:14px;display:inline-block;margin-right: 2%;">
								<i class="fa fa-phone"></i>
							</div>
						</li>
						<li>
							<div class="porto-sicon-header"style="margin-top: 3%;">
								<h3 class="porto-sicon-title" style="font-size:14px;color:#777777;">
									<strong><?= get_line_front('phone');?>:</strong>
									<span><?= $mobile_number;?></span>
								</h3>
							</div>
						</li>
					</ul>
					<ul>
						<li>
							<div class="porto-icon circle " style="color:#ffffff;background:#0088cc;font-size:14px;display:inline-block;margin-right: 2%;">
								<i class="fa fa-envelope"></i>
							</div>
						</li>
						<li>
							<div class="porto-sicon-header"style="margin-top: 3%;">
								<h3 class="porto-sicon-title" style="font-size:14px;color:#777777;">
									<strong><?= get_line_front('mail');?>:</strong>
									<span><?= $email_address;?></span>
								</h3>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12">
				<div class="contact-title-2 porto-u-main-heading">
					<h2><?= get_line_front('contact_us');?></h2>
				</div>
				<form id="form" method="post" class="row">
			 	 	<div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-6">
				    	<label for="name"><?= get_line_front('name');?></label>
				    	<input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="<?= get_line_front('enter_name');?>" style="border: none;height: 50px;">
				    	<?= form_error("name", "<label class='error'>", "</label>");?>
				  	</div>
			 	 	<div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-6">
				    	<label for="Email"><?= get_line_front('email_address');?></label>
				    	<input type="email" class="form-control" name="email" id="Email" aria-describedby="emailHelp" placeholder="<?= get_line_front('enter_email_address');?>" style="border: none;height: 50px;">
				    	<?= form_error("email", "<label class='error'>", "</label>");?>
				  	</div>
			 	 	<div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
				    	<label for="subject"><?= get_line_front('subject');?></label>
				    	<input type="text" class="form-control" name="subject" id="subject" aria-describedby="emailHelp" placeholder="<?= get_line_front('enter_subject');?>" style="border: none;height: 50px;">
				    	<?= form_error("subject", "<label class='error'>", "</label>");?>
				  	</div>
				  	<div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
					    <label for="messege"><?= get_line_front('message');?></label>
					    <textarea class="form-control" name="message" id="messege" placeholder="<?= get_line_front('enter_message');?>" rows="3" style="border: none;height: 90px;background-color: #f7f7f7"></textarea>
					    <?= form_error("message", "<label class='error'>", "</label>");?>
				  	</div>
				  	<div class="col-md-12">
						<button type="submit" class="form-submit-btn" style="float: left;width: 20%;"><?= get_line_front('submit');?> <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
				  	</div>
				</form>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('include/copyright');?>
<script>
    $(document).ready(function(){
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {        
                name:{required : true},      
                email:{required : true,email:true},      
                subject:{required : true},      
                message:{required : true},     
            },
            messages: {          
                name: {required :'<?= get_line_front('this_field_is_required')?>'},       
                email: {required: "<?= get_line_front('this_field_is_required')?>", email: "<?= get_line_front('please_enter_valid_email_address')?>"},
                subject: {required: "<?= get_line_front('this_field_is_required')?>"},     
                message: {required :"<?= get_line_front('this_field_is_required')?>"},       
            }
        });
    });
</script>
<?php $this->load->view('include/footer');?>