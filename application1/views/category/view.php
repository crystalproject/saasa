<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
?>
<!-- banner Section Strat Here -->
<section class="banner-section">
    <div class="container">
        <?php
        if (isset($banner) && $banner != null) {
        ?>
            <style>
                .banner-section-content {
                    background-image: url("<?= base_url(CAT_SUB_CAT_BANNER . $banner[0]['banner' . get_language_front()]); ?>") !important;
                }
            </style>
            <div class="banner-section-content">
                <h1 style="font-size: 50px;color: #ffffff;text-align: center"><?= $banner[0]['name' . get_language_front()]; ?></h1>
            </div>
        <?php }
        ?>
    </div>
</section>
<!-- banner Section End Here -->
<!-- Clothes Section Strat Here -->
<section class="clothes-section">
    <div class="clothes-section-content">
        <div class="container">
            <div class="porto-u-main-heading-2">
                <h2><?= isset($banner) && $banner != null ? $banner[0]['name' . get_language_front()] : ''; ?></h2>
            </div>
            <div class="row">
                <?php
                if (isset($sub_category) && $sub_category != null) {
                    foreach ($sub_category as $key => $value) {
                        $id = $value['id'];
                ?>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                            <div class="clothes-section-box text-center">
                                <a href="<?php echo $value['base_url']; ?>" class="new-link">
                                    <img src="<?= base_url(CATEGORY_IMAGE . $value['image' . get_language_front()]); ?>">
                                    <div class="product-box-title">
                                        <h4><b><?= $value['name' . get_language_front()]; ?></b></h4>
                                    </div>
                                </a>
                                <?php /*<a href="<?php echo $value['base_url']; ?>" class="discover-btn"><?= get_line_front('discover'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>*/ ?>
                            </div>
                        </div>
                <?php }
                }
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Clothes Section End Here -->
<?php $this->load->view('include/copyright'); ?>
<?php $this->load->view('include/footer'); ?>