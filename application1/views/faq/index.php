<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
?>
<section class="our-story-section">
	<div class="container">
		<div class="porto-u-main-heading">
			<h2><?= get_line_front('faq'); ?></h2>
		</div>
		<div id="" class="myaccordion">
			<?php
			if (isset($details) && $details != null) {
				foreach ($details as $key => $value) {
			?>
					<div class="card">
						<div class="card-header" id="headingOne<?= $key; ?>">
							<h2 class="mb-0">
								<button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-target="#collapseOne<?= $key; ?>" aria-expanded="false" aria-controls="collapseOne<?= $key; ?>">
									<?= $value['title' . get_language_front()]; ?>
									<span class="fa-stack fa-sm">
										<i class="fas fa-circle fa-stack-2x"></i>
										<i class="toggle-icon fas fa-<?= $key == 0 ? 'minus' : 'plus'; ?> fa-stack-1x fa-inverse"></i>
									</span>
								</button>
							</h2>
						</div>
						<div id="collapseOne<?= $key; ?>" class="collapse show" style="<?= $key == 0 ? '' : 'display:none;'; ?>" aria-labelledby="headingOne<?= $key; ?>" data-parent="#accordion">
							<div class="card-body">
								<p><?= $value['description' . get_language_front()]; ?></p>
							</div>
						</div>
					</div>
			<?php }
			}
			?>
		</div>
	</div>
</section>
<?php $this->load->view('include/copyright'); ?>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>

<script type="text/javascript">
	/*$("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
		$(e.target)
			.prev()
			.find("i:last-child")
			.toggleClass("fa-minus fa-plus");
	});*/
	$(document).ready(function() {
		$('.toggle-icon').click(function() {
			$(this).toggleClass('fa-minus fa-plus');
			$(this).closest('.card').find('.collapse').slideToggle();
		});
	});
</script>
<?php $this->load->view('include/footer'); ?>