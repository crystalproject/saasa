<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
if (isset($info) && !empty($info)) {
    foreach ($info as $value) {
        ?>
        <tr>
            <td style="width: 10px;">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?php echo $value['id_unique']; ?>" value="<?php echo $value['id_unique']; ?>">
                    <label for="customCheckbox<?php echo $value['id_unique']; ?>" class="custom-control-label"></label>
                </div>
            </td>
            <td><?php echo $value['title']; ?></td>
            <td><?php echo $value['description']; ?></td>
            <td><img src="<?php echo base_url('uploads/gift-wraping/'.$value['image_name']); ?>" class="img img-thumbnail" width="80" height="80"/></td>
            <td>
                <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug . '/add-edit/' . $value['id_unique']); ?>" class="btn bg-gradient-primary btn-xs"><i class="fas fa-edit"></i></a>
                <a href="javascript:void(0)" class="btn bg-gradient-danger btn-xs delete_record" id="<?php echo $value['id_unique']; ?>"><i class="fa fa-trash-o"></i></a>
                <?php
                /*if ($value['is_active'] == '1') {
                    echo '<span class="btn bg-gradient-success btn-xs change-status" data-table="attributes" data-id="' . $value['id_unique'] . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                } else {
                    echo '<span class="btn bg-gradient-danger btn-xs change-status" data-table="attributes" data-id="' . $value['id_unique'] . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                }*/
                ?>
            </td>
        </tr>
        <?php
    }
    ?>
    <tr>
        <td style="width: 10px;">
            <button class="btn btn-sm btn-danger delete_all"><?php echo get_line('delete'); ?></button>
        </td>
        <td colspan="4">
            <?php
            if (isset($pagination) && !empty($pagination)) {
                echo $pagination;
            }
            ?>
        </td>
    </tr>
    <?php
} else {
    ?>
    <tr>
        <td colspan="5" class="text-center"><?php echo get_line('record_not_available'); ?></td>
    </tr>
    <?php
}