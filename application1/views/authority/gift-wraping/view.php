<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->view('authority/common/header');
$this->view('authority/common/sidebar');
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo isset($page_title) ? $page_title : ''; ?></li>
                    </ol>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug . '/add-edit'); ?>" class="btn btn-sm btn-info text-white cursor-pointer"><?php echo get_line('add'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo isset($page_title) ? $page_title : ''; ?></h3>
                            <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="search_value" class="form-control float-right" placeholder="<?php echo get_line('search'); ?>" autocomplete="off">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-default search-btn"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" id="select_all">
                                                <label for="select_all" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th><?php echo get_line('attribute_name'); ?></th>
                                        <th><?php echo get_line('attribute_value'); ?></th>
                                        <th><?php echo get_line('image'); ?></th>
                                        <th><?php echo get_line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody class="attribute-info">
                                    <tr>
                                        <td colspan="4" class="text-center"><?php echo get_line('loading'); ?></td>
                                    </tr>
                                    <?php /* <tr>
                                      <td style="width: 10px;">
                                      <div class="custom-control custom-checkbox">
                                      <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox1" value="">
                                      <label for="customCheckbox1" class="custom-control-label"></label>
                                      </div>
                                      </td>
                                      <td><?php echo get_line('attribute_name'); ?></td>
                                      <td>
                                      <a href="#" class="btn bg-gradient-primary btn-xs"><i class="fas fa-edit"></i></a>
                                      <a href="javascript:void(0)" class="btn bg-gradient-danger btn-xs delete_record_cat" id="#"><i class="fa fa-trash-o"></i></a>
                                      <span class="btn bg-gradient-danger btn-xs change-status-tmp" data-table="category" data-id="" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>
                                      </td>
                                      </tr>
                                      <tr>
                                      <td style="width: 10px;">
                                      <button class="btn btn-sm btn-danger"><?php echo get_line('delete'); ?></button>
                                      </td>
                                      <td colspan="2">
                                      <ul class="pagination pagination-sm m-0 float-right">
                                      <li class="page-item"><a class="page-link" href="#">«</a></li>
                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                                      <li class="page-item"><a class="page-link" href="#">»</a></li>
                                      </ul>
                                      </td>
                                      </tr> */ ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal" id="value-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo get_line('attribute_value'); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body p-0">
                <div class="table-responsive value-listing">

                </div>
            </div>
        </div>
    </div>
</div>
<?php
$footer_js = array();
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<script>
    var slug = '<?php echo $this->_slug; ?>';
    $(document).ready(function() {
        //Listing
        function get_records(url = '') {
            loading(true);
            if (url == '') {
                url = BASE_URL + 'authority/' + slug + '/get_records';
            }
            $.ajax({
                url: url,
                data: {
                    'search_value': $('[name="search_value"]').val()
                },
                method: 'POST',
                dataType: 'json',
                success: function(response) {
                    loading(false);
                    if (response.status) {
                        $('.attribute-info').html(response.data);
                    }
                }
            });
        }
        get_records();

        $(document).on('click', '[data-ci-pagination-page]', function(e) {
            get_records($(this).attr('href'));
            e.preventDefault();
        });
        $(document).on('click', '.search-btn', function(e) {
            get_records();
            e.preventDefault();
        });

        //Status Change
        $(document).on('click', '.change-status', function() {
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/' + slug + '/change_status',
                data: post_data,
                async: false,
                beforeSend: function() {
                    loading(true);
                },
                success: function(response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('bg-gradient-danger bg-gradient-success');
                        if (current_element.hasClass('bg-gradient-success')) {
                            current_element.html('<i class="fa fa-check" aria-hidden="true"></i>');
                            current_element.attr('data-current-status', '1');
                        } else {
                            current_element.html('<i class="fa fa-times" aria-hidden="true"></i>');
                            current_element.attr('data-current-status', '0');
                        }
                    } else {
                        toastr.error(response.message);
                        //window.location = window.location.href;
                    }
                },
                complete: function() {
                    setTimeout(function() {
                        loading(false);
                    }, 100);
                },
            });
        });

        //Delete Record
        $(document).on('click', '.delete_record', function(e) {
            var id = []
            id.push($(this).attr('id'));
            var post_data = {
                'id': id
            };
            var current = $(this).closest('tr');
            $.confirm({
                title: '<?php echo get_line('confirm'); ?>',
                type: 'red',
                content: '<?php echo get_line('delete_confirm'); ?>',
                buttons: {
                    confirm: function() {
                        $.ajax({
                            url: BASE_URL + 'authority/' + slug + '/delete',
                            type: 'POST',
                            dataType: 'json',
                            data: post_data,
                            beforeSend: function() {
                                loading(true);
                            },
                            success: function(response) {
                                if (response.status) {
                                    current.remove();
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            },
                            complete: function() {
                                setTimeout(function() {
                                    loading(false);
                                }, 1000);
                            },
                        });
                    },
                    cancel: function() {},
                }
            });
        });

        // multiple delete //
        $(document).on('click', '.delete_all', function() {
            var id = [];
            $(".chk_all:checked").each(function() {
                id.push($(this).val());
            });
            var post_data = {
                'id': id
            };
            var boxes = $('.chk_all:checkbox');
            if (boxes.length > 0) {
                if ($('.chk_all:checkbox:checked').length < 1) {
                    $.alert({
                        title: '<?php echo get_line('oops'); ?>',
                        type: 'red',
                        content: '<?php echo get_line('atleast_one_checkbox'); ?>',
                    });
                    return false;
                } else {
                    $.confirm({
                        title: '<?php echo get_line('confirm'); ?>',
                        type: 'red',
                        content: '<?php echo get_line('delete_confirm'); ?>',
                        buttons: {
                            confirm: function() {
                                $.ajax({
                                    url: BASE_URL + 'authority/' + slug + '/delete',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: post_data,
                                    beforeSend: function() {
                                        loading(true);
                                    },
                                    success: function(response) {
                                        if (response.status) {
                                            $(".chk_all:checked").each(function() {
                                                $(this).closest('tr').remove();
                                            });
                                            toastr.success(response.message);
                                        } else {
                                            toastr.error(response.message);
                                        }
                                    },
                                    complete: function() {
                                        setTimeout(function() {
                                            loading(false);
                                        }, 1000);
                                    },
                                });
                            },
                            cancel: function() {},
                        }
                    });
                    return false;
                }
            }
        });

        //Attribute value submit 
        /*$(document).on('click', '.attr-value-submit', function (e) {
         var current = $(this).closest('.value-form');
         loading(true);
         $.ajax({
         url: BASE_URL + 'authority/' + slug + '/add_edit_values',
         method: 'POST',
         data: {'id_unique_attributes': current.find('[name="attribute_value"]').attr('data-id_unique_attributes'), 'attribute_value': current.find('[name="attribute_value"]').val(), 'edit_id': current.find('[name="attribute_value"]').attr('data-edit_id')},
         dataType: 'json',
         success: function (response) {
         loading(false);
         if (response.status) {
         //current.find('input:visible').val('');
         current.addClass('hidden');
         if (current.find('[name="attribute_value"]').attr('data-edit_id') != '') {
         //Edit Mode
         current.prev('.name').text(current.find('[name="attribute_value"]').val());
         } else {
         //Add Mode
         current.closest('td').find('.view-values').text(parseInt(current.closest('td').find('.view-values').text()) + 1);
         }
         toastr.success(response.message);
         } else {
         toastr.error(response.message);
         }
         }
         });
         }); */
        $('#add-value-form').submit(function() {
            var current = $(this);
            var jform = new FormData(this);            
            jform.append('image', $('[name="image"]').get(0).files[0]);
            loading(true);
            $.ajax({
                url: BASE_URL + 'authority/' + slug + '/add_edit_values',
                method: 'POST',
                //data: current.serialize(),
                data: jform,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    loading(false);
                    if (response.status) {
                        current.find('input[type="text"]:visible').val('');
                        toastr.success(response.message);
                        $('#add-edit-value-modal').modal('hide');
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });

        $(document).on('click', '.view-values', function() {
            var current = $(this);
            loading(true);
            $.ajax({
                url: BASE_URL + 'authority/' + slug + '/get_attribute_values',
                method: 'POST',
                data: {
                    'id_unique_attributes': current.attr('data-id_unique_attributes')
                },
                dataType: 'json',
                success: function(response) {
                    loading(false);
                    if (response.status) {
                        $('#value-modal').modal('show');
                        $('.value-listing').html(response.data);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });

        $(document).on('click', '.add-value', function() {
            $('#add-edit-value-modal').find('[name="id_unique"]').val('');
            $('#add-edit-value-modal').find('[name="id_unique_attributes"]').val($(this).attr('id'));
            $('[name="image"]').val('');
            $('.custom-file-label').text('');
            $('#add-edit-value-modal').modal('show');
        });
        $(document).on('click', '.edit-attribute-value', function() {
            var current = $(this);
            $.ajax({
                url: BASE_URL + 'authority/' + slug + '/get_value_detail',
                type: 'POST',
                dataType: 'json',
                data: {
                    'id_unique': current.attr('id-unique'),
                    'id_unique_attributes': current.attr('id-unique-attribtues')
                },
                beforeSend: function() {
                    loading(true);
                },
                success: function(response) {
                    if (response.status) {
                        $('#value-modal').modal('hide');
                        $(response.data).each(function(key, value) {
                            $('[name="attribute_value_' + value.id_language + '"]').val(value.attribute_value);
                        });
                    } else {
                        toastr.error(response.message);
                    }
                },
                complete: function() {
                    setTimeout(function() {
                        loading(false);
                    }, 1000);
                },
            });
            $('#add-edit-value-modal').find('[name="id_unique"]').val(current.attr('id-unique'));
            $('#add-edit-value-modal').find('[name="id_unique_attributes"]').val(current.attr('id-unique-attribtues'));
            $('[name="image"]').val('');
            $('.custom-file-label').text('');
            $('#add-edit-value-modal').modal('show');
        });
        /*
         //This was used for inline input
         $(document).on('click', '.add-value', function () {
         $('.value-form').addClass('hidden');
         $('.value-form').find('input').val('');
         $(this).next('.value-form').removeClass('hidden');
         });
         $(document).on('click', '.edit-attribute-value', function () {
         $('.value-form').addClass('hidden');
         $(this).closest('tr').find('.value-form').removeClass('hidden');
         }); */

        //Delete Record
        $(document).on('click', '.delete-attribute-value', function(e) {
            var id = []
            id.push($(this).attr('id'));
            var post_data = {
                'id': id
            };
            var current = $(this).closest('tr');
            $.confirm({
                title: '<?php echo get_line('confirm'); ?>',
                type: 'red',
                content: '<?php echo get_line('delete_confirm'); ?>',
                buttons: {
                    confirm: function() {
                        $.ajax({
                            url: BASE_URL + 'authority/' + slug + '/delete_value',
                            type: 'POST',
                            dataType: 'json',
                            data: post_data,
                            beforeSend: function() {
                                loading(true);
                            },
                            success: function(response) {
                                if (response.status) {
                                    current.remove();
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            },
                            complete: function() {
                                setTimeout(function() {
                                    loading(false);
                                }, 1000);
                            },
                        });
                    },
                    cancel: function() {},
                }
            });
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>