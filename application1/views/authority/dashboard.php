<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->view('authority/common/header');
    $this->view('authority/common/sidebar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo get_line('dashboard'); ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('dashboard'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">                
                <!-- <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('authority/user')?>">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">New user</span>
                                <span class="info-box-number"><?= isset($count_user) && $count_user !=null ? $count_user : 0;?></span>
                            </div>
                        </div>
                    </a>
                </div> -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $this->view('authority/common/copyright'); ?>
<?php $this->view('authority/common/footer'); ?>