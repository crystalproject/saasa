<?php
defined('BASEPATH') or exit('No direct script access allowed');

$header_css = array(base_url('assets/authority/summernote/summernote.css'), base_url('assets/authority/bootstrap-select/css/bootstrap-select.min.css'));
$this->view('authority/common/header', array('header_css' => $header_css));
$this->view('authority/common/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo isset($page_title) ? $page_title : ''; ?></li>
                    </ol>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug); ?>" class="btn btn-sm btn-info text-white cursor-pointer"><?php echo get_line('view'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo isset($page_title) ? $page_title : ''; ?></h3>
                        </div>
                        <?php
                        echo form_open('', array('id' => 'add-form', 'method' => 'POST', 'onsubmit' => 'return false;', 'class' => ''));
                        ?>
                        <div class="card-body">
                            <?php /* <h4><?php echo get_line('product'); ?> Information</h4> */ ?>
                            <ul class="nav nav-tabs" id="product-tabs-tab" role="tablist">
                                <li class="nav-item ">
                                    <a class="nav-link active" id="product-tabs-category-tab" href="#product-tabs-category" data-toggle="pill" role="tab" aria-controls="product-tabs-category" aria-selected="false"><?php echo get_line('category'); ?></a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" id="product-tabs-images-tab" href="#product-tabs-images" data-toggle="pill" role="tab" aria-controls="product-tabs-images" aria-selected="false"><?php echo get_line('image'); ?>s</a>
                                </li>
                                <?php
                                foreach ($this->_language_info as $l_value) {
                                ?>
                                    <li class="nav-item ">
                                        <a class="nav-link" id="product-tabs-<?php echo 'lanaguage-' . $l_value['id'] ?>-tab" href="#product-tabs-<?php echo 'lanaguage-' . $l_value['id'] ?>" aria-controls="product-tabs-<?php echo 'lanaguage-' . $l_value['id'] ?>" data-toggle="pill" role="tab" aria-selected="false"><?php echo $l_value['name']; ?></a>
                                    </li>
                                <?php
                                }
                                ?>
                                <li class="nav-item ">
                                    <a class="nav-link" id="product-tabs-attribute-price-tab" href="#product-tabs-attribute-price" data-toggle="pill" role="tab" aria-controls="product-tabs-attribute-price" aria-selected="false"><?php echo get_line('attribute'); ?> & <?php echo get_line('price'); ?></a>
                                </li>
                            </ul>
                            <?php /*
                              <div class="tab-custom-content">
                              <p class="lead mb-0">Custom Content goes here</p>
                              </div> */ ?>
                            <div class="tab-content" id="product-tabs-tabContent">
                                <div class="tab-pane fade active show" id="product-tabs-category" role="tabpanel" aria-labelledby="product-tabs-category-tab">
                                    <div class="row mt-3 ml-2">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('category'); ?> <span class="required">*</span></label>
                                                <?php
                                                $GLOBALS['category_options'] = array();

                                                function get_category_tree($parent_id = 0, $sub_mark = '')
                                                {
                                                    $details = get_details('category', array('parent_id' => $parent_id));
                                                    if (isset($details) && $details != null) {
                                                        foreach ($details as $key => $value) {
                                                            $GLOBALS['category_options'][] = array('id' => $value['id'], 'name' => $sub_mark . $value['name' . get_language()]);
                                                            get_category_tree($value['id'], $sub_mark . '--&nbsp;&nbsp;');
                                                        }
                                                    }
                                                }

                                                get_category_tree();
                                                $options = array();
                                                foreach ($GLOBALS['category_options'] as $value) {
                                                    $options[$value['id']] = $value['name'];
                                                }
                                                $selected = isset($product_info['id_category']) ? $product_info['id_category'] : '';
                                                echo form_dropdown('id_category', $options, $selected, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('brand'); ?> <span class="required">*</span></label>
                                                <?php
                                                $GLOBALS['brand_options'] = array();

                                                function get_brand_tree()
                                                {
                                                    $details = get_details('brands', array('is_active' => 1));
                                                    if (isset($details) && $details != null) {

                                                        foreach ($details as $key => $value) {
                                                            $GLOBALS['brand_options'][] = array('id' => $value['id'], 'name' => $value['name' . get_language('admin')]);
                                                        }
                                                    }
                                                }

                                                get_brand_tree();
                                                $options = array();

                                                foreach ($GLOBALS['brand_options'] as $value) {
                                                    $options[$value['id']] = $value['name'];
                                                }
                                                
                                                $selected = isset($product_info['id_brand']) ? $product_info['id_brand'] : '';
                                                echo form_dropdown('id_brand', $options, $selected, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo get_line('is_best_seller'); ?> </label>
                                                <?php
                                                $selected = isset($product_info['is_best_seller']) && $product_info['is_best_seller'] == 1 ? true : false ;

                                                $chkdata = [
                                                    'name'    => 'is_best_seller',
                                                    'value'   => '1',
                                                    'checked' => $selected,
                                                    'style'   => 'margin:10px'  
                                                ];
                                                echo form_checkbox($chkdata);
                                                ?>
                                            </div>
                                        </div>


                                        <?php /*
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo get_line('is_related'); ?> <?php echo get_line('product'); ?>? <span class="required">*</span></label>
                                                <?php
                                                $options = array();
                                                $options['0'] = get_line('no');
                                                $options['1'] = get_line('yes');
                                                $selected = isset($product_info['is_popular']) ? $product_info['is_popular'] : '';
                                                echo form_dropdown('is_popular', $options, $selected, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div> */ ?>
                                        <!--<div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo get_line('is_new'); ?> <?php echo get_line('product'); ?>? <span class="required">*</span></label>
                                                <?php
                                                $options = array();
                                                $options['0'] = get_line('no');
                                                $options['1'] = get_line('yes');
                                                $selected = isset($product_info['is_new']) ? $product_info['is_new'] : '';
                                                echo form_dropdown('is_new', $options, $selected, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo get_line('is_gift_wraping'); ?> <?php echo get_line('product'); ?>? <span class="required">*</span></label>
                                                <?php
                                                $options = array();
                                                $options['0'] = get_line('no');
                                                $options['1'] = get_line('yes');
                                                $selected = isset($product_info['is_gift_wraping']) ? $product_info['is_gift_wraping'] : '';
                                                echo form_dropdown('is_gift_wraping', $options, $selected, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?php echo get_line('is_customizable'); ?>? <span class="required">*</span></label>
                                                <?php
                                                $options = array();
                                                $options['0'] = get_line('no');
                                                $options['1'] = get_line('yes');
                                                $selected = isset($product_info['is_customizable']) ? $product_info['is_customizable'] : '';
                                                echo form_dropdown('is_customizable', $options, $selected, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="product-tabs-images" role="tabpanel" aria-labelledby="product-tabs-images-tab">
                                    <div class="row mt-3 ml-2">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('main_image'); ?> <span class="required">*</span></label>
                                                <?php
                                                echo form_input(array('type' => 'file', 'name' => 'main_image', 'accept' => 'image/*', 'class' => 'form-control'));
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('image'); ?> <span class="required">*</span></label>
                                                <?php
                                                echo form_input(array('type' => 'file', 'name' => 'images[]', 'multiple' => 'multiple', 'accept' => 'image/*', 'class' => 'form-control'));
                                                ?>
                                            </div>
                                        </div>
                                        <!--<div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('video'); ?> </label>
                                                <?php
                                                echo form_input(array('type' => 'file', 'name' => 'product_video', 'accept' => 'video/*', 'class' => 'form-control'));
                                                ?>
                                            </div>
                                        </div>-->
                                        <?php /*
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('youtube_link'); ?> </label>
                                                <?php
                                                echo form_input(array('autocomplete' => 'off', 'type' => 'text', 'name' => 'youtube_link','value'=>isset($product_info['youtube_link']) ? $product_info['youtube_link'] : '',  'placeholder' => get_line('youtube_link'), 'class' => 'form-control'));
                                                ?>
                                            </div>
                                        </div> */ ?>
                                        <!--<div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('enter_sku'); ?> <span class="required">*</span></label>
                                                <?php
                                                echo form_input(array('autocomplete' => 'off', 'type' => 'text', 'name' => 'sku', 'value' => isset($product_info['sku']) ? $product_info['sku'] : '', 'placeholder' => get_line('enter_sku'), 'class' => 'form-control'));
                                                ?>
                                            </div>
                                        </div>-->
                                        <div class="col-md-12">
                                            <?php
                                            if (isset($product_info['main_image']) && !empty($product_info['main_image'])) {
                                            ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4><?php echo get_line('main_image'); ?></h4>
                                                        <div class="img-wrap">
                                                            <img style="max-width:100px;max-height: 100px;" class="img img-thumbnail" src="<?php echo base_url('uploads/product/' . $product_info['main_image']); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            <div class="row">
                                                <?php
                                                if (isset($product_images) && !empty($product_images)) {
                                                ?>
                                                    <div class="col-md-12">
                                                        <h4><?php echo get_line('image'); ?></h4>
                                                    </div>
                                                    <?php
                                                    foreach ($product_images as $image_name) {
                                                    ?>
                                                        <div class="col-md-2">
                                                            <div class="img-wrap">
                                                                <span data-id-unique="<?php echo isset($product_info['id_unique']) ? $product_info['id_unique'] : ''; ?>" data-image="<?php echo $image_name['image_name']; ?>" class="delete-icon delete-image"><i class="fa fa-trash"></i></span>
                                                                <img style="max-width:100px;max-height: 100px;" class="img img-thumbnail" src="<?php echo base_url('uploads/product/' . $image_name['image_name']); ?>">
                                                            </div>
                                                        </div>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <?php
                                            if (isset($product_info['product_video']) && !empty($product_info['product_video'])) {
                                            ?>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4><?php echo get_line('video'); ?></h4>
                                                        <div class="img-wrap">
                                                            <span data-id-unique="<?php echo isset($product_info['id_unique']) ? $product_info['id_unique'] : ''; ?>" data-video="<?php echo $product_info['product_video']; ?>" class="delete-icon delete-video"><i class="fa fa-trash"></i></span>
                                                            <video style="width:100%;height:100%;" controls>
                                                                <source src="<?php echo base_url('uploads/product/' . $product_info['product_video']); ?>">
                                                            </video>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                foreach ($this->_language_info as $l_value) {
                                ?>
                                    <div class="tab-pane fade" id="product-tabs-<?php echo 'lanaguage-' . $l_value['id'] ?>" role="tabpanel" aria-labelledby="product-tabs-<?php echo 'lanaguage-' . $l_value['id'] ?>-tab">
                                        <div class="row mt-3 ml-2">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?php echo get_line('title'); ?> <span class="required">*</span></label>
                                                    <?php
                                                    $attributes = array(
                                                        'name' => 'title_' . $l_value['id'],
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'placeholder' => '',
                                                        'class' => 'form-control',
                                                        'autocomplete' => 'off',
                                                        'value' => isset($language_info[$l_value['id']]) ? $language_info[$l_value['id']]['title'] : '',
                                                    );
                                                    echo form_input($attributes);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?php echo get_line('description'); ?> <span class="required">*</span></label>
                                                    <?php
                                                    $attributes = array(
                                                        'name' => 'description_' . $l_value['id'],
                                                        'value' => '',
                                                        'placeholder' => '',
                                                        'class' => 'form-control summernote',
                                                        'rows' => '5',
                                                        'value' => isset($language_info[$l_value['id']]) ? $language_info[$l_value['id']]['description'] : '',
                                                    );
                                                    echo form_textarea($attributes);
                                                    ?>
                                                </div>
                                            </div>
                                            <!--<div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?php echo get_line('long_description'); ?> </label>
                                                    <?php
                                                    $attributes = array(
                                                        'name' => 'long_description_' . $l_value['id'],
                                                        'value' => '',
                                                        'placeholder' => '',
                                                        'class' => 'form-control summernote',
                                                        'rows' => '5',
                                                        'value' => isset($language_info[$l_value['id']]) ? $language_info[$l_value['id']]['long_description'] : '',
                                                    );
                                                    echo form_textarea($attributes);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?php echo get_line('additional_description'); ?> </label>
                                                    <?php
                                                    $attributes = array(
                                                        'name' => 'additional_description_' . $l_value['id'],
                                                        'value' => '',
                                                        'placeholder' => '',
                                                        'class' => 'form-control summernote',
                                                        'rows' => '5',
                                                        'value' => isset($language_info[$l_value['id']]) ? $language_info[$l_value['id']]['additional_description'] : '',
                                                    );
                                                    echo form_textarea($attributes);
                                                    ?>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                                <div class="tab-pane fade" id="product-tabs-attribute-price" role="tabpanel" aria-labelledby="product-tabs-attribute-price-tab">
                                    <div class="row mt-3 ml-2">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('has_attribute'); ?> <span class="required">*</span></label>
                                                <?php
                                                $options = array('0' => 'No', '1' => 'Yes');
                                                $selected = isset($product_info['has_attributes']) ? $product_info['has_attributes'] : '0';
                                                echo form_dropdown('has_attributes', $options, $selected, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_line('manage_quantity'); ?> <span class="required">*</span></label>
                                                <?php
                                                $options = array('0' => get_line('no'), '1' => get_line('yes'));
                                                $selected = isset($product_info['by_quantity_or_availability']) ? $product_info['by_quantity_or_availability'] : '0';
                                                echo form_dropdown('by_quantity_or_availability', $options, $selected, 'class="form-control by-quantity-or-availability"');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-sec">
                                        <div class="row ml-2">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label><?php echo get_line('price'); ?> <span class="required">*</span></label>
                                                    <?php
                                                    $attributes = array(
                                                        'name' => 'price',
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'placeholder' => '',
                                                        'class' => 'form-control',
                                                        'autocomplete' => 'off',
                                                        'value' => isset($product_info['price']) ? $product_info['price'] : '',
                                                        'onkeypress' => 'return validate_float_key_press(this, event);',
                                                    );
                                                    echo form_input($attributes);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label><?php echo get_line('discount'); ?> (%)<span class="required">*</span></label>
                                                    <?php
                                                    $attributes = array(
                                                        'name' => 'discount',
                                                        'type' => 'number',
                                                        'value' => '',
                                                        'placeholder' => '',
                                                        'min' => '0',
                                                        'max' => '100',
                                                        'class' => 'form-control discount-input',
                                                        'autocomplete' => 'off',
                                                        'value' => isset($product_info['discount']) ? $product_info['discount'] : 0,
                                                        'onkeypress' => 'return validate_float_key_press(this, event);',
                                                    );
                                                    echo form_input($attributes);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3 is-product-available">
                                                <div class="form-group">
                                                    <label><?php echo get_line('is_product_available'); ?> <span class="required">*</span></label>
                                                    <?php
                                                    $options = array('0' => get_line('no'), '1' => get_line('yes'));
                                                    $selected = isset($product_info['is_product_available']) ? $product_info['is_product_available'] : '1';
                                                    echo form_dropdown('is_product_available', $options, $selected, 'class="form-control"');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3 available-quantity" style="display:none;">
                                                <div class="form-group">
                                                    <label><?php echo get_line('available_quantity'); ?> <span class="required">*</span></label>
                                                    <?php
                                                    $attributes = array(
                                                        'name' => 'quantity',
                                                        'type' => 'number',
                                                        'value' => '',
                                                        'placeholder' => '',
                                                        'min' => '0',
                                                        'class' => 'form-control quantity',
                                                        'autocomplete' => 'off',
                                                        'value' => isset($product_info['quantity']) ? $product_info['quantity'] : 0,
                                                        'onkeypress' => 'return validate_float_key_press(this, event);',
                                                    );
                                                    echo form_input($attributes);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="attribute-sec">
                                        <div class="row ml-2">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><?php echo get_line('select_attribute'); ?> <span class="required">*</span></label>
                                                    <?php
                                                    $info = get_records('attributes', array('SELECT' => 'id_unique,attribute_name', 'WHERE' => array('id_language' => $this->_language_default['id'], 'is_active' => '1')));
                                                    $all_values = array();
                                                    $options = array();
                                                    if (!empty($info)) {
                                                        foreach ($info as $o_value) {
                                                            $options[$o_value['id_unique']] = $o_value['attribute_name'];
                                                            $all_values[$o_value['id_unique']] = array('name' => $o_value['attribute_name'], 'value_info' => get_records('attribute_values', array('SELECT' => 'id,attribute_value,id_unique', 'WHERE' => array('id_unique_attributes' => $o_value['id_unique'], 'id_language' => $this->_language_default['id'], 'is_active' => '1'))));
                                                        }
                                                    }
                                                    $selected = isset($product_info['id_attributes']) && !empty($product_info['id_attributes']) ? $product_info['id_attributes'] : array();
                                                    echo form_dropdown('id_attributes[]', $options, $selected, 'class="form-control"');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <?php
                                                                foreach ($all_values as $key => $value) {
                                                                    echo '<th class="hidden" data-id_unique_attributes="' . $key . '">' . $value['name'] . '</th>';
                                                                }
                                                                ?>
                                                                <th><?php echo get_line('price'); ?></th>
                                                                <th><?php echo get_line('discount'); ?> (%)</th>
                                                                <th class="is-product-available"><?php echo get_line('is_product_available'); ?></th>
                                                                <th class="available-quantity" style="display:none;"><?php echo get_line('available_quantity'); ?></th>
                                                                <!--<th class="attribute-image"><?php echo get_line('image'); ?></th>-->
                                                                <th><?php echo get_line('action'); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="clone-sec">
                                                            <?php
                                                            if (isset($product_attributes) && !empty($product_attributes)) {
                                                                $count = 0;
                                                                foreach ($product_attributes as $attr_key => $attr_value) {
                                                                    $id_unique_product_attributes = '';
                                                            ?>
                                                                    <tr>
                                                                        <?php
                                                                        $attribute_price = 0;
                                                                        $attribute_discount = 0;
                                                                        $temp_data = array();
                                                                        $temp_data['attribute_is_product_available'] = 1;
                                                                        $temp_data['attribute_quantity'] = 0;
                                                                        foreach ($all_values as $key => $value) {
                                                                            echo '<td class="hidden" data-id_unique_attributes="' . $key . '">';
                                                                            $options = array();
                                                                            foreach ($value['value_info'] as $v_value) {
                                                                                $options[$v_value['id_unique']] = $v_value['attribute_value'];
                                                                            }
                                                                            $selected = '';
                                                                            foreach ($attr_value as $av_value) {
                                                                                if ($key == $av_value['id_unique_attributes']) {
                                                                                    $id_unique_product_attributes = $av_value['id_unique'];
                                                                                    $attribute_price = $av_value['attribute_price'];
                                                                                    $attribute_discount = $av_value['attribute_discount'];
                                                                                    $temp_data['attribute_is_product_available'] = $av_value['attribute_is_product_available'];
                                                                                    $temp_data['attribute_quantity'] = $av_value['attribute_quantity'];
                                                                                    $selected = isset($av_value['id_unique_attribute_values']) ? $av_value['id_unique_attribute_values'] : '';
                                                                                    break;
                                                                                }
                                                                            }
                                                                            echo form_dropdown($key . '[]', $options, $selected, 'class="form-control form-control-sm cloned-field"');
                                                                            echo '</td>';
                                                                        }
                                                                        ?>
                                                                        <td><input type="text" name="attribute_price[]" value="<?php echo $attribute_price; ?>" class="form-control form-control-sm cloned-field mw-100p" autocomplete="off" onkeypress="return validate_float_key_press(this, event);" /></td>
                                                                        <td><input type="text" name="attribute_discount[]" value="<?php echo $attribute_discount; ?>" class="form-control form-control-sm cloned-field discount-input mw-100p" autocomplete="off" onkeypress="return validate_float_key_press(this, event);" /></td>
                                                                        <td class="is-product-available">
                                                                            <?php
                                                                            $options = array('0' => get_line('no'), '1' => get_line('yes'));
                                                                            $selected = isset($temp_data['attribute_is_product_available']) ? $temp_data['attribute_is_product_available'] : 1;
                                                                            echo form_dropdown('attribute_is_product_available[]', $options, $selected, 'class="form-control form-control-sm mw-100p"');
                                                                            ?>
                                                                        </td>
                                                                        <td class="available-quantity" style="display: none;">
                                                                            <?php
                                                                            $attributes = array(
                                                                                'name' => 'attribute_quantity[]',
                                                                                'type' => 'number',
                                                                                'value' => '',
                                                                                'placeholder' => '',
                                                                                'min' => '0',
                                                                                'class' => 'form-control form-control-sm quantity mw-100p',
                                                                                'autocomplete' => 'off',
                                                                                'value' => isset($temp_data['attribute_quantity']) ? $temp_data['attribute_quantity'] : 0,
                                                                                'onkeypress' => 'return validate_float_key_press(this, event);',
                                                                            );
                                                                            echo form_input($attributes);
                                                                            ?>
                                                                        </td>
                                                                        <!--<td class="attribute-image">
                                                                            <?php
                                                                            $attributes = array(
                                                                                'name' => 'attribute_image[' . $count . '][]',
                                                                                'type' => 'file',
                                                                                'value' => '',
                                                                                'placeholder' => '',
                                                                                'min' => '0',
                                                                                'class' => 'pull-left',
                                                                                'autocomplete' => 'off',
                                                                                'multiple' => 'multiple',
                                                                                'accept' => 'image/*',
                                                                            );
                                                                            echo form_input($attributes);
                                                                            ?>
                                                                        </td>-->
                                                                        <td>
                                                                            <div class="btn-group">
                                                                                <!--<button type="button" class="btn btn-sm btn-info view-attribute-product pull-right" data-id="<?php echo $id_unique_product_attributes; ?>"><i class="fa fa-eye"></i></button>-->
                                                                                <button type="button" class="btn btn-sm btn-primary add-more-attribute"><i class="fa fa-plus"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger mr-1 remove-attribute"><i class="fa fa-minus"></i></button>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                <?php
                                                                    $count++;
                                                                }
                                                            } else {
                                                                ?>
                                                                <tr>
                                                                    <?php
                                                                    foreach ($all_values as $key => $value) {
                                                                        echo '<td class="hidden" data-id_unique_attributes="' . $key . '">';
                                                                        $options = array();
                                                                        foreach ($value['value_info'] as $v_value) {
                                                                            $options[$v_value['id_unique']] = $v_value['attribute_value'];
                                                                        }
                                                                        echo form_dropdown($key . '[]', $options, '', 'class="form-control form-control-sm cloned-field"');
                                                                        echo '</td>';
                                                                    }
                                                                    ?>
                                                                    <td><input type="text" name="attribute_price[]" class="form-control form-control-sm cloned-field mw-100p" autocomplete="off" onkeypress="return validate_float_key_press(this, event);" /></td>
                                                                    <td><input type="text" name="attribute_discount[]" class="form-control form-control-sm cloned-field discount-input mw-100p" autocomplete="off" onkeypress="return validate_float_key_press(this, event);" /></td>
                                                                    <td class="is-product-available">
                                                                        <?php
                                                                        $options = array('0' => 'No', '1' => 'Yes');
                                                                        $selected = isset($product_info['attribute_is_product_available']) ? $product_info['attribute_is_product_available'] : '1';
                                                                        echo form_dropdown('attribute_is_product_available[]', $options, $selected, 'class="form-control form-control-sm mw-100p"');
                                                                        ?>
                                                                    </td>
                                                                    <td class="available-quantity" style="display: none;">
                                                                        <?php
                                                                        $attributes = array(
                                                                            'name' => 'attribute_quantity[]',
                                                                            'type' => 'number',
                                                                            'value' => '',
                                                                            'placeholder' => '',
                                                                            'min' => '0',
                                                                            'class' => 'form-control form-control-sm quantity mw-100p',
                                                                            'autocomplete' => 'off',
                                                                            'value' => isset($product_info['quantity']) ? $product_info['quantity'] : 0,
                                                                            'onkeypress' => 'return validate_float_key_press(this, event);',
                                                                        );
                                                                        echo form_input($attributes);
                                                                        ?>
                                                                    </td>
                                                                    <!--<td class="attribute-image">
                                                                        <?php
                                                                        $attributes = array(
                                                                            'name' => 'attribute_image[0][]',
                                                                            'type' => 'file',
                                                                            'value' => '',
                                                                            'placeholder' => '',
                                                                            'min' => '0',
                                                                            'class' => '',
                                                                            'autocomplete' => 'off',
                                                                            'multiple' => 'multiple',
                                                                            'accept' => 'image/*',
                                                                        );
                                                                        echo form_input($attributes);
                                                                        ?>
                                                                    </td>-->
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-sm btn-primary add-more-attribute"><i class="fa fa-plus"></i></button>
                                                                            <button type="button" class="btn btn-sm btn-danger mr-1 remove-attribute"><i class="fa fa-minus"></i></button>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info"><?php echo get_line('submit'); ?></button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade attribute-images" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo get_line('image'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td><?php echo get_line('image'); ?></td>
                                <td><?php echo get_line('action'); ?></td>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_line('close'); ?></button>
            </div>
        </div>

    </div>
</div>
<?php
$this->load->view('authority/common/copyright', array('footer_js' => array(base_url('assets/authority/summernote/summernote.js'), base_url('assets/authority/bootstrap-select/js/bootstrap-select.min.js'))));
?>
<script>
    var slug = '<?php echo $this->_slug; ?>';
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 300,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                //['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        var attributes_element = $('[name="id_attributes[]"]');
        //attributes_element.selectpicker();
        $('#add-form').submit(function() {
            var current = $(this);
            var $form = $('#add-form');
            var form_data = new FormData($form[0]);
            loading(true);
            $.ajax({
                url: window.location.href,
                method: 'POST',
                data: form_data,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    loading(false);
                    if (response.status) {
                        current.find('input:visible').val('');
                        toastr.success(response.message);
                        setTimeout(function() {
                            window.location = BASE_URL + 'authority/' + slug;
                        }, 2000);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });

        //Clone Section        
        $(document).on('change', '[name="has_attributes"]', function() {
            if ($(this).val() == '1') {
                $('.attribute-sec').show();
                $('.price-sec').hide();
            } else {
                $('.price-sec').show();
                $('.attribute-sec').hide();
            }
        });
        $(document).find('[name="has_attributes"]').trigger('change');

        $(document).on('change', '[name="id_attributes[]"]', function() {
            
            $('[data-id_unique_attributes]').addClass('hidden');
            $('[data-id_unique_attributes="' + $(this).val() + '"]').removeClass('hidden');

            /*
                $.each($(this).val(), function(key, value) {
                    $('[data-id_unique_attributes="' + value + '"]').removeClass('hidden');
                });
            */
        });
        $(document).find('[name="id_attributes[]"]').trigger('change');

        var temp_clone = $('.clone-sec tr:first').clone();
        $(document).on('click', '.add-more-attribute', function() {
            if (!check_attributes()) {
                var clone_2 = temp_clone.clone();
                clone_2.insertAfter($('.clone-sec tr:last'));
                $('[name="id_attributes[]"]').trigger('change');
                $('[name="by_quantity_or_availability"]').trigger('change');
                set_file_input_names();
            } else {
                toastr.remove();
                toastr.error('Please select all values first');
            }
        });
        $(document).on('click', '.remove-attribute', function() {
            if ($('.clone-sec').find('tr').length > 1) {
                $(this).closest('tr').remove();
                set_file_input_names();
            }
        });

        function check_attributes() {
            var error = false;
            $('.cloned-field:empty').each(function() {
                if ($(this).val() == '') {
                    error = true;
                    return false;
                }
            });
            return error;
        }

        //Delete Image
        $(document).on('click', '.delete-image', function(e) {
            var post_data = {
                'id_unique_product': $(this).attr('data-id-unique'),
                'image': $(this).attr('data-image')
            };
            var current = $(this).closest('.col-md-2');
            $.confirm({
                title: '<?php echo get_line('confirm'); ?>',
                type: 'red',
                content: '<?php echo get_line('delete_confirm'); ?>',
                buttons: {
                    confirm: function() {
                        $.ajax({
                            url: BASE_URL + 'authority/' + slug + '/delete_image',
                            type: 'POST',
                            dataType: 'json',
                            data: post_data,
                            beforeSend: function() {
                                loading(true);
                            },
                            success: function(response) {
                                if (response.status) {
                                    current.remove();
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            },
                            complete: function() {
                                setTimeout(function() {
                                    loading(false);
                                }, 1000);
                            },
                        });
                    },
                    cancel: function() {},
                }
            });
        });

        //Delete Video
        $(document).on('click', '.delete-video', function(e) {
            var post_data = {
                'id_unique_product': $(this).attr('data-id-unique'),
                'video': $(this).attr('data-video')
            };
            var current = $(this).closest('.col-md-2');
            $.confirm({
                title: '<?php echo get_line('confirm'); ?>',
                type: 'red',
                content: '<?php echo get_line('delete_confirm'); ?>',
                buttons: {
                    confirm: function() {
                        $.ajax({
                            url: BASE_URL + 'authority/' + slug + '/delete_video',
                            type: 'POST',
                            dataType: 'json',
                            data: post_data,
                            beforeSend: function() {
                                loading(true);
                            },
                            success: function(response) {
                                if (response.status) {
                                    current.remove();
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            },
                            complete: function() {
                                setTimeout(function() {
                                    loading(false);
                                }, 1000);
                            },
                        });
                    },
                    cancel: function() {},
                }
            });
        });

        $(document).on('keyup', '.discount-input', function(e) {
            var discount = parseFloat($(this).val());
            if (discount <= 0 || discount > 100) {
                $(this).val(0);
            }
        });

        /**
         * 1 = Quantity
         * 0 = Stock Available
         */
        $(document).on('change', '.by-quantity-or-availability', function(e) {
            if ($(this).val() == '1') {
                $('.is-product-available').hide();
                $('.available-quantity').show();
            } else {
                $('.is-product-available').show();
                $('.available-quantity').hide();
            }
        });
        $('[name="by_quantity_or_availability"]').trigger('change');

        //getting attribute image
        $(document).on('click', '.view-attribute-product', function() {
            var current = $(this);
            $.ajax({
                url: BASE_URL + 'authority/' + slug + '/get_attribute_image',
                type: 'POST',
                dataType: 'json',
                data: {
                    'id_unique_product_attributes': current.attr('data-id')
                },
                beforeSend: function() {
                    loading(true);
                },
                success: function(response) {
                    $('.attribute-images').find('.modal-body tbody').html('');
                    if (response.status) {
                        var html = '';
                        $.each(response.data, function(key, value) {
                            html += '<tr><td><img src="' + value.image_name + '" class="img img-thumbnail" style="max-height:150px;max-width:150px;"></td><td><a href="javascript:void(0)" class="btn bg-gradient-danger btn-xs delete-attribute-image" data-id="' + value.id + '"><i class="fa fa-trash-o"></i></a></td></tr>';
                        });
                        $('.attribute-images').find('.modal-body tbody').html(html);
                        $('.attribute-images').modal('show');
                    } else {
                        toastr.error(response.message);
                    }
                },
                complete: function() {
                    setTimeout(function() {
                        loading(false);
                    }, 150);
                },
            });
        });

        //Delete attribute image
        $(document).on('click', '.delete-attribute-image', function(e) {
            var post_data = {
                'id': $(this).attr('data-id')
            };
            var current = $(this);
            $.confirm({
                title: '<?php echo get_line('confirm'); ?>',
                type: 'red',
                content: '<?php echo get_line('delete_confirm'); ?>',
                buttons: {
                    confirm: function() {
                        $.ajax({
                            url: BASE_URL + 'authority/' + slug + '/delete_attribute_image',
                            type: 'POST',
                            dataType: 'json',
                            data: post_data,
                            beforeSend: function() {
                                loading(true);
                            },
                            success: function(response) {
                                if (response.status) {
                                    current.closest('tr').remove();
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            },
                            complete: function() {
                                setTimeout(function() {
                                    loading(false);
                                }, 1000);
                            },
                        });
                    },
                    cancel: function() {},
                }
            });
        });
    });

    function validate_float_key_press(el, evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        var number = el.value.split('.');
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        //just one dot
        if (number.length > 1 && charCode == 46) {
            return false;
        }
        //get the carat position
        var caratPos = get_selection_start(el);
        var dotPos = el.value.indexOf(".");
        if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
            return false;
        }
        return true;
    }

    function get_selection_start(o) {
        if (o.createTextRange) {
            var r = document.selection.createRange().duplicate()
            r.moveEnd('character', o.value.length)
            if (r.text == '')
                return o.value.length
            return o.value.lastIndexOf(r.text)
        } else
            return o.selectionStart
    }

    function set_file_input_names() {
        var i = 0;
        $('.clone-sec tr').each(function() {
            $(this).find('.attribute-image').find('input').attr('name', 'attribute_image[' + i + '][]');
            i++;
        });
    }
</script>
<?php $this->view('authority/common/footer'); ?>