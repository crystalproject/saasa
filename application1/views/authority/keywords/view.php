<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$header_css = array();
$this->view('authority/common/header', array('header_css' => $header_css));
$this->view('authority/common/sidebar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('keywords'); ?></li>
                    </ol>
                </div>                
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-12">                    
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('keywords'); ?></h3>
                        </div>
                        <?php
                        $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                        echo form_open('', $attributes);
                        ?>
                        <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : ''; ?>">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-sm-5">
                                    <label><?php echo get_line('english'); ?></label>
                                </div>
                                <div class="col-sm-5">
                                    <label><?php echo get_line('turkish'); ?></label>
                                </div>
                            </div>
                            <div class="form-group row">
                            <?php 
                            if(isset($keywords) && !empty($keywords)) {                                
                                foreach ($keywords as $key => $value) {
                                    ?>
                                    <div class="col-sm-5 pb-2">
                                        <?php
                                        $attributes = array(
                                            'type' => 'text',
                                            'name' => $value['language_key'],
                                            'class' => 'form-control',
                                            'value' => $value['language_value']
                                        );
                                        echo form_input($attributes);
                                        ?>
                                    </div>
                                    <?php                                     
                                }
                            }
                            ?>
                            </div>
                        </div>
                        <div class="card-footer">
                            <?php
                            $attributes = array(
                                'type' => 'submit',
                                'class' => 'btn btn-info',
                                'value' => get_line('submit'),
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>                
            </div>
        </div>
    </section>
</div>
<?php
$this->load->view('authority/common/copyright', array('footer_js' => array()));
?>
<script>
$(document).ready(function(){
    var message = '<?php echo isset($message) ? $message : '' ?>';
    if(message != '') {
        toastr.success(message);
    }
});
</script>
<?php
$this->view('authority/common/footer');