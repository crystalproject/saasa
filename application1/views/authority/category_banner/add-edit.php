<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('category'); ?> <?php echo get_line('banner'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open('', $attributes);
                                ?>
                                    <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : '';?>">
                                    <?php
                                        $language = $this->production_model->get_all_with_where('language','id','asc',array('is_active'=>1));
                                        if (isset($language) && $language !=null) {
                                            ?>
                                            <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label"><?php echo get_line('banner_for'); ?>: <span class="required">*</span> </label>
                                                    <div class="col-sm-9">                                                        
                                                        <?php 
                                                        $options = array();
                                                        foreach(get_banner_for() as $key => $value_temp) {
                                                            $options[$key] = $value_temp;
                                                        }
                                                        $selected = '';
                                                        echo form_dropdown('banner_for',$options,$selected,'class="form-control"');
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php
                                            foreach ($language as $key => $value) {
                                                $id = $value['id'];
                                                $newname = strtolower('title_'.$value['name']);
                                                $new_description = strtolower('description_'.$value['name']);
                                            ?>
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-3 col-form-label"><?php echo get_line('title'); ?> (<?= $value['name'];?>):<span class="required">*</span> </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="<?= $id ==1 ? 'title' : (isset($newname) && $newname !=null ? $newname : '');?>" class="form-control" placeholder='<?php echo get_line('name'); ?>' value="<?= isset($id) && $id !=null && $id == 1 ? (isset($title) && $title !=null ? $title : '') : (isset($$newname) && $$newname !=null ? $$newname : '');?>" maxlength="50">
                                                        <?= form_error($id ==1 ? 'title' : $newname, "<label class='error'>", "</label>");?>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="title" class="col-sm-3 col-form-label"><?php echo get_line('description'); ?> (<?= $value['name'];?>):<span class="required">*</span></label>
                                                    <div class="col-sm-9">
                                                        <textarea name="<?= $id ==1 ? 'description' : (isset($new_description) && $new_description !=null ? $new_description : '');?>" placeholder='<?php echo get_line('description'); ?>' class="form-control"><?= isset($id) && $id !=null && $id == 1 ? (isset($description) && $description !=null ? $description : '') : (isset($$new_description) && $$new_description !=null ? $$new_description : '');?></textarea>
                                                        <?= form_error("description", "<label class='error'>", "</label>");?>
                                                    </div>
                                                </div>
                                            <?php }
                                        }
                                    ?>
                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-3 col-form-label"><?php echo get_line('image'); ?>:<span class="required">*</span> (<?php echo get_line('upload_size'); ?> 1900&#215;682)</label>
                                        <div class="col-sm-9">
                                            <input type="file" name="image" class="form-control" accept="image/*">
                                            <span id="image_error"></span>
                                            <?= form_error("image", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <?php
                                        if (isset($image) && $image != null) {
                                        ?>
                                            <div class="form-group row">
                                                <label for="inputName3" class="col-sm-3 col-form-label"><?php echo get_line('current_image'); ?></label>
                                                <div class="col-sm-9">
                                                    <img src="<?= base_url(CATEGORY_BANNER.'thumbnail/').$image;?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">                
                                                </div>
                                            </div>
                                        <?php } 
                                    ?>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => get_line('submit'),
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        var id = $("input[name='id']").val();
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {   
                title:{required:true},       
                title_turkish:{required:true},       
                description:{required:true},       
                description_turkish:{required:true},       
                image: { 
                    required: function(element) {
                        if (id == '') {  
                            return true;
                        }
                        else {
                            return false;
                        }
                    }, 
                },                  
            },
            messages: {      
                title: {required :"<?php echo get_line('this_field_is_required'); ?>"},       
                title_turkish: {required :"<?php echo get_line('this_field_is_required'); ?>"},       
                description: {required :"<?php echo get_line('this_field_is_required'); ?>"},       
                description_turkish: {required :"<?php echo get_line('this_field_is_required'); ?>"},       
                image: {required :"<?php echo get_line('this_field_is_required'); ?>"},       
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>