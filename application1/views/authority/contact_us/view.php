<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('contact_us'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('contact_us'); ?></h3>
                            <div class="card-tools">
                                <?php
                                    $name = isset($this->session->contact_us_info['name']) && $this->session->contact_us_info['name'] != null ? $this->session->contact_us_info['name'] : '';
                                ?>
                                <div class="input-group input-group-sm" style="width: 200px;">
                                    <input type="text" name="name" value="<?= $name; ?>" class="form-control float-right" placeholder="<?php echo get_line('search'); ?>">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default btn_filter"><i class="fas fa-search"></i></button>
                                    </div>&nbsp;&nbsp;
                                    <a href="<?= base_url('authority/contact-us?clear-search=1') ?>" class="btn btn-info btn-sm" type="button"><?php echo get_line('clear'); ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" id="select_all">
                                                <label for="select_all" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th><?php echo get_line('name'); ?></th>
                                        <th><?php echo get_line('email_address'); ?></th>
                                        <th><?php echo get_line('subject'); ?></th>
                                        <th><?php echo get_line('message'); ?></th>
                                        <th style="min-width: 100px;"><?php echo get_line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody class="attribute-info data-response">
                                    <?php
                                        if (isset($details) && $details != null) {
                                            foreach ($details as $key => $value) {
                                                $id = $value['id'];
                                                ?>
                                                <tr>
                                                    <td style="width: 10px;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id; ?>" value="<?= $id ?>">
                                                            <label for="customCheckbox<?= $id; ?>" class="custom-control-label"></label>
                                                        </div>
                                                    </td>
                                                    <td><?= $value['name']; ?></td>
                                                    <td><?= $value['email']; ?></td>
                                                    <td><?= $value['subject']; ?></td>
                                                    <td><?= $value['message']; ?></td>
                                                    <td>
                                                        <a href="javascript:void(0)" data-table="contact_us" data-image-path="" class="btn bg-gradient-danger btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                                <tr>
                                                    <td style="width: 10px;">
                                                        <button type="button" class="btn btn-sm btn-danger delete_all" data-table="contact_us" data-image-path=""><?php echo get_line('delete'); ?></button>
                                                    </td>
                                                </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr data-expanded="true">
                                                <td colspan="6" align="center"><?php echo get_line('record_not_available'); ?></td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination_filter">
                            <?php
                                if (isset($pagination) && $pagination != null) {
                                    echo $pagination;
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$footer_js = array(base_url('assets/authority/js/delete_record.js'), base_url('assets/authority/js/change_status.js'));
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    $(document).ready(function () {
        /*Pagination filter*/
        $(document).on('click', '.pagination_filter a', function (e) {
            var url = $(this).attr('href');
            var split_url = url.split('/');
            var page_no = split_url[7];
            e.preventDefault();

            var name = $('input[name="name"]').val();
            get_filtered_data(page_no, name);
        });

        $(document).on('click', '.btn_filter', function () {
            var name = $('input[name="name"]').val();
            get_filtered_data('', name);
        });
    });
    function get_filtered_data(page_no = '', name = '') {
        var post_data = {'page_no': page_no, 'name': name};
        $.ajax({
            url: '<?= base_url('authority/contact-us/filter/') ?>' + page_no,
            type: 'POST',
            dataType: 'json',
            data: post_data,
            beforeSend: function () {
                loading(true);
            },
            success: function (response) {
                if (response.success) {
                    $('.data-response').html(response.details);
                    $('.pagination_filter').html(response.pagination);
                } else if (response.error && response.data_error) {
                    $('.data-response').html(response.data_error);
                    $('.pagination_filter').html(response.pagination);
                }
            },
            complete: function () {
                setTimeout(function () {
                    loading(false);
                }, 1000);
            },
        });
    }
</script>
<?php $this->view('authority/common/footer'); ?>									