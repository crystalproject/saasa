<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('home_slider'); ?> </li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open('', $attributes);
                                ?>
                                    <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : '';?>">
                                    
                                    <?php
                                    $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('is_active' => 1));
                                    if (isset($language) && $language != null) {
                                        foreach ($language as $key => $value) {
                                            $id = $value['id'];
                                            $newname = strtolower('name_' . $value['slug']);
                                            ?>
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('name'); ?> (<?= $value['name']; ?>): <span class="required">*</span></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="<?= $id == 1 ? 'name' : (isset($newname) && $newname != null ? $newname : ''); ?>" class="form-control" placeholder="<?php echo get_line('name'); ?>" value="<?= isset($id) && $id != null && $id == 1 ? (isset($name) && $name != null ? $name : '') : (isset($$newname) && $$newname != null ? $$newname : ''); ?>" maxlength="50" autocomplete="off">
                                                    <?= form_error($id == 1 ? 'name' : $newname, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div><?= $key == 0 ? '<hr class="card card-primary card-outline">' : '';?>
                                            <?php
                                        }
                                    }
                                    ?>                                     
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => get_line('submit'),
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>