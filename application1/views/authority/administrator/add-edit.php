<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<style type="text/css">
    .profile-user-img {
        width: 50%;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('profile'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <?php
                                $profile_photo = $this->production_model->get_all_with_where('administrator','','',array('id'=>$this->session->user_info['id']));
                                if ($profile_photo != null) {
                                ?>
                                    <div class="text-center">
                                        <label><?php echo get_line('current_photo'); ?></label><br>
                                        <img class="profile-user-img img-fluid img-circle" src="<?= $profile_photo !=null ? base_url(PROFILE_PICTURE).$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" alt="profile picture">
                                    </div>
                                <?php } 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open("", $attributes);
                                ?>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label"><?= get_line('name');?> <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <?php 
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'name' => 'full_name',
                                                    'placeholder' => 'Enter full name',
                                                    'value' => (isset($full_name) ? $full_name : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("full_name", "<div class='error'>", "</div>"); 
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-3 col-form-label"><?= get_line('admin_email_address');?> <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'email',
                                                    'class' => 'form-control',
                                                    'name' => 'email_address',
                                                    'placeholder' => 'Enter email address',
                                                    'value' => (isset($email_address) ? $email_address : ""),
                                                    'readonly' => ''
                                                );
                                                echo form_input($attributes);
                                                echo form_error("email_address", "<div class='error'>", "</div>");
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-3 col-form-label"><?= get_line('email_address_website');?> <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'email',
                                                    'class' => 'form-control',
                                                    'name' => 'email_address_website',
                                                    'placeholder' => 'Enter email address',
                                                    'value' => (isset($email_address_website) ? $email_address_website : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("email_address_website", "<div class='error'>", "</div>");
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName2" class="col-sm-3 col-form-label"><?= get_line('mobile_number_1');?> <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'name' => 'mobile_number_1',
                                                    'placeholder' => 'Enter mobile number',
                                                    'maxlength' =>'15',
                                                    'value' => (isset($mobile_number_1) ? $mobile_number_1 : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("mobile_number_1", "<div class='error'>", "</div>");
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputExperience" class="col-sm-3 col-form-label"><?= get_line('mobile_number_2');?></label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control only_digits',
                                                    'name' => 'mobile_number_2',
                                                    'placeholder' => 'Enter mobile number',
                                                    'maxlength' =>'15',
                                                    'value' => (isset($mobile_number_2) ? $mobile_number_2 : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("mobile_number_2", "<div class='error'>", "</div>");
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputSkills" class="col-sm-3 col-form-label"><?= get_line('address');?></label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'class' => 'form-control',
                                                    'name' => 'address',
                                                    'placeholder' => 'Enter address',
                                                    'rows' => '5',
                                                    'value' => (isset($address) ? $address : ""),
                                                );
                                                echo form_textarea($attributes);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-3 col-form-label"><?= get_line('logo');?>:(Upload by 120&#215;124)</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <?php
                                                        $attributes = array(
                                                            'type' => 'file',
                                                            'name' => 'profile_photo',
                                                            'class' => 'custom-file-input',
                                                            'accept' => 'image/*',
                                                        );
                                                        echo form_input($attributes);
                                                    ?>
                                                    <label class="custom-file-label" for="exampleInputFile"><?= get_line('upload_photo');?></label>
                                                </div>
                                                <?php /*<div class="input-group-append">
                                                    <span class="input-group-text" id="">Upload</span>
                                                </div> */ ?>
                                            </div>
                                            <?php 
                                                echo form_error("profile_photo", "<div class='error'>", "</div>");
                                                if (isset($profile_photo_error) && $profile_photo_error != "") {
                                                    echo "<div class='error'>" . $profile_photo_error . "</div>";
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => get_line('submit'),
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function () {
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                full_name: {required:true},
                email_address: {required: true, email: true},
                mobile_number_1: {required: true, maxlength: 15, minlength: 10},
            },
            messages: {
                full_name: {required: "Please enter full name"},
                email_address: {required: "Please enter email address", email: "Please enter valid email address"},
                mobile_number_1: {required: "Please enter mobile number", maxlength: "Please enter no more than 15 digits", minlength: "Please enter at least 10 digits"},
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>