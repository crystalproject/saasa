<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('cms_page'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-6">
                    <?php
                        if (count($details) < 4) {
                            ?>
                            <a href="<?= base_url('authority/static-page/add-edit'); ?>" class="btn btn-secondary btn-flat"><?php echo get_line('add'); ?></a>
                        <?php }
                    ?>
                </div>
                <div class="col-md-6">
                    <?php
                    $name = isset($this->session->page_info['name']) && $this->session->page_info['name'] != null ? $this->session->page_info['name'] : '';
                    ?>
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" name="name" value="<?= $name; ?>" placeholder="<?php echo get_line('search'); ?>" aria-label="<?php echo get_line('search'); ?>">
                        <div class="input-group-append">
                            <button class="btn btn-primary btn_filter" type="button"><i class="fas fa-search"></i></button>
                        </div>&nbsp;&nbsp;
                        <a href="<?= base_url('authority/static-page?clear-search=1') ?>" class="btn btn-primary btn-sm" type="button"><?php echo get_line('clear'); ?></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body p-0" style="overflow-x: auto;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?php echo get_line('title'); ?></th>
                                        <!-- <th><?php echo get_line('image'); ?></th> -->
                                        <th style="min-width: 100px;"><?php echo get_line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                    <?php
                                    if (isset($details) && $details != null) {
                                        foreach ($details as $key => $value) {
                                            $id = $value['id'];
                                            ?>
                                            <tr>
                                                <td><?= $value['title'. get_language('admin')]; ?></td>
                                                <!-- <td>
                                                    <?php
                                                        if ($id == 4) {
                                                        ?>
                                                            <img src="<?= base_url(STATIC_PAGE . 'thumbnail/') . $value['image'. get_language('admin')] ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">
                                                        <?php }
                                                    ?>
                                                </td> -->
                                                <td>
                                                    <a href="<?= base_url('authority/static-page/add-edit/' . $id); ?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>

                                                    <!-- <a href="javascript:void(0)" data-table="static_page" data-image-path="<?= STATIC_PAGE; ?>" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id; ?>"><i class="fa fa-trash-o"></i></a> -->

                                                    <?php
                                                    if ($value['is_active'] == '1') {
                                                        echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status" data-table="static_page" data-id="' . $id . '" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                                                    } else {
                                                        echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status" data-table="static_page" data-id="' . $id . '" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr data-expanded="true">
                                            <td colspan="7" align="center"><?php echo $this->lang->line('records_not_found'); ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->                        
                        <?php
                        echo '<div class="card-footer clearfix pagination_filter">';
                        if (isset($pagination) && $pagination != null) {                            
                            echo $pagination;                            
                        }
                        echo '</div>';
                        ?>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$footer_js = array(base_url('assets/authority/js/delete_record.js'), base_url('assets/authority/js/change_status.js'));
$this->load->view('authority/common/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    $(document).ready(function () {
        /*Pagination filter*/
        $(document).on('click', '.pagination_filter a', function (e) {
            var url = $(this).attr('href');
            var split_url = url.split('/');
            var page_no = split_url[7];
            e.preventDefault();

            var name = $('input[name="name"]').val();
            get_filtered_data(page_no, name);
        });

        $(document).on('click', '.btn_filter', function () {
            var name = $('input[name="name"]').val();
            get_filtered_data('', name);
        });
    });
    function get_filtered_data(page_no = '', name = '') {
        var post_data = {'page_no': page_no, 'name': name};
        $.ajax({
            url: '<?= base_url('authority/static_page/filter/') ?>' + page_no,
            type: 'POST',
            dataType: 'json',
            data: post_data,
            beforeSend: function () {
                loading(true);
            },
            success: function (response) {
                if (response.success) {
                    $('.data-response').html(response.details);
                    $('.pagination_filter').html(response.pagination);
                } else if (response.error && response.data_error) {
                    $('.data-response').html(response.data_error);
                    $('.pagination_filter').html(response.pagination);
                }
            },
            complete: function () {
                setTimeout(function () {
                    loading(false);
                }, 1000);
            },
        });
    }
</script>
<?php $this->view('authority/common/footer'); ?>									