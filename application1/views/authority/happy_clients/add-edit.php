<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$header_css = array(base_url('assets/authority/summernote/summernote.css'));
$this->view('authority/common/header', array('header_css' => $header_css));
$this->view('authority/common/sidebar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo get_line('happy_clients'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open('', $attributes);
                                ?>
                                <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : ''; ?>">
                                <?php
                                    $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('is_active' => 1));
                                    if (isset($language) && $language != null) {
                                        foreach ($language as $key => $value) {
                                            $lang_id = $value['id'];
                                            $newname = strtolower('name_' . $value['slug']);
                                            $newdesignation = strtolower('designation_' . $value['slug']);
                                            $newdescription = strtolower('description_' . $value['slug']);
                                            $img_name = strtolower('image_' . $value['slug']);
                                            ?>
                                            <div class="form-group row">
                                                <label for="inputName" class="col-sm-3 col-form-label"><?php echo get_line('name'); ?>  (<?= $value['name']; ?>)<span class="required">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="<?= $lang_id == 1 ? 'name' : (isset($newname) && $newname != null ? $newname : ''); ?>" placeholder="<?php echo get_line('name'); ?>" class="form-control" value="<?= isset($lang_id) && $lang_id != null && $lang_id == 1 ? (isset($name) && $name != null ? $name : '') : (isset($$newname) && $$newname != null ? $$newname : ''); ?>">
                                                    <?= form_error($lang_id == 1 ? 'name' : $newname, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputName" class="col-sm-3 col-form-label"><?php echo get_line('designation'); ?>  (<?= $value['name']; ?>)<span class="required">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="<?= $lang_id == 1 ? 'designation' : (isset($newdesignation) && $newdesignation != null ? $newdesignation : ''); ?>" placeholder="<?php echo get_line('designation'); ?>" class="form-control" value="<?= isset($lang_id) && $lang_id != null && $lang_id == 1 ? (isset($designation) && $designation != null ? $designation : '') : (isset($$newdesignation) && $$newdesignation != null ? $$newdesignation : ''); ?>">
                                                    <?= form_error($lang_id == 1 ? 'designation' : $newname, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputName3" class="col-sm-3 col-form-label"><?php echo get_line('description'); ?>  (<?= $value['name']; ?>)<span class="required">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea class="textarea" placeholder="<?php echo get_line('description'); ?>" name="<?= $lang_id == 1 ? 'description' : (isset($newdescription) && $newdescription != null ? $newdescription : ''); ?>"  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= isset($lang_id) && $lang_id != null && $lang_id == 1 ? (isset($description) && $description != null ? $description : '') : (isset($$newdescription) && $$newdescription != null ? $$newdescription : ''); ?></textarea>
                                                    <?= form_error($lang_id == 1 ? 'description' : $newdescription, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="profile_photo" class="col-sm-3 col-form-label"><?php echo get_line('image'); ?> (<?= $value['name']; ?>):<span class="required">*</span> (<?php echo get_line('upload_size'); ?> 100&#215;100)</label>
                                                <div class="col-sm-9">
                                                    <input type="file" name="<?= $lang_id == 1 ? 'image' : (isset($img_name) && $img_name != null ? $img_name : ''); ?>" class="form-control" accept="image/*">
                                                    <?= form_error($lang_id == 1 ? 'image' : $img_name, "<label class='error'>", "</label>"); ?>
                                                </div>
                                            </div><?= $key == 0 ? '<hr class="card card-primary card-outline">' : '';?>
                                        <?php } 
                                    }
                                ?>
                                <?php                                                                           
                                    if (isset($image) && $image != null) {
                                        ?>
                                        <div class="form-group row">
                                            <label for="inputName3" class="col-sm-3 col-form-label"><?php echo get_line('current_image'); ?></label>
                                            <div class="col-sm-9">
                                                <img src="<?= base_url(HAPPY_CLIENTS . 'thumbnail/') . $image; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">                
                                            </div>
                                        </div>
                                    <?php }
                                    if (isset($image_hindi) && $image_hindi != null) {
                                        ?>
                                        <div class="form-group row">
                                            <label for="inputName3" class="col-sm-3 col-form-label"><?php echo get_line('current_image'); ?></label>
                                            <div class="col-sm-9">
                                                <img src="<?= base_url(HAPPY_CLIENTS . 'thumbnail/') . $image_hindi; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">                
                                            </div>
                                        </div>
                                    <?php }
                                    
                                ?>
                                <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <?php
                                        $attributes = array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-success',
                                            'value' => get_line('submit'),
                                        );
                                        echo form_input($attributes);
                                        ?>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('authority/common/copyright');?>
<?php $this->view('authority/common/footer'); ?>