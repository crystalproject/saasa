<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<table class="table table-bordered m-0">
    <thead>
        <tr>
            <th><?php echo get_line('attribute_value'); ?></th>
            <!--<th><?php echo get_line('image'); ?></th>-->
            <th><?php echo get_line('action'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($info) && !empty($info)) {
            foreach ($info as $value) {
                ?>
                <tr>            
                    <td>
                        <p class="name m-0"><?php echo $value['attribute_value']; ?></p>
                        <?php /*<div class="value-form hidden">
                            <input name="attribute_value" value="<?php echo $value['attribute_value']; ?>" class="form-control form-control-sm col-4" data-edit_id="<?php echo $value['id']; ?>" placeholder="Enter value" data-id_unique_attributes="<?php echo $value['id_unique_attributes']; ?>"/>
                            <button type="button" class="btn btn-xs btn-primary attr-value-submit"><?php echo get_line('submit'); ?></button>
                        </div> */ ?>
                    </td>
                    <!--<td>
                        <?php 
                        if(isset($value['image']) && $value['image'] != '') {
                            echo '<img src="'.base_url('uploads/attribute/'.$value['image']).'" style="max-width:50px;max-height:50px;"/>';
                        }
                        ?>
                    </td>-->
                    <td>
                        <a href="javascript:void(0);" class="btn bg-gradient-primary btn-xs edit-attribute-value" id-unique="<?php echo $value['id_unique']; ?>" id-unique-attribtues="<?php echo $value['id_unique_attributes']; ?>"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0);" class="btn bg-gradient-danger btn-xs delete-attribute-value" id="<?php echo $value['id_unique']; ?>"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="3" class="text-center"><?php echo get_line('record_not_available'); ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>