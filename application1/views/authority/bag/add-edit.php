<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard'); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li class="breadcrumb-item active"><?php echo $this->_page_title; ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <div class="card-header">
                            <h3 class="card-title"><?php echo get_line('add'); ?> <?php echo $this->_page_title; ?></h3>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                echo form_open('', $attributes);
                                ?>
                                <input type="hidden" name="id" value="<?= isset($id) && $id != null ? $id : ''; ?>">
                                <?php
                                $language = $this->production_model->get_all_with_where('language', 'id', 'asc', array('is_active' => 1));
                                if (isset($language) && $language != null) {
                                    foreach ($language as $key => $value) {
                                        $id = $value['id'];
                                        $newname = strtolower('title_' . $value['slug']);
                                        ?>
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('title'); ?> (<?= $value['name']; ?>):<span class="required">*</span> </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="<?= $id == 1 ? 'title' : (isset($newname) && $newname != null ? $newname : ''); ?>" class="form-control" placeholder='Enter value' value="<?= isset($id) && $id != null && $id == 1 ? (isset($title) && $title != null ? $title : '') : (isset($$newname) && $$newname != null ? $$newname : ''); ?>" maxlength="50" autocomplete="off">                                                
                                                <?php echo form_error($id == 1 ? 'title' : (isset($newname) && $newname != null ? $newname : ''), "<label class='error'>", "</label>"); ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label"><?php echo get_line('price'); ?><span class="required">*</span> </label>
                                    <div class="col-sm-4">
                                        <?php
                                        $attributes = array(
                                            'type' => 'text',
                                            'class' => 'form-control only_digits',
                                            'name' => 'price',
                                            'value' => isset($price) ? $price : '',
                                            'autocomplete' => 'off',
                                        );
                                        echo form_input($attributes);
                                        echo form_error("price", "<label class='error'>", "</label>");
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label"><?php echo get_line('image'); ?>:<span class="required">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="file" name="image" class="form-control" accept="image/*">
                                        <?php echo form_error("image", "<label class='error'>", "</label>"); ?>
                                    </div>
                                </div>
                                <?php
                                if (isset($image) && $image != null) {
                                    ?>
                                    <div class="form-group row">
                                        <label for="inputName3" class="col-sm-2 col-form-label"><?php echo get_line('image'); ?></label>
                                        <div class="col-sm-4">
                                            <img src="<?= base_url('uploads/bag/') . $image; ?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png') ?>'" height="50px" width="50px">                
                                        </div>
                                    </div>
                                <?php }
                                ?>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-2">
                                        <?php
                                        $attributes = array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-success',
                                            'value' => get_line('submit'),
                                        );
                                        echo form_input($attributes);
                                        ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="<?php echo base_url('authority/bag'); ?>" class="btn btn-info"><?php echo get_line('cancel'); ?></a>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<?php
$this->view('authority/common/copyright');
$this->view('authority/common/footer');
