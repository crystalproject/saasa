<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
if (isset($info) && !empty($info)) {
    foreach ($info as $value) {
        ?>
        <tr>            
            <td><?php echo $value['first_name']; ?></td>
            <td><?php echo $value['email']; ?></td>
            <td>
                <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug . '/detail/' . $value['id']); ?>" class="" title="Detail"><?php echo $value['order_id']; ?></a>
            </td>
            <td><?php echo date('d-m-Y h:i A', strtotime($value['order_date_time'])); ?></td>
            <td><?php echo get_payment_mode($value['payment_mode'],$this->_language_default['id']); ?></td>
            <td><?php echo get_order_status($value['order_status'])['name']; ?></td>
            <td>
                <a href="<?php echo base_url(AUTHORITY . '/' . $this->_slug . '/detail/' . $value['id']); ?>" class="btn bg-gradient-info btn-xs" title="Detail"><i class="fas fa-eye"></i></a>
            </td>
        </tr>
        <?php
    }
    ?>
    <?php
    if (isset($pagination) && !empty($pagination)) {
        echo '<tr><td colspan="6">';
        echo $pagination;
        echo '</td></tr>';
    }
    ?>        
    <?php
} else {
    ?>
    <tr>
        <td colspan="7" class="text-center"><?php echo get_line('record_not_available'); ?></td>
    </tr>
    <?php
}