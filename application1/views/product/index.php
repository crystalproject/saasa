<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
?>
<section class="mens-clothes-section">
    <div class="container">
        <div class="row no-breadcrumbs">
            <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                <div class="mens-clothes-section-sidebar category">
                    <div class="sidebar-link sidebar-link-new">
                        <?php
                        function get_category_tree($first_category_id, $display = true)
                        {
                            $CI = &get_instance();
                            $details = get_details('category', array('parent_id' => $first_category_id));
                            if (isset($details) && $details != null) {
                                foreach ($details as $key => $value) {
                                    $angle_class = 'right';
                                    $angle_class = $value['id'] <= $GLOBALS['first_id'] ?  'right' : 'down';                                    
                                    $style = '';
                                    if ($display == false) {
                                        $style = 'display:none;';
                                    } else {
                                        $style = $value['id'] <= $GLOBALS['first_id']  ?  'display:none;' : '';
                                    }
                        ?>
                                    <ul style="<?php echo $style; ?>">
                                        <?php
                                        $li_class = "";
                                        $temp = $CI->common_model->select_data('category', array('where' => array('parent_id' => $value['id'])));
                                        if ($temp['row_count'] > 0) {
                                            $li_class = '<i class="fa fa-angle-' . $angle_class . '"></i>';
                                        }
                                        ?>
                                        <li data-category-id="<?php echo $value['id']; ?>"><span><?php echo $value['name' . get_language_front()]; ?></span> <?php echo $li_class; ?></li>
                                        <?php
                                        get_category_tree($value['id'], $display);
                                        ?>
                                    </ul>
                            <?php
                                }
                            }
                        }
                        if (isset($first_category_info) && !empty($first_category_info)) {
                            ?>
                            <ul>
                                <li data-category-id="<?php echo $first_category_info['id']; ?>"><span><?php echo $first_category_info['name' . get_language_front()] ?></span> <i class="fa fa-angle-down"></i></li>
                                <?php
                                get_category_tree($first_id);
                                ?>
                            </ul>
                            <?php
                        }
                        $info = $this->common_model->select_data('category', array('WHERE' => array('parent_id' => 0, 'id != ' => $first_id)));
                        if ($info['row_count'] > 0) {
                            foreach ($info['data'] as $key => $value) {
                            ?>
                                <ul>
                                    <li data-category-id="<?php echo $value['id']; ?>"><span><?php echo $value['name' . get_language_front()] ?></span> <i class="fa fa-angle-right"></i></li>
                                    <?php
                                    get_category_tree($value['id'], $display = false);
                                    ?>
                                </ul>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="main-content col-md-9 col-lg-9 col-sm-12 col-xs-12">
                <div class="filter">
                    <form>
                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext " name="category" data-size="7" data-live-search="true" data-title="<?php echo get_line('select'); ?>" id="state_list" data-width="100%">
                                    <?php
                                    function get_category_tree_select($first_category_id, $sub_mark = '')
                                    {
                                        $CI = &get_instance();
                                        $details = get_details('category', array('parent_id' => $first_category_id));
                                        if (isset($details) && $details != null) {
                                            foreach ($details as $key => $value) {
                                                $selected = $GLOBALS['current_category'] == $value['id'] ? 'selected="selected"' : '';
                                                echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($sub_mark) . trim($value['name' . get_language_front()]) . '</option>';
                                                get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                            }
                                        }
                                    }
                                    //echo '<option value="">' . get_line('select') . '</option>';
                                    $sub_mark = '';
                                    $temp = get_details('category', array('id' => $first_id));
                                    if (isset($temp) && $temp != null) {
                                        foreach ($temp as $key => $value) {
                                            $selected = $GLOBALS['current_category'] == $value['id'] ? 'selected="selected"' : '';
                                            echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($value['name' . get_language_front()]) . '</option>';
                                            get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                        }
                                    }
                                    $sub_mark = '';
                                    $info = $this->common_model->select_data('category', array('WHERE' => array('parent_id' => 0, 'id != ' => $first_id)));
                                    if ($info['row_count'] > 0) {
                                        foreach ($info['data'] as $key => $value) {
                                            echo '<option value="' . $value['id'] . '" ' . $selected . '>' . trim($value['name' . get_language_front()]) . '</option>';
                                            get_category_tree_select($value['id'], $sub_mark . '--&nbsp;');
                                        }
                                    }
                                    ?>
                                </select>

                            </div>
                            <?php
                            if (isset($attribute_info) && !empty($attribute_info)) {
                                foreach ($attribute_info as $value) {
                            ?>
                                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                        <select class="form-control-plaintext attribute-filter" name="<?php echo $value['id_unique']; ?>">
                                            <option value=""><?php echo $value['attribute_name']; ?></option>
                                            <?php
                                            foreach ($value['attribute_info'] as $a_value) {
                                                echo '<option value="' . $a_value['id_unique'] . '">' . $a_value['attribute_value'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                            <?php
                                }
                            }
                            ?>

                            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext sorting_filter" name="">
                                    <option value="">Sort</option>
                                    <option value="id desc">New</option>
                                    <!--<option value="price_asc">Price : low to high</option>
                                    <option value="price_desc">Price : high to low</option>-->
                                    <option value="title asc">Name : A to Z</option>
                                    <option value="title desc">Name : Z to A</option>
                                </select>
                            </div>



                            
                        </div>
                    </form>
                    
                </div>
                <div class="row filter-result">
                    <?php
                    if (isset($details) && $details != null) {
                        foreach ($details as $key => $value) {
                            $id = $value['id_unique'];
                            $id_unique = $value['id_unique'];
                    ?>
                            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                                <div class="mens-clothes-box">
                                    <ul>
                                        <li>
                                            <?php
                                            $get_pro_id = get_details('wishlist', array('id_product' => $id, 'login_id' => get_front_login_id()));
                                            if (isset($get_pro_id) && $get_pro_id != null) { ?>
                                                <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                            <?php } else { ?>
                                                <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                            <?php } ?>
                                        </li>
                                        <?php
                                        if (isset($value['is_new']) && $value['is_new'] == '1') {
                                        ?>
                                            <li class="new"><?php echo get_line('new'); ?></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                    <a href="<?= $current_url . '/' . $value['slug'] . '/' . $id_unique; ?>" style="color: #000;">
                                        <div class="mens-clothes-img text-center img-effect">
                                            <img src="<?= $value['main_image']; ?>">
                                        </div>
                                        <div class="mens-clothes-name text-center">
                                            <h4 style="font-size: 18px;"><b><?= $value['title']; ?></b></h4>
                                            <h4><?= $value['other_info']['final_price'][0]; ?> SAR</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                    <?php }
                    } else {
                        echo '<div class="col-md-12">';
                        echo "<h3>" . get_line_front('product_not_available') . "</h3>";
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var current_url = '<?php echo $current_url; ?>';
</script>
<?php
$footer_js = array(base_url('assets/js/bootstrap-select.js'));
$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(window).scroll(function() {

            if ($(window).scrollTop() > 200) {
                $('.mens-clothes-section-sidebar').css('position', 'fixed');
                $('.mens-clothes-section-sidebar').css('top', '0');
                $('.mens-clothes-section-sidebar').css('width', '20%');
            } else if ($(window).scrollTop() <= 200) {
                $('.mens-clothes-section-sidebar').css('position', '');
                $('.mens-clothes-section-sidebar').css('top', '');
                $('.mens-clothes-section-sidebar').css('width', '');
            }
            if ($('.mens-clothes-section-sidebar').offset().top + $(".mens-clothes-section-sidebar").height() > $(".footer-wrapper").offset().top) {
                $('.mens-clothes-section-sidebar').css('top', -($(".mens-clothes-section-sidebar").offset().top + $(".mens-clothes-section-sidebar").height() - $("#footer").offset().top));
            }
        });

        $('.sidebar-link-new li i').click(function() {
            //$(this).find('i').toggleClass('fa fa-angle-right fa fa-angle-down');
            //$(this).nextAll('ul').slideToggle('slow');
            $(this).toggleClass('fa fa-angle-right fa fa-angle-down');
            $(this).closest('li').nextAll('ul').slideToggle('slow');
        });
    });
</script>
<?php $this->load->view('include/footer'); ?>