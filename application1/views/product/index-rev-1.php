<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
?>
<!-- Men's Clothes Section Strat Here -->
<section class="mens-clothes-section">
    <div class="container">
        <div class="row">
            <!-- <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				<div class="porto-u-main-heading">
					<?php
                    if (isset($category_ids) && !empty($category_ids)) {
                        foreach ($category_ids as $c_key => $c_id) {
                            $conditions = array(
                                'where' => array('id' => $c_id)
                            );
                            $info =  $this->common_model->select_data('category', $conditions);
                            if ($info['row_count'] > 0) {
                    ?>
								<h2 style="text-transform: capitalize;font-size: 35px;"><?php echo $info['data'][0]['name' . get_language()] ?></h2>
					<?php
                            }
                            break;
                        }
                    }
                    ?>
				</div>
			</div> -->
            <!-- 
			<div class="col-md-2"></div>
			
			<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
				<div class="filter">
					<form class="row">
						<div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
							<select class="form-control-plaintext" id="gender">
								<option>Colors</option>
								<option>Blue</option>
								<option>Black</option>
								<option>Red</option>
								<option>Whight</option>
							</select>
						</div>
						<div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
							<select class="form-control-plaintext" id="gender">
								<option>Size</option>
								<option>xl</option>
								<option>l</option>
								<option>xxl</option>
								<option>xxxl</option>
								<option>sm</option>
							</select>
						</div>
						<div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
					        <select class="form-control-plaintext" id="gender">
					        	<option>Default Sorting</option>
						      	<option>Sort By - 1</option>
						      	<option>Sort By - 2</option>
						    </select>
					  	</div>
					</form>
				</div>
			</div> -->
        </div>
        <div class="row no-breadcrumbs">
            <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                <div class="mens-clothes-section-sidebar category">
                    <!-- <?php
                            /* foreach ($category_ids as $c_key => $c_id) {
                                $conditions = array(
                                    'where' => array('id' => $c_id)
                                );
                                $info =  $this->common_model->select_data('category', $conditions);
                                if ($info['row_count'] > 0) {
                            ?>
								<div class="sidebar-link">
									<?php
                                    unset($category_ids[$c_key]);
                                    foreach ($category_ids as $c_key => $c_id) {
                                        $conditions = array(
                                            'where' => array('id' => $c_id)
                                        );
                                        $info =  $this->common_model->select_data('category', $conditions);
                                        if ($info['row_count'] > 0) {
                                    ?>
											<li><a href="javascript:void(0);"><?php echo $info['data'][0]['name' . get_language()]; ?></a></li>
									<?php
                                        }
                                    }
                                    ?>
								</div>
						<?php
                                }
                                break;
                            } */
                            ?> -->
                    <div class="sidebar-link sidebar-link-new">
                        <?php
                        function get_category_tree($parent_id = 0, $sub_mark = '')
                        {
                            $details = get_details('category', array('parent_id' => $parent_id));
                            if (isset($details) && $details != null) {
                                foreach ($details as $key => $value) {
                                    $GLOBALS['category_options'][] = array('id' => $value['id'], 'name' => $sub_mark . $value['name' . get_language()]);
                                    get_category_tree($value['id'], $sub_mark . '--&nbsp;&nbsp;');
                                }
                            }
                        }

                        get_category_tree();
                        ?>
                        <ul>
                            <li>Mi <i class="fa fa-angle-right"></i></li>
                            <ul style="display:none;">
                                <li>A serialize 1<i class="fa fa-angle-right"></i></li>
                                <ul style="display:none;">
                                    <li>A serialize 2<i class="fa fa-angle-right"></i></li>

                                    <li>A serialize 2</li>
                                </ul>
                                <li>A serialize </li>
                            </ul>
                        </ul>
                    </div>
                    <!-- <div class="sidebar-link">
                        <?php

                        $conditions = array(
                            'where' => array('id' => $category_ids[0])
                        );
                        unset($category_ids[0]);
                        $info =  $this->common_model->select_data('category', $conditions);
                        if ($info['row_count'] > 0) {
                            $info = $info['data'][0];
                        ?>
                            <li><a href="javascript:void(0);"><?php echo $info['name' . get_language_front()];  ?></a><span class="toggle" onclick="myFunction()"></span></li>
                            <?php
                            if (isset($category_ids[1]) && !empty($category_ids[1])) {
                                get_category_tree($category_ids[1]);
                            }
                        }
                        /*function get_category_tree($parent_id)
                        {
                            $details = get_details('category', array('parent_id' => $parent_id));
                            if (isset($details) && $details != null) {
                                foreach ($details as $key => $value) {
                                    $GLOBALS['category_options'][] = array('id' => $value['id'], 'name' =>  $value['name' . get_language()]);
                                    /*
                            ?>
                                    <div id="myDIV" style="display:none;padding-left: 1.0714em">
                                        <li><a href="javascript:void(0);"><?php echo $value['name' . get_language()]; ?></a><span class="toggle" onclick="myFunction2()"></span></li>
                                        <div id="myDIV2" style="display:none;padding-left: 1.0714em">
                                            <li><a href="#">Mi</a></li>
                                        </div>
                                    </div>
                        <?php*//*
                                    get_category_tree($value['id']);
                                }
                            }
                        } */
                            ?>
                        <?php /*<li><a href="#">A Series</a><span class="toggle" onclick="myFunction3()"></span></li>
                        <div id="myDIV3" style="display:none;padding-left: 1.0714em">
                            <li><a href="#">Mi</a><span class="toggle" onclick="myFunction4()"></span></li>
                            <div id="myDIV4" style="display:none;padding-left: 1.0714em">
                                <li><a href="#">Mi</a></li>
                            </div>
                        </div>*/ ?>
                    </div> -->
                </div>
                <?php
                if (isset($category_ids) && !empty($category_ids)) {/*
				?>
					<div class="mens-clothes-section-sidebar category">
						<!-- <?php
						foreach ($category_ids as $c_key => $c_id) {
							$conditions = array(
								'where' => array('id' => $c_id)
							);
							$info =  $this->common_model->select_data('category', $conditions);
							if ($info['row_count'] > 0) {
						?>
								<div class="sidebar-link">
									<?php
									unset($category_ids[$c_key]);
									foreach ($category_ids as $c_key => $c_id) {
										$conditions = array(
											'where' => array('id' => $c_id)
										);
										$info =  $this->common_model->select_data('category', $conditions);
										if ($info['row_count'] > 0) {
									?>
											<li><a href="javascript:void(0);"><?php echo $info['data'][0]['name' . get_language()]; ?></a></li>
									<?php
										}
									}
									?>
								</div>
						<?php
							}
							break;
						}
						?> -->
						<div class="sidebar-link">
							<li><a href="#">Mi</a><span class="toggle" onclick="myFunction()"></span></li>
							<div id="myDIV" style="display:none;padding-left: 1.0714em">
								<li><a href="#">Mi</a><span class="toggle" onclick="myFunction2()"></span></li>
								<div id="myDIV2" style="display:none;padding-left: 1.0714em">
									<li><a href="#">Mi</a></li>
								</div>
							</div>
							<li><a href="#">A Series</a><span class="toggle" onclick="myFunction3()"></span></li>
							<div id="myDIV3" style="display:none;padding-left: 1.0714em">
								<li><a href="#">Mi</a><span class="toggle" onclick="myFunction4()"></span></li>
								<div id="myDIV4" style="display:none;padding-left: 1.0714em">
									<li><a href="#">Mi</a></li>
								</div>
							</div>
						</div> 
					</div>
				<?php
				*/
                }
                ?>
            </div>
            <div class="main-content col-md-9 col-lg-9 col-sm-12 col-xs-12">
                <p>
                    <?php
                    if (isset($category_ids) && !empty($category_ids)) {
                        foreach ($category_ids as $c_key => $c_id) {
                            $conditions = array(
                                'where' => array('id' => $c_id)
                            );
                            $info =  $this->common_model->select_data('category', $conditions);
                            if ($info['row_count'] > 0) {
                    ?>
                                <?php echo $info['data'][0]['name' . get_language()] ?>
                    <?php
                            }
                            break;
                        }
                    }
                    ?>
                </p>
                <div class="filter">
                    <form>
                        <div class="row">
                            <?php /*<div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
					        <select class="form-control-plaintext" id="gender">
					        	<option value="">Category</option>
					        	<?php
			                        $category_details = get_details('category',array('parent_id'=>0,'is_active'=>'1'));
			                        if (isset($category_details) && $category_details !=null) {
			                            foreach ($category_details as $key => $value) {
			                                $id = $value['id'];
			                            ?>
			                                <option value="<?= $id;?>"><?= $value['name'.get_language_front()];?></option>
			                            <?php } 
			                        }
			                    ?>
						    </select>
					  	</div> */ ?>
                            <div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext selectpicker" name="" data-size="7" data-live-search="true" data-title="Location" id="state_list" data-width="100%">
                                    <option value="" selected>Mobile Section</option>
                                    <option value="" class="sub-value">Nokia</option>
                                    <option value="" class="sub-value">Redmi</option>
                                    <option value="" class="sub-value">iphone</option>
                                    <option value="" class="sub-value">samsung</option>
                                </select>
                                <!-- <select class="form-control-plaintext" id="gender">
								<option>Manufacturer's Name</option>
								<option>Aarkay Engineering Corpot</option>
								<option>Aarkay Engineering Corpot</option>
								<option>LIAAEN BAMFORD LTD,ENGLAND</option>
								<option>SAAB, SWEDEN</option>
							</select> -->
                            </div>
                            <div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext" id="gender">
                                    <option>Colors</option>
                                    <option>Blue</option>
                                    <option>Black</option>
                                    <option>Red</option>
                                    <option>Whight</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext" id="gender-2">
                                    <option>Size</option>
                                    <option>xl</option>
                                    <option>l</option>
                                    <option>xxl</option>
                                    <option>xxxl</option>
                                    <option>sm</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                <select class="form-control-plaintext" id="gender-3">
                                    <option>Default Sorting</option>
                                    <option>Sort By - 1</option>
                                    <option>Sort By - 2</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <div class="gridlist-toggle">
                        <a href="#" id="grid" title="Grid View"></a>
                        <a href="#" id="list" title="List View"></a>
                    </div>
                </div>
                <div class="row">
                    <?php
                    if (isset($details) && $details != null) {
                        foreach ($details as $key => $value) {
                            $id = $value['id'];
                            $id_unique = $value['id_unique'];
                    ?>
                            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                                <div class="mens-clothes-box">
                                    <ul>
                                        <li>
                                            <?php
                                            $get_pro_id = get_details('wishlist', array('id_product' => $id, 'login_id' => $this->session->userdata('login_id')));

                                            if (isset($get_pro_id) && $get_pro_id != null) {
                                            ?>
                                                <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                            <?php } else {
                                            ?>
                                                <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                            <?php }
                                            ?>
                                        </li>
                                        <li class="new">New</li>
                                    </ul>
                                    <a href="<?= $current_url . '/' . $value['slug'] . '/' . $id_unique; ?>" style="color: #000;">
                                        <div class="mens-clothes-img text-center img-effect">
                                            <img src="<?= base_url('uploads/product/' . $value['main_image']); ?>">
                                        </div>
                                        <div class="mens-clothes-name text-center">
                                            <h4 style="font-size: 18px;"><b><?= $value['title']; ?></b></h4>
                                            <h4><?= $value['price']; ?> SAR</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                    <?php }
                    } else {
                        echo "<h3>" . get_line_front('product_not_available') . "</h3>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Men's Clothes Section End Here -->
<?php
$footer_js = array(base_url('assets/js/bootstrap-select.js'));

$this->load->view('include/copyright', array('footer_js' => $footer_js));
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(window).scroll(function() {

            if ($(window).scrollTop() > 200) {
                $('.mens-clothes-section-sidebar').css('position', 'fixed');
                $('.mens-clothes-section-sidebar').css('top', '0');
                $('.mens-clothes-section-sidebar').css('width', '20%');
            } else if ($(window).scrollTop() <= 200) {
                $('.mens-clothes-section-sidebar').css('position', '');
                $('.mens-clothes-section-sidebar').css('top', '');
                $('.mens-clothes-section-sidebar').css('width', '');
            }
            if ($('.mens-clothes-section-sidebar').offset().top + $(".mens-clothes-section-sidebar").height() > $(".footer-wrapper").offset().top) {
                $('.mens-clothes-section-sidebar').css('top', -($(".mens-clothes-section-sidebar").offset().top + $(".mens-clothes-section-sidebar").height() - $("#footer").offset().top));
            }
        });

        $('.sidebar-link-new li').click(function() {
            $(this).find('i').toggleClass('fa fa-angle-right fa fa-angle-down');
            $(this).next('ul').slideToggle('slow');
        });
    });
</script>
<?php $this->load->view('include/footer'); ?>