<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (isset($details) && $details != null) {
    foreach ($details as $key => $value) {
        $id = $value['id_unique'];
        $id_unique = $value['id_unique'];
?>
        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
            <div class="mens-clothes-box">
                <ul>
                    <li>
                        <?php
                        $get_pro_id = get_details('wishlist', array('id_product' => $id, 'login_id' => get_front_login_id()));
                        if (isset($get_pro_id) && $get_pro_id != null) { ?>
                            <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart" aria-hidden="true"></i></a>
                        <?php } else { ?>
                            <a href="javascript:void(0)" class="wishlist" id="<?= $id ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                        <?php } ?>
                    </li>
                    <?php
                    if (isset($value['is_new']) && $value['is_new'] == '1') {
                    ?>
                        <li class="new"><?php echo get_line('new'); ?></li>
                    <?php
                    }
                    ?>
                </ul>
                <a href="<?= $current_url . '/' . $value['slug'] . '/' . $id_unique; ?>" style="color: #000;">
                    <div class="mens-clothes-img text-center img-effect">
                        <img src="<?= $value['main_image']; ?>">
                    </div>
                    <div class="mens-clothes-name text-center">
                        <h4 style="font-size: 18px;"><b><?= $value['title']; ?></b></h4>
                        <h4><?= $value['other_info']['final_price'][0]; ?> SAR</h4>
                    </div>
                </a>
            </div>
        </div>
<?php }
} else {
    echo '<div class="col-md-12">';
    echo "<h3>" . get_line_front('product_not_available') . "</h3>";
    echo '</div>';
}
