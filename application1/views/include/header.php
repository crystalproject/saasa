<!DOCTYPE html>
<?php
$set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
?>
<html <?= $set_language == 2 ? 'dir="rtl" lang="ar"' : ''; ?>>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= SITE_TITLE; ?></title>
    <?php
    if ($set_language == 1) {
    ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/plugins.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/theme.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/shortcodes.css"> -->
    <?php }
    if ($set_language == 2) {
    ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/rtl/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/rtl/style.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/rtl/style_rtl.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/theme.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/theme_rtl.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/plugins.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/rtl/shortcodes.css">
        <style type="text/css">
            .footer-main #text-16 {
                margin-left: -7px !important;
            }
        </style>
    <?php
    }
    ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
    <!--New add-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <?php
    if (isset($header_css) && !empty($header_css)) {
        foreach ($header_css as $key => $value) {
            echo '<link rel="stylesheet" href="' . $value . '">';
        }
    }
    ?>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/developer.css" />
    <style type="text/css">
        /*@import '~@fortawesome/fontawesome-free/css/all.css';*/
    </style>
    <script>
        var BASE_URL = '<?= base_url(); ?>';
        var toastr_success = '';
        var toastr_error = '';
    </script>
</head>

<body class="<?= $set_language == 2 ? 'arabic' : 'english'; ?>">
    <?php
    $shop = get_details('menus', array('is_active' => '1', 'id' => 4));
    $home = get_details('menus', array('is_active' => '1', 'id' => 3));
    $about_us = get_details('menus', array('is_active' => '1', 'id' => 2));
    $contact_us = get_details('menus', array('is_active' => '1', 'id' => 5));
    $return_policy = get_details('menus', array('is_active' => '1', 'id' => 6));
    $privacy_policy = get_details('menus', array('is_active' => '1', 'id' => 8));
    $terms_condition = get_details('menus', array('is_active' => '1', 'id' => 10));
    ?>
    <div class="page-wrapper">
        <div class="header-wrapper">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()" onmouseover="closeNav1()">&times;</a>
                <br /> <br /> <br />
                <h1 onmouseover="closeNav1()"><?= get_line_front('menu'); ?></h1>
                <ul>
                    <?php
                    if (isset($shop) && $shop != null) {
                    ?>
                        <li><a href="#" onmouseover="openNav1()"><?= $shop[0]['name' . get_language_front()]; ?> &nbsp;&nbsp; ></a></li>
                    <?php }
                    ?>
                    <ul id="menu-menu-home" class="secondary-menu main-menu mega-menu show-arrow">
                        <?php
                        if (isset($home) && $home != null) {
                        ?>
                            <li id="nav-menu-item-4188" onmouseover="closeNav1()" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active narrow"><a href="<?= base_url(); ?>" class=" current"><?= $home[0]['name' . get_language_front()]; ?></a></li>
                        <?php }
                        if (isset($about_us) && $about_us != null) {
                        ?>
                            <li id="nav-menu-item-4187" onmouseover="closeNav1()" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
                                <a href="<?= base_url('cms-page/about-us'); ?>"><?= $about_us[0]['name' . get_language_front()]; ?></a>
                            </li>
                        <?php }
                        if (isset($contact_us) && $contact_us != null) {
                        ?>
                            <li id="nav-menu-item-4195" onmouseover="closeNav1()" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
                                <a href="<?= base_url('contact-us'); ?>"><?= $contact_us[0]['name' . get_language_front()]; ?></a>
                            </li>
                        <?php }
                        if (isset($privacy_policy) && $privacy_policy != null) {
                        ?>
                            <li id="nav-menu-item-4195" onmouseover="closeNav1()" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
                                <a href="<?= base_url('cms-page/privacy-policy'); ?>"><?= $privacy_policy[0]['name' . get_language_front()]; ?></a>
                            </li>
                        <?php }
                        if (isset($return_policy) && $return_policy != null) {
                        ?>
                            <li id="nav-menu-item-4197" onmouseover="closeNav1()" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
                                <a href="<?= base_url('cms-page/return-policy'); ?>"><?= $return_policy[0]['name' . get_language_front()]; ?></a>
                            </li>
                        <?php }
                        if (isset($terms_condition) && $terms_condition != null) {
                        ?>
                            <li id="nav-menu-item-4197" onmouseover="closeNav1()" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
                                <a href="<?= base_url('cms-page/terms-condition'); ?>"><?= $terms_condition[0]['name' . get_language_front()]; ?></a>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </ul>
            </div>
            <div id="mySidenav1" class="sidenav1" style="margin-left:300px;">
                <h1><?= get_line_front('categories'); ?></h1>
                <ul>
                    <?php
                    $category_details = get_details('category', array('parent_id' => 0, 'is_active' => '1'));
                    if (isset($category_details) && $category_details != null) {
                        foreach ($category_details as $key => $value) {
                    ?>
                            <?php /*<li><a href="<?= base_url('category/'.$value['slug'.get_language_front()]);?>"><?= $value['name'.get_language_front()];?></a></li>*/ ?>
                            <li><a href="<?php echo base_url('category/' . $value['slug' . get_language()] . '/' . $value['id']); ?>"><?= $value['name' . get_language_front()]; ?></a></li>
                    <?php }
                    }
                    ?>
                </ul>
            </div>
            <header id="header" class="header-separate header-1 sticky-menu-header">
                <?php
                $profile_details = $this->production_model->get_all_with_where('administrator', '', '', array());
                ?>
                <div class="header-top">
                    <div class="container">
                        <div class="header-left">
                            <div class="switcher-wrap">Tel: <?= isset($profile_details) && $profile_details != null ? $profile_details[0]['mobile_number_1'] : ''; ?></div>
                            <span class="gap switcher-gap">|</span>
                            <ul class="view-switcher porto-view-switcher mega-menu show-arrow">
                                <?php
                                $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
                                ?>
                                <li class="menu-item has-sub narrow sub-ready">
                                    <?php
                                    if ($set_language == 1) {
                                    ?>
                                        <a href="javascript:void();" class="nolink change-language" data-id="1" style="color: #000">
                                            <span><img src="<?= base_url(); ?>assets/images/au-flag.png" height="12" width="18"></span> <?php echo get_line('english'); ?>
                                        </a>
                                    <?php
                                    } elseif ($set_language == 2) {
                                    ?>
                                        <a href="javascript:void();" class="nolink change-language" data-id="2" style="color: #000">
                                            <span><img src="<?= base_url(); ?>assets/images/arab-flag.png" height="12" width="18"></span> <?php echo get_line('arabic'); ?>
                                        </a>
                                    <?php
                                    }
                                    ?>
                                    <div class="popup" style="display: block;">
                                        <div class="inner">
                                            <ul class="sub-menu">
                                                <li class="menu-item">
                                                    <?php
                                                    if ($set_language == 1) {
                                                    ?>
                                                        <a href="javascript:void();" class="change-language" data-id="2" style="color: #000;">
                                                            <span class="flag"><img src="<?= base_url(); ?>assets/images/arab-flag.png" height="12" width="18"></span>
                                                            <?php echo get_line('arabic'); ?>
                                                        </a>
                                                    <?php }
                                                    if ($set_language == 2) {
                                                    ?>
                                                        <a href="javascript:void();" class="change-language" data-id="1" style="color: #000;">
                                                            <span class="flag"><img src="<?= base_url(); ?>assets/images/au-flag.png" height="12" width="18"></span>
                                                            <?php echo get_line('english'); ?>
                                                        </a>
                                                    <?php }
                                                    ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="header-right">
                            <div class="share-links">
                                <a target="_blank" rel="nofollow" class="share-facebook" href="#" title="Facebook"></a>
                                <a target="_blank" rel="nofollow" class="share-twitter" href="#" title="Twitter"></a>
                                <a target="_blank" rel="nofollow" class="share-instagram" href="#" title="Instagram"></a>
                                <a target="_blank" rel="nofollow" class="share-linkedin" href="#" title="LinkedIn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-main">
                    <div class="container header-row">
                        <div class="header-left" style="max-width: 25%;">
                            <span style="font-size:25px;cursor:pointer;color: #000;" onclick="openNav()">
                                <img src="<?= base_url(); ?>assets/images/menu.png">&nbsp; <?= get_line_front('menu'); ?>
                            </span>
                        </div>
                        <div class="header-center">
                            <h1 class="logologo">
                                <a href="<?= base_url(); ?>" title="Customizer - " rel="home">
                                    <img class="img-responsive sticky-logo sticky-retina-logo" src="<?= base_url(); ?>assets/images/E-Shop-New-Brand.png" alt="Customizer" />
                                    <img class="img-responsive standard-logo" src="<?= isset($profile_details) && $profile_details != null ? base_url(PROFILE_PICTURE) . $profile_details[0]['profile_photo'] : base_url('assets/uploads/default_img.png') ?>" alt="Customizer" />
                                    <img class="img-responsive retina-logo" src="<?= base_url(); ?>assets/images/E-Shop-New-Brand.png" alt="Customizer" style="max-height:px;" />
                                </a>
                            </h1>
                        </div>
                        <div class="header-right">
                            <div class="searchform-popup">
                                <a class="search-toggle"><i class="fas fa-search"></i><span class="search-text">Search</span></a>
                                <form action="#" method="get" class="searchform">
                                    <div class="searchform-fields">
                                        <span class="text"><input name="s" type="text" value="" placeholder="Search" autocomplete="off" /></span>
                                        <input type="hidden" name="post_type" value="product" />
                                        <span class="button-wrap">
                                            <button class="btn btn-special" title="Search" type="submit"><i class="fas fa-search"></i></button>
                                        </span>
                                    </div>
                                    <div class="live-search-list"></div>
                                </form>
                            </div>
                            <div class="header-minicart" style="padding-right:10px;">
                                <div class="cart-head">
                                    <span class="cart-icon">
                                        <a href="<?= base_url('wishlist'); ?>" class="wishlist_products_counter top_wishlist-heart top_wishlist- no-txt wishlist-counter-with-products">
                                            <span class="wishlist_products_counter_text"></span>
                                            <span class="wishlist_products_counter_number wishlist-counter"><?php echo get_total_wishlist(); ?></span>
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="header-minicart">
                                <div id="mini-cart" class="mini-cart minicart-arrow-alt">
                                    <div class="cart-head">
                                        <span class="cart-icon">
                                            <a href="<?= base_url('wishlist'); ?>">
                                                <img src="<?= base_url(); ?>assets/images/heart.png" style="height: 23px;width: 22px;">
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="header-minicart" style="padding-right:10px;">
                                <div class="cart-head dropdown active-user">
                                    <span class="cart-icon">
                                        <a class="dropbtn" style="color: #000;"><img src="<?= base_url(); ?>assets/images/user-1.png"></a>
                                        <div class="dropdown-content">
                                            <?php
                                            if ($this->session->userdata('login_id') == null) {
                                            ?>
                                                <form id="login_form" action="<?= base_url('login/dologin'); ?>" method="post">
                                                    <h2 class="text-left" style="font-weight: 900;"><?= get_line_front('sign_in'); ?></h2>
                                                    <p class="status"></p>
                                                    <div class="form-group">
                                                        <input type="text" id="username" name="email" class="form-control" placeholder="<?= get_line_front('email_address'); ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="password" id="password" name="password" class="form-control" placeholder="<?= get_line_front('password'); ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="submit_button btn btn-primary btn-block" type="submit" value="<?= get_line_front('sign_in'); ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <p class="send-otp-login m-0"><a href="<?php echo base_url('forgot-password'); ?>" class="pull-right"><?= get_line_front('forgot_password'); ?>?</a></p>
                                                    </div>
                                                    <h2 class="text-left" style="font-weight: 900;"><?= get_line_front('or'); ?></h2>
                                                    <p class="status"></p>
                                                    <div class="form-group">
                                                        <select name="country_code" class="form-control">
                                                            <option data-countryCode="DZ" value="213">Algeria (+213)</option>
                                                            <option data-countryCode="AD" value="376">Andorra (+376)</option>
                                                            <option data-countryCode="AO" value="244">Angola (+244)</option>
                                                            <option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
                                                            <option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
                                                            <option data-countryCode="AR" value="54">Argentina (+54)</option>
                                                            <option data-countryCode="AM" value="374">Armenia (+374)</option>
                                                            <option data-countryCode="AW" value="297">Aruba (+297)</option>
                                                            <option data-countryCode="AU" value="61">Australia (+61)</option>
                                                            <option data-countryCode="AT" value="43">Austria (+43)</option>
                                                            <option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
                                                            <option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
                                                            <option data-countryCode="BH" value="973">Bahrain (+973)</option>
                                                            <option data-countryCode="BD" value="880">Bangladesh (+880)</option>
                                                            <option data-countryCode="BB" value="1246">Barbados (+1246)</option>
                                                            <option data-countryCode="BY" value="375">Belarus (+375)</option>
                                                            <option data-countryCode="BE" value="32">Belgium (+32)</option>
                                                            <option data-countryCode="BZ" value="501">Belize (+501)</option>
                                                            <option data-countryCode="BJ" value="229">Benin (+229)</option>
                                                            <option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
                                                            <option data-countryCode="BT" value="975">Bhutan (+975)</option>
                                                            <option data-countryCode="BO" value="591">Bolivia (+591)</option>
                                                            <option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
                                                            <option data-countryCode="BW" value="267">Botswana (+267)</option>
                                                            <option data-countryCode="BR" value="55">Brazil (+55)</option>
                                                            <option data-countryCode="BN" value="673">Brunei (+673)</option>
                                                            <option data-countryCode="BG" value="359">Bulgaria (+359)</option>
                                                            <option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
                                                            <option data-countryCode="BI" value="257">Burundi (+257)</option>
                                                            <option data-countryCode="KH" value="855">Cambodia (+855)</option>
                                                            <option data-countryCode="CM" value="237">Cameroon (+237)</option>
                                                            <option data-countryCode="CA" value="1">Canada (+1)</option>
                                                            <option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
                                                            <option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
                                                            <option data-countryCode="CF" value="236">Central African Republic (+236)</option>
                                                            <option data-countryCode="CL" value="56">Chile (+56)</option>
                                                            <option data-countryCode="CN" value="86">China (+86)</option>
                                                            <option data-countryCode="CO" value="57">Colombia (+57)</option>
                                                            <option data-countryCode="KM" value="269">Comoros (+269)</option>
                                                            <option data-countryCode="CG" value="242">Congo (+242)</option>
                                                            <option data-countryCode="CK" value="682">Cook Islands (+682)</option>
                                                            <option data-countryCode="CR" value="506">Costa Rica (+506)</option>
                                                            <option data-countryCode="HR" value="385">Croatia (+385)</option>
                                                            <option data-countryCode="CU" value="53">Cuba (+53)</option>
                                                            <option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
                                                            <option data-countryCode="CY" value="357">Cyprus South (+357)</option>
                                                            <option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
                                                            <option data-countryCode="DK" value="45">Denmark (+45)</option>
                                                            <option data-countryCode="DJ" value="253">Djibouti (+253)</option>
                                                            <option data-countryCode="DM" value="1809">Dominica (+1809)</option>
                                                            <option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
                                                            <option data-countryCode="EC" value="593">Ecuador (+593)</option>
                                                            <option data-countryCode="EG" value="20">Egypt (+20)</option>
                                                            <option data-countryCode="SV" value="503">El Salvador (+503)</option>
                                                            <option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
                                                            <option data-countryCode="ER" value="291">Eritrea (+291)</option>
                                                            <option data-countryCode="EE" value="372">Estonia (+372)</option>
                                                            <option data-countryCode="ET" value="251">Ethiopia (+251)</option>
                                                            <option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
                                                            <option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
                                                            <option data-countryCode="FJ" value="679">Fiji (+679)</option>
                                                            <option data-countryCode="FI" value="358">Finland (+358)</option>
                                                            <option data-countryCode="FR" value="33">France (+33)</option>
                                                            <option data-countryCode="GF" value="594">French Guiana (+594)</option>
                                                            <option data-countryCode="PF" value="689">French Polynesia (+689)</option>
                                                            <option data-countryCode="GA" value="241">Gabon (+241)</option>
                                                            <option data-countryCode="GM" value="220">Gambia (+220)</option>
                                                            <option data-countryCode="GE" value="7880">Georgia (+7880)</option>
                                                            <option data-countryCode="DE" value="49">Germany (+49)</option>
                                                            <option data-countryCode="GH" value="233">Ghana (+233)</option>
                                                            <option data-countryCode="GI" value="350">Gibraltar (+350)</option>
                                                            <option data-countryCode="GR" value="30">Greece (+30)</option>
                                                            <option data-countryCode="GL" value="299">Greenland (+299)</option>
                                                            <option data-countryCode="GD" value="1473">Grenada (+1473)</option>
                                                            <option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
                                                            <option data-countryCode="GU" value="671">Guam (+671)</option>
                                                            <option data-countryCode="GT" value="502">Guatemala (+502)</option>
                                                            <option data-countryCode="GN" value="224">Guinea (+224)</option>
                                                            <option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
                                                            <option data-countryCode="GY" value="592">Guyana (+592)</option>
                                                            <option data-countryCode="HT" value="509">Haiti (+509)</option>
                                                            <option data-countryCode="HN" value="504">Honduras (+504)</option>
                                                            <option data-countryCode="HK" value="852">Hong Kong (+852)</option>
                                                            <option data-countryCode="HU" value="36">Hungary (+36)</option>
                                                            <option data-countryCode="IS" value="354">Iceland (+354)</option>
                                                            <option data-countryCode="IN" value="91">India (+91)</option>
                                                            <option data-countryCode="ID" value="62">Indonesia (+62)</option>
                                                            <option data-countryCode="IR" value="98">Iran (+98)</option>
                                                            <option data-countryCode="IQ" value="964">Iraq (+964)</option>
                                                            <option data-countryCode="IE" value="353">Ireland (+353)</option>
                                                            <option data-countryCode="IL" value="972">Israel (+972)</option>
                                                            <option data-countryCode="IT" value="39">Italy (+39)</option>
                                                            <option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
                                                            <option data-countryCode="JP" value="81">Japan (+81)</option>
                                                            <option data-countryCode="JO" value="962">Jordan (+962)</option>
                                                            <option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
                                                            <option data-countryCode="KE" value="254">Kenya (+254)</option>
                                                            <option data-countryCode="KI" value="686">Kiribati (+686)</option>
                                                            <option data-countryCode="KP" value="850">Korea North (+850)</option>
                                                            <option data-countryCode="KR" value="82">Korea South (+82)</option>
                                                            <option data-countryCode="KW" value="965">Kuwait (+965)</option>
                                                            <option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
                                                            <option data-countryCode="LA" value="856">Laos (+856)</option>
                                                            <option data-countryCode="LV" value="371">Latvia (+371)</option>
                                                            <option data-countryCode="LB" value="961">Lebanon (+961)</option>
                                                            <option data-countryCode="LS" value="266">Lesotho (+266)</option>
                                                            <option data-countryCode="LR" value="231">Liberia (+231)</option>
                                                            <option data-countryCode="LY" value="218">Libya (+218)</option>
                                                            <option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
                                                            <option data-countryCode="LT" value="370">Lithuania (+370)</option>
                                                            <option data-countryCode="LU" value="352">Luxembourg (+352)</option>
                                                            <option data-countryCode="MO" value="853">Macao (+853)</option>
                                                            <option data-countryCode="MK" value="389">Macedonia (+389)</option>
                                                            <option data-countryCode="MG" value="261">Madagascar (+261)</option>
                                                            <option data-countryCode="MW" value="265">Malawi (+265)</option>
                                                            <option data-countryCode="MY" value="60">Malaysia (+60)</option>
                                                            <option data-countryCode="MV" value="960">Maldives (+960)</option>
                                                            <option data-countryCode="ML" value="223">Mali (+223)</option>
                                                            <option data-countryCode="MT" value="356">Malta (+356)</option>
                                                            <option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
                                                            <option data-countryCode="MQ" value="596">Martinique (+596)</option>
                                                            <option data-countryCode="MR" value="222">Mauritania (+222)</option>
                                                            <option data-countryCode="YT" value="269">Mayotte (+269)</option>
                                                            <option data-countryCode="MX" value="52">Mexico (+52)</option>
                                                            <option data-countryCode="FM" value="691">Micronesia (+691)</option>
                                                            <option data-countryCode="MD" value="373">Moldova (+373)</option>
                                                            <option data-countryCode="MC" value="377">Monaco (+377)</option>
                                                            <option data-countryCode="MN" value="976">Mongolia (+976)</option>
                                                            <option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
                                                            <option data-countryCode="MA" value="212">Morocco (+212)</option>
                                                            <option data-countryCode="MZ" value="258">Mozambique (+258)</option>
                                                            <option data-countryCode="MN" value="95">Myanmar (+95)</option>
                                                            <option data-countryCode="NA" value="264">Namibia (+264)</option>
                                                            <option data-countryCode="NR" value="674">Nauru (+674)</option>
                                                            <option data-countryCode="NP" value="977">Nepal (+977)</option>
                                                            <option data-countryCode="NL" value="31">Netherlands (+31)</option>
                                                            <option data-countryCode="NC" value="687">New Caledonia (+687)</option>
                                                            <option data-countryCode="NZ" value="64">New Zealand (+64)</option>
                                                            <option data-countryCode="NI" value="505">Nicaragua (+505)</option>
                                                            <option data-countryCode="NE" value="227">Niger (+227)</option>
                                                            <option data-countryCode="NG" value="234">Nigeria (+234)</option>
                                                            <option data-countryCode="NU" value="683">Niue (+683)</option>
                                                            <option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
                                                            <option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
                                                            <option data-countryCode="NO" value="47">Norway (+47)</option>
                                                            <option data-countryCode="OM" value="968">Oman (+968)</option>
                                                            <option data-countryCode="PW" value="680">Palau (+680)</option>
                                                            <option data-countryCode="PA" value="507">Panama (+507)</option>
                                                            <option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
                                                            <option data-countryCode="PY" value="595">Paraguay (+595)</option>
                                                            <option data-countryCode="PE" value="51">Peru (+51)</option>
                                                            <option data-countryCode="PH" value="63">Philippines (+63)</option>
                                                            <option data-countryCode="PL" value="48">Poland (+48)</option>
                                                            <option data-countryCode="PT" value="351">Portugal (+351)</option>
                                                            <option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
                                                            <option data-countryCode="QA" value="974">Qatar (+974)</option>
                                                            <option data-countryCode="RE" value="262">Reunion (+262)</option>
                                                            <option data-countryCode="RO" value="40">Romania (+40)</option>
                                                            <option data-countryCode="RU" value="7">Russia (+7)</option>
                                                            <option data-countryCode="RW" value="250">Rwanda (+250)</option>
                                                            <option data-countryCode="SM" value="378">San Marino (+378)</option>
                                                            <option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
                                                            <option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
                                                            <option data-countryCode="SN" value="221">Senegal (+221)</option>
                                                            <option data-countryCode="CS" value="381">Serbia (+381)</option>
                                                            <option data-countryCode="SC" value="248">Seychelles (+248)</option>
                                                            <option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
                                                            <option data-countryCode="SG" value="65">Singapore (+65)</option>
                                                            <option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
                                                            <option data-countryCode="SI" value="386">Slovenia (+386)</option>
                                                            <option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
                                                            <option data-countryCode="SO" value="252">Somalia (+252)</option>
                                                            <option data-countryCode="ZA" value="27">South Africa (+27)</option>
                                                            <option data-countryCode="ES" value="34">Spain (+34)</option>
                                                            <option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
                                                            <option data-countryCode="SH" value="290">St. Helena (+290)</option>
                                                            <option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
                                                            <option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
                                                            <option data-countryCode="SD" value="249">Sudan (+249)</option>
                                                            <option data-countryCode="SR" value="597">Suriname (+597)</option>
                                                            <option data-countryCode="SZ" value="268">Swaziland (+268)</option>
                                                            <option data-countryCode="SE" value="46">Sweden (+46)</option>
                                                            <option data-countryCode="CH" value="41">Switzerland (+41)</option>
                                                            <option data-countryCode="SI" value="963">Syria (+963)</option>
                                                            <option data-countryCode="TW" value="886">Taiwan (+886)</option>
                                                            <option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
                                                            <option data-countryCode="TH" value="66">Thailand (+66)</option>
                                                            <option data-countryCode="TG" value="228">Togo (+228)</option>
                                                            <option data-countryCode="TO" value="676">Tonga (+676)</option>
                                                            <option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
                                                            <option data-countryCode="TN" value="216">Tunisia (+216)</option>
                                                            <option data-countryCode="TR" value="90">Turkey (+90)</option>
                                                            <option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
                                                            <option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
                                                            <option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
                                                            <option data-countryCode="TV" value="688">Tuvalu (+688)</option>
                                                            <option data-countryCode="UG" value="256">Uganda (+256)</option>
                                                            <option data-countryCode="GB" value="44">UK (+44)</option>
                                                            <option data-countryCode="UA" value="380">Ukraine (+380)</option>
                                                            <option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
                                                            <option data-countryCode="UY" value="598">Uruguay (+598)</option>
                                                            <option data-countryCode="US" value="1">USA (+1)</option>
                                                            <option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
                                                            <option data-countryCode="VU" value="678">Vanuatu (+678)</option>
                                                            <option data-countryCode="VA" value="379">Vatican City (+379)</option>
                                                            <option data-countryCode="VE" value="58">Venezuela (+58)</option>
                                                            <option data-countryCode="VN" value="84">Vietnam (+84)</option>
                                                            <option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
                                                            <option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
                                                            <option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
                                                            <option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
                                                            <option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
                                                            <option data-countryCode="ZM" value="260">Zambia (+260)</option>
                                                            <option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="number" name="mobile_number" class="form-control" placeholder="<?= get_line_front('mobile_number'); ?>">
                                                        <br />
                                                        <p class="send-otp-login m-0"><?php echo get_line('send_otp'); ?></p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="number" name="otp" class="form-control" placeholder="<?= get_line_front('otp'); ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="submit_button btn btn-primary btn-block mobile-signin" type="button" value="<?= get_line_front('sign_in'); ?>">
                                                    </div>
                                                    <div class="clearfix forgot-password">
                                                        <label class="pull-left checkbox-inline"><a href="#" class="pull-right"><?= get_line_front('forgot_password'); ?>?</a></label>
                                                    </div>
                                                </form>
                                                <p class="text-center"><a href="<?= base_url('register'); ?>"><?= get_line_front('new_account'); ?></a></p>
                                            <?php } elseif ($this->session->userdata('login_id') != null) {
                                            ?>
                                                <div class="clearfix forgot-password">
                                                    <a href="<?= base_url('login/logout') ?>" class="text-center"><?= get_line_front('sign_out'); ?></a>
                                                </div>
                                            <?php }
                                            ?>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="header-minicart">
                                <div id="mini-cart" class="mini-cart minicart-arrow-alt">
                                    <div class="cart-head">
                                        <a href="<?php echo base_url('cart'); ?>">
                                            <span class="cart-icon">
                                                <!-- <img src="<?= base_url(); ?>assets/images/cart.png" style="height: 23px;width: 22px;"> -->
                                                <i class="minicart-icon"></i>
                                                <span class="cart-items"><?php echo get_total_cart_product(); ?></span>
                                            </span></a>
                                    </div>
                                    <?php
                                    $set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
                                    $form_data = array();
                                    $form_data['id_user'] = get_front_login_id();
                                    $form_data['language_slug'] = get_language_front();
                                    $form_data['id_language'] = $set_language;
                                    $cart_data = get_cart_products($form_data);
                                    if ($cart_data['status'] == true) {
                                        $total_items = count($cart_data['data']);
                                    ?>
                                        <div class="cart-popup widget_shopping_cart new-cart-popup" style="background: #ffffff;">
                                            <div class="widget_shopping_cart_content">
                                                <div class="total-count">
                                                    <span><?php echo $total_items; ?> <?php echo get_line('item'); ?></span>
                                                    <a class="pull-right" href="<?php echo base_url('cart'); ?>"><?php echo get_line('view_cart'); ?></a>
                                                </div>
                                            </div>
                                            <div class="scroll-wrapper cart_list product_list_widget scrollbar-inner" style="position: relative;">
                                                <?php /*<ul class="cart_list product_list_widget scrollbar-inner  scroll-content">*/ ?>
                                                <ul class="cart_list">
                                                    <?php
                                                    foreach ($cart_data['data'] as $c_value) {
                                                    ?>
                                                        <li class="woocommerce-mini-cart-item mini_cart_item">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="pull-left font-weight-bold"><a class="theme-link" href="<?php echo base_url('product/details/' . $c_value['slug'] . '/' . $c_value['id_unique']); ?>"><?php echo $c_value['title']; ?></a></div>
                                                                    <div class="pull-right"><a href="javascript:void(0);" class="remove remove-from-cart" data-id="<?php echo $c_value['id']; ?>"><i class="fa fa-times" aria-hidden="true"></i></a></div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <?php
                                                                    if (!empty($c_value['other_info']['attribute_info'])) {
                                                                        foreach ($c_value['other_info']['attribute_info'] as $a_value) {
                                                                            echo '<b>' . $a_value['attribute_name'] . ': ' . $a_value['attribute_values'][0]['attribute_value'] . '</b>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <?php
                                                                    if (isset($c_value['customized_image']) && !empty($c_value['customized_image'])) {
                                                                    ?>
                                                                        <img height="150" src="<?php echo $c_value['customized_image']; ?>" />
                                                                    <?php
                                                                    } else {
                                                                    ?>
                                                                        <img height="150" src="<?php echo $c_value['main_image']; ?>">
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <?php echo $c_value['quantity'] ?> x <span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY; ?></span><?php echo ($c_value['other_info']['final_price'][0]); ?>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                            <p class="woocommerce-mini-cart__total total">
                                                <strong><?php echo get_line('subtotal'); ?>:</strong>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol"><?php echo PRICE_KEY; ?></span> <?php echo get_cart_total($form_data['id_user']); ?>
                                                </span>
                                            </p>
                                            <p class="woocommerce-mini-cart__buttons buttons">
                                                <a href="<?php echo base_url('cart/checkout'); ?>" class="button checkout wc-forward">Checkout</a>
                                            </p>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <label><?= $this->session->userdata('username') ?></label>
                        </div>
                    </div>
                </div>
            </header>
            <div id="cover-spin"></div>