<!-- Footer Section Strat Here -->
<div class="footer-wrapper">
    <div id="footer" class="footer-1">
        <div class="footer-main">
            <div class="container">
                <?php
                $website = get_details('menus', array('is_active' => '1', 'id' => 7));
                $home = get_details('menus', array('is_active' => '1', 'id' => 3));
                $about_us = get_details('menus', array('is_active' => '1', 'id' => 2));
                $privacy_policy = get_details('menus', array('is_active' => '1', 'id' => 8));
                $contact_us = get_details('menus', array('is_active' => '1', 'id' => 5));
                $return_policy = get_details('menus', array('is_active' => '1', 'id' => 6));
                $terms_condition = get_details('menus', array('is_active' => '1', 'id' => 10));

                $categories = get_details('menus', array('is_active' => '1', 'id' => 11));
                $clothes = get_details('menus', array('is_active' => '1', 'id' => 23));
                $accessories = get_details('menus', array('is_active' => '1', 'id' => 12));
                $devices = get_details('menus', array('is_active' => '1', 'id' => 13));
                $decor = get_details('menus', array('is_active' => '1', 'id' => 14));
                $stationary = get_details('menus', array('is_active' => '1', 'id' => 15));

                $other = get_details('menus', array('is_active' => '1', 'id' => 16));
                $faq = get_details('menus', array('is_active' => '1', 'id' => 17));
                $site_map = get_details('menus', array('is_active' => '1', 'id' => 18));

                $registration = get_details('menus', array('is_active' => '1', 'id' => 19));
                $sign_in = get_details('menus', array('is_active' => '1', 'id' => 20));
                $sign_up = get_details('menus', array('is_active' => '1', 'id' => 21));
                $my_account = get_details('menus', array('is_active' => '1', 'id' => 22));
                $forgot_password = get_details('menus', array('is_active' => '1', 'id' => 24));
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <aside id="text-20" class="widget widget_text">
                            <h3 class="widget-title"><?= get_line_front('subscribe_to_our_newsletter'); ?></h3>
                            <div class="textwidget">
                                <form action="#" class="wpcf7-form">
                                    <div class="widget_wysija_cont widget_wysija">
                                        <div class="wysija-paragraph d-table-cell">
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <input type="email" name="your-email" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control wysija-input" placeholder="<?= get_line_front('your_email'); ?>">
                                            </span>
                                        </div>
                                        <div class="d-table-cell">
                                            <input type="submit" value=">" class="wpcf7-form-control wpcf7-submit btn btn-quaternary wysija-submit font-weight-bold">
                                        </div>
                                    </div>
                                </form>
                                <div id="header" class="D_sharelinks footer-sharelinks">
                                    <div class="share-links">
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-facebook" style="color: #000">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-twitter" style="color: #000">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-instagram" style="color: #000">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                        <a href="#" target="_blank" rel="nofollow noopener noreferrer" class="share-linkedin" style="color: #000">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <aside id="text-16" class="widget widget_text">
                            <?php
                            if (isset($website) && $website != null) {
                            ?>
                                <h3 class="widget-title"><?= $website[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        if (isset($home) && $home != null) {
                                        ?>
                                            <li><a href="<?= base_url(); ?>"><?= $home[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($about_us) && $about_us != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/about-us'); ?>"><?= $about_us[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($privacy_policy) && $privacy_policy != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/privacy-policy'); ?>"><?= $privacy_policy[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($return_policy) && $return_policy != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/return-policy'); ?>"><?= $return_policy[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($terms_condition) && $terms_condition != null) {
                                        ?>
                                            <li><a href="<?= base_url('cms-page/terms-condition'); ?>"><?= $terms_condition[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <aside id="text-23" class="widget widget_text">
                            <?php
                            if (isset($categories) && $categories != null) {
                            ?>
                                <h3 class="widget-title"><?= $categories[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        $conditions = array(
                                            'where' => array('parent_id' => 0, 'is_active' => '1'),
                                            'limit' => '5'
                                        );
                                        $info = $this->common_model->select_data('category', $conditions);
                                        if ($info['row_count'] > 0) {
                                            foreach ($info['data'] as $key => $value) {
                                        ?>
                                                <li><a href="<?php echo base_url('category/' . $value['slug' . get_language()] . '/' . $value['id']); ?>"><?= $value['name' . get_language()]; ?></a></li>
                                        <?php
                                            }
                                        }


                                        /*if (isset($clothes) && $clothes != null) {
                                        ?>
                                            <li><a href="#"><?= $clothes[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($accessories) && $accessories != null) {
                                        ?>
                                            <li><a href="#"><?= $accessories[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($devices) && $devices != null) {
                                        ?>
                                            <li><a href="#"><?= $devices[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($decor) && $decor != null) {
                                        ?>
                                            <li><a href="#"><?= $decor[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($stationary) && $stationary != null) {
                                        ?>
                                            <li><a href="#"><?= $stationary[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }*/
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <aside id="text-23" class="widget widget_text">
                            <?php
                            if (isset($other) && $other != null) {
                            ?>
                                <h3 class="widget-title"><?= $other[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        if (isset($contact_us) && $contact_us != null) {
                                        ?>
                                            <li><a href="<?= base_url('contact-us'); ?>"><?= $contact_us[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($faq) && $faq != null) {
                                        ?>
                                            <li><a href="<?= base_url('faq'); ?>"><?= $faq[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        if (isset($site_map) && $site_map != null) {
                                        ?>
                                            <li><a href="#"><?= $site_map[0]['name' . get_language_front()]; ?></a></li>
                                        <?php }
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <aside id="text-23" class="widget widget_text">
                            <?php
                            if (isset($registration) && $registration != null) {
                            ?>
                                <h3 class="widget-title"><?= $registration[0]['name' . get_language_front()]; ?></h3>
                                <div class="textwidget website-link">
                                    <ul>
                                        <?php
                                        /*if (isset($sign_in) && $sign_in != null) {
                                            if ($this->session->userdata('login_id') == null) {
                                        ?>
                                                <li><a href="#"><?= $sign_in[0]['name' . get_language_front()]; ?></a></li>
                                            <?php }
                                        }*/
                                        if (isset($sign_up) && $sign_up != null) {
                                            if ($this->session->userdata('login_id') == null) {
                                        ?>
                                                <li><a href="<?= base_url('register'); ?>"><?= $sign_up[0]['name' . get_language_front()]; ?></a></li>
                                            <?php }
                                        }
                                        if (isset($my_account) && $my_account != null) {
                                            if ($this->session->userdata('login_id') != null) {
                                            ?>
                                                <li><a href="<?= base_url('account'); ?>"><?= $my_account[0]['name' . get_language_front()]; ?></a></li>
                                            <?php }
                                        }
                                        if (isset($forgot_password) && $forgot_password != null && $this->session->userdata('login_id') == null) {
                                            ?>
                                            <li><a href="<?= base_url('forgot-password'); ?>"><?= $forgot_password[0]['name' . get_language_front()]; ?></a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php }
                            ?>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<!-- <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script> -->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.tr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<?php /*<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>*/ ?>
<script src="<?= base_url(); ?>assets/validation/common.js"></script>
<script src="<?= base_url('assets/validation/jquery.validate.js'); ?>"></script>
<script src="<?= base_url() ?>assets/js/toastr.min.js"></script>
<script src="<?= base_url() ?>assets/js/cart.js"></script>
<?php
if (isset($footer_js) && $footer_js != null) {
    foreach ($footer_js as $key => $js) {
        echo '<script src=' . $js . '></script>';
    }
}
?>
<script type="text/javascript">
    "use strict"; // Start of use strict
    (function($) {
        function bootstrapAnimatedLayer() {
            function doAnimations(elems) {
                //Cache the animationend event in a variable
                var animEndEv = "webkitAnimationEnd animationend";
                elems.each(function() {
                    var $this = $(this),
                        $animationType = $this.data("animation");
                    $this.addClass($animationType).one(animEndEv, function() {
                        $this.removeClass($animationType);
                    });
                });
            }

            //Variables on page load
            var $myCarousel = $("#minimal-bootstrap-carousel"),
                $firstAnimatingElems = $myCarousel
                .find(".carousel-item:first")
                .find("[data-animation ^= 'animated']");

            //Initialize carousel
            try {
                $myCarousel.carousel();
            } catch (err) {

            }

            //Animate captions in first slide on page load
            doAnimations($firstAnimatingElems);

            //Other slides to be animated on carousel slide event
            $myCarousel.on("slide.bs.carousel", function(e) {
                var $animatingElems = $(e.relatedTarget).find(
                    "[data-animation ^= 'animated']"
                );
                doAnimations($animatingElems);
            });
        }

        bootstrapAnimatedLayer();



    })(jQuery);

    function openNav() {
        document.getElementById("mySidenav").style.width = "300px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    function openNav1() {
        document.getElementById("mySidenav1").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("mySidenav1").style.width = "0";
        document.body.style.backgroundColor = "white";
    }

    function closeNav1() {
        document.getElementById("mySidenav1").style.width = "0";
    }

    //  $( "li.fade" ).hover(function() {
    //   $( this ).fadeOut( 100 );
    //   $( this ).fadeIn( 500 );
    // });

    $(document).ready(function() {

        if (toastr_success != '') {
            toastr.success(toastr_success);
        }
        if (toastr_error != '') {
            toastr.error(toastr_error);
        }

        $('.send-otp-login').click(function() {
            var current = $(this);
            $('.active-user').addClass('active');
            loading(true);
            var data = $('#login_form').serialize();
            $.ajax({
                url: BASE_URL + 'ajax/send_otp_login',
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(response) {
                    setTimeout(function() {
                        loading(false);
                    }, 150);
                    toastr.remove();
                    if (response.status) {
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });
        $('.mobile-signin').click(function() {
            var current = $(this);
            loading(true);
            var data = $('#login_form').serialize();
            $.ajax({
                url: BASE_URL + 'ajax/mobile-signin',
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(response) {
                    setTimeout(function() {
                        loading(false);
                    }, 150);
                    toastr.remove();
                    if (response.status) {
                        toastr.success(response.message);
                        window.location = window.location.href;
                    } else {
                        $('.active-user').removeClass('active');
                        toastr.error(response.message);
                    }
                }
            });
        });

        loading(false);
        $('.change-language').click(function() {
            var language = $(this).attr('data-id');
            loading(true);
            $.ajax({
                url: '<?= base_url('home/change_language') ?>',
                type: 'POST',
                data: {
                    'id_language': language
                },
                dataType: 'json',
                success: function(response) {
                    loading(false);
                    if (response.status) {
                        window.location = window.location.href;
                    }
                }
            });
        });
        $(document).on('click', '.wishlist', function() {
            var id_product = $(this).attr('id');
            var current = $(this);
            loading(true);
            $.ajax({
                url: "<?php echo base_url('wishlist/add_wishlist') ?>",
                data: {
                    id_product: id_product
                },
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    loading(false);
                    toastr.remove();
                    if (data.success) {
                        current.addClass('available');
                        current.find('i').addClass('fa-heart');
                        current.find('i').removeClass('fa-heart-o');
                        current.find('span').text('<?= get_line_front('remove_favorite'); ?>');
                        toastr.success('<?= get_line_front('wishlist_added_successfully') ?>');
                        $('.wishlist-counter').text(data.data.total_product);
                    }
                    if (data.delete) {
                        if (current.hasClass('from-wishlist')) {
                            current.closest('tr').remove();
                        } else {
                            current.removeClass('available');
                            current.find('i').removeClass('fa-heart');
                            current.find('i').addClass('fa-heart-o');
                            current.find('span').text('<?= get_line_front('add_to_favorite'); ?>');
                        }
                        toastr.success('<?= get_line_front('wishlist_removed_successfully') ?>');
                        $('.wishlist-counter').text(data.data.total_product);
                    }
                    if (data.error == false) {
                        toastr.error('<?= get_line_front('please_login_after_add_wishlist') ?>');
                    }
                }
            });
        });
    });
</script>