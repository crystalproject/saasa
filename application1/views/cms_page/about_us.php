<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
?>
<?php
if ($set_language == 2) {
?>
	<style type="text/css">
		.porto-sicon-title {
			text-align: right;
		}
	</style>
<?php }
?>
<!-- About Us Section Strat Here -->
<section class="our-story-section">
	<div class="container">
		<div class="porto-u-main-heading">
			<h2><?= isset($details) && $details != null ? $details[0]['title' . get_language_front()] : ''; ?></h2>
		</div>
		<?= isset($details) && $details != null ? $details[0]['description' . get_language_front()] : ''; ?>
	</div>
</section>
<!-- About Us Section End Here -->
<!-- Why Choose Us Section Strat Here -->
<section class="why-choose-us-section">
	<div class="container">
		<div class="porto-u-main-heading">
			<h2><?= get_line_front('why_choose_us'); ?></h2>
		</div>
		<div class="row">
			<?php
			$free_shipping = get_details('why_choose_us', array('id' => 1, 'is_active' => '1'));
			$mony_bank = get_details('why_choose_us', array('id' => 2, 'is_active' => '1'));
			$online_support = get_details('why_choose_us', array('id' => 3, 'is_active' => '1'));
			?>
			<div class="col-md-4 col-lg-4 col-sm-12">
				<div class="porto-sicon-box style_1 top-icon">
					<div class="icon">
						<i class="porto-icon-shipped"></i>
					</div>
					<div class="porto-sicon-header">
						<h3 class="porto-sicon-title" style="font-weight:600;font-size:18px;line-height:20px;color:#21293c;"><?= isset($free_shipping) && $free_shipping != null ? $free_shipping[0]['title' . get_language_front()] : ''; ?></h3>
					</div>
					<div class="porto-sicon-description"><?= isset($free_shipping) && $free_shipping != null ? $free_shipping[0]['description' . get_language_front()] : ''; ?></div>
				</div>
			</div>
			<div class="col-md-4 col-lg-4 col-sm-12">
				<div class="porto-sicon-box style_1 top-icon">
					<div class="icon">
						<i class="porto-icon-us-dollar"></i>
					</div>
					<div class="porto-sicon-header">
						<h3 class="porto-sicon-title" style="font-weight:600;font-size:18px;line-height:20px;color:#21293c;"><?= isset($mony_bank) && $mony_bank != null ? $mony_bank[0]['title' . get_language_front()] : ''; ?></h3>
					</div>
					<div class="porto-sicon-description"><?= isset($mony_bank) && $mony_bank != null ? $mony_bank[0]['description' . get_language_front()] : ''; ?></div>
				</div>
			</div>
			<div class="col-md-4 col-lg-4 col-sm-12">
				<div class="porto-sicon-box style_1 top-icon">
					<div class="icon">
						<i class="porto-icon-online-support"></i>
					</div>
					<div class="porto-sicon-header">
						<h3 class="porto-sicon-title" style="font-weight:600;font-size:18px;line-height:20px;color:#21293c;"><?= isset($online_support) && $online_support != null ? $online_support[0]['title' . get_language_front()] : ''; ?></h3>
					</div>
					<div class="porto-sicon-description"><?= isset($online_support) && $online_support != null ? $online_support[0]['description' . get_language_front()] : ''; ?></div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Why Choose Us Section End Here -->
<!-- Happy Client Section Strat Here -->
<section class="happy-client">
	<div class="container">
		<div class="porto-u-heading" data-hspacer="no_spacer" data-halign="center" style="text-align:center">
			<div class="porto-u-main-heading">
				<h2 style="font-weight:700;color:#21293c;margin-bottom: 40px;font-size:19px;">HAPPY CLIENTS</h2>
			</div>
		</div>
		<div class="row">
			<?php
			if (isset($happy_clients) && $happy_clients != null) {
				foreach ($happy_clients as $key => $value) {
			?>
					<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
						<div class="testimonial">
							<div class="testimonial-author clearfix">
								<div class="testimonial-author-thumbnail">
									<img src="<?= base_url(HAPPY_CLIENTS . $value['image' . get_language_front()]); ?>">
								</div>
								<p><a href="#"><strong style="color:#2b2b2d"><?= $value['name' . get_language_front()]; ?></strong></a><span style="color:#62615e"><?= $value['designation' . get_language_front()]; ?></span></p>
							</div>
							<blockquote style="color:#62615e">
								<p><?= $value['description' . get_language_front()]; ?></p>
							</blockquote>
						</div>
					</div>
			<?php }
			}
			?>
		</div>
	</div>
</section>
<!-- Happy Client Section End Here -->
<!-- Counter Section Strat Here -->
<section class="counter">
	<div class="container">
		<div class="row" id="counter">
			<?php
			if (defined('MILLION_CUSTOMERS') && MILLION_CUSTOMERS >= 0) {
			?>
				<div class="col-md-2 col-lg-2 col-sm-12 counter-Txt"> <span class="counter-value" data-count="<?php echo (int) MILLION_CUSTOMERS; ?>" style="color: #ff6840"><?php echo (int) MILLION_CUSTOMERS; ?></span> <?php echo get_line('million_customers'); ?></div>
			<?php
			}
			if (defined('TEAM_MEMBER') && TEAM_MEMBER >= 0) {
			?>
				<div class="col-md-2 col-lg-2 col-sm-12 counter-Txt"> <span class="counter-value" data-count="<?php echo (int) TEAM_MEMBER; ?>" style="color: #ff6840"><?php echo (int) TEAM_MEMBER; ?></span> <?php echo get_line('team_members'); ?></div>
			<?php
			}
			if (defined('SUPPORT_AVAILABLE') && SUPPORT_AVAILABLE >= 0) {
			?>
				<div class="col-md-2 col-lg-2 col-sm-12 counter-Txt"> <span class="counter-value" data-count="<?php echo (int) SUPPORT_AVAILABLE; ?>" style="color: #ff6840"><?php echo (int) SUPPORT_AVAILABLE; ?></span> <?php echo get_line('support_available'); ?></div>
			<?php
			}
			if (defined('CUP_OF_COFFEE') && CUP_OF_COFFEE >= 0) {
			?>
				<div class="col-md-2 col-lg-2 col-sm-12 counter-Txt"> <span class="counter-value" data-count="<?php echo (int) CUP_OF_COFFEE; ?>" style="color: #ff6840"><?php echo (int) CUP_OF_COFFEE; ?></span> <?php echo get_line('cup_of_coffee'); ?></div>
			<?php
			}
			if (defined('POSITIVE_REVIEWS') && POSITIVE_REVIEWS >= 0) {
			?>
				<div class="col-md-2 col-lg-2 col-sm-12 counter-Txt"> <span class="counter-value" data-count="<?php echo (int) POSITIVE_REVIEWS; ?>" style="color: #ff6840"><?php echo (int) POSITIVE_REVIEWS; ?></span> <?php echo get_line('positive_reviews'); ?></div>
			<?php
			}
			?>			
		</div>
	</div>
</section>
<!-- Counter Section End Here -->
<?php $this->load->view('include/copyright'); ?>
<script type="text/javascript">
	var a = 0;
	$(window).scroll(function() {

		var oTop = $('#counter').offset().top - window.innerHeight;
		if (a == 0 && $(window).scrollTop() > oTop) {
			$('.counter-value').each(function() {
				var $this = $(this),
					countTo = $this.attr('data-count');
				$({
					countNum: $this.text()
				}).animate({
						countNum: countTo
					},

					{

						duration: 7000,
						easing: 'swing',
						step: function() {
							$this.text(Math.floor(this.countNum));
						},
						complete: function() {
							$this.text(this.countNum);
							//alert('finished');
						}

					});
			});
			a = 1;
		}

	});
</script>
<?php $this->load->view('include/footer'); ?>