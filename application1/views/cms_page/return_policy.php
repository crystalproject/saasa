<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>
<section class="our-story-section">
	<div class="container">
		<div class="porto-u-main-heading">
			<h2><?= isset($details) && $details !=null ? $details[0]['title'.get_language_front()] : '';?></h2>
		</div>
		<?= isset($details) && $details !=null ? $details[0]['description'.get_language_front()] : '';?>
	</div>
</section>
<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>