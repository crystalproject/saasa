<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('include/header');
$this->load->view('include/messages');
$set_language = isset($this->session->language) ? $this->session->language : DEFAULT_LANGUAGE;
?>
<!-- Slider Section Strat Here -->
<div id="minimal-bootstrap-carousel" class="carousel slide carousel-fade slider-content-style slider-home-one">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php
        if (isset($slider_details) && $slider_details != null) {
            foreach ($slider_details as $key => $value) {
        ?>
                <div class="carousel-item <?= $key == 0 ? 'active' : ''; ?> slide-<?= $key + 1; ?>" style="background-image: url(<?= base_url(HOME_SLIDER . $value['image' . get_language_front()]); ?>);">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="box valign-middle">
                                <div class="content text-center">
                                    <h4 data-animation="animated fadeInUp"><?= $value['title'] != '' ? $value['title' . get_language_front()] : ''; ?></h4>
                                    <p data-animation="animated fadeInUp"><b><?= $value['description'] != '' ? $value['description' . get_language_front()] : ''; ?></b></p>
                                    <a data-animation="animated fadeInDown" href="<?= $value['link' . get_language_front()] != '' ? $value['link' . get_language_front()] : 'javascript:void(0)'; ?>" class="thm-btn"><?= get_line_front('start_now'); ?> <i class="fa fa-chevron-<?= $set_language == 1 ? 'right' : 'left'; ?>" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php }
        }
        ?>
    </div>
    <!-- Controls -->
    <a class="carousel-control-prev" href="#minimal-bootstrap-carousel" role="button" data-slide="prev">
        <img src="<?= base_url(); ?>assets/images/back.png" style="height: 25px; width: 25px;">
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#minimal-bootstrap-carousel" role="button" data-slide="next">
        <img src="<?= base_url(); ?>assets/images/next.png" style="height: 25px; width: 25px;">
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- Slider Section End Here -->
<!-- Product Section Strat  Here -->
<section class="home-product-section">
    <div class="container">
        <div class="row">
            <?php
            if (isset($category_details) && $category_details != null) {
                foreach ($category_details as $key => $value) {
                    $id = $value['id'];
            ?>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                        <div class="product-box text-center">
                            <a class="new-link" href="<?= base_url('category/' . $value['slug' . get_language_front()] . '/' . $id); ?>">
                                <img src="<?= base_url(CATEGORY_IMAGE . $value['image' . get_language_front()]); ?>">
                                <div class="product-box-title">
                                    <h3><?= $value['name' . get_language_front()]; ?></h3>
                                </div>
                            </a>
                            <a href="<?= base_url('category/' . $value['slug' . get_language_front()] . '/' . $id); ?>" class="product-box-button"><?php echo get_line('discover'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
            <?php }
            }
            ?>
        </div>
    </div>
</section>
<!-- Product Secction End Here -->

<?php $this->load->view('include/copyright'); ?>
<?php $this->load->view('include/footer'); ?>